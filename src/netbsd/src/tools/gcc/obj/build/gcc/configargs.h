/* Generated automatically. */
static const char configuration_arguments[] = "/home/moggles/odc/bsd/netbsd/src/tools/gcc/../../external/gpl3/gcc/dist/configure --target=x86_64--netbsd --enable-long-long --enable-threads --with-bugurl=http://www.NetBSD.org/Misc/send-pr.html --with-pkgversion='NetBSD nb2 20111202' --enable-__cxa_atexit --with-tune=nocona --with-mpc=/home/moggles/odc/bsd/netbsd/src/obj/tooldir.Linux-3.0.0-16-generic-x86_64 --with-mpfr=/home/moggles/odc/bsd/netbsd/src/obj/tooldir.Linux-3.0.0-16-generic-x86_64 --with-gmp=/home/moggles/odc/bsd/netbsd/src/obj/tooldir.Linux-3.0.0-16-generic-x86_64 --disable-nls --enable-multilib --program-transform-name='s,^,x86_64--netbsd-,' --enable-languages='c c++ objc' --prefix=/home/moggles/odc/bsd/netbsd/src/obj/tooldir.Linux-3.0.0-16-generic-x86_64";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" }, { "tune", "nocona" } };
