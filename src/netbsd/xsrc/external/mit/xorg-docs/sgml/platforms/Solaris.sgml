<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook V4.2//EN" [
<!ENTITY % defs SYSTEM "X11/defs.ent"> %defs;
]>

<Article>

<!-- Title information -->
<articleinfo>

 <title>X Window System support for <trademark>Solaris</trademark> &amp;
      <trademark>OpenSolaris</trademark> from X.Org
 </title>
 <authorgroup>
  <author>
    <firstname>David</firstname><surname>Holland</surname>
      <affiliation><orgname><ulink url="http://www.xfree86.org/">XFree86
	      Project</ulink></orgname></affiliation>
  </author>

  <author>
    <firstname>Marc</firstname><surname>Aurele La France</surname>
      <affiliation><orgname><ulink url="http://www.xfree86.org/">XFree86
	      Project</ulink></orgname></affiliation>
  </author>

  <author>
      <firstname>Alan</firstname><surname>Coopersmith</surname>
      <affiliation><orgname><ulink url="http://www.sun.com/">Sun
	      Microsystems, Inc.</ulink></orgname></affiliation>
  </author>
 </authorgroup>

 <date>2009 September 16</date>
</articleinfo>

<!-- Table of contents -->

<!-- Begin the document -->
<Sect1>
<Title>The VT-switching sub-system in Solaris</Title>

<Para>
The original virtual terminal sub-system is a undocumented, and
unsupported feature of Solaris x86 releases 2.1 through 7.  It was removed
in Solaris 8 and later releases, and was never present on Solaris SPARC.
Support for this version of virtual terminals is only present in Xorg 1.6.x
and earlier releases of Xorg, and has been removed in Xorg 1.7 and later.
If you use this form of virtual terminals, you do so at
<Emphasis remap="bf">YOUR OWN RISK</Emphasis>.
</Para>

<Para>
A new virtual terminal sub-system has been introduced in OpenSolaris 2010.02
(currently available in pre-release development builds).   This version
is supported on both SPARC and x86 platforms, though SPARC support is
limited to devices with "Coherent Console" support in the kernel frame buffer
driver.   Support for it is found only in Xorg 1.7 and later releases.
</Para>

<Para>
When available, the virtual terminals of Solaris work basically the same way as
most other VT sub-systems.
</Para>

</Sect1>

<Sect1>
<Title>Notes for building X11R&relvers; on Solaris</Title>

<Para>

<OrderedList>
<ListItem>
<Para>
Both GCC, and the Sun Studio compilers are supported by X11R&relvers;.
The minimum recommended GCC release is 3.4.
Some earlier GCC's are known to not work and should be avoided.
</Para>
<Para>
You should also make certain your version of GCC predefines `sun'.
If needed edit <filename>/usr/local/lib/gcc-lib/*/*/specs</filename>, and modify the
<Literal remap="tt">*predefines:</Literal> line.
</Para>

</ListItem>
<ListItem>
<Para>
To build X11R&relvers; with GCC you need gcc and (optionally) c++filt from GNU
binutils.
Don't install gas or ld from GNU binutils, use the one provided by Sun.
</Para>
</ListItem>
<ListItem>
<Para>
If you are using Sun compilers to compile the X11R&relvers; distribution, you need to
modify your PATH appropriately so the Sun compiler tools are available.
Normally, they should be in
<filename class="directory">/opt/SUNWspro/bin</filename>
</Para>
</ListItem>
<ListItem>
<Para>
You <Emphasis remap="bf">MUST</Emphasis> put
<filename class="directory">/usr/ccs/bin</filename>
at the front of your PATH.  There are known problems with some GNU
replacements for the utilities found there,
so the <filename class="directory">/usr/ccs/bin</filename> versions of
these programs must be found before any possible GNU versions.
(Most notably GNU '<command>ar</command>' does not work during the build).
</Para>
</ListItem>

</OrderedList>

</Para>

</Sect1>

<Sect1>
<Title>Notes for running Xorg on Solaris</Title>

<Para>

<OrderedList>
<ListItem>
<Para>
Depending on the release or architecture of Solaris you are running, you might
need to install an OS driver for an aperture device.
</Para>
<Para>
Under Solaris x86 2.5 and later, there's a system driver
(<filename class="devicefile">/dev/xsvc</filename>)
that provides this functionality.
It will be detected automatically by the server, so you don't need to install
the aperture driver.
</Para>

<Para>
For older Solaris x86 and for Solaris SPARC releases, the source for this
driver is included in
<filename>hw/xfree86/os-support/solaris/apSolaris.shar</filename>
in the xserver source distribution.
Building, and installing the driver is relatively straight forward. Please read
its accompanying README file.
</Para>
</ListItem>
<ListItem>
<Para>
Xqueue is <Emphasis remap="bf">NOT</Emphasis> supported under Solaris.
</Para>
</ListItem>

</OrderedList>

</Para>

</Sect1>


<Sect1>
<Title>Bug Notification</Title>

<Para>
Bug reports should be reported at
<ulink url="http://bugs.freedesktop.org/"></ulink> using the
xorg product or sent to <EMAIL
>xorg@lists.freedesktop.org</EMAIL
>.
</Para>

</Sect1>

</Article>
