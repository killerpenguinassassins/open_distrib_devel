The TH3 test harness is documented in a protected area of the sqlite.org website:

	http://sqlite.org/th3    (apple/mudlite)

The test harness sources in TH3 folder should be synced to the version of sqlite.  Find the correct spot in the timeline (http://sqlite.org/th3/timeline) based on the current public source version of sqlite being tested.  Download the TH3 .zip archive from the UUID tagged reference from the timeline.

or

fossil clone http://apple:mudlite@www.sqlite.org/th3 th3.fossil
mkdir th3; cd th3
fossil open ../th3.fossil
fossil update <UUID>

