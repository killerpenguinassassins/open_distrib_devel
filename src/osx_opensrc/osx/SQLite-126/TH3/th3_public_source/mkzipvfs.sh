#!/bin/sh
cp ../bld/sqlite3.h .
echo '#define SQLITE_ENABLE_ZIPVFS 1' >>sqlite3.h
grep -v sqlite3.h ../zipvfs/src/zipvfs.h >>sqlite3.h

echo '#define SQLITE_ENABLE_ZIPVFS 1' >sqlite3.c
cat ../bld/sqlite3.c >>sqlite3.c
grep -v sqlite3.h ../zipvfs/src/zipvfs.h >>sqlite3.c
cat ../zipvfs/src/zipvfs.c >>sqlite3.c
