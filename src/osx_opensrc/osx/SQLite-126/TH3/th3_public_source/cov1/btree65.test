/*
** Test module for btree.c.
**
** Database corruption detected by btreeInitPage
**
** SCRIPT_MODULE_NAME:        btree65
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     NO_OOM MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
--new test.db
--testcase 100
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(1, 'hello');
INSERT INTO t1 VALUES(2, 'world');
DELETE FROM t1 WHERE a=1;
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t1';
--store $root
--checkpoint
# There should be a single free block at the end of the page.  Increase
# the size of this freeblock by 1 byte so that it overflows the page.
seek $root move 1 read16 store $ofst
seek $root move $ofst move 2 add16 1
incr-chng
--edit test.db
/* Try to read the page with the malformed free block.  Verify that the
** corruption is detected. */
SELECT * FROM t1;
--result SQLITE_CORRUPT {database disk image is malformed}

/* Repeat the above test with some reserve space at the end of each
** page, just to verify that the usableSize is used and not pageSize.
*/
--reserve 25
--new test.db
--testcase 200
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(1, 'hello');
INSERT INTO t1 VALUES(2, 'world');
DELETE FROM t1 WHERE a=1;
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t1';
--store $root
--checkpoint
# There should be a single free block at the end of the page.  Increase
# the size of this freeblock by 1 byte so that it overflows the page.
seek $root move 1 read16 store $ofst
seek $root move $ofst move 2 add16 1
incr-chng
--edit test.db
/* Try to read the page with the malformed free block.  Verify that the
** corruption is detected. */
SELECT * FROM t1;
--result SQLITE_CORRUPT {database disk image is malformed}
