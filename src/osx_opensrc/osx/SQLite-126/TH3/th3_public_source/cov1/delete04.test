/*
** This module contains tests for the DELETE statement.
**
** These test are written from user documentation.
** The focus of this module is checking the ability to delete
** entries of the sqlite_master table if and only if the
** writable_schema PRAGMA is enabled.
**
** SCRIPT_MODULE_NAME:        delete04
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x);
CREATE INDEX i1 ON t1(x);
INSERT INTO t1 VALUES(1);
SELECT name FROM sqlite_master ORDER BY name;
--result i1 t1

--testcase 110
DELETE FROM sqlite_master WHERE name='i1';
--result SQLITE_ERROR {table sqlite_master may not be modified}

--testcase 120
CREATE TEMP TABLE t2(y);
CREATE INDEX i2 ON t2(y);
INSERT INTO t2 VALUES('hello');
SELECT name FROM sqlite_temp_master ORDER BY name;
--result i2 t2

--testcase 130
DELETE FROM sqlite_temp_master WHERE name='i2';
--result SQLITE_ERROR {table sqlite_temp_master may not be modified}

/* Enable writable schema and try the deletes again */
--testcase 200
PRAGMA writable_schema=ON;
DELETE FROM sqlite_master WHERE name='i1';
SELECT name FROM sqlite_master;
--result t1

--open test.db
--testcase 210
PRAGMA integrity_check
--store :ck
SELECT :ck!='ok';
--result 1
