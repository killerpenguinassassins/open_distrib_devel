/*
** This module contains tests for build.c module.
**
** authorizer testing for CREATE TABLE
**
** SCRIPT_MODULE_NAME:        build05
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
/* Set up a separate memory database for allowing or denying
** authorization requests. */
--db 1
--open :memory:
--testcase 2
CREATE TABLE authcheck(res, authCode, zArg1, zArg2, zArg3, zArg4);
INSERT INTO authcheck VALUES('SQLITE_DENY','SQLITE_CREATE_TABLE',0,0,0,0);
--result

--db 0
--auth-check 1
--testcase 100
CREATE TEMP TABLE t100a(a);
--result
--testcase 101
CREATE TABLE t100b(a);
--result SQLITE_AUTH {not authorized}
--testcase 110
UPDATE authcheck SET authCode='SQLITE_DROP_TABLE';
--run 1
CREATE TABLE t100b(a);
--result
--testcase 111
DROP TABLE t100b;
--result SQLITE_AUTH {not authorized}
--testcase 111
DROP TABLE t100a;
--result



--testcase 200
UPDATE authcheck SET authCode='SQLITE_CREATE_TEMP_TABLE';
--run 1
CREATE TEMP TABLE t200a(a);
--result SQLITE_AUTH {not authorized}
--testcase 201
CREATE TABLE t200b(a);
--result
--testcase 210
UPDATE authcheck SET authCode='SQLITE_DROP_TEMP_TABLE';
--run 1
CREATE TEMP TABLE t200a(a);
--result 
--testcase 211
DROP TABLE t200a;
--result SQLITE_AUTH {not authorized}
--testcase 212
DROP TABLE t200b;
--result


--testcase 300
UPDATE authcheck SET authCode='SQLITE_CREATE_VIEW';
--run 1
CREATE VIEW v300a AS SELECT name FROM sqlite_master;
--result SQLITE_AUTH {not authorized}
--testcase 301
CREATE TEMP VIEW v300b AS SELECT name FROM sqlite_master;
--result
--testcase 310
UPDATE authcheck SET authCode='SQLITE_DROP_VIEW';
--run 1
CREATE VIEW v300a AS SELECT name FROM sqlite_master;
--result
--testcase 311
DROP VIEW v300a;
--result SQLITE_AUTH {not authorized}
--testcase 312
DROP VIEW v300b;
--result

--testcase 400
UPDATE authcheck SET authCode='SQLITE_CREATE_TEMP_VIEW';
--run 1
CREATE TEMP VIEW v400a AS SELECT name FROM sqlite_master;
--result SQLITE_AUTH {not authorized}
--testcase 101
CREATE VIEW v400b AS SELECT name FROM sqlite_master;
--result
--testcase 410
UPDATE authcheck SET authCode='SQLITE_DROP_TEMP_VIEW';
--run 1
CREATE TEMP VIEW v400a AS SELECT name FROM sqlite_master;
--result
--testcase 411
DROP VIEW v400a;
--result SQLITE_AUTH {not authorized}
--testcase 412
DROP VIEW v400b;
--result

--testcase 500
UPDATE authcheck SET authCode='SQLITE_INSERT', zArg1='sqlite_master';
--run 1
CREATE TABLE t500a(a);
--result SQLITE_AUTH {not authorized}
--testcase 501
CREATE TEMP TABLE t500b(a);
--result
--testcase 510
UPDATE authcheck SET authCode='SQLITE_DELETE', zArg1='sqlite_master';
--run 1
CREATE TABLE t500a(a);
--result
--testcase 511
DROP TABLE t500a;
--result SQLITE_AUTH {not authorized}
--testcase 512
DROP TABLE t500b;
--result
--testcase 520
UPDATE authcheck SET authCode='SQLITE_DELETE', zArg1='t500a';
--run 1
DROP TABLE t500a;
--result SQLITE_AUTH {not authorized}

--testcase 600
UPDATE authcheck SET authCode='SQLITE_INSERT', zArg1='sqlite_temp_master';
--run 1
CREATE TEMP TABLE t600a(a);
--result SQLITE_AUTH {not authorized}
--testcase 601
CREATE TABLE t600b(a);
--result
--testcase 610
UPDATE authcheck SET authCode='SQLITE_DELETE', zArg1='sqlite_temp_master';
--run 1
CREATE TEMP TABLE t600a(a);
--result
--testcase 611
DROP TABLE t600a;
--result SQLITE_AUTH {not authorized}
--testcase 612
DROP TABLE t600b;
--result
--testcase 620
UPDATE authcheck SET authCode='SQLITE_DELETE', zArg1='t600a';
--run 1
DROP TABLE t600a;
--result SQLITE_AUTH {not authorized}

--db 2
--open :memory:
--testcase 700
CREATE TABLE authlog(authCode, zArg1, zArg2, zArg3, zArg4);
--result
--db 0
--auth-log 2
--db 2

--testcase 701
ATTACH ':memory:' AS aux;
CREATE TABLE aux.t700(a);
--run 0
SELECT * FROM authlog WHERE authcode='SQLITE_CREATE_TABLE';
--result SQLITE_CREATE_TABLE t700 nil aux nil
--testcase 702
SELECT * FROM authlog WHERE authcode='SQLITE_INSERT' ORDER BY rowid LIMIT 1;
DELETE FROM authlog;
--result SQLITE_INSERT sqlite_master nil aux nil

--testcase 801
DELETE FROM authlog;
--run 2
DROP TABLE t700;
--run 0
SELECT * FROM authlog WHERE authcode='SQLITE_DROP_TABLE';
--result SQLITE_DROP_TABLE t700 nil aux nil
--testcase 802
SELECT * FROM authlog WHERE authcode='SQLITE_DELETE' ORDER BY rowid LIMIT 1;
DELETE FROM authlog;
--result SQLITE_DELETE sqlite_master nil aux nil
