/*
** This module contains tests of the VIRTUAL TABLE.
**
** SCRIPT_MODULE_NAME:        vtab01
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
SELECT 1;
--store a
SELECT 2;
--store b
SELECT 3;
--store c
--oom
CREATE VIRTUAL TABLE vt1 USING bvs;
--result
--testcase 101
SELECT * FROM vt1 ORDER BY name;
--result a 1 b 2 c 3

--testcase 110
DELETE FROM vt1 WHERE name='b';
SELECT * FROM vt1 ORDER BY name;
--result a 1 c 3

--testcase 120
UPDATE vt1 SET value=5 WHERE value='1';
SELECT * FROM vt1 ORDER BY value+0;
--result c 3 a 5

--db 1
--open test.db
--testcase 130
--oom
SELECT * FROM vt1 ORDER BY name;
--result a 5 c 3

--db 0
--open test.db
--testcase 131
SELECT * FROM vt1 ORDER BY name;
--result a 5 c 3

/* Close database connection #1.  Needed to prevent it from holding
** open database files when we reinitialize connection #0 below. */
--db 1
--open :memory:

--db 0
--new test.db
--testcase 200
CREATE VIRTUAL TABLE temp.vt1 USING bvs(argument 1, arg 2(5,20), arg3);
--result bvs temp vt1 {argument 1} {arg 2(5,20)} arg3

--testcase 201
CREATE VIRTUAL TABLE vt2 USING nonsense(a,b,c);
--result SQLITE_ERROR {no such module: nonsense}

--testcase 210
DROP TABLE vt1;
CREATE VIRTUAL TABLE vt1 USING bvs;
--run 0
SELECT * FROM vt1 ORDER BY name;
--result a 5 c 3
--testcase 213
DELETE FROM vt1;
SELECT * FROM vt1;
--result

--testcase 220
CREATE VIRTUAL TABLE aux1.vt3 USING bvs;
--result SQLITE_ERROR {unknown database aux1}
--testcase 221
CREATE TABLE t220(x,y);
CREATE VIRTUAL TABLE t220 USING bvs;
--result SQLITE_ERROR {table t220 already exists}

--close all
--db 1
--new test.db
--db 0
--new test.db
--testcase 300
BEGIN;
CREATE VIRTUAL TABLE vt1 USING bvs;
CREATE VIRTUAL TABLE vt2 USING bvs;
CREATE VIRTUAL TABLE vt3 USING bvs;
CREATE VIRTUAL TABLE vt4 USING bvs;
CREATE VIRTUAL TABLE vt5 USING bvs;
CREATE VIRTUAL TABLE vt6 USING bvs;
CREATE VIRTUAL TABLE vt7 USING bvs;
CREATE VIRTUAL TABLE vt8 USING bvs;
CREATE VIRTUAL TABLE vt9 USING bvs;
CREATE VIRTUAL TABLE vt10 USING bvs;
CREATE VIRTUAL TABLE vt11 USING bvs;
COMMIT;
--result

--testcase 310
SELECT * FROM vt1, vt2, vt3, vt4, vt5, vt6, vt7, vt8, vt9, vt10, vt11;
--result

--testcase 320
DROP TABLE vt3;
DROP TABLE vt6;
DROP TABLE vt9;
DROP TABLE vt1;
DROP TABLE vt11;
DROP TABLE vt2;
DROP TABLE vt10;
DROP TABLE vt5;
DROP TABLE vt7;
DROP TABLE IF EXISTS vt9;
DROP TABLE IF EXISTS vt8;
DROP TABLE vt4;
--result
--testcase 321
SELECT * FROM vt3;
--result SQLITE_ERROR {no such table: vt3}

/* What happens if sqlite3_declare_vtab() is not called in the
** constructor.
*/
--testcase 400
CREATE VIRTUAL TABLE vt400 USING vfs(schema);
--result SQLITE_ERROR {vtable constructor did not declare schema: vt400}

/* Check the "hidden" type parsing logic
*/
--testcase 410
CREATE VIRTUAL TABLE vt410 USING vfs(schema
    CREATE TABLE x(
       nRef          INTEGER NOT NULL,
       sz            Hidden_not INT CHECK(sz>0),
       szSync        INT hidden,
       zerogapStart  INT hidden default 0,
       zerogapCount  LONG HIDDEN_NOT INT,
       notSynced     unsigned hidden int,
       deleteOnClose HIDDEN BOOLEAN,
       isExclusive   HIDDEN BOOLEAN,
       readOnly      BOOLEAN hidden DEFAULT 0,
       nReadlLock    INTEGER hidden,
       mxLock        INTEGER "not" hidden,
       zFilename     TEXT PRIMARY KEY,
       content       BLOB
    )
);
PRAGMA table_info(vt410);
--result 0 nRef INTEGER 1 nil 0 1 sz {Hidden_not INT} 0 nil 0 2 zerogapCount {LONG HIDDEN_NOT INT} 0 nil 0 3 zFilename TEXT 0 nil 0 4 content BLOB 0 nil 0

/* Other examples of invalid schemas passed to sqlite3_declare_vtab()
*/
--testcase 420
CREATE VIRTUAL TABLE vt420 USING vfs(schema
    CREATE VIRTUAL TABLE x USING bvs
);
--result SQLITE_ERROR {vtable constructor failed: vt420}
--testcase 430
CREATE VIRTUAL TABLE vt430 USING vfs(schema
    CREATE VIEW x AS SELECT 5 AS 'a';
);
--result SQLITE_ERROR {vtable constructor failed: vt430}
