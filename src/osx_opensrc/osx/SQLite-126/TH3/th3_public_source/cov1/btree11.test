/*
** Test module for btree.c and especially the allocateBtreePage()
** function.
**
** SCRIPT_MODULE_NAME:        btree11
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     AUTOVACUUM
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY,y);
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(1, zeroblob($pgsz*5));
DROP TABLE t1;
--result

/* Corrupt the number of free pages byte in the header */
--testcase 110
seek 36 add32 2 incr-chng
--edit test.db
CREATE TABLE t1(x);
--result SQLITE_CORRUPT {database disk image is malformed}

/*
** Scenario: The trunk page number is out of range.
*/
--new test.db
--testcase 200
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(zeroblob($pgsz));
DELETE FROM t1;
PRAGMA integrity_check;
--result ok

--testcase 210
seek 32 read32 read32
--edit test.db
--result 3 1

--testcase 220
seek 32 write32 4 incr-chng
--edit test.db
INSERT INTO t1 VALUES(zeroblob($pgsz));
--result SQLITE_CORRUPT {database disk image is malformed}

/*
** Scenario: The trunk with leaves but the number leaves is out of range.
*/
--new test.db
--testcase 300
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(zeroblob($pgsz));
DELETE FROM t1;
PRAGMA integrity_check;
--result ok

--testcase 310
seek 32 read32 read32
--edit test.db
--result 3 1

--testcase 320
SELECT $pgsz*(3-1);
--store $ofst
seek $ofst read32 read32
--edit test.db
--result 0 0

--testcase 320
seek $ofst move 4 write32 10000 incr-chng
--edit test.db
INSERT INTO t1 VALUES(zeroblob($pgsz));
--result SQLITE_CORRUPT {database disk image is malformed}

/*
** Scenario: I/O error while trying to use a single empty trunk page.
*/
--new test.db
--testcase 400
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(zeroblob($pgsz));
DELETE FROM t1;
--result

--testcase 410
--ioerr
--open test.db
INSERT INTO t1 VALUES(zeroblob($pgsz));
--result

--testcase 420
SELECT length(x)==$pgsz+0, typeof(x) FROM t1;
--result 1 blob

/*
** Scenario: Trunk page with one leaf.  The leaf page number is invalid.
*/
--new test.db
--testcase 500
CREATE TABLE t1(x, y);
INSERT INTO t1 VALUES(1, zeroblob($pgsz));
INSERT INTO t1 VALUES(2, zeroblob($pgsz));
DELETE FROM t1 WHERE x=1;
DELETE FROM t1;
--result

--testcase 510
seek 32 read32 read32
--edit test.db
--result 3 2

--testcase 520
SELECT $pgsz*(3-1);
--store $ofst
seek $ofst read32 read32 read32
--edit test.db
--result 0 1 4

--testcase 530
seek $ofst move 8 write32 5 incr-chng
--edit test.db
INSERT INTO t1 VALUES(3, zeroblob($pgsz));
--result SQLITE_CORRUPT {database disk image is malformed}
