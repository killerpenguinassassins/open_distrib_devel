/*
** This module contains tests for the build.c source file.
**
** This particular file focuses is verification that redundant
** UNIQUE and PRIMARY KEY constraints on a table definition do
** not result in redundant indices.
**
** SCRIPT_MODULE_NAME:        build18
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t100(a TEXT UNIQUE PRIMARY KEY);
SELECT count(*) FROM sqlite_master WHERE type='index' AND tbl_name='t100';
--result 1

--testcase 110
CREATE TABLE t110(a,b, UNIQUE(b,a), PRIMARY KEY(b,a));
SELECT count(*) FROM sqlite_master WHERE type='index' AND tbl_name='t110';
--result 1

--testcase 120
CREATE TABLE t120(a,b, UNIQUE(b COLLATE NOCASE,a), PRIMARY KEY(b,a));
SELECT count(*) FROM sqlite_master WHERE type='index' AND tbl_name='t120';
--result 2

--testcase 130
CREATE TABLE t130(a,b,c, UNIQUE(a,b), PRIMARY KEY(a,c));
SELECT count(*) FROM sqlite_master WHERE type='index' AND tbl_name='t130';
--result 2

--testcase 140
CREATE TABLE t140(a,b, UNIQUE(a,b DESC), PRIMARY KEY(a,b ASC));
SELECT count(*) FROM sqlite_master WHERE type='index' AND tbl_name='t140';
--result 1

--testcase 150
CREATE TABLE t150(a,b, UNIQUE(a,b) ON CONFLICT REPLACE,
                       PRIMARY KEY(a,b ASC) ON CONFLICT ABORT);
--result SQLITE_ERROR {conflicting ON CONFLICT clauses specified}

--testcase 160
CREATE TABLE t160(a,b,c, UNIQUE(a,b) ON CONFLICT REPLACE, PRIMARY KEY(a,b));
SELECT count(*) FROM sqlite_master WHERE type='index' AND tbl_name='t160';
--result 1
--testcase 161
INSERT INTO t160 VALUES(1,2,3);
INSERT INTO t160 VALUES(4,5,6);
INSERT INTO t160 VALUES(1,2,7);
SELECT * FROM t160 ORDER BY a;
--result 1 2 7 4 5 6
--testcase 165
CREATE TABLE t160b(a,b,c, UNIQUE(a,b), PRIMARY KEY(a,b) ON CONFLICT REPLACE);
SELECT count(*) FROM sqlite_master WHERE type='index' AND tbl_name='t160b';
--result 1
--testcase 166
INSERT INTO t160b VALUES(1,2,3);
INSERT INTO t160b VALUES(4,5,6);
INSERT INTO t160b VALUES(1,2,7);
SELECT * FROM t160b ORDER BY a;
--result 1 2 7 4 5 6

--testcase 170
CREATE TABLE t170(a,b, UNIQUE(a,b), PRIMARY KEY(a));
SELECT count(*) FROM sqlite_master WHERE type='index' AND tbl_name='t170';
--result 2

--testcase 180
CREATE TABLE t180(a,b, UNIQUE(a,b COLLATE nocase),
                       PRIMARY KEY(a, b COLLATE NoCase));
SELECT count(*) FROM sqlite_master WHERE type='index' AND tbl_name='t180';
--result 1
