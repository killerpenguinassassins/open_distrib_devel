/*
** This module contains tests of the C-language interfaces found in
** the main.c source file. 
**
** MODULE_NAME:               main11
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
**
** The focus of this test module is the error message string interfaces.
*/

/*
** An SQL function that returns an arbitrary error code.
*/
static void main11_errfunc(
  sqlite3_context *context,
  int argc,
  sqlite3_value **argv
){
  sqlite3_result_error_code(context, sqlite3_value_int(argv[0]));
}

/*
** Convert UTF16 into ASCII using a static buffer for storage.
*/
static char *main11_utf16_to_ascii(const void *a16, char *z8){
  unsigned short int *a = (unsigned short int*)a16;
  int i;
  for(i=0; a[i]; i++){
    z8[i] = a[i]&0xff;
  }
  z8[i] = 0;
  return z8;
}


int main11(th3state *p){
  int rc;
  int i;
  sqlite3 *db;
  sqlite3_stmt *pStmt;
  char zBuf[200];
  static const struct {
    int errCode;
    const char *zErrText;
  } aErr[] = {
    { SQLITE_OK,          "not an error" },
    { SQLITE_ERROR,       "SQL logic error or missing database" },
    { SQLITE_INTERNAL,    "unknown error" },
    { SQLITE_PERM,        "access permission denied" },
    { SQLITE_ABORT,       "callback requested query abort" },
    { SQLITE_BUSY,        "database is locked" },
    { SQLITE_LOCKED,      "database table is locked" },
    { SQLITE_NOMEM,       "out of memory" },
    { SQLITE_READONLY,    "attempt to write a readonly database" },
    { SQLITE_INTERRUPT,   "interrupted" },
    { SQLITE_IOERR,       "disk I/O error" },
    { SQLITE_CORRUPT,     "database disk image is malformed" },
    { SQLITE_NOTFOUND,    "unknown operation" },
    { SQLITE_FULL,        "database or disk is full" },
    { SQLITE_CANTOPEN,    "unable to open database file" },
    { SQLITE_PROTOCOL,    "locking protocol" },
    { SQLITE_EMPTY,       "table contains no data" },
    { SQLITE_SCHEMA,      "database schema has changed" },
    { SQLITE_TOOBIG,      "string or blob too big" },
    { SQLITE_CONSTRAINT,  "constraint failed" },
    { SQLITE_MISMATCH,    "datatype mismatch" },
    { SQLITE_MISUSE,      "library routine called out of sequence" },
    { SQLITE_NOLFS,       "large file support is disabled" },
    { SQLITE_AUTH,        "authorization denied" },
    { SQLITE_FORMAT,      "auxiliary database format error" },
    { SQLITE_RANGE,       "bind or column index out of range" },
    { SQLITE_NOTADB,      "file is encrypted or is not a database" },
    { SQLITE_NOTADB+1,    "unknown error" },
  };

  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, "CREATE TABLE t1(x); INSERT INTO t1 VALUES(1);");
  db = th3dbPointer(p, 0);
  sqlite3_create_function(db, "main11_errfunc", 1, SQLITE_ANY, 0,
                          main11_errfunc, 0, 0);
  pStmt = th3dbPrepare(p, 0, "SELECT main11_errfunc(?) FROM t1");

  for(i=0; i<sizeof(aErr)/sizeof(aErr[0]); i++){
    char *zErr1;
    th3testBegin(p, th3format(p, "%d", i+1));
    sqlite3_bind_int(pStmt, 1, aErr[i].errCode);
    sqlite3_step(pStmt);
    rc = sqlite3_reset(pStmt);
    zErr1 = sqlite3_mprintf("%s", sqlite3_errmsg(db));
    th3testAppendResult(p, th3format(p, "%d [%s] [%s]", rc, zErr1,
         main11_utf16_to_ascii(sqlite3_errmsg16(db), zBuf)
    ));
    sqlite3_free(zErr1);
    th3testCheck(p, th3format(p, "%d [%s] [%s]",
       aErr[i].errCode, aErr[i].zErrText, aErr[i].zErrText
    ));
  }


  sqlite3_finalize(pStmt);

  return 0;
}
