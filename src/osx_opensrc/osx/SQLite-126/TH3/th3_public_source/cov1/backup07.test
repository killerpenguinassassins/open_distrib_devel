/*
** Cannot chage page sizes when backup up to an in-memory database.
**
** MODULE_NAME:               backup07
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
int backup07(th3state *p){
  sqlite3 *pSrc;    /* Source database */
  sqlite3 *pDest;   /* Destination database */
  int srcPgsz;          /* Source page size */
  int destPgsz;         /* Destination page size */

  sqlite3_backup *pBackup;
  int rc;
  
  /* Create a source database database */
  th3testBegin(p, "1");
  th3dbNew(p, 0, "src.db");
  th3dbEval(p, 0,
     "CREATE TABLE t1(a,b);"
     "INSERT INTO t1 VALUES(th3random(), th3randomBlob(1801));"
     "INSERT INTO t1 VALUES(th3random(), th3randomBlob(1802));"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(1803) FROM t1;"
     "SELECT count(*) FROM t1;"
  );
  th3testCheck(p, "4");

  /* Get the page size of the source database */
  th3testResetResult(p);
  th3dbEval(p, 0, "PRAGMA page_size");
  srcPgsz = atoi(p->zResult);
  th3testResetResult(p);
  destPgsz = srcPgsz<4096 ? srcPgsz*2 : srcPgsz/2;

  /* Create the destination database and do the copy.  We are
  ** copying from TEMP to AUX - not the main databases.
  */
  th3testBegin(p, "2");
  pSrc = th3dbPointer(p, 0);
  th3dbOpen(p, 1, ":memory:", TH3_OPEN_RAW);
  pDest = th3dbPointer(p, 1);
  th3dbEval(p, 1, th3format(p, "PRAGMA page_size=%d", destPgsz));
  th3dbEval(p, 1, "CREATE TABLE bogus(x)"); /* Force the pagesize to be fixed */
  pBackup = sqlite3_backup_init(pDest, "main", pSrc, "main");
  if( pBackup==0 ){
    th3testFailed(p, sqlite3_errmsg(pDest));
    return 0;
  }
  rc = sqlite3_backup_step(pBackup, 1);
  th3testCheckInt(p, SQLITE_READONLY, rc);
  th3testBegin(p, "3");
  th3dbEval(p, 0,
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(1804) FROM t1;"
  );
  rc = sqlite3_backup_step(pBackup, 1);
  th3testCheckInt(p, SQLITE_READONLY, rc);
  th3testBegin(p, "4");
  rc = sqlite3_backup_step(pBackup, 9999);
  th3testCheckInt(p, SQLITE_READONLY, rc);

  th3testBegin(p, "99");
  rc = sqlite3_backup_finish(pBackup);
  th3testCheckInt(p, SQLITE_READONLY, rc);
 
  return 0;
}
