/*
** This module contains tests of the VIRTUAL TABLE.
**
** SCRIPT_MODULE_NAME:        vtab05
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
**
** Multiple virtual table updates in the body of a trigger.
*/
--testcase 100
CREATE TABLE t1(x, y);
CREATE VIRTUAL TABLE vta USING bvs;
CREATE VIRTUAL TABLE vtb USING bvs;
CREATE VIRTUAL TABLE vtc USING bvs;
INSERT INTO vta VALUES('a',0);
INSERT INTO vtb VALUES('b',0);
INSERT INTO vtc VALUES('c',0);
CREATE TRIGGER r1 AFTER INSERT ON t1 BEGIN
  UPDATE vta SET value=new.y WHERE name='a' AND new.x='a';
  UPDATE vtb SET value=new.y WHERE name='b' AND new.x='b';
  UPDATE vtc SET value=new.y WHERE name='c' AND new.x='c';
END;
CREATE TRIGGER r2 BEFORE INSERT ON t1 BEGIN
  UPDATE vta SET value=new.y WHERE name='a' AND new.x='a';
  UPDATE vtb SET value=new.y WHERE name='b' AND new.x='b';
  UPDATE vtc SET value=new.y WHERE name='c' AND new.x='c';
END;
INSERT INTO t1 VALUES('a',11);
SELECT * FROM vta ORDER BY name;
--result a 11 b 0 c 0

--testcase 110
--oom
INSERT INTO t1 VALUES('b',17);
--result
--testcase 111
SELECT * FROM vtb ORDER BY name;
--result a 11 b 17 c 0

/* Ticket [d2f02d37f52bfe23e421f2c60fbb8586ac76ff01]:  and UPDATE 
** involving two different virtual tables.
*/
--testcase 200
SELECT * FROM vta ORDER BY name;
--result a 11 b 17 c 0
--testcase 201
SELECT * FROM vtb ORDER BY name;
--result a 11 b 17 c 0
--testcase 210
UPDATE vta SET value=(SELECT value+1 FROM vtb WHERE name='b') WHERE name='a';
SELECT * FROM vta ORDER BY name;
--result a 18 b 17 c 0
