/*
** Backup fails with SQLITE_LOCKED because another database connection
** with a shared cache has the database locked.
**
** MODULE_NAME:               backup10
** REQUIRED_PROPERTIES:       SHARED_CACHE
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
int backup10(th3state *p){
  sqlite3 *pSrc;    /* Source database */
  sqlite3 *pDest;   /* Destination database */
  char zCksum[100]; /* Database checksum */
  sqlite3_backup *pBackup;
  int rc;
  
  /* Create a source database database */
  th3testBegin(p, "1");
  th3dbNew(p, 0, "src.db");
  th3dbEval(p, 0,
     "CREATE TABLE t1(a,b);"
     "INSERT INTO t1 VALUES(th3random(), th3randomBlob(1501));"
     "INSERT INTO t1 VALUES(th3random(), th3randomBlob(1502));"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(1503) FROM t1;"
     "CREATE TABLE t2(x UNIQUE, y UNIQUE);"
     "SELECT count(*) FROM t1;"
  );
  th3testCheck(p, "4");

  /* Compute a checksum over the source database */
  th3testResetResult(p);
  th3dbEval(p, 0, "SELECT count(*), md5sum(a,b) FROM t1");
  assert( strlen(p->zResult)<sizeof(zCksum) );
  th3strcpy(zCksum, p->zResult);
  th3testResetResult(p);


  /* Create the destination database.  Start the copy.
  */
  th3testBegin(p, "2");
  pSrc = th3dbPointer(p, 0);
  th3dbNew(p, 1, "dest.db");
  pDest = th3dbPointer(p, 1);
  pBackup = sqlite3_backup_init(pDest, "main", pSrc, "main");
  if( pBackup==0 ){
    th3testFailed(p, sqlite3_errmsg(pDest));
    return 0;
  }
  rc = sqlite3_backup_step(pBackup, 1);
  th3testCheckInt(p, SQLITE_OK, rc);

  /* Start a write transaction on the source, then try to step the
  ** backup.
  */
  th3testBegin(p, "3");
  th3dbOpen(p, 2, "src.db", 0);
  th3dbEval(p, 2, 
     "BEGIN;"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(1504) FROM t1"
  );
  rc = sqlite3_backup_step(pBackup, 1);
  th3testCheckInt(p, SQLITE_BUSY, rc);
  th3testBegin(p, "4");
  rc = sqlite3_backup_step(pBackup, 9999);
  th3testCheckInt(p, SQLITE_BUSY, rc);

  /* Rollback the transaction.  Finish the backup. */
  th3testBegin(p, "3");
  th3dbEval(p, 2, "ROLLBACK");
  rc = sqlite3_backup_step(pBackup, 1);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "4");
  rc = sqlite3_backup_step(pBackup, -1);
  th3testCheckInt(p, SQLITE_DONE, rc);

  th3testBegin(p, "5");
  rc = sqlite3_backup_finish(pBackup);
  th3testCheckInt(p, SQLITE_OK, rc);

  /* Verify that the destination is identical to the source */
  th3testBegin(p, "6");
  th3dbEval(p, 1, "SELECT count(*), md5sum(a,b) FROM t1");
  th3testCheck(p, zCksum);
 
  return 0;
}
