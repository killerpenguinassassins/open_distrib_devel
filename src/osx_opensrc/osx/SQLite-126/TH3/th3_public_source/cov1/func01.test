/*
** Module for testing functions.  Tests are written by hand from
** the documentation in lang_corefunc.html
**
** SCRIPT_MODULE_NAME:        func01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/

/**************************************************************************
** abs(X)
** EV: R-23979-26855 The abs(X) function returns the absolute value of the
** numeric argument X.
*/
--testcase abs-1
--oom
SELECT abs(0);
--result 0

--testcase abs-2
SELECT abs(1)
--result 1

--testcase abs-3
SELECT abs(-1)
--result 1

--testcase abs-4
SELECT abs(9223372036854775807)
--result 9223372036854775807

--testcase abs-5
SELECT abs(-9223372036854775807);
--result 9223372036854775807

/* EV: R-35460-15084 If X is the integer -9223372036854775807 then abs(X)
** throws an integer overflow error since there is no equivalent positive
** 64-bit two complement value. */
--testcase abs-6
SELECT abs(-9223372036854775808);
--result SQLITE_ERROR {integer overflow}

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase abs-7
SELECT abs(0.0);
--result 0.0

--testcase abs-8
SELECT abs(1.0e-90);
--result 1.0e-90

--testcase abs-9
SELECT abs(-1.0e-90);
--result 1.0e-90

--testcase abs-10
SELECT abs(1.0e+90);
--result 1.0e+90

--testcase abs-11
SELECT abs(-1.0000e+90);
--result 1.0e+90

--testcase abs-12
SELECT abs('-123');
--result 123.0

--testcase abs-13
SELECT abs('-123x');
--result 123.0

/* EV: R-57326-31541 Abs(X) return 0.0 if X is a string or blob that cannot
** be converted to a numeric value. */
--testcase abs-14
SELECT abs('x-123');
--result 0.0

--store $enc PRAGMA encoding
--if $enc=='UTF-8'
--testcase abs-15utf8
SELECT abs(x'2d313233');
--result 123.0
--endif

--if $enc=='UTF-16le'
--testcase abs-15utf16le
SELECT abs(x'2d00310032003300');
--result 123.0
--endif

--if $enc=='UTF-16be'
--testcase abs-15utf16be
SELECT abs(x'002d003100320033');
--result 123.0
--endif
#endif /* SQLITE_OMIT_FLOATING_POINT */

/* EV: R-37434-19929 Abs(X) returns NULL if X is NULL. */
--testcase abs-16
SELECT abs(NULL);
--result nil

--testcase abs-17
SELECT abs();
--result SQLITE_ERROR {wrong number of arguments to function abs()}

--testcase abs-18
SELECT abs(1,2);
--result SQLITE_ERROR {wrong number of arguments to function abs()}

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase abs-19
SELECT abs('-1234'), abs('1234');
--result 1234.0 1234.0
#else
--testcase abs-19-nofp
SELECT abs('-1234'), abs('1234');
--result 1234 1234
#endif

/*****************************************************************************
** coalesce(X,Y,...):
** EV: R-22655-13879 The coalesce() function returns a copy of its first
** non-NULL argument, or NULL if all arguments are NULL.
*/
--testcase coalesce-1
SELECT coalesce(1,2);
--result 1

--testcase coalesce-2
SELECT coalesce(NULL,2);
--result 2

--testcase coalesce-3
SELECT coalesce(NULL,NULL);
--result nil

--testcase coalesce-4
SELECT coalesce(NULL,NULL,'abc',1,2);
--result abc

--testcase coalesce-5
SELECT hex(coalesce(NULL,NULL,NULL,x'010203',1,2));
--result 010203

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase coalesce-6
SELECT coalesce(NULL,NULL,NULL,NULL,3.0,1,2);
--result 3.0
#else
--testcase coalesce-6
SELECT coalesce(NULL,NULL,NULL,NULL,3,1,2);
--result 3
#endif

--testcase coalesce-7
SELECT coalesce(NULL,NULL,NULL,NULL,NULL,1,2);
--result 1

--testcase coalesce-8
SELECT coalesce(NULL,NULL,NULL,NULL,NULL,NULL,1,2);
--result 1

--testcase coalesce-9
SELECT coalesce(NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2);
--result 1

--testcase coalesce-10
SELECT coalesce(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2);
--result 1

--testcase coalesce-11
SELECT coalesce(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2);
--result 1

--testcase coalesce-12
SELECT coalesce(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2);
--result 1

--testcase coalesce-13
SELECT coalesce(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,2);
--result 1

--testcase coalesce-14
SELECT coalesce(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
--result 1

--testcase coalesce-15
SELECT coalesce(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
--result nil

/* EV: R-65402-64812 Coalesce() must be at least 2 arguments. */
--testcase coalesce-16
SELECT coalesce(1);
--result SQLITE_ERROR {wrong number of arguments to function coalesce()}

--testcase coalesce-17
SELECT coalesce();
--result SQLITE_ERROR {wrong number of arguments to function coalesce()}

/**************************************************************************
** glob(X,Y): This function is used to implement the "Y GLOB X" syntax of
** SQLite. Note that the X and Y arguments are reversed in the glob() 
** function relative to the infix GLOB operator.
*/
--testcase glob-1
SELECT glob('xyz','abc');
--result 0

--testcase glob-2
SELECT glob('ABC','abc');
--result 0

--testcase glob-3
SELECT glob('A?C','abc');
--result 0

--testcase glob-4
SELECT glob('a?c','abc');
--result 1

--testcase glob-5
SELECT glob('abc?','abc');
--result 0

--testcase glob-6
SELECT glob('A*C','abc');
--result 0

--testcase glob-7
SELECT glob('a*c','abc');
--result 1

--testcase glob-8
SELECT glob('a*c','abxyzzyc');
--result 1

--testcase glob-9
SELECT glob('a*c','abxyzzy');
--result 0

--testcase glob-10
SELECT glob('a*c','abxyzzycx');
--result 0

--testcase glob-11
SELECT glob('a[bx]c','abc');
--result 1

--testcase glob-12.1
SELECT glob('a[cx]c','abc');
--result 0
--testcase glob-12.2
SELECT th3_load_normal_extension();
SELECT glob('a[cx'||hex_to_utf16be('1234 FEDC')||'b]c','abc');
--result nil 1
--testcase glob-12.3
SELECT glob('a[cx'||hex_to_utf16be('1234 FEDC')||']c','abc');
--result 0

--testcase glob-13
SELECT glob('a[a-d]c','abc');
--result 1

--testcase glob-14
SELECT glob('a[^a-d]c','abc');
--result 0

--testcase glob-15
SELECT glob('a[A-Dc]c','abc');
--result 0

--testcase glob-16
SELECT glob('a[^A-Dc]c','abc');
--result 1

--testcase glob-17
SELECT glob('a[]b]c','abc');
--result 1

--testcase glob-18
SELECT glob('a[^]b]c','abc');
--result 0

--testcase glob-19.1
SELECT glob('a*[de]g','abcdefg');
--result 0
--testcase glob-19.2
SELECT glob('a*[de]g','abcdfeg');
--result 1
--testcase glob-19.3
SELECT glob('a*[d'||hex_to_utf16be('1234 FEDC')||'e]g',
            'abc'||hex_to_utf16be('FEDC 1234')||'efg');
--result 0
--testcase glob-19.4
SELECT glob('a*[d'||hex_to_utf16be('1234 FEDC')||'e]g',
            'abc'||hex_to_utf16be('FEDC 1234')||'g');
--result 1

--testcase glob-20
SELECT glob('a*[df]g','abcdefg');
--result 1

--testcase glob-21
SELECT glob('a*[d-h]g','abcdefg');
--result 1

--testcase glob-22
SELECT glob('a*[b-e]g','abcdefg');
--result 0

--testcase glob-23
SELECT glob('a*[^de]g','abcdefg');
--result 1

--testcase glob-24
SELECT glob('a*[^def]g','abcdefg');
--result 0

--testcase glob-25
SELECT glob('a*?g','abcdefg');
--result 1

--testcase glob-26
SELECT glob('a*c','ac');
--result 1

--testcase glob-27
SELECT glob('a*?c','ac');
--result 0

--testcase glob-28
SELECT glob('a[*]c','a*c');
--result 1

--testcase glob-29
SELECT glob('a[?]c','a?c');
--result 1

--testcase glob-30.1
SELECT glob('a[[]c','a[c');
--result 1

--testcase glob-30.2
SELECT glob('a[]]c','a]c');
--result 1

--testcase glob-31
SELECT glob('a[xy','a[xy');
--result 0

--testcase glob-33
SELECT glob('a[xy]','a');
--result 0

--testcase glob-34
SELECT glob('a[','a[');
--result 0

--testcase glob-35
SELECT glob('a[xy-]','a-');
--result 1

--testcase glob-36
SELECT glob('a[xy-','a-');
--result 0

--testcase glob-37
SELECT glob('a[xy-]b','axb');
--result 1

--testcase glob-38
SELECT glob('a[-xy]b','axb');
--result 1

--testcase glob-39
SELECT glob('a[-xy]b','a-b');
--result 1

--testcase glob-50
SELECT th3_sqlite3_limit('SQLITE_LIMIT_LIKE_PATTERN_LENGTH',10);
SELECT glob('0123456789','0123456789');
--result nil 1

--testcase glob-51
SELECT glob('0123456789a','0123456789a');
--result SQLITE_ERROR {LIKE or GLOB pattern too complex}
--testcase glob-52
SELECT th3_sqlite3_limit('SQLITE_LIMIT_LIKE_PATTERN_LENGTH',1000000000);
--result nil

--testcase glob-98
SELECT glob(1,2,3);
--result SQLITE_ERROR {wrong number of arguments to function glob()}

--testcase glob-99
SELECT glob(1);
--result SQLITE_ERROR {wrong number of arguments to function glob()}


/****************************************************************************
** ifnull(X,Y): Return a copy of the first non-NULL argument. If both
** arguments are NULL then NULL is returned. This behaves the same as
** coalesce().
**
** EVIDENCE-OF: R-54671-04027 The ifnull() function returns a copy of its
** first non-NULL argument, or NULL if both arguments are NULL.
*/
--testcase ifnull-1
SELECT ifnull(1,2);
--result 1

--testcase ifnull-2
SELECT ifnull(1,NULL);
--result 1

--testcase ifnull-3
SELECT ifnull(NULL,2);
--result 2

--testcase ifnull-4
SELECT ifnull(NULL,NULL);
--result nil

/* EVIDENCE-OF: R-35443-03595 Ifnull() must have exactly 2 arguments. */
--testcase ifnull-5
SELECT ifnull(1);
--result SQLITE_ERROR {wrong number of arguments to function ifnull()}

--testcase ifnull-6
SELECT ifnull(1,2,3);
--result SQLITE_ERROR {wrong number of arguments to function ifnull()}

/* EVIDENCE-OF: R-47349-36088 The ifnull() function is equivalent to
** coalesce() with two arguments. */
--testcase ifnull-1c
SELECT coalesce(1,2);
--result 1
--testcase ifnull-2c
SELECT coalesce(1,NULL);
--result 1
--testcase ifnull-3c
SELECT coalesce(NULL,2);
--result 2
--testcase ifnull-4c
SELECT coalesce(NULL,NULL);
--result nil


/*****************************************************************************
** hex(X): The argument is interpreted as a BLOB. The result is a
** hexadecimal rendering of the content of that blob.
**
** EVIDENCE-OF: R-57089-20897 The hex() function interprets its argument
** as a BLOB and returns a string which is the upper-case hexadecimal
** rendering of the content of that blob.
*/
--testcase hex-1
SELECT hex(x'0102030405060708090a0b0c0d0e0f');
--result 0102030405060708090A0B0C0D0E0F

--testcase hex-2
SELECT hex(zeroblob(0));
--result {}

--testcase hex-3
SELECT hex(x'000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f404142434445464748494a4b4c4d4e4f505152535455565758595a5b5c5d5e5f606162636465666768696a6b6c6d6e6f707172737475767778797a7b7c7d7e7f808182838485868788898a8b8c8d8e8f909192939495969798999a9b9c9d9e9fa0a1a2a3a4a5a6a7a8a9aaabacadaeafb0b1b2b3b4b5b6b7b8b9babbbcbdbebfc0c1c2c3c4c5c6c7c8c9cacbcccdcecfd0d1d2d3d4d5d6d7d8d9dadbdcdddedfe0e1e2e3e4e5e6e7e8e9eaebecedeeeff0f1f2f3f4f5f6f7f8f9fafbfcfdfeff');
--result 000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F606162636465666768696A6B6C6D6E6F707172737475767778797A7B7C7D7E7F808182838485868788898A8B8C8D8E8F909192939495969798999A9B9C9D9E9FA0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF

--store $enc PRAGMA encoding
--if $enc=='UTF-8'
--testcase hex-4-utf8
SELECT hex('abc');
--result 616263
--endif

--if $enc=='UTF-16le'
--testcase hex-4-utf16le
SELECT hex('abc');
--result 610062006300
--endif

--if $enc=='UTF-16be'
--testcase hex-4-utf16be
SELECT hex('abc');
--result 006100620063
--endif

--testcase hex-5
SELECT hex(123);
--result 313233

--testcase hex-6
SELECT hex();
--result SQLITE_ERROR {wrong number of arguments to function hex()}

--testcase hex-7
SELECT hex(1,2);
--result SQLITE_ERROR {wrong number of arguments to function hex()}


/*****************************************************************************
** last_insert_rowid(): Return the ROWID  of the last row insert from this
** connection to the database. This is the same value that would be returned
** from the sqlite3_last_insert_rowid() API function.
*/
--testcase rowid-1
SELECT last_insert_rowid();
--result 0

--testcase rowid-2
CREATE TABLE t1(x);
INSERT INTO t1(rowid, x) VALUES(123456789,1);
SELECT last_insert_rowid();
--result 123456789

--testcase rowid-3
SELECT last_insert_rowid(1);
--result SQLITE_ERROR {wrong number of arguments to function last_insert_rowid()}

--testcase rowid-4
SELECT last_insert_rowid();
--result 123456789

/* NOTE:  Additional test occurs with the sqlite3_last_insert_rowid()
** API */


/******************************************************************************
** length(X): Return the string length of X in characters if X is a string,
** or in bytes if X is a blob.
**
** EVIDENCE-OF: R-16712-46349 The length(X) function returns the length
** of X in characters if X is a string, or in bytes if X is a blob.
*/
--testcase length-1
SELECT length('abcd');
--result 4

/* EVIDENCE-OF: R-35003-32738 If X is numeric then length(X) returns the
** length of a string representation of X. */
--testcase length-2
SELECT length(1);
--result 1

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase length-3
SELECT length(123.45);
--result 6
#endif

--testcase length-4
SELECT length(123456789012345);
--result 15

--testcase length-5
SELECT length(000001);
--result 1

--testcase length-6
SELECT length(x'24c2a2e282ac');
--result 6

--testcase length-7
SELECT length(x'e282acf48aaf8dc2a2e282ac');
--result 12

--store $enc PRAGMA encoding
--if $enc=='UTF-8'
--testcase length-8-utf8
SELECT length(CAST(x'e282acf48aaf8dc2a2e282ac' AS text));
--result 4
--endif

--if $enc=='UTF-16le'
--testcase length-9-utf16le
SELECT length(CAST(x'e282acf48aaf8dc2a2e282ac' AS text));
--result 6
--endif

--if $enc=='UTF-16be'
--testcase length-10-utf16be
SELECT length(CAST(x'e282acf48aaf8dc2a2e282ac' AS text));
--result 6
--endif

/* EVIDENCE-OF: R-06121-44139 If X is NULL then length(X) is NULL. */
--testcase length-11
SELECT length(NULL);
--result nil

--testcase length-12
--oom
SELECT length('This is a very long string that, when converting from UTF16 into UTF8, will cause a memory allocation, the failure of which will show up in this OOM test.');
--result 154

--testcase length-98
SELECT length('abc',2);
--result SQLITE_ERROR {wrong number of arguments to function length()}
--testcase length-99
SELECT length(*);
--result SQLITE_ERROR {wrong number of arguments to function length()}


/*****************************************************************************
** like(X,Y), like(X,Y,Z): This function is used to implement the 
** "Y LIKE X [ESCAPE Z]" syntax of SQL. If the optional ESCAPE clause 
** is present, then the user-function is invoked with three arguments.
** Otherwise, it is invoked with two arguments only. Note that the X and 
** Y parameters are reversed in the like() function relative to the infix
** LIKE operator. The sqlite3_create_function() interface can be used to
** override this function and thereby change the operation of the LIKE 
** operator. When doing this, it may be important to override both the
** two and three argument versions of the like() function. Otherwise,
** different code may be called to implement the LIKE operator depending 
** on whether or not an ESCAPE clause was specified.
*/
--testcase like-1
SELECT like('xyz','abc');
--result 0

--testcase like-2
SELECT like('abc','abc');
--result 1

--testcase like-3
PRAGMA case_sensitive_like=1;
SELECT like('ABC','abc');
--result 0
--testcase like-4
PRAGMA case_sensitive_like=0;
SELECT like('ABC','abc');
--result 1

--testcase like-5
SELECT like('a_c','abc');
--result 1

--testcase like-6
PRAGMA case_sensitive_like=1;
SELECT like('A_C','abc');
--result 0
--testcase like-7
PRAGMA case_sensitive_like=0;
SELECT like('A_C','abc');
--result 1

--testcase like-8
SELECT like('abc_','abc');
--result 0

--testcase like-9
SELECT like('a%c','abc');
--result 1

--testcase like-10
PRAGMA case_sensitive_like=1;
SELECT like('A%C','abc');
--result 0
--testcase like-11
PRAGMA case_sensitive_like=0;
SELECT like('A%C','abc');
--result 1

--testcase like-12
SELECT like('a%c','abdc');
--result 1

--testcase like-13
SELECT like('a%c','ac');
--result 1

--testcase like-14
PRAGMA case_sensitive_like=1;
SELECT like('A%C','ac');
--result 0
--testcase like-15
PRAGMA case_sensitive_like=0;
SELECT like('A%C','ac');
--result 1

--testcase like-16
SELECT like('a%c','abxyzzyc');
--result 1

--testcase like-17
PRAGMA case_sensitive_like=1;
SELECT like('A%C','abxyzzyc');
--result 0
--testcase like-18
PRAGMA case_sensitive_like=0;
SELECT like('A%C','abxyzzyc');
--result 1

--testcase like-19
SELECT like('a%c','abxyzzy');
--result 0

--testcase like-20
SELECT like('A%C','abxyzzy');
--result 0

--testcase like-21
SELECT like('a%c','abxyzzycx');
--result 0

--testcase like-22
SELECT like('a%cx','abxyzzycy');
--result 0

--testcase like-23
SELECT like('A%C','abxyzzycx');
--result 0

--testcase like-24
SELECT like('A%CX','abxyzzycy');
--result 0

--testcase like-25a
SELECT like('a%_c','abc');
--result 1

--testcase like-25b
SELECT like('a%_%c','abc');
--result 1

--testcase like-25c
SELECT like('a%%%_%c','abc');
--result 1

--testcase like-25d
SELECT like('a%_%%%%c','abc');
--result 1

--testcase like-25e
SELECT like('a%_%%%%'||hex_to_utf16be('FEDC'),
            'ab'||hex_to_utf16be('FEDC'));
--result 1

--testcase like-26a
SELECT like('a%_c','ac');
--result 0

--testcase like-26b
SELECT like('a%%%%_c','ac');
--result 0

--testcase like-26.1
SELECT like('a%%%%_%%%','a');
--result 0

--testcase like-27
PRAGMA case_sensitive_like=1;
SELECT like('A%_C','abc');
--result 0
--testcase like-28
PRAGMA case_sensitive_like=0;
SELECT like('A%_C','abc');
--result 1

--testcase like-29
SELECT like('A%_C','ac');
--result 0

--testcase like-30
SELECT like('a%_c','abxyzzyc');
--result 1

--testcase like-31
PRAGMA case_sensitive_like=1;
SELECT like('A%_C','abxyzzyc');
--result 0
--testcase like-32
PRAGMA case_sensitive_like=0;
SELECT like('A%_C','abxyzzyc');
--result 1

--testcase like-33
SELECT like('ax_c','abc','x');
--result 0
--testcase like-34
SELECT like('ax_c','a_c','x');
--result 1

--testcase like-40
SELECT like('12%', 123);
--result 1
#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase like-41
SELECT like('12%.%', 123.45);
--result 1
#endif

--testcase like-50
SELECT like('a%'||hex_to_utf16be('1234 FEDC'), 
            'abcde'||hex_to_utf16be('1234 FEDC'));
--result 1
--testcase like-51
SELECT like('a%'||hex_to_utf16be('FEDC'), 
            'abcde'||hex_to_utf16be('1234 FEDC'));
--result 1

--testcase like-60
SELECT like('ab%=%c','abxyz%c','=');
--result 1
--testcase like-61
SELECT like('ab%=_c','abxyz%c','=');
--result 0
--testcase like-62
SELECT like('ab%=','abxyz=','=');
--result 0
--testcase like-63
SELECT like('ab=%cd','ab%cd','=');
--result 1
--testcase like-64
SELECT like('ab=%cd','abxcd','=');
--result 0
--testcase like-65
SELECT like('ab==cd','ab=cd','=');
--result 1
--testcase like-66
SELECT like('ab==cd','ab=cd','==');
--result SQLITE_ERROR {ESCAPE expression must be a single character}


--testcase like-80
SELECT like('abc',NULL);
--result nil
--testcase like-81
SELECT like(NULL,'abc');
--result nil
--testcase like-82
SELECT like('abc','abc',NULL)
--result nil

--testcase like-98
SELECT like('abc');
--result SQLITE_ERROR {wrong number of arguments to function like()}
--testcase like-99
SELECT like('abc','abc','x',1);
--result SQLITE_ERROR {wrong number of arguments to function like()}

/* There is no REGEXP function installed by default but the parser
** recognizes it.
*/
--testcase regexp-1
SELECT 'abc' REGEXP 'xyz';
--result SQLITE_ERROR {no such function: REGEXP}
