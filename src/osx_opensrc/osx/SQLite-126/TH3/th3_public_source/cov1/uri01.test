/*
** This module contains tests of URI file name capabilities
**
** SCRIPT_MODULE_NAME:        uri01
** REQUIRED_PROPERTIES:       URI TEST_VFS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--open /test.db
--testcase 100
CREATE TABLE t1(x,y);
INSERT INTO t1 VALUES(123,456);
SELECT * FROM t1;
--result 123 456
--testcase 101
PRAGMA database_list;
--result 0 main [testvfs008]/test.db

--open file:///test.db?vxx=1
--testcase 110
SELECT * FROM t1;
--result 123 456
--testcase 111
PRAGMA database_list;
--result 0 main [testvfs008]/test.db
--open file:///test.db#fragment
--testcase 112
SELECT * FROM t1;
--result 123 456
--testcase 113
PRAGMA database_list;
--result 0 main [testvfs008]/test.db
--open file:///test.db%00xyzzy#fragment
--testcase 114
SELECT * FROM t1;
--result 123 456
--testcase 115
PRAGMA database_list;
--result 0 main [testvfs008]/test.db
--open file:///test.db%00xyzzy?vfx=xfv#fragment
--testcase 114
SELECT * FROM t1;
--result 123 456
--testcase 115
PRAGMA database_list;
--result 0 main [testvfs008]/test.db

--open file://localhost/test.db?modx=1&cachx=1
--testcase 120
SELECT * FROM t1;
--result 123 456
--testcase 121
PRAGMA database_list;
--result 0 main [testvfs008]/test.db

--open file://localhost/%74%65st.db?mode=rw
--testcase 130
SELECT * FROM t1;
--result 123 456
--testcase 131
PRAGMA database_list;
--result 0 main [testvfs008]/test.db

--open file:/test.db?mode=ro
--testcase 150
SELECT * FROM t1;
--result 123 456
--testcase 151
PRAGMA database_list;
--result 0 main [testvfs008]/test.db
--testcase 152
UPDATE t1 SET x=x+1;
--result SQLITE_READONLY {attempt to write a readonly database}
--open file:/test.db?mode%00xyz=ro
--testcase 153
SELECT * FROM t1;
--result 123 456
--testcase 154
PRAGMA database_list;
--result 0 main [testvfs008]/test.db
--testcase 155
UPDATE t1 SET x=x+1;
--result SQLITE_READONLY {attempt to write a readonly database}
--open file:/test.db?xyz%00abc&mode=ro%00xyz&cachx=hi
--testcase 156
SELECT * FROM t1;
--result 123 456
--testcase 157
PRAGMA database_list;
--result 0 main [testvfs008]/test.db
--testcase 158
UPDATE t1 SET x=x+1;
--result SQLITE_READONLY {attempt to write a readonly database}

--open file:/test.db?mode=%72%6f
--testcase 160
SELECT * FROM t1;
--result 123 456
--testcase 161
UPDATE t1 SET x=x+1;
--result SQLITE_READONLY {attempt to write a readonly database}

--open file:/test.db?mode=%72%6F%00
--testcase 162
SELECT * FROM t1;
--result 123 456
--testcase 163
UPDATE t1 SET x=x+1;
--result SQLITE_READONLY {attempt to write a readonly database}
--testcase 164
ATTACH 'file:aux1.db?mode=rw' AS rw;
--result SQLITE_CANTOPEN {unable to open database: file:aux1.db?mode=rw}
--testcase 165
ATTACH 'file:aux1.db?mode=rwc' AS rw;
--result

--open /test.db ro
--testcase 170
SELECT * FROM t1;
--result 123 456
--testcase 171
UPDATE t1 SET x=x+1;
--result SQLITE_READONLY {attempt to write a readonly database}
--testcase 172
ATTACH 'file:aux1.db?mode=rw' AS rw;
--result SQLITE_ERROR {access mode not allowed: rw}
--testcase 173
ATTACH 'file:aux1.db?mode=rwc' AS rw;
--result SQLITE_ERROR {access mode not allowed: rwc}
--testcase 174
ATTACH 'file:aux1.db?mode=ro' AS rw;
--result 



/* The MISUSE return is not coming out of sqlite3_open().  TH3 tries
** to prepare a statement on the new database after opening it, and
** that is triggering the MISUSE error.  Might as well leave this test
** as it is, since real-world applications are also likely to hit the
** same misuse problem.
*/
--testcase 200
--open file://localhostx/test.db
--result SQLITE_MISUSE {invalid uri authority: localhostx}

--testcase 210
--open file://localhost/test.db?mode=xyz
--result SQLITE_MISUSE {no such access mode: xyz}
--testcase 211
--open file://localhost/test.db?mode=
--result SQLITE_MISUSE {no such access mode: }
--testcase 212
--open file://localhost/test.db?mode
--result SQLITE_MISUSE {no such access mode: }

--testcase 220
--open file://localhost/test.db?cache=xyz
--result SQLITE_MISUSE {no such cache mode: xyz}
--testcase 221
--open file://localhost/test.db?cache=
--result SQLITE_MISUSE {no such cache mode: }
--testcase 222
--open file://localhost/test.db?cache
--result SQLITE_MISUSE {no such cache mode: }

--testcase 230
--open file:///test.db?vfs=unknown
--result SQLITE_MISUSE {no such vfs: unknown}
--testcase 231
--open file:///test.db?vfs=
--result SQLITE_MISUSE {no such vfs: }
