/*
** Test module for btree.c.  PRAGMA integrity_check.
**
** SCRIPT_MODULE_NAME:        btree25
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY,y);
PRAGMA page_size;
--store $pgsz
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t1';
--store $root
INSERT INTO t1 VALUES(0, zeroblob($pgsz-200));
INSERT INTO t1 VALUES(1, NULL);
INSERT INTO t1 VALUES(2, NULL);
INSERT INTO t1 VALUES(3, NULL);
INSERT INTO t1 VALUES(4, NULL);
INSERT INTO t1 VALUES(5, NULL);
DELETE FROM t1 WHERE x=3;
--result
--checkpoint

/* Corrupt the "cell content area" offset in the header.
*/
--testcase 110
seek $root move 5 write16 $pgsz move -2 add16 1 incr-chng
--edit test.db
PRAGMA integrity_check;
--store $msg
SELECT $msg like '%btreeinitpage() returns error code 11%'
    OR (($pgsz+0)==65536 AND $msg LIKE '%Fragmentation %');
--result 1

/* Create a table with at least three layers.  Shorten one leg by a
** single layer.  Verify that the corruption is detected.
*/
--new test.db
--testcase 200
CREATE TABLE t1(x);
CREATE INDEX t1x ON t1(x);
INSERT INTO t1 VALUES(th3randomBlob($pgsz/5));
INSERT INTO t1 VALUES(th3randomBlob($pgsz/5));
INSERT INTO t1 SELECT th3randomBlob($pgsz/5) FROM t1;
INSERT INTO t1 SELECT th3randomBlob($pgsz/5) FROM t1;
INSERT INTO t1 SELECT th3randomBlob($pgsz/5) FROM t1;
INSERT INTO t1 SELECT th3randomBlob($pgsz/5) FROM t1;
INSERT INTO t1 SELECT th3randomBlob($pgsz/5) FROM t1;
SELECT count(*) FROM t1;
--result 64

--testcase 210
--oom-ck
PRAGMA integrity_check;
--result ok

--checkpoint
--testcase 220
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t1x';
--store $root
seek $root move 12 read16 store $ofst1
seek $root move $ofst1 read32 store $child1
seek $child1 move 12 read16 store $ofst2
seek $child1 move $ofst2 read32 store $child2
seek $root move $ofst1 write32 $child2
incr-chng
--edit test.db
PRAGMA integrity_check;
--store $msg
SELECT $msg LIKE '%child page depth differs%';
--result 1

--testcase 230
PRAGMA integrity_check(1);
--store $msg
SELECT $msg != 'ok'
--result 1

/* An overflow record is too long for the page.
*/
--new test.db
--testcase 300
CREATE TABLE longtab(
  a TEXT /* The point of these comments is to make a large table definition */,
  b TEXT /* The point of these comments is to make a large table definition */,
  c TEXT /* The point of these comments is to make a large table definition */,
  d TEXT /* The point of these comments is to make a large table definition */,
  e TEXT /* The point of these comments is to make a large table definition */,
  f TEXT /* The point of these comments is to make a large table definition */,
  g TEXT /* The point of these comments is to make a large table definition */,
  h TEXT /* The point of these comments is to make a large table definition */,
  i TEXT /* The point of these comments is to make a large table definition */,
  j TEXT /* The point of these comments is to make a large table definition */,
  k TEXT /* The point of these comments is to make a large table definition */,
  l TEXT /* The point of these comments is to make a large table definition */,
  m TEXT /* The point of these comments is to make a large table definition */,
  n TEXT /* The point of these comments is to make a large table definition */,
  o TEXT /* The point of these comments is to make a large table definition */,
  p TEXT /* The point of these comments is to make a large table definition */
);
--result
--checkpoint

--testcase 310
PRAGMA page_size;
--store $pgsz
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='longtab';
--store $root
seek 20 write8 8
seek $root move 5 add16 -8
incr-chng
--edit test.db
PRAGMA integrity_check;
--store $msg
SELECT $msg != 'ok';
--result 1
