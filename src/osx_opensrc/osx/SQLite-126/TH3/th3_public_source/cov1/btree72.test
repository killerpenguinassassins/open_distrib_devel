/*
** Testing of the allocateBtreePage() routine during incremental vacuum,
** to make sure ticket 5e10420e8de26a086cd0f532643bedbfa94921e0 
** "Free-list corruption in incremental vacuum mode" is fixed.
**
** We have to construct a freelist containing multiple trunk pages
** that each have no leaf pages, then try to use the trunk pages for
** an incremental vacuum.
**
** SCRIPT_MODULE_NAME:        btree72
** REQUIRED_PROPERTIES:       TEST_VFS AUTOVACUUM CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB JOURNAL_WAL
** MINIMUM_HEAPSIZE:          100000
*/

/* Construct a database in incremental vacuum mode that has multiple
** freelist pages
*/
--testcase 100
PRAGMA page_size=1024;
PRAGMA auto_vacuum=INCREMENTAL;
CREATE TABLE t1(x);
VACUUM;
INSERT INTO t1 VALUES(zeroblob(10000));
DELETE FROM t1;
PRAGMA freelist_count;
PRAGMA page_count;
PRAGMA page_size;
--result 9 12 1024

/* Verify that there is a single freelist trunk page on page 4 and
** that it contains 8 leaves:
*/
--testcase 110
seek 32 read32
--edit test.db
--result 4
--testcase 111
seek 3072 read32 read32
read32 read32 read32 read32
read32 read32 read32 read32
--edit test.db
--result 0 8 5 6 7 8 9 10 11 12

/* Rewrite the freelist to be a list of trunk pages each with no
** leaves.  Make sure the largest is neither the first nor the last
** on the list.
*/
--testcase 120
seek 3072  write32 5  write32 0  # Edits to page 4
seek 4096  write32 6  write32 0  # Edits to page 5
seek 5120  write32 7  write32 0  # Edits to page 6
seek 6144  write32 12 write32 0  # Edits to page 7
seek 7168  write32 9  write32 0  # Edits to page 8
seek 8192  write32 10 write32 0  # Edits to page 9
seek 9216  write32 0  write32 0  # Edits to page 10
seek 10240 write32 8  write32 0  # Edits to page 11
seek 11264 write32 11 write32 0  # Edits to page 12
incr-chng
--edit test.db
PRAGMA integrity_check;
PRAGMA freelist_count;
--result ok 9
--snapshot

/* The following test would trigger the 5e10420e8de26a086cd0f5326 bug
** were it not fixed.
*/
--testcase 200
PRAGMA freelist_count;
PRAGMA incremental_vacuum;
PRAGMA integrity_check;
PRAGMA freelist_count;
--result 9 ok 0
--revert
--raw-open test.db
--testcase 201
PRAGMA auto_vacuum=INCREMENTAL;
PRAGMA freelist_count;
PRAGMA incremental_vacuum;
PRAGMA integrity_check;
PRAGMA freelist_count;
--result 9 ok 0

/* In an OOM loop.
*/
--testcase 300
--oom-ck
--revert
--raw-open test.db
PRAGMA freelist_count;
PRAGMA incremental_vacuum;
PRAGMA integrity_check;
PRAGMA freelist_count;
--result 9 ok 0

/* In an I/O error loop.
*/
--testcase 400
--ioerr
--revert
--raw-open test.db
PRAGMA incremental_vacuum;
--result

/* Use up the freelist trunk pages one by one.  Make sure they are all
** used and no corruption occurs.
*/
--revert
--raw-open test.db
--testcase 500
PRAGMA auto_vacuum=INCREMENTAL;
CREATE TABLE t4(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 8 ok
--testcase 501
CREATE TABLE t5(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 7 ok
--testcase 502
CREATE TABLE t6(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 6 ok
--testcase 503
CREATE TABLE t7(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 5 ok
--testcase 504
CREATE TABLE t8(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 4 ok
--testcase 505
CREATE TABLE t9(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 3 ok
--testcase 506
CREATE TABLE t10(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 2 ok
--testcase 507
CREATE TABLE t11(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 1 ok
--testcase 508
CREATE TABLE t12(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 0 ok
--testcase 509
CREATE TABLE t13(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 0 ok

/* Do another sequence of freelist trunk page allocates, but with
** a different order this time.
*/
--revert
--raw-open test.db
--testcase 600
seek 32    write32 12            # First freelist page
seek 11264 write32 11 write32 0  # Edits to page 12
seek 10240 write32 10 write32 0  # Edits to page 11
seek 9216  write32 9  write32 0  # Edits to page 10
seek 8192  write32 8  write32 0  # Edits to page 9
seek 7168  write32 7  write32 0  # Edits to page 8
seek 6144  write32 6  write32 0  # Edits to page 7
seek 5120  write32 5  write32 0  # Edits to page 6
seek 4096  write32 4  write32 0  # Edits to page 5
seek 3072  write32 0  write32 0  # Edits to page 4
incr-chng
--edit test.db
PRAGMA auto_vacuum=INCREMENTAL;
PRAGMA integrity_check;
PRAGMA freelist_count;
--result ok 9
--testcase 601
CREATE TABLE t4(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 8 ok
--testcase 602
CREATE TABLE t5(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 7 ok
--testcase 603
CREATE TABLE t6(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 6 ok
--testcase 604
CREATE TABLE t7(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 5 ok
--testcase 605
CREATE TABLE t8(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 4 ok
--testcase 606
CREATE TABLE t9(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 3 ok
--testcase 607
CREATE TABLE t10(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 2 ok
--testcase 608
CREATE TABLE t11(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 1 ok
--testcase 609
CREATE TABLE t12(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 0 ok
--testcase 610
CREATE TABLE t13(x);
PRAGMA freelist_count;
PRAGMA integrity_check;
--result 0 ok
