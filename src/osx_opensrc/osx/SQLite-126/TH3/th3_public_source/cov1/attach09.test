/*
** This module contains tests for the ATTACH statement.
**
** The focus of this module is checking the interaction
** of ATTACH and DETACH with sqlite3_set_authorizer().
**
** SCRIPT_MODULE_NAME:        attach09
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* Set up a separate memory database for logging authorization
** requests. */
--db 1
--open :memory:
--testcase 1
CREATE TABLE authlog(authCode, zArg1, zArg2, zArg3, zArg4);
--result

/* Set up a separate memory database for allowing or denying
** authorization requests.  The initial configuration denies
** all ATTACH and DETACH. */
--db 2
--open :memory:
--testcase 2
CREATE TABLE authcheck(res, authCode, zArg1, zArg2, zArg3, zArg4);
INSERT INTO authcheck VALUES('SQLITE_DENY','SQLITE_ATTACH',0,0,0,0);
INSERT INTO authcheck VALUES('SQLITE_DENY','SQLITE_DETACH',0,0,0,0);
--result

/* With the authorizer enabled, ATTACH is disallowed. */
--db 0
--auth-check 2
--testcase 100
ATTACH ':memory:' AS mem1;
--result SQLITE_AUTH {not authorized}

/* Allow the attach */
--auth-check 99
--testcase 101
ATTACH ':memory:' AS mem1;
CREATE TABLE mem1.m1(x);
SELECT name FROM mem1.sqlite_master;
--result m1

/* With the authorizer enabled, DETACH is disallowed */
--auth-check 2
--testcase 200
DETACH mem1;
--result SQLITE_AUTH {not authorized}

--testcase 201
SELECT * FROM m1;
--result

/* Disable the authorizer and the DETACH is allowed again */
--auth-check 99
--testcase 202
DETACH mem1;
SELECT * FROM m1;
--result SQLITE_ERROR {no such table: m1}
