/*
** Module for testing vdbeblob.c.
**
** General interface testing
**
** MODULE_NAME:               vdbeblob01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
int vdbeblob01(th3state *p){
  sqlite3 *db;
  sqlite3_blob *pBlob;
  int rc;
  char zBuf[200];

  /************************************************************************
  ** Create a blob.  Open it and read and write it
  */
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, 
     "CREATE TABLE t1(a INTEGER PRIMARY KEY, b, c, d);"
     "INSERT INTO t1(a,b) VALUES(1, zeroblob(15));"
     "INSERT INTO t1(a,b) VALUES(2, null);"
     "INSERT INTO t1(a,b) VALUES(3, 123);"
#ifndef SQLITE_OMIT_FLOATING_POINT
     "INSERT INTO t1(a,b) VALUES(4, 456.78);"
#endif
     "INSERT INTO t1(a,b) VALUES(5, 'this is a test');"
  );
  db = th3dbPointer(p, 0);
  rc = sqlite3_blob_open(db, "main", "t1", "b", 1, 1, &pBlob);
  th3testCheckInt(p, SQLITE_OK, rc);

  th3testBegin(p, "105");
  rc = sqlite3_blob_bytes(pBlob);
  th3testCheckInt(p, 15, rc);

  th3testBegin(p, "110");
  rc = sqlite3_blob_read(pBlob, zBuf, 10, 0);
  th3testCheckInt(p, SQLITE_OK, rc);

  th3testBegin(p, "120");
  th3testCheckInt(p, 0, memcmp(zBuf,"\0\0\0\0\0\0\0\0\0\0",10));

  th3testBegin(p, "130");
  rc = sqlite3_blob_write(pBlob, "hello, world!", 14, 0);
  th3testCheckInt(p, SQLITE_OK, rc);

  th3testBegin(p, "132");
  rc = sqlite3_blob_read(pBlob, zBuf, 5, 7);
  th3testCheckInt(p, 0, memcmp(zBuf, "world", 5));

  th3testBegin(p, "133");
  rc = sqlite3_blob_read(pBlob, zBuf, -1, 5);
  th3testCheckInt(p, SQLITE_ERROR, rc);

  th3testBegin(p, "134");
  rc = sqlite3_blob_read(pBlob, zBuf, 0, 16);
  th3testCheckInt(p, SQLITE_ERROR, rc);

  th3testBegin(p, "135");
  rc = sqlite3_blob_read(pBlob, zBuf, 0, -1);
  th3testCheckInt(p, SQLITE_ERROR, rc);

  th3testBegin(p, "140");
  th3dbEval(p, 0, "SELECT hex(b) FROM t1 WHERE a=1");
  th3testCheck(p, "68656C6C6F2C20776F726C64210000");

  th3testBegin(p, "150");
  th3dbEval(p, 0, "UPDATE t1 SET b=null WHERE a=1");
  rc = sqlite3_blob_read(pBlob, zBuf, 5, 7);
  th3testCheckInt(p, SQLITE_ABORT, rc);

  th3testBegin(p, "160");
  rc = sqlite3_blob_read(pBlob, zBuf, 5, 7);
  th3testCheckInt(p, SQLITE_ABORT, rc);

  th3testBegin(p, "199");
  rc = sqlite3_blob_close(pBlob);
  th3testCheckInt(p, SQLITE_OK, rc);


  /************************************************************************
  ** Open the blob read-only.  Read it.  Verify that writing gives 
  ** an error 
  */
  th3testBegin(p, "200");
  th3dbEval(p, 0, 
     "UPDATE t1 SET b=x'68656C6C6F2C20776F726C64210000' WHERE a=1"
  );
  rc = sqlite3_blob_open(db, "main", "t1", "b", 1, 0, &pBlob);
  th3testCheckInt(p, SQLITE_OK, rc);

  th3testBegin(p, "205");
  rc = sqlite3_blob_bytes(pBlob);
  th3testCheckInt(p, 15, rc);

  th3testBegin(p, "210");
  rc = sqlite3_blob_read(pBlob, zBuf, 14, 0);
  th3testCheckInt(p, SQLITE_OK, rc);

  th3testBegin(p, "220");
  th3testCheckInt(p, 0, memcmp(zBuf,"hello, world!",14));

  th3testBegin(p, "230");
  rc = sqlite3_blob_write(pBlob, "HELLO, WORLD!", 14, 0);
  th3testCheckInt(p, SQLITE_READONLY, rc);
  th3testBegin(p, "231");
  th3testAppendResult(p, sqlite3_errmsg(db));
  th3testCheck(p, "attempt to write a readonly database");

  th3testBegin(p, "240");
  rc = sqlite3_blob_close(pBlob);
  th3testCheckInt(p, SQLITE_READONLY, rc);

  th3testBegin(p, "250");
  th3dbEval(p, 0, "SELECT hex(b) FROM t1 WHERE a=1");
  th3testCheck(p, "68656C6C6F2C20776F726C64210000");

  /**********************************************************************
  ** Try to blob-io open a view
  */
  th3testBegin(p, "300");
  th3dbEval(p, 0, "CREATE VIEW v1 AS SELECT * FROM t1");
  rc = sqlite3_blob_open(db, "main", "v1", "b", 1, 1, &pBlob);
  th3testCheckInt(p, SQLITE_ERROR, rc);

  th3testBegin(p, "310");
  th3testAppendResult(p, sqlite3_errmsg(db));
  th3testCheck(p, "cannot open view: v1");

  th3testBegin(p, "320");
  th3testCheckInt(p, 1, pBlob==0);
  
  /**********************************************************************
  ** Try to blob-io open a table that does not exist
  */
  th3testBegin(p, "400");
  rc = sqlite3_blob_open(db, "main", "t2", "b", 1, 1, &pBlob);
  th3testCheckInt(p, SQLITE_ERROR, rc);

  th3testBegin(p, "410");
  th3testAppendResult(p, sqlite3_errmsg(db));
  th3testCheck(p, "no such table: main.t2");

  th3testBegin(p, "420");
  th3testCheckInt(p, 1, pBlob==0);
  
  /**********************************************************************
  ** Try to blob-io open a column that does not exist
  */
  th3testBegin(p, "500");
  rc = sqlite3_blob_open(db, "main", "t1", "x", 1, 1, &pBlob);
  th3testCheckInt(p, SQLITE_ERROR, rc);

  th3testBegin(p, "510");
  th3testAppendResult(p, sqlite3_errmsg(db));
  th3testCheck(p, "no such column: \"x\"");

  th3testBegin(p, "520");
  th3testCheckInt(p, 1, pBlob==0);
  
  /**********************************************************************
  ** Try to blob-io open a row that does not exist
  */
  th3testBegin(p, "600");
  rc = sqlite3_blob_open(db, "main", "t1", "b", 99, 1, &pBlob);
  th3testCheckInt(p, SQLITE_ERROR, rc);

  th3testBegin(p, "610");
  th3testAppendResult(p, sqlite3_errmsg(db));
  th3testCheck(p, "no such rowid: 99");

  th3testBegin(p, "620");
  th3testCheckInt(p, 1, pBlob==0);
  
  /**********************************************************************
  ** Try to blob-io open non-blob and non-text values.
  */
  th3testBegin(p, "700");
  rc = sqlite3_blob_open(db, "main", "t1", "b", 2, 1, &pBlob);
  th3testCheckInt(p, SQLITE_ERROR, rc);
  th3testBegin(p, "701");
  th3testAppendResult(p, sqlite3_errmsg(db));
  th3testCheck(p, "cannot open value of type null");
  th3testBegin(p, "702");
  th3testCheckInt(p, 1, pBlob==0);
  
  th3testBegin(p, "710");
  rc = sqlite3_blob_open(db, "main", "t1", "b", 3, 1, &pBlob);
  th3testCheckInt(p, SQLITE_ERROR, rc);
  th3testBegin(p, "711");
  th3testAppendResult(p, sqlite3_errmsg(db));
  th3testCheck(p, "cannot open value of type integer");
  th3testBegin(p, "712");
  th3testCheckInt(p, 1, pBlob==0);

#ifndef SQLITE_OMIT_FLOATING_POINT  
  th3testBegin(p, "720");
  rc = sqlite3_blob_open(db, "main", "t1", "b", 4, 1, &pBlob);
  th3testCheckInt(p, SQLITE_ERROR, rc);
  th3testBegin(p, "721");
  th3testAppendResult(p, sqlite3_errmsg(db));
  th3testCheck(p, "cannot open value of type real");
  th3testBegin(p, "722");
  th3testCheckInt(p, 1, pBlob==0);
#endif

  /**********************************************************************
  ** Try to blob-io open an indexed column
  */
  th3testBegin(p, "800");
  th3dbEval(p, 0, "CREATE INDEX t1cb ON t1(c,b);");
  th3dbEval(p, 0, "CREATE UNIQUE INDEX t1d ON t1(d);");
  rc = sqlite3_blob_open(db, "main", "t1", "b", 2, 1, &pBlob);
  th3testCheckInt(p, SQLITE_ERROR, rc);
  th3testBegin(p, "801");
  th3testAppendResult(p, sqlite3_errmsg(db));
  th3testCheck(p, "cannot open indexed column for writing");
  th3testBegin(p, "802");
  th3testCheckInt(p, 1, pBlob==0);
  
  return 0;
}
