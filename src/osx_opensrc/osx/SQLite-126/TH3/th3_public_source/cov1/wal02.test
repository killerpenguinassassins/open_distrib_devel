/*
** Tests against write-ahead logging logic
**
** MIXED_MODULE_NAME:         wal02
** REQUIRED_PROPERTIES:       JOURNAL_WAL
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
/*
** This is a wal_hook callback for testing.  It records the
** database name and nFrame parameters.  Then returns the
** value of the $rc parameter.
*/
static int wal02_hook(
  void *pClientData,     /* Argument */
  sqlite3 *db,           /* Connection */
  const char *zDb,       /* Database */
  int nFrame             /* Size of WAL */
){
  th3state *p = (th3state*)pClientData;
  th3bindText(p, "$zDb", zDb);
  th3bindInt(p, "$nFrame", nFrame);
  return (int)th3valueInt64(p, "$rc", SQLITE_OK);
}

/*
** Install the testing wal_hook on database 0.
*/
static void wal02_install_hook(th3state *p, int iDb, char *zArg){
  sqlite3 *db0 = th3dbPointer(p, iDb);
  sqlite3_wal_hook(db0, wal02_hook, (void*)p);
}

/* Begin test scripts. */

/* The default autocheckpoint value is 1000 */
--testcase 100
PRAGMA wal_autocheckpoint;
--result 1000

--testcase 110
PRAGMA wal_autocheckpoint=OFF;
--result 0

/* Checkpoint callbacks */
--call wal02_install_hook
--testcase 200
PRAGMA page_size;
--store $pgsz
CREATE TABLE t1(x);
PRAGMA wal_checkpoint;
INSERT INTO t1 VALUES(1);
--glob # # #

/* The size of the WAL is larger that these tests indicate when the
** sector size exceeds to the page size due to the extra padding that
** is appended to the WAL.  So disable these tests in that case.
*/
--if ($pgsz+0) >= $sector_size
--testcase 201
SELECT x, $zDb, $nFrame FROM t1;
--result 1 main 2
--testcase 210
UPDATE t1 SET x=2;
SELECT x, $nFrame FROM t1;
--result 2 4
--testcase 211
UPDATE t1 SET x=3;
SELECT x, $nFrame FROM t1;
--result 3 6
--endif

--testcase 300
PRAGMA wal_autocheckpoint;
--result 0
--testcase 301
PRAGMA wal_autocheckpoint=5;
--result 5
--bind-reset
--testcase 310
UPDATE t1 SET x=4;
SELECT x, $nFrame FROM t1;
--result 4 nil

--testcase 400
PRAGMA wal_autocheckpoint;
--result 5
--call wal02_install_hook
--testcase 401
PRAGMA wal_autocheckpoint;
--result 0

--store $rc SELECT 1
--testcase 500
INSERT INTO t1 VALUES(5);
--result SQLITE_ERROR {SQL logic error or missing database}
--testcase 501
SELECT * FROM t1 ORDER BY 1
--result 4 5
