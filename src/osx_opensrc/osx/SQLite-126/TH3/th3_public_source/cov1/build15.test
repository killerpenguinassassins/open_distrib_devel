/*
** This module contains tests for the build.c source file.
**
** This particular file focuses on coverage testing of
** the CREATE INDEX statement and also automatically created
** indices that result from PRIMARY KEY and UNIQUE constraints
** in CREATE TABLE statements.
**
** SCRIPT_MODULE_NAME:        build15
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a,b,c);
CREATE INDEX aux.t1a ON t1(a);
--result SQLITE_ERROR {unknown database aux}

--testcase 110
CREATE INDEX t2a ON t2(a);
--result SQLITE_ERROR {no such table: main.t2}

--testcase 120
CREATE TEMP TABLE t2(a);
--result
--testcase 121
--oom
CREATE INDEX t2a ON t2(a);
--result

--testcase 130
--new-filename aux.db
ATTACH :filename AS aux;
CREATE TABLE aux.t3(m);
CREATE INDEX aux.t1a ON t1(a);
--result SQLITE_ERROR {no such table: aux.t1}

--testcase 140
CREATE TABLE IF NOT EXISTS t1(a UNIQUE);
INSERT INTO t1 DEFAULT VALUES;
SELECT * FROM t1;
--result nil nil nil

--testcase 150
CREATE INDEX e150 ON sqlite_master(name);
--result SQLITE_ERROR {table sqlite_master may not be indexed}

--testcase 160
CREATE TABLE t160(a UNIQUE);
ALTER TABLE t160 ADD COLUMN b UNIQUE;
--result SQLITE_ERROR {Cannot add a UNIQUE column}

--testcase 170
CREATE VIEW v170 AS SELECT a+b+c AS x FROM t1;
CREATE INDEX v170x ON v170(x);
--result SQLITE_ERROR {views may not be indexed}

--testcase 180
CREATE INDEX sqlite_t1a ON t1(a);
--result SQLITE_ERROR {object name reserved for internal use: sqlite_t1a}

--testcase 190
CREATE INDEX t2a ON t2(a);
--result SQLITE_ERROR {index t2a already exists}
--testcase 191
CREATE INDEX IF NOT EXISTS t2a ON t2(a);
--result

--testcase 200
CREATE INDEX t2 ON t2(a);
--result SQLITE_ERROR {there is already a table named t2}

--testcase 300
CREATE TABLE t3(a INTEGER PRIMARY KEY, b TEXT COLLATE RTRIM);
CREATE INDEX t3b ON t3(b);
CREATE INDEX t3bnc ON t3(b COLLATE NOCASE);
INSERT INTO t3 VALUES(1, 'abc');
INSERT INTO t3 VALUES(2, 'BCD');
INSERT INTO t3 VALUES(3, 'cde');
--scan-count 1
SELECT b FROM t3 ORDER BY b COLLATE RTRIM;
--result BCD abc cde scan 0 sort 0

--testcase 301
SELECT b FROM t3 ORDER BY b COLLATE NOCASE;
--result abc BCD cde scan 0 sort 0
--scan-count 0

--testcase 302
SELECT a FROM t3 WHERE b='BCD   ';
--result 2
--testcase 303
SELECT a FROM t3 WHERE b='BCD   ' COLLATE BINARY;
--result

/* Indices with a large number of columns */
--testcase 400
CREATE TABLE t4(a,b,c,d,e,f,g,h,i,j,l,m,n);
CREATE INDEX t4_1 ON t4(a,b,c,d,e,f,g,h);
CREATE INDEX t4_2 ON t4(b,c,d,e,f,g,h,i);
CREATE INDEX t4_3 ON t4(c,d,e,f,g,h,i,j);
INSERT INTO t4 VALUES(1,2,3,4,5,6,7,8,9,10,11,12,13);
INSERT INTO t4 VALUES(2,1,4,5,6,6,8,9,10,11,12,13,14);
INSERT INTO t4 VALUES(3,4,2,6,7,8,9,10,11,12,13,14,15);
--scan-count 1
SELECT a, n FROM t4 ORDER BY a, b, c, d, e, f;
--result 1 13 2 14 3 15 scan 0 sort 0
--testcase 401
SELECT b, n FROM t4 ORDER BY b, c, d, e, f, g;
--result 1 14 2 13 4 15 scan 0 sort 0
--testcase 402
SELECT c, n FROM t4 ORDER BY c, d, e, f, g, h;
--result 2 15 3 13 4 14 scan 0 sort 0
