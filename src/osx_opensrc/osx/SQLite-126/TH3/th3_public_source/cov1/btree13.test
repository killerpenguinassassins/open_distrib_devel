/*
** Test module for btree.c.  PRAGMA integrity_check logic.
**
** SCRIPT_MODULE_NAME:        btree13
** REQUIRED_PROPERTIES:       TEST_VFS AUTOVACUUM CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY,y);
PRAGMA auto_vacuum=INCREMENTAL;
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(1, zeroblob($pgsz));
INSERT INTO t1 VALUES(2, zeroblob($pgsz));
INSERT INTO t1 VALUES(3, zeroblob($pgsz));
INSERT INTO t1 VALUES(4, zeroblob($pgsz));
INSERT INTO t1 VALUES(5, zeroblob($pgsz));
DELETE FROM t1 WHERE x=1;
DELETE FROM t1 WHERE x=2;
DELETE FROM t1 WHERE x=3;
DELETE FROM t1 WHERE x=4;
DELETE FROM t1 WHERE x=5;
--result

/* Verify that the freelist is what we think it is.
*/
--testcase 110
--checkpoint
seek 32 read32 read32
--edit test.db
/* freelist trunk at page 4.  Total 5 freelist pages */
--result 4 5

--testcase 120
SELECT $pgsz*3;
--store $pg4
seek $pg4 read32 read32 read32 read32 read32 read32
--edit test.db
--result 0 4 5 6 7 8

--testcase 130
seek $pg4 move 12 write32 2 incr-chng
--edit test.db
PRAGMA integrity_check;
--result "*** in database main ***\012Main freelist: Failed to read ptrmap key=2\012Pointer map page 2 is referenced\012Page 6 is never used"

--testcase 131
PRAGMA integrity_check(1);
--result "*** in database main ***\012Main freelist: Failed to read ptrmap key=2"

--testcase 140
seek $pg4 move 12 write32 8 incr-chng
--edit test.db
PRAGMA integrity_check;
--result "*** in database main ***\012Main freelist: 2nd reference to page 8\012Page 6 is never used"
