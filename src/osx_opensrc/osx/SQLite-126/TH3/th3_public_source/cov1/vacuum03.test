/*
** This module contains tests for the VACUUM statement.
**
** Change page size by vacuuming.
**
** These tests do not work when AUTOVACUUM is on.  The reason is simple.
** When AUTOVACUUM is defined, a "PRAGMA auto_vacuum=ON" is run on each
** database connection as it is opened.  But that locks in the page size
** such that it cannot be changed.  For the same reasonm, theses tests
** do not work with journal_mode=WAL.
**
** SCRIPT_MODULE_NAME:        vacuum03
** REQUIRED_PROPERTIES:       CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB AUTOVACUUM JOURNAL_WAL
** MINIMUM_HEAPSIZE:          100000
*/
--new test.db
--raw-open test.db
--testcase 100
PRAGMA page_size=512;
CREATE TABLE t1(a PRIMARY KEY, b);
CREATE TRIGGER r1 AFTER INSERT ON t1 BEGIN SELECT 'hi'; END;
CREATE VIEW v1 AS SELECT a+b FROM t1;
CREATE INDEX i1 ON t1(b);
CREATE TEMP TABLE t2(x,y);
INSERT INTO t1 VALUES(123,'xyz');
INSERT INTO t1(rowid,a,b) VALUES(2,zeroblob(100000),'hello');
DELETE FROM t1 WHERE rowid=2;
--result

--open test.db
--testcase 101
SELECT * FROM t1 WHERE 0;
PRAGMA page_size;
--result 512

--testcase 102
PRAGMA page_size=1024;
VACUUM;
PRAGMA page_size;
--result 1024

--open test.db
--testcase 103
SELECT * FROM t1 WHERE 0;
PRAGMA page_size;
--result 1024

--testcase 104
PRAGMA page_size=4096;
VACUUM;
--result

--open test.db
--testcase 105
SELECT * FROM t1 WHERE 0;
PRAGMA page_size;
--result 4096
