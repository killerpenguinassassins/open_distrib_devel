/*
** This module contains tests of WHERE clause handling.
**
** The focus of this module is the index-only optimization on tables
** with many columns (more than BMS entries).   BMS defaults to 64
**
** SCRIPT_MODULE_NAME:        where15
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(
  c00, c01, c02, c03, c04, c05, c06, c07, c08, c09,
  c10, c11, c12, c13, c14, c15, c16, c17, c18, c19,
  c20, c21, c22, c23, c24, c25, c26, c27, c28, c29,
  c30, c31, c32, c33, c34, c35, c36, c37, c38, c39,
  c40, c41, c42, c43, c44, c45, c46, c47, c48, c49,
  c50, c51, c52, c53, c54, c55, c56, c57, c58, c59,
  c60, c61, c62, c63, c64, c65, c66, c67, c68, c69
);
INSERT INTO t1 VALUES(
   0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
  10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
  20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
  30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
  40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
  50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
  60, 61, 62, 63, 64, 65, 66, 67, 68, 69
);
INSERT INTO t1 VALUES(
  100, 101, 102, 103, 104, 105, 106, 107, 108, 109,
  110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
  120, 121, 122, 123, 124, 125, 126, 127, 128, 129,
  130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
  140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
  150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
  160, 161, 162, 163, 164, 165, 166, 167, 168, 169
);
INSERT INTO t1 VALUES(
  200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
  210, 211, 212, 213, 214, 215, 216, 217, 218, 219,
  220, 221, 222, 223, 224, 225, 226, 227, 228, 229,
  230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
  240, 241, 242, 243, 244, 245, 246, 247, 248, 249,
  250, 251, 252, 253, 254, 255, 256, 257, 258, 259,
  260, 261, 262, 263, 264, 265, 266, 267, 268, 269
);

CREATE INDEX t1a ON t1(c00, c48, c62, c63, c64, c69);
SELECT c00, c48, c62 FROM t1 WHERE c00>=0 ORDER BY c00;
--result 0 48 62 100 148 162 200 248 262

--testcase 110
SELECT c00, c48, c63 FROM t1 WHERE c00>=0 ORDER BY c00;
--result 0 48 63 100 148 163 200 248 263

--testcase 120
SELECT c00, c48, c64 FROM t1 WHERE c00>=0 ORDER BY c00;
--result 0 48 64 100 148 164 200 248 264

--testcase 130
SELECT c00, c48, c69 FROM t1 WHERE c00>=0 ORDER BY c00;
--result 0 48 69 100 148 169 200 248 269
