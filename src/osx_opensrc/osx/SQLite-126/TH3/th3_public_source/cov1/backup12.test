/*
** Try to restore an empty file into a non-empty database.
**
** This does not work with journal_mode=WAL
**
** MODULE_NAME:               backup12
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB JOURNAL_WAL ZIPVFS
** MINIMUM_HEAPSIZE:          100000
*/
int backup12(th3state *p){
  sqlite3 *pSrc;    /* Source database */
  sqlite3 *pDest;   /* Destination database */
  int nTotal;       /* Total size of source in pages */
  int pgszSrc;      /* Source page size */
  int pgszDest;     /* Destination page size */
  char zCksum[100]; /* Database checksum */

  sqlite3_backup *pBackup;
  int rc;

  /* Create an empty source database */
  th3testBegin(p, "100");
  th3dbOpen(p, 0, "src.db", TH3_OPEN_NEW | TH3_OPEN_RAW);
  th3testCheck(p, "");

  /* Compute a checksum over the source database */
  th3testResetResult(p);
  th3dbEval(p, 0, "SELECT count(*), md5sum(a,b) FROM t1");
  assert( strlen(p->zResult)<sizeof(zCksum) );
  th3strcpy(zCksum, p->zResult);

  /* Get page size */
  th3testResetResult(p);
  th3dbEval(p, 0, "PRAGMA page_size");
  pgszSrc = th3atoi64(p->zResult);
  th3testResetResult(p);

  /* Create the destination database with content */
  th3testBegin(p, "110");
  pSrc = th3dbPointer(p, 0);
  th3dbNew(p, 1, "dest.db");
  th3dbEval(p, 1,
     "CREATE TABLE t1(a,b);"
     "INSERT INTO t1 VALUES(th3random(), th3randomBlob(800));"
     "INSERT INTO t1 VALUES(th3random(), th3randomBlob(800));"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(800) FROM t1;"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(800) FROM t1;"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(800) FROM t1;"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(800) FROM t1;"
     "SELECT count(*) FROM t1;"
  );
  th3testCheck(p, "32");

  /* Get page size */
  th3testResetResult(p);
  th3dbEval(p, 1, "PRAGMA page_size");
  pgszDest = th3atoi64(p->zResult);
  th3testResetResult(p);

  th3testBegin(p, "120");
  th3dbOpen(p, 1, "dest.db", TH3_OPEN_RAW);
  pDest = th3dbPointer(p, 1);
  pBackup = sqlite3_backup_init(pDest, "main", pSrc, "main");
  if( pBackup==0 ){
    th3testFailed(p, sqlite3_errmsg(pDest));
    return 0;
  }
  rc = sqlite3_backup_step(pBackup, 0);
  nTotal = sqlite3_backup_pagecount(pBackup);
  th3testAppendResult(p, th3format(p, "%d %d %d", 
            rc,
            sqlite3_backup_pagecount(pBackup),
            sqlite3_backup_remaining(pBackup)
  ));
  if( (p->config.maskProp & TH3_JOURNAL_WAL)!=0 && pgszSrc!=pgszDest ){
    th3testCheck(p, th3format(p, "%d %d %d", SQLITE_READONLY, nTotal, nTotal));
  }else{
    th3testCheck(p, th3format(p, "%d %d %d", SQLITE_DONE, nTotal, nTotal));
  }

  /* Verify that the destination is identical to the source */
  th3testBegin(p, "130");
  rc = sqlite3_backup_finish(pBackup);
  if( (p->config.maskProp & TH3_JOURNAL_WAL)!=0 && pgszSrc!=pgszDest ){
    th3testCheckInt(p, SQLITE_READONLY, rc);
  }else{
    if( rc!=SQLITE_OK ){
      th3testFailed(p, sqlite3_errmsg(pDest));
      return 0;
    }
    th3dbEval(p, 1, "SELECT name FROM sqlite_master");
    th3testCheck(p, "");
  }
  return 0;
}
