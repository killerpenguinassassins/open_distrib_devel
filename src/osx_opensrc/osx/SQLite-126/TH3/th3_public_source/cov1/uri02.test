/*
** This module contains tests of URI file name capabilities
**
** MIXED_MODULE_NAME:         uri02
** REQUIRED_PROPERTIES:       URI
** DISALLOWED_PROPERTIES:     THREADS 8_3_NAMES
** MINIMUM_HEAPSIZE:          100000
*/


/*
** A new VFS that captures the query parameters.
*/
static struct {
   sqlite3_vfs vfs;              /* The new VFS that captures query params */
   sqlite3_vfs *pRealVfs;        /* The real VFS */
   th3state *p;                  /* The test context */
   const char *zFilename;        /* Last filename parameter to uri02_xOpen. */
} uri02_g;

/*
** This tricked-out version of xOpen records the name of the file being
** opened together will all query parameters, in the result buffer of
** the test script.
*/
static int uri02_xOpen(
  sqlite3_vfs *pVfs,
  const char *zName,
  sqlite3_file *pFile,
  int flags,
  int *pOutFlags
){
  if( flags & SQLITE_OPEN_MAIN_DB ){
    const char *z = zName;
    char *zCopy = sqlite3_mprintf("%s",zName);
    int i;
    for(i=0; zCopy[i]; i++){
      if( zCopy[i]=='\\' ) zCopy[i] = '/';
      else if( zCopy[i]<=' ' ) zCopy[i] = '_';
    }
    th3testAppendResultTerm(uri02_g.p, zCopy);
    sqlite3_free(zCopy);
    z += strlen(z) + 1;
    while( z[0] ){
      th3testAppendResultTerm(uri02_g.p, z);
      z += strlen(z) + 1;
      th3testAppendResultTerm(uri02_g.p, z);
      z += strlen(z) + 1;
    }
    uri02_g.zFilename = zName;
  }
  return uri02_g.pRealVfs->xOpen(uri02_g.pRealVfs,zName,pFile,flags,pOutFlags);
}


static void uri02_setup(th3state *p, int iDb, char *zArg){
  uri02_g.pRealVfs = p->pVfs;
  memcpy(&uri02_g.vfs, p->pVfs, sizeof(uri02_g.vfs));
  uri02_g.vfs.zName = "uri02_vfs";
  uri02_g.vfs.xOpen = uri02_xOpen;
  sqlite3_vfs_register(&uri02_g.vfs, 0);
  uri02_g.p = p;
}

static void uri02_finish(th3state *p, int iDb, char *zArg){
  sqlite3_vfs_unregister(&uri02_g.vfs);
}

static void uri02_param(th3state *p, int iDb, char *zArg){
  th3testAppendResultTerm(p, sqlite3_uri_parameter(uri02_g.zFilename, zArg));
}

/*****************************************************************************/
--close all
--call uri02_setup
--testcase 100
--open file:test1.db?a=b&c=d&e&vfs=uri02_vfs#efg
--glob *test1.db a b c d e {} vfs uri02_vfs
--testcase 100a
--call uri02_param a
--result b
--testcase 100c
--call uri02_param c
--result d
--testcase 100vfs
--call uri02_param vfs
--result uri02_vfs
--testcase 100eft
--call uri02_param e
--result {}
--testcase 100x
--call uri02_param x
--result nil

--testcase 101
--open file:test1.db?a&b&&c&=&vfs=uri02_vfs#efg
--glob *test1.db a {} b {} c {} vfs uri02_vfs
--testcase 102
--open file:test1.db?a=x=y&%00b&c=%00xyz&vfs=uri02_vfs#efg
--glob *test1.db a x=y c {} vfs uri02_vfs
--testcase 103
--open file:test1.db?vfs=uri02_vfs&a#efg
--glob *test1.db vfs uri02_vfs a {}
--testcase 104
--open file:test1.db?vfs=uri02_vfs&a=%%0x
--glob *test1.db vfs uri02_vfs a %%0x
--testcase 105
--open file:test1.db?vfs=uri%30%32_vfs&=xyz&b
--glob *test1.db vfs uri02_vfs b {}
--testcase 106
ATTACH 'filx:test1.db?vfs=xyz&x=y' AS fail1;
--glob *filx:test1.db?vfs=xyz&x=y
--testcase 107
--open file:test1.db?vfs=uri02_vfs&=abc
--glob *test1.db vfs uri02_vfs
--testcase 108
--open file:test1.db?vfs=uri02_vfs&&=abc
--glob *test1.db vfs uri02_vfs
--testcase 109
--open file:test1.db?vfs=uri02_vfs&
--glob *test1.db vfs uri02_vfs

--testcase 110
--open file:test1.db?vfs=uri02_vfs&&#
--glob *test1.db vfs uri02_vfs

--testcase 120
--raw-open file://no-path
--result SQLITE_MISUSE {invalid uri authority: no-path}

--testcase 130
--raw-open file://
--result
--testcase 131
PRAGMA database_list
--result 0 main {}

--testcase 135
--raw-open file://localhost
--result
--testcase 136
PRAGMA database_list
--result 0 main {}

--testcase 140
--raw-open file://localhosx/test1.db?vfs=uri01_vfs
--result SQLITE_MISUSE {invalid uri authority: localhosx}


--close all
--call uri02_finish
