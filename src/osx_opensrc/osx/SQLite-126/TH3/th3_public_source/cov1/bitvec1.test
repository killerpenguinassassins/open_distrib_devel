/*
** Module for the Bitvec object.
**
** This module does not really require TEMPSTORE_MEM.  We included it in the
** required properties list simply to limit the number of times this
** modules is run on a full test.  This is a slow module, and it helps
** a complete test run faster if we limit the number of times this module
** is evaluated.
**
** MODULE_NAME:               bitvec1
** REQUIRED_PROPERTIES:       TEMPSTORE_MEM
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          20000000
*/


/*
** BITVEC Test Programs:
**
** There are 6 opcodes numbered from 0 through 5.  0 is the
** "halt" opcode and causes the test to end.
**
**    0          Halt and return the number of errors
**    1 N S X    Set N bits beginning with S and incrementing by X
**    2 N S X    Clear N bits beginning with S and incrementing by X
**    3 N        Set N randomly chosen bits
**    4 N        Clear N randomly chosen bits
**    5 N S X    Set N bits from S increment X in array only, not in bitvec
*/
int bitvec1(th3state *p){
  /* 
  ** The Built-in Test facility must be enabled in order for theses
  ** tests to work.
  */
#ifndef SQLITE_OMIT_BUILTIN_TEST

  int rc;         /* Result code from sqlite3_test_control() */
  int i, j;       /* Loop counters */
  int a[100];     /* Mutable copy of the test program */

  /* Make sure BitvecBuiltinTest correctly reports errors that
  ** are deliberately introduced.
  */
  th3testBegin(p, "1.1");
  {
    static const int aProg[] = { 5, 1, 1, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "1");
  th3testBegin(p, "1.2");
  {
    static const int aProg[] = { 5, 1, 234, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "234");

  /*
  ** Run test cases that set every bit in vectors of various sizes.
  ** for larger cases, this should cycle the bit vector representation
  ** from hashing into subbitmaps.  The subbitmaps should start as
  ** hashes then change to either subbitmaps or linear maps, depending
  ** on their size.
  */
  th3testBegin(p, "1.3");
  {
    static const int aProg[] = { 1, 400, 1, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.4");
  {
    static const int aProg[] = { 1, 4000, 1, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 4000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.5");
  {
    static const int aProg[] = { 1, 40000, 1, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 40000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.6");
  {
    static const int aProg[] = { 1, 400000, 1, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");

  /* Use a larger increment to spread the load around.
  */
  th3testBegin(p, "1.7");
  {
    static const int aProg[] = { 1, 400, 1, 7, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.8");
  {
    static const int aProg[] = { 1, 4000, 1, 7, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 4000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.9");
  {
    static const int aProg[] = { 1, 40000, 1, 7, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 40000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.10");
  {
    static const int aProg[] = { 1, 400000, 1, 7, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");

  /* Initially fill the bitvec with 1s.  Then stress the clearing
  ** mechanism by going through and clearing all the bits, not necessarily
  ** in order.
  */
  th3testBegin(p, "1.11");
  {
    static const int aProg[] = { 1, 400, 1, 1, 2, 400, 1, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.12");
  {
    static const int aProg[] = { 1, 4000, 1, 1, 2, 4000, 1, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 4000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.13");
  {
    static const int aProg[] = { 1, 40000, 1, 1, 2, 40000, 1, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 40000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.14");
  {
    static const int aProg[] = { 1, 400000, 1, 1, 2, 400000, 1, 1, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.15");
  {
    static const int aProg[] = { 1, 400, 1, 1, 2, 400, 1, 7, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.16");
  {
    static const int aProg[] = { 1, 4000, 1, 1, 2, 4000, 1, 7, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 4000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.17");
  {
    static const int aProg[] = { 1, 40000, 1, 1, 2, 40000, 1, 77, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 40000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.18");
  {
    static const int aProg[] = { 1, 400000, 1, 1, 2, 400000, 1, 777, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "1.19");
  {
    static const int aProg[] = { 1, 5000, 100000, 1, 2, 400000, 1, 37, 0 };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");

  /* Force some hash collisions
  */
  for(i=1; i<=8; i++){
    for(j=124; j<=125; j++){
      th3testBegin(p, th3format(p, "1.20.%d.%d", i, j));
      a[0] = 1;
      a[1] = 60;
      a[2] = i;
      a[3] = j;
      a[4] = 2;
      a[5] = 5000;
      a[6] = 1;
      a[7] = 1;
      a[8] = 0;
      rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400000, a);
      th3testAppendResult(p, th3format(p, "%d", rc));
      th3testCheck(p, "0");
    }
  }
      
  /* A stress test
  */
  th3testBegin(p, "1.30.big_and_slow");
  {
    static const int aProg32[] = { 1, 17000000, 1, 1, 2, 17000000, 1, 1, 0 };
    static const int aProg64[] = { 1,  8500000, 1, 1, 2,  8500000, 1, 1, 0 };
    if( sizeof(char*)==4 ){
      memcpy(a, aProg32, sizeof(aProg32));
    }else{
      memcpy(a, aProg64, sizeof(aProg64));
    }
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, a[1], a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
 

  /* Random bit testing.
  */
  th3testBegin(p, "2.1");
  {
    static const int aProg[] = { 3, 2000, 4, 2000, 0};
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 4000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "2.2");
  {
    static const int aProg[] = {
        3, 1000, 4, 1000, 3, 1000, 4, 1000, 3, 1000, 4, 1000,
        3, 1000, 4, 1000, 3, 1000, 4, 1000, 3, 1000, 4, 1000,
        3, 1000, 4, 1000, 3, 1000, 4, 1000, 3, 1000, 4, 1000, 0
    };
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 4000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "2.3");
  {
    static const int aProg[] = { 3, 10, 0};
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 400000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "2.4");
  {
    static const int aProg[] = { 3, 10, 2, 4000, 1, 1, 0};
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 4000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "2.5");
  {
    static const int aProg[] = { 3, 20, 2, 5000, 1, 1, 0};
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 5000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  th3testBegin(p, "2.6");
  {
    static const int aProg[] = { 3, 60, 2, 50000, 1, 1, 0};
    memcpy(a, aProg, sizeof(aProg));
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 50000, a);
    th3testAppendResult(p, th3format(p, "%d", rc));
  }
  th3testCheck(p, "0");
  for(i=120; i<=130; i++){
    static const int aTemplate[] = {
        1, 25, 121, 000,
        1, 50, 121, 000,
        2, 25, 121, 000,
        0
    };
    th3testBegin(p, th3format(p, "2.7.%d", i));
    memcpy(a, aTemplate, sizeof(aTemplate));
    a[ 3] = i;
    a[ 7] = i;
    a[11] = i;
    rc = sqlite3_test_control(SQLITE_TESTCTRL_BITVEC_TEST, 5000, a);
    th3testCheckInt(p, 0, rc);
  }

#endif /* SQLITE_OMIT_BUILTIN_TEST */
  return 0;
}
