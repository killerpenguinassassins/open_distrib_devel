/*
** This module contains tests for the build.c source file.
**
** Miscellaneous test cases to poke into obsure corners of the code.
** 
** SCRIPT_MODULE_NAME:        build28
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* The pParse->rc==SQLITE_OK test near the end of sqlite3FinishCoding() */
--testcase 100
CREATE TABLE t1(a, b, c);
CREATE INDEX i1 ON t1(a);
CREATE INDEX i2 ON t1(b);
--result
--testcase 101
--oom
SELECT c FROM t1 WHERE a = 1 OR a = 2 OR a = 3 OR a = 4 OR a = 5 OR a = 6;
--result

/* The pParse->rc==SQLITE_OK test near the end of sqlite3FinishCoding() */
--new test.db
--testcase 200
--oom
DROP TABLE IF EXISTS t1;
--result

/* Drop indices out of order */
--new test.db
--testcase 300
CREATE TABLE t1(a,b,c,d,e,f);
CREATE INDEX t1a ON t1(a);
CREATE INDEX t1b ON t1(b);
CREATE INDEX t1c ON t1(c);
CREATE INDEX t1d ON t1(d);
CREATE INDEX t1e ON t1(e);
CREATE INDEX t1f ON t1(f);
INSERT INTO t1 VALUES(1,2,3,4,5,6);
INSERT INTO t1 VALUES(7,8,9,10,11,12);
SELECT * FROM t1;
--result 1 2 3 4 5 6 7 8 9 10 11 12
--testcase 301
DROP INDEX t1c;
SELECT name FROM sqlite_master WHERE type='index' ORDER BY name;
--result t1a t1b t1d t1e t1f
--testcase 302
DROP INDEX t1e;
SELECT name FROM sqlite_master WHERE type='index' ORDER BY name;
--result t1a t1b t1d t1f
--testcase 303
DROP INDEX t1f;
SELECT name FROM sqlite_master WHERE type='index' ORDER BY name;
--result t1a t1b t1d
--testcase 304
DROP INDEX t1a;
SELECT name FROM sqlite_master WHERE type='index' ORDER BY name;
--result t1b t1d
--testcase 305
DROP INDEX t1d;
SELECT name FROM sqlite_master WHERE type='index' ORDER BY name;
--result t1b
--testcase 306
DROP INDEX t1b;
SELECT name FROM sqlite_master WHERE type='index' ORDER BY name;
--result

/* DETACH databases in a different order from ATTACH */
--new test.db
--testcase 400
CREATE TABLE t1(rownum,colname,value);
--new-filename aux1.db
ATTACH :filename AS aux1;
--result
--testcase 401
--new-filename aux2.db
ATTACH :filename AS aux2;
--result
--testcase 402
--new-filename aux3.db
ATTACH :filename AS aux3;
--result
--testcase 404
--new-filename aux4.db
ATTACH :filename AS aux4;
--result
--testcase 405
DETACH aux2;
PRAGMA database_list;
--table t1
SELECT value FROM t1 WHERE colname='name' ORDER BY value;
DELETE FROM t1;
--result aux1 aux3 aux4 main
--testcase 406
DETACH aux1;
PRAGMA database_list;
--table t1
SELECT value FROM t1 WHERE colname='name' ORDER BY value;
DELETE FROM t1;
--result aux3 aux4 main
--testcase 407
DETACH aux3;
PRAGMA database_list;
--table t1
SELECT value FROM t1 WHERE colname='name' ORDER BY value;
DELETE FROM t1;
--result aux4 main

/* Construct a transient table so that sqlite3DeleteTable() will be
** called on a table that contains Table.dbMem!=0 */
--testcase 500
CREATE TABLE t500(a,b);
INSERT INTO t500 VALUES(1,2);
INSERT INTO t500 VALUES(3,4);
CREATE TABLE t501(x,y);
INSERT INTO t501 VALUES(111,222);
--result
--testcase 501
SELECT * FROM t500 AS P, (SELECT 7,8 UNION SELECT 9,10), t501 AS Q
 WHERE P.a==(Q.x%10);
--result 1 2 7 8 111 222 1 2 9 10 111 222

#ifndef SQLITE_OMIT_FLOATING_POINT
/* For pParse->nErr>0 at the beginning of sqlite3EndTable().  This test
** case came from fuzz.tcl in the TCL test suite. */
--testcase 600
CREATE VIEW also AS SELECT DISTINCT hex(56.1 NOT IN (SELECT EXISTS (SELECT DISTINCT a FROM abc ORDER BY EXISTS (SELECT 'experiments' UNION     SELECT ALL 456) ASC))), b FROM (SELECT DISTINCT CAST(- 123456789.1234567899 AS text), hex(abs(EXISTS (SELECT DISTINCT (SELECT ALL 'first' ORDER BY 'hardware' LIMIT 456 OFFSET -2147483648) NOT IN (SELECT 'injection' ORDER BY 2147483647 ASC) ORDER BY (SELECT 'in' INTERSECT SELECT 'hardware') DESC)))) UNION     SELECT 0, hex(randomblob(min(max((SELECT 'in' IN (SELECT 2147483649 UNION ALL SELECT 2147483648) IN (SELECT 0 ORDER BY 'fault' DESC) NOT IN (SELECT 2147483648) IN (SELECT ALL 'The') UNION     SELECT DISTINCT -456) IN (SELECT DISTINCT (SELECT 2147483648) UNION ALL SELECT 'hardware' ORDER BY 2147483649 DESC),1), 500))) FROM (SELECT a, length((SELECT 'first' UNION     SELECT -1) IN (SELECT -2147483648)) FROM abc);
--result SQLITE_ERROR {no such table: main.abc}
#endif

/* Get sqlite3VdbeGet() to fail within sqlite3CodeVerifySchema() */
--new test.db
--testcase 700
CREATE TABLE t1(a PRIMARY KEY, b);
CREATE INDEX t1b ON t1(b);
--result
--testcase 701
--oom
ANALYZE;
--result
