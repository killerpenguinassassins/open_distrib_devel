/*
** Test module for btree.c and especially the ability of getAndInitPage() to
** handle database corruption.
**
** SCRIPT_MODULE_NAME:        btree09
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY,y);
PRAGMA page_size;
--store $pgsz
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t1';
--store $root
INSERT INTO t1 VALUES(0, zeroblob($pgsz-100));
INSERT INTO t1 VALUES(1, zeroblob($pgsz-100));
INSERT INTO t1 VALUES(2, zeroblob($pgsz-100));
INSERT INTO t1 VALUES(3, zeroblob($pgsz-100));
INSERT INTO t1 VALUES(4, zeroblob($pgsz-100));
INSERT INTO t1 VALUES(5, zeroblob($pgsz-100));
SELECT x FROM t1;
--result 0 1 2 3 4 5
--checkpoint

/* Each entry is on a separate page.  We can adjust the "right child" value
** of the root page to send any "pgno" value into getAndInitPage() that we
** desire.
*/
--testcase 110
PRAGMA page_count;
--store $pgcnt
seek $root move 8 write32 0 incr-chng
--edit test.db
SELECT x FROM t1 ORDER BY x DESC;
--result SQLITE_CORRUPT {database disk image is malformed}

--testcase 120
seek $root move 8 write32 $pgcnt move -4 add32 1 incr-chng
--edit test.db
SELECT x FROM t1 ORDER BY x DESC;
--result SQLITE_CORRUPT {database disk image is malformed}
