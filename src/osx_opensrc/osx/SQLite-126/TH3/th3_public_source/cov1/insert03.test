/*
** This module contains tests for the INSERT statement.
**
** These test are written from user documentation.  The focus
** here is inserting into virtual tables.
**
** SCRIPT_MODULE_NAME:        insert03
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 1
CREATE VIRTUAL TABLE vt1 USING bvs;
SELECT * FROM vt1;
--result

--testcase 2
INSERT INTO vt1 VALUES('name1','value1');
SELECT * FROM vt1;
--result name1 value1

--testcase 3
INSERT INTO vt1(value,name) VALUES('value2','name2');
SELECT * FROM vt1 ORDER BY name;
--result name1 value1 name2 value2

--testcase 101
CREATE TABLE t1(x,y);
INSERT INTO t1 SELECT name, value FROM vt1 ORDER BY name;
SELECT x, y FROM t1 ORDER BY x;
--result name1 value1 name2 value2

--testcase 102
DELETE FROM vt1;
INSERT INTO vt1 SELECT substr(x,3), substr(y,4) FROM t1;
SELECT * FROM vt1 ORDER BY name;
--result me1 ue1 me2 ue2

--testcase 103
DELETE FROM vt1;
INSERT INTO vt1(value,name) SELECT substr(y,4), substr(x,3) FROM t1;
SELECT * FROM vt1 ORDER BY name;
--result me1 ue1 me2 ue2


/* Special test to exercise the IsVirtual(pTab) test at the beginning
** of the sqlite3OpenTable() routine. */
--testcase 201
SELECT value FROM vt1 WHERE name='me1' OR value='ue2' ORDER BY value;
--result ue1 ue2
