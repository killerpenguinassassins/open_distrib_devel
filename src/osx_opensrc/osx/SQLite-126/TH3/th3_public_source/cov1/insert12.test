/*
** This module contains tests for the INSERT statement.
**
** CHECK constraints.
**
** SCRIPT_MODULE_NAME:        insert12
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a CHECK(a<100));
CREATE TABLE testdata(a);
INSERT INTO testdata(rowid,a) VALUES(1,11);
INSERT INTO testdata(rowid,a) VALUES(2,111);
INSERT INTO testdata(rowid,a) VALUES(3,22);
--result

--testcase 101
INSERT INTO t1 SELECT * FROM testdata;
--result SQLITE_CONSTRAINT {constraint failed}

--testcase 102
SELECT * FROM t1;
--result

--testcase 103
PRAGMA ignore_check_constraints=ON;
INSERT INTO t1 SELECT * FROM testdata;
PRAGMA ignore_check_constraints=OFF;
SELECT * FROM t1 ORDER BY a;
--result 11 22 111

--testcase 201
DELETE FROM t1;
INSERT OR IGNORE INTO t1 SELECT * FROM testdata;
--result

--testcase 202
SELECT * FROM t1 ORDER BY a;
--result 11 22

--testcase 301
DELETE FROM t1;
BEGIN;
INSERT INTO t1 VALUES(0);
INSERT OR REPLACE INTO t1 SELECT * FROM testdata;
--result SQLITE_CONSTRAINT {constraint failed}
--testcase 302
SELECT * FROM t1 ORDER BY a;
--result 0
--testcase 303
COMMIT;
--result


--testcase 401
DELETE FROM t1;
INSERT OR FAIL INTO t1 SELECT * FROM testdata;
--result SQLITE_CONSTRAINT {constraint failed}

--testcase 402
SELECT * FROM t1 ORDER BY a;
--result 11

--testcase 501
DELETE FROM t1;
INSERT OR ABORT INTO t1 SELECT * FROM testdata;
--result SQLITE_CONSTRAINT {constraint failed}

--testcase 502
SELECT * FROM t1 ORDER BY a;
--result

--testcase 601
DELETE FROM t1;
BEGIN;
INSERT OR ROLLBACK INTO t1 SELECT * FROM testdata;
--result SQLITE_CONSTRAINT {constraint failed}

--testcase 602
COMMIT;
--result SQLITE_ERROR {cannot commit - no transaction is active}

--testcase 603
SELECT * FROM t1 ORDER BY a;
--result
