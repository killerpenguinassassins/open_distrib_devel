/*
** This module contains tests of the C-language interfaces found in
** the main.c source file. 
**
** MODULE_NAME:               main07
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
**
** The focus of this test module is sqlite3_sleep()
*/
int main07(th3state *p){
  int rc;
  int i;
  sqlite3_vfs *pDfltVfs;
  sqlite3_vfs *apOther[30];
  sqlite3_vfs tempVfs;

  /* Unregister all VFSes.  Attempt to call sqlite3_sleep().  Should get
  ** a zero return. */
  th3testBegin(p, "1");
  pDfltVfs = sqlite3_vfs_find(0);
  sqlite3_vfs_unregister(pDfltVfs);
  for(i=0; i<COUNT(apOther) && (apOther[i] = sqlite3_vfs_find(0))!=0; i++){
    sqlite3_vfs_unregister(apOther[i]);
  }
  assert( i<COUNT(apOther) );
  rc = sqlite3_sleep(1234);
  th3testAppendResult(p, th3format(p, "%d", rc));
  th3testCheck(p, "0");

  /* Put in the TEST_VFS as the default VFS.  Make calls to sqlite3_sleep()
  ** to verify that the xSleep method is being invoked */
  th3testBegin(p, "101");
  memcpy(&tempVfs, &th3vfsObjTemplate, sizeof(tempVfs));
  tempVfs.zName = "temp_vfs_test";
  sqlite3_vfs_register(&tempVfs, 1);
  th3sleepCount = 0;
  th3sleepMicrosec = 0;
  th3sleepReturn = 987654321;
  rc = sqlite3_sleep(12345);
  th3testAppendResult(p,
      th3format(p, "%d %d %d", rc, th3sleepCount, th3sleepMicrosec)
  );
  th3testCheck(p, "987654 1 12345000");

  /* Restore the VFS setup before returning */
  sqlite3_vfs_unregister(&tempVfs);
  for(i=0; apOther[i]; i++){
    sqlite3_vfs_register(apOther[i], 0);
  }
  sqlite3_vfs_register(pDfltVfs, 1);

  /* Calling sqlite3_vfs_unregister() will NULL is a harmless no-op */
  sqlite3_vfs_unregister(0);

  return 0;
}
