/*
** This module contains tests of WHERE clause handling.
**
** The focus of this module is WHERE clauses containing virtual tables.
**
** MODULE_NAME:               where12
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     SHARED_CACHE THREADS
** MINIMUM_HEAPSIZE:          100000
*/

/*
** Global variable in which to record information about the
** most recent call to where12BestIndex().
*/
static struct {
  int nOrderBy;          /* Value of pInfo->nOrderBy */
  struct sqlite3_index_orderby aOrderBy[4];   /* 4 entries of pInfo->aOrderBy */
  int retval;            /* Value to return from xBestIndex */
  char *zErrMsg;         /* Copy this error message */
  int mkInvalidPlan1;    /* Return an invalid plan if true */
  char *zIdxStr;         /* Index string */
  int bigCost;           /* Make the cost huge if true */
} where12g;

/*
** A substitute xBestIndex function for BVS that remembers information
** constraints and sets the where06_match_seen global if found.
*/
static int where12BestIndex(sqlite3_vtab *pVTab, sqlite3_index_info *pInfo){
  int i;

  /* Record the ORDER BY clause information so that the test module below
  ** can verify that it was set correctly.
  */
  where12g.nOrderBy = pInfo->nOrderBy;
  for(i=0; i<where12g.nOrderBy && i<4; i++){
    where12g.aOrderBy[i] = pInfo->aOrderBy[i];
  }

  /* Make the cost small if there are = constraints.  Make it larger
  ** if there are no = constraints.
  */
  if( where12g.bigCost ){
#ifdef SQLITE_OMIT_FLOATING_POINT
    pInfo->estimatedCost = (((sqlite3_int64)1)<<61);
#else
    pInfo->estimatedCost = 1e100;
#endif
  }else{
    pInfo->estimatedCost = (double)100;
  }
  for(i=0; i<pInfo->nConstraint; i++){
    if( pInfo->aConstraint[i].usable
      && pInfo->aConstraint[i].op==SQLITE_INDEX_CONSTRAINT_EQ ){
      pInfo->estimatedCost = (double)1;
      break;
    }
  }

  /* Set the orderByConsumed flag if appropriate */
  if( pInfo->nOrderBy && pInfo->aOrderBy[0].iColumn==-1
        && pInfo->aOrderBy[0].desc==0 ){
    pInfo->orderByConsumed = 1;
  }

  /* The virtual table is only suppose to use constraints marked with
  ** pInfo->aConstraint[].usable.  If it mkInvalidPlan1 is true, try
  ** to use other constraints and verify that SQLite detects this misuse.
  */
  for(i=0; i<pInfo->nConstraint; i++){
    if( pInfo->aConstraint[i].usable || where12g.mkInvalidPlan1 ){
      pInfo->aConstraintUsage[i].argvIndex = 1+i;
    }
  }

  /* Make a copy of the zIdxStr (if it exists) to verify that SQLite
  ** will correctly reclaim the memory.  If zIdxStr is not defined,
  ** set a static string to verify that it is not freed.
  */
  assert( pInfo->idxStr==0 );
  if( where12g.zIdxStr ){
    pInfo->idxStr = sqlite3_mprintf("%s", where12g.zIdxStr);
    pInfo->needToFreeIdxStr = 1;
  }else{
    pInfo->idxStr = "do not free this string";
  }

  /* Set the error message if requested.
  */
  if( where12g.retval && where12g.zErrMsg ){
    sqlite3_free(pVTab->zErrMsg);
    pVTab->zErrMsg = sqlite3_mprintf("(where12) %s", where12g.zErrMsg);
  }
  return where12g.retval;
}

/* The main testing routine */
int where12(th3state *p){
  sqlite3 *db;
  sqlite3_module where12module;

  where12module = th3bvsvtabModule;
  where12module.xBestIndex = where12BestIndex;
  memset(&where12g, 0, sizeof(where12g));

  th3bindInt(p, ":abc", 123);
  th3bindInt(p, ":xyz", 456);
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  sqlite3_create_module(db, "where12", &where12module, (void*)p);
  th3dbEval(p, 0,
     "CREATE VIRTUAL TABLE vt1 USING where12;"
     "SELECT value FROM vt1 WHERE name IN (':abc',':xyz') ORDER BY name;"
  );
  th3testCheck(p, "123 456");

  th3testBegin(p, "110");
  th3dbEval(p, 0,
     "SELECT value FROM vt1 WHERE name IS NULL;"
  );
  th3testCheck(p, "");

  th3testBegin(p, "200");
  th3dbEval(p, 0,
     "SELECT name, value FROM vt1 WHERE name NOT NULL ORDER BY -value;"
  );
  th3testCheck(p, ":xyz 456 :abc 123");
  th3testBegin(p, "201");
  th3testCheckInt(p, 0, where12g.nOrderBy);

  th3testBegin(p, "300");
  th3dbEval(p, 0,
     "CREATE TABLE t3(a,b);"
     "INSERT INTO t3 VALUES(123,'one');"
     "INSERT INTO t3 VALUES(234,'two');"
     "INSERT INTO t3 VALUES(345,'three');"
     "INSERT INTO t3 VALUES(456,'four');"
     "CREATE INDEX t3a ON t3(a);"
     "SELECT name, b FROM vt1 JOIN t3 ON value=a ORDER BY name;"
  );
  th3testAppendScan(p);
  th3testCheck(p, ":abc one :xyz four scan 0 sort 1");
  th3testBegin(p, "301");
  th3testCheckInt(p, 1, where12g.nOrderBy);
  th3testBegin(p, "302");
  th3testCheckInt(p, 0, where12g.aOrderBy[0].iColumn);
  th3testBegin(p, "303");
  th3testCheckInt(p, 0, where12g.aOrderBy[0].desc);

  th3testBegin(p, "310");
  th3dbEval(p, 0,
      "SELECT name, b FROM vt1 JOIN t3 ON value=a ORDER BY name, b;"
  );
  th3testAppendScan(p);
  th3testCheck(p, ":abc one :xyz four scan 0 sort 1");
  th3testBegin(p, "311");
  th3testCheckInt(p, 0, where12g.nOrderBy);

  th3testBegin(p, "320");
  th3dbEval(p, 0,
      "SELECT name, b FROM vt1 JOIN t3 ON value=a ORDER BY vt1.rowid;"
  );
  th3testAppendScan(p);
  th3testCheck(p, ":abc one :xyz four scan 0 sort 0");
  th3testBegin(p, "321");
  th3testCheckInt(p, 1, where12g.nOrderBy);
  th3testBegin(p, "322");
  th3testCheckInt(p, -1, where12g.aOrderBy[0].iColumn);
  th3testBegin(p, "323");
  th3testCheckInt(p, 0, where12g.aOrderBy[0].desc);

  th3testBegin(p, "330");
  th3dbEval(p, 0,
      "SELECT name, b FROM vt1 JOIN t3 ON value=a ORDER BY vt1.rowid DESC;"
  );
  th3testAppendScan(p);
  th3testCheck(p, ":xyz four :abc one scan 0 sort 1");
  th3testBegin(p, "331");
  th3testCheckInt(p, 1, where12g.nOrderBy);
  th3testBegin(p, "332");
  th3testCheckInt(p, -1, where12g.aOrderBy[0].iColumn);
  th3testBegin(p, "333");
  th3testCheckInt(p, 1, where12g.aOrderBy[0].desc);

  th3testBegin(p, "400");
  where12g.retval = SQLITE_NOMEM;
  th3dbEval(p, 0,
     "SELECT value FROM vt1 WHERE name IS NULL;"
  );
  th3testCheck(p, "SQLITE_NOMEM {out of memory}");

  th3testBegin(p, "410");
  where12g.retval = SQLITE_TOOBIG;
  th3dbEval(p, 0,
     "SELECT value FROM vt1 WHERE name IS NULL;"
  );
  th3testCheck(p, "SQLITE_ERROR {string or blob too big}");
  
  th3testBegin(p, "420");
  where12g.retval = SQLITE_ERROR;
  where12g.zErrMsg = "fake error";
  th3dbEval(p, 0,
     "SELECT value FROM vt1 WHERE name IS NULL;"
  );
  th3testCheck(p, "SQLITE_ERROR {(where12) fake error}");
  where12g.retval = SQLITE_OK;
 
  th3testBegin(p, "500");
  where12g.mkInvalidPlan1 = 1;
  th3dbEval(p, 0,
      "CREATE TABLE t5(x UNIQUE);"
      "INSERT INTO t5 VALUES('frog');"
      "INSERT INTO t5 VALUES(':abc');"
      "SELECT * FROM vt1 JOIN t5 ON name=x;"
  );
  th3testCheck(p,
       "SQLITE_ERROR {table vt1: xBestIndex returned an invalid plan}"
  );
  where12g.mkInvalidPlan1 = 0;
  th3testBegin(p, "501");
  th3dbEval(p, 0,
      "SELECT * FROM vt1 JOIN t5 ON name=x;"
  );
  th3testCheck(p, ":abc 123 :abc");


  th3testBegin(p, "600");
  where12g.zIdxStr = "this string is copied and freed";
  th3dbEval(p, 0,
      "SELECT x.*, y.*, z.*"
      "  FROM vt1 x, vt1 y, vt1 z"
      " WHERE x.name=y.name AND x.value=z.value"
  );
  th3testCheck(p, ":abc 123 :abc 123 :abc 123 :xyz 456 :xyz 456 :xyz 456");
  th3testBegin(p, "601");
  th3dbEval(p, 0,
      "SELECT value FROM vt1 WHERE name=':abc' OR name=':xyz'"
  );
  th3testCheck(p, "123 456");
  where12g.zIdxStr = 0;
  th3testBegin(p, "602");
  th3dbEval(p, 0,
      "SELECT value FROM vt1 WHERE name=':abc' OR name=':xyz'"
  );
  th3testCheck(p, "123 456");

  th3testBegin(p, "700");
  where12g.bigCost = 1;
  th3dbEval(p, 0,
      "CREATE TABLE t7(m INTEGER PRIMARY KEY); INSERT INTO t7 VALUES(456);"
      "SELECT name, b, m "
      "  FROM vt1 CROSS JOIN t3 ON +value=a JOIN t7 ORDER BY name"
  );
  th3testCheck(p, ":abc one 456 :xyz four 456");
  where12g.bigCost = 0;

  th3dbCloseAll(p);
  return 0;
}
