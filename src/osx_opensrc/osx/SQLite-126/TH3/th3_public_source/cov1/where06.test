/*
** This module contains tests of WHERE clause handling.
**
** The focus of this module is MATCH operator on virtual tables
**
** MODULE_NAME:               where06
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     SHARED_CACHE THREADS
** MINIMUM_HEAPSIZE:          100000
*/

/*
** a MATCH function that always returns TRUE.
*/
static void where06match(
  sqlite3_context *context,
  int argc,
  sqlite3_value **argv
){
  sqlite3_result_int(context, 1);
}

/*
** This flag is set when xBestIndex below sees
** an SQLITE_INDEX_CONSTRAINT_MATCH operator.
*/
static int where06_match_seen = 0;

/*
** A substitute xBestIndex function for BVS that checks for MATCH
** constraints and sets the where06_match_seen global if found.
*/
static int where06BestIndex(sqlite3_vtab *pVTab, sqlite3_index_info *pInfo){
  int i;
  for(i=0; i<pInfo->nConstraint; i++){
    if( pInfo->aConstraint[i].usable
      && pInfo->aConstraint[i].op==SQLITE_INDEX_CONSTRAINT_MATCH ){
      where06_match_seen = 1;
      break;
    }
  }
  return th3bvsvtabBestIndex(pVTab, pInfo);
}

/* The test module
*/
int where06(th3state *p){
  sqlite3 *db;
  sqlite3_module where06module;

  where06module = th3bvsvtabModule;
  where06module.xBestIndex = where06BestIndex;

  th3testBegin(p, "1");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  sqlite3_create_module(db, "where06", &where06module, (void*)p);
  sqlite3_create_function(db, "MATCH", 2, SQLITE_UTF8, 0,
                          where06match, 0, 0);
  sqlite3_create_function(db, "MATCH", -1, SQLITE_UTF8, 0,
                          where06match, 0, 0);
  th3bindInt(p, ":abc", 1);
  th3bindInt(p, ":xyz", 2);
  th3dbEval(p, 0,
    "CREATE VIRTUAL TABLE t1 USING where06;"
    "SELECT * FROM t1;"
  );
  th3testCheck(p, ":abc 1 :xyz 2");

  th3testBegin(p, "100");
  where06_match_seen = 0;
  th3dbEval(p, 0, 
    "SELECT * FROM t1 WHERE name MATCH 'pqr'"
  );
  th3testCheck(p, ":abc 1 :xyz 2");
  th3testBegin(p, "101");
  th3testCheckInt(p, 1, where06_match_seen);

  th3testBegin(p, "110");
  where06_match_seen = 0;
  th3dbEval(p, 0, 
    "SELECT * FROM t1 WHERE MATCH('pqr',name)"
  );
  th3testCheck(p, ":abc 1 :xyz 2");
  th3testBegin(p, "111");
  th3testCheckInt(p, 1, where06_match_seen);

  th3testBegin(p, "120");
  where06_match_seen = 0;
  th3dbEval(p, 0, 
    "SELECT * FROM t1 WHERE MATCH('pqr','mno')"
  );
  th3testCheck(p, ":abc 1 :xyz 2");
  th3testBegin(p, "121");
  th3testCheckInt(p, 0, where06_match_seen);

  th3testBegin(p, "130");
  where06_match_seen = 0;
  th3dbEval(p, 0, 
    "SELECT * FROM t1 WHERE MATCH('pqr','mno',name)"
  );
  th3testCheck(p, ":abc 1 :xyz 2");
  th3testBegin(p, "131");
  th3testCheckInt(p, 0, where06_match_seen);

  th3testBegin(p, "140");
  where06_match_seen = 0;
  th3dbEval(p, 0, 
    "SELECT * FROM t1 WHERE GLOB('pqr','pqr')"
  );
  th3testCheck(p, ":abc 1 :xyz 2");
  th3testBegin(p, "141");
  th3testCheckInt(p, 0, where06_match_seen);

  th3testBegin(p, "200");
  th3dbEval(p, 0,
    "CREATE TABLE t2(x,y);"
    "INSERT INTO t2 VALUES(8,9);"
    "SELECT * FROM t1 JOIN t2 ON name MATCH x ORDER BY name;"
  );
  th3testCheck(p, ":abc 1 8 9 :xyz 2 8 9");

  th3testBegin(p, "210");
  where06_match_seen = 0;
  th3dbEval(p, 0,
    "SELECT * FROM t1 CROSS JOIN t2 ON name MATCH x ORDER BY name;"
  );
  th3testCheck(p, ":abc 1 8 9 :xyz 2 8 9");
  th3testBegin(p, "211");
  th3testCheckInt(p, 1, where06_match_seen);

  th3testBegin(p, "300");
  where06_match_seen = 0;
  th3dbEval(p, 0,
    "SELECT * FROM t1 WHERE name MATCH value ORDER BY name;"
  );
  th3testCheck(p, ":abc 1 :xyz 2");
  th3testBegin(p, "301");
  th3testCheckInt(p, 0, where06_match_seen);

  th3oomBegin(p, "400");
  while( th3oomNext(p) ){
    int rc;
    th3oomEnable(p, 1);
    rc = th3dbEval(p, 0,
      "SELECT * FROM t1 WHERE name MATCH ':xyz'"
      "   AND name!='2'  AND name!='3'  AND name!='4'  AND name!='5'"
      "   AND name!='6'  AND name!='7'  AND name!='8'"
      " ORDER BY name;"
    );
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM);
    }else{
      th3oomResult(p, 2, ":abc 1 :xyz 2");
    }
  }
  th3oomEnd(p);

  th3testBegin(p, "500");
  th3dbEval(p, 0,
    "SELECT * FROM t1 WHERE name MATCH ':xyz' ORDER BY name"
  );
  th3testCheck(p, ":abc 1 :xyz 2");
  th3testBegin(p, "501");
  th3dbEval(p, 0,
    "SELECT * FROM t1 WHERE name NOT MATCH ':xyz' ORDER BY name"
  );
  th3testCheck(p, "");
  th3testBegin(p, "502");
  th3dbEval(p, 0,
    "SELECT * FROM t1 WHERE NOT name NOT MATCH ':xyz' ORDER BY name"
  );
  th3testCheck(p, ":abc 1 :xyz 2");

  /* Must close all connections before existing because the where06module
  ** variable is on the stack.
  */
  th3dbCloseAll(p);

  return 0;
}
