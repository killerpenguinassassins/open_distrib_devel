/*
** This module contains tests for the INSERT statement.
**
** These test are written from user documentation.  The focus
** here is on triggers
**
** SCRIPT_MODULE_NAME:        insert02
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 1
CREATE TABLE t1(a,b,c,d,e);
CREATE TABLE tlog1(x);
CREATE TABLE tlog2(y);
CREATE TRIGGER t1r1 BEFORE INSERT ON t1 BEGIN
  INSERT INTO tlog1 VALUES('before: a=' || new.a);
END;
CREATE TRIGGER t1r2 AFTER INSERT ON t1 BEGIN
  INSERT INTO tlog1 VALUES('after: a=' || new.a);
END;
INSERT INTO t1 VALUES(1,2,3,4,5);
SELECT * FROM t1;
SELECT * FROM tlog1;
SELECT * FROM tlog2;
--result 1 2 3 4 5 {before: a=1} {after: a=1}

--testcase 2
CREATE TABLE t2(a,b,c,d,e);
INSERT INTO t2 VALUES(6,7,8,9,10);
INSERT INTO t2 VALUES(11,12,13,14,15);
INSERT INTO t2 SELECT a+10, b+10, c+10, d+10, e+10 FROM t2;
INSERT INTO t1 SELECT * FROM t2;
SELECT * FROM tlog1;
--result {before: a=1} {after: a=1} {before: a=6} {after: a=6} {before: a=11} {after: a=11} {before: a=16} {after: a=16} {before: a=21} {after: a=21}

--testcase 100
CREATE VIEW v1 AS SELECT a, b, c FROM t1 WHERE a<10;
SELECT * FROM v1;
--result 1 2 3 6 7 8

--testcase 101
INSERT INTO v1 VALUES(100,200,300);
--result SQLITE_ERROR {cannot modify v1 because it is a view}

--testcase 102
DELETE FROM tlog1;
CREATE TRIGGER v1r1 INSTEAD OF INSERT ON v1 BEGIN
  INSERT INTO t1(a,b,c) VALUES(new.a, new.b, new.c);
END;
INSERT INTO v1 VALUES(100,200,300);
SELECT * FROM t1 WHERE a>=100;
SELECT * FROM tlog1;
--result 100 200 300 nil nil {before: a=100} {after: a=100}

--testcase 201
--oom
CREATE VIEW v2 AS SELECT a+b+c AS p, d+e AS q FROM t1 WHERE b BETWEEN 10 AND 20;
CREATE TRIGGER v2r1 INSTEAD OF INSERT ON v2 BEGIN
  INSERT INTO tlog2 VALUES(NEW.p);
  INSERT INTO tlog2 VALUES(NEW.q);
END;
INSERT INTO v2 VALUES(1,2);
--result

--testcase 202
SELECT * FROM tlog2 ORDER BY rowid;
--result 1 2

--testcase 301
DELETE FROM tlog2;
INSERT INTO tlog2(y, rowid) VALUES('one',111);
INSERT INTO tlog2(oid, y) VALUES(222,'two');
INSERT INTO tlog2(_rowid_, y) VALUES(123,'three');
SELECT rowid, y FROM tlog2 ORDER BY oid;
--result 111 one 123 three 222 two
