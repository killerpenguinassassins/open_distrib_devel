/*
** This module contains tests of WHERE clause handling.
**
** The focus of this module is conversion of OR connected terms
** into an IN operator.
**
** SCRIPT_MODULE_NAME:        where07
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a,b,c,d);
CREATE INDEX t1b ON t1(b);
CREATE INDEX t1c ON t1(c);
CREATE INDEX t1d ON t1(d);
CREATE TABLE t2(x,y);
CREATE INDEX t2y ON t2(y);

INSERT INTO t1 VALUES(1,2,3,4);
INSERT INTO t1 VALUES(5,6,7,8);
INSERT INTO t2 VALUES(1,2);
INSERT INTO t2 VALUES(2,7);
INSERT INTO t2 VALUES(3,4);

SELECT a, x FROM t1, t2 WHERE y=b OR y=c OR y=d ORDER BY a, x
--result 1 1 1 3 5 2
--testcase 101
--oom
SELECT a, x FROM t1, t2 WHERE b=y OR c=y OR d=y ORDER BY a, x
--result 1 1 1 3 5 2
--testcase 102
SELECT a, x FROM t1, t2 WHERE y=b OR y=c ORDER BY a, x
--result 1 1 5 2

--testcase 110
SELECT a, x FROM t1 JOIN t2 ON y=b OR y=c OR y=d OR y=11 ORDER BY a, x;
--result 1 1 1 3 5 2
--testcase 111
SELECT a, x FROM t1 JOIN t2 ON b=y OR c=y OR d=y OR 11=y ORDER BY a, x;
--result 1 1 1 3 5 2
--testcase 112
SELECT a, x FROM t1 JOIN t2 ON d=y OR y=7 ORDER BY a, x;
--result 1 2 1 3 5 2
--testcase 113
SELECT a, x FROM t1 JOIN t2 ON y=d OR x=7 ORDER BY a, x;
--result 1 3


--testcase 120
--oom
SELECT a, x FROM t1, t2 
 WHERE (y=b OR y=c)
   AND a!=99
 ORDER BY a, x
--result 1 1 5 2
--testcase 121
--oom
SELECT a, x FROM t1, t2 
 WHERE (y=b OR y=c)
   AND a!=102 AND a!=103 AND a!=104 AND a!=105 AND a!=106
   AND a!=107 AND a!=108
 ORDER BY a, x
--result 1 1 5 2

/* Do not do the IN optimization due to affinity mismatch */
--testcase 200
CREATE TABLE t3(a, b TEXT, c BLOB, d INT);
CREATE INDEX t3b ON t3(b);
CREATE INDEX t3c ON t3(c);
CREATE INDEX t3d ON t3(d);

INSERT INTO t3 VALUES(1,2,3,4);
INSERT INTO t3 VALUES(5,6,7,8);

SELECT a, x FROM t3, t2 WHERE y=b OR y=c OR y=d ORDER BY a, x
--result 1 3 5 2
--testcase 201
SELECT a, x FROM t3, t2 WHERE b=y OR c=y OR d=y ORDER BY a, x
--result 1 3 5 2
