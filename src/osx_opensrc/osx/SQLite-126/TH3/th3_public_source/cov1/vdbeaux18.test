/*
** This module contains tests of vdbeaux.c source module.
**
** MODULE_NAME:               vdbeaux18
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** Memory allocation failures during EXPLAIN.
**
** The output of EXPLAIN is not defined and it shifts from one release to
** the next.  So we cannot really make persistent tests for the EXPLAIN
** output other than to simply run some EXPLAINs and make sure there are
** no memory leaks or assertion faults.
*/

/*
** Count the number of occurances of "zPattern" text in "zSrc".
*/
static int vdbeaux18_count(const char *zPattern, const char *zSrc){
  int n, i;
  int cnt = 0;
  n = strlen(zPattern);
  for(i=0; zSrc[i]; i++){
    if( zSrc[i]==zPattern[0] && memcmp(&zSrc[i], zPattern, n)==0 ) cnt++;
  }
  return cnt;
}


int vdbeaux18(th3state *p){
  th3dbNew(p, 0, "test.db");
  th3testBegin(p, "100");
  th3dbEval(p, 0, 
    "CREATE TABLE t200a(a,b,c);"
    "CREATE TABLE t200b(x,y,z);"
    "CREATE TABLE t200c(p,q,r);"
    "CREATE TRIGGER t200r1 AFTER INSERT ON t200a BEGIN"
    "  INSERT INTO t200b VALUES(new.a,new.b,new.c);"
    "  INSERT INTO t200b VALUES(new.b,new.c,new.a);"
    "  INSERT INTO t200b VALUES(new.c,new.a,new.b);"
    "END;"
    "CREATE TRIGGER t200r2 AFTER INSERT ON t200b BEGIN"
    "  INSERT INTO t200c VALUES(new.y,new.z,new.x);"
    "END;"
  );
  th3testCheck(p, "");

  th3oomBegin(p, "110");
  while( th3oomNext(p) ){
    int rc;
    th3oomEnable(p, 1);
    rc = th3dbEval(p, 0, "EXPLAIN INSERT INTO t200a VALUES(1,2,3)");
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM || rc==SQLITE_OK );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK );
#if !defined(SQLITE_OMIT_FLOATING_POINT) && !defined(SQLITE_OMIT_TRACE)
      th3oomCheck(p,3, vdbeaux18_count("0 Trace ", p->zResult)==3);
#endif
    }
  }
  th3oomEnd(p);

  return 0;
}
