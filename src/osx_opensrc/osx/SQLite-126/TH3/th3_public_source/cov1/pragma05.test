/*
** Module for testing pragmas.  Tests are written by hand from
** the documentation in pragma.html
**
** SCRIPT_MODULE_NAME:        pragma05
** REQUIRED_PROPERTIES:       CLEARTEXT
** DISALLOWED_PROPERTIES:     AUTOVACUUM JOURNAL_WAL
** MINIMUM_HEAPSIZE:          100000
*/

/***************************************************************************
** PRAGMA incremental_vacuum(N);
*/
--testcase iv-1
PRAGMA page_size=1024;
PRAGMA auto_vacuum=OFF;
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(zeroblob(10000));
DELETE FROM t1;
PRAGMA page_count;
--result 11

--testcase iv-2
PRAGMA incremental_vacuum(5);
PRAGMA page_count;
--result 11


--new test.db
--testcase iv-3
PRAGMA page_size=1024;
PRAGMA auto_vacuum=INCREMENTAL;
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(zeroblob(10000));
DELETE FROM t1;
PRAGMA page_count;
--result 12

--testcase iv-4
PRAGMA incremental_vacuum(5);
PRAGMA page_count;
--result 7

/* A zero argument means do a complete autovacuum */
--testcase iv-5
PRAGMA incremental_vacuum(0);
PRAGMA page_count;
--result 3

/* A negative argument means do a complete autovacuum */
--testcase iv-6
INSERT INTO t1 VALUES(zeroblob(10000)); DELETE FROM t1;
PRAGMA incremental_vacuum(-10);
PRAGMA page_count;
--result 3

/* No argument means do a complete autovacuum */
--testcase iv-7
INSERT INTO t1 VALUES(zeroblob(10000)); DELETE FROM t1;
PRAGMA incremental_vacuum;
PRAGMA page_count;
--result 3

/* Disable the rest of these test cases for SEE */
#ifndef SQLITE_HAS_CODEC

--new-filename test2.db
--testcase iv-11
ATTACH :filename AS aux1;
PRAGMA aux1.page_size=1024;
PRAGMA aux1.auto_vacuum=INCREMENTAL;
CREATE TABLE aux1.t2(y);
INSERT INTO t1 VALUES(zeroblob(10000)); DELETE FROM t1;
INSERT INTO t2 VALUES(zeroblob(10000)); DELETE FROM t2;
PRAGMA main.page_count;
PRAGMA main.page_size;
PRAGMA aux1.page_count;
PRAGMA aux1.page_size;
--result 12 1024 12 1024

--testcase iv-12
PRAGMA aux1.incremental_vacuum(3);
PRAGMA main.page_count;
PRAGMA aux1.page_count;
--result 12 9

--testcase iv-13
PRAGMA main.incremental_vacuum(2);
PRAGMA main.page_count;
PRAGMA aux1.page_count;
--result 10 9

--testcase iv-14
PRAGMA incremental_vacuum(2);
PRAGMA main.page_count;
PRAGMA aux1.page_count;
--result 8 9

--testcase iv-15
PRAGMA incremental_vacuum;
PRAGMA main.page_count;
PRAGMA aux1.page_count;
--result 3 9

--testcase iv-16
PRAGMA aux1.incremental_vacuum;
PRAGMA main.page_count;
PRAGMA aux1.page_count;
--result 3 3

--testcase iv-21
INSERT INTO t1 VALUES(zeroblob(10000)); DELETE FROM t1;
PRAGMA incremental_vacuum(unknown);
PRAGMA page_count;
--result 3

--testcase iv-22
INSERT INTO t1 VALUES(zeroblob(10000)); DELETE FROM t1;
PRAGMA incremental_vacuum(0);
PRAGMA page_count;
--result 3

--testcase iv-23
INSERT INTO t1 VALUES(zeroblob(10000)); DELETE FROM t1;
PRAGMA incremental_vacuum(-4);
PRAGMA page_count;
--result 3

--testcase iv-24
INSERT INTO t1 VALUES(zeroblob(10000)); DELETE FROM t1;
PRAGMA incremental_vacuum(2147483648);
PRAGMA page_count;
--result 3
#endif /* SQLITE_HAS_CEROD */
