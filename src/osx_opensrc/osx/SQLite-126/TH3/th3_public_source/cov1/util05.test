/*
** This module contains tests for the util.c module.
**
** This module arranges to have sqlite3GetVarint32() called to decode
** a variable-length integer that will not fit in a unsigned 32-bit
** integer (either because it is larger than 4294967295 or less than 0.)
** This can only be accomplished by trying to interpret a corrupt database.
**
** SCRIPT_MODULE_NAME:        util05
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/

/* Create a database to work with */
--testcase 100
CREATE TABLE t1(x,y);
INSERT INTO t1 VALUES(123, zeroblob(200));
--result
--checkpoint

/* Verify that the database looks like we think it does */
--testcase 110
SELECT rootpage FROM sqlite_master WHERE name='t1';
--store :t1root
PRAGMA page_size;
--store :pgsz
SELECT :t1root*:pgsz - 208;
--store :ofst
seek :ofst read 16 store :result
--edit test.db
SELECT :result;
--result 814d010401831c7b0000000000000000
--testcase 111
SELECT x FROM t1;
--result 123

/* Corrupt the header so that the hdr size is negative number */
--testcase 120
seek :ofst write ffffffffffffffffff010200 incr-chng
--edit test.db
/* There are two possible answers, depending on whether or not
** SQLITE_ENABLE_OVERSIZE_CELL_CHECK is defined at compile-time */
SELECT x FROM t1;
--store $res1
SELECT $res1 IN (
  'SQLITE_CORRUPT {database disk image is malformed}',
  'SQLITE_TOOBIG {string or blob too big}'
);
--result 1
--testcase 121
seek :ofst write 814d010401831c7b00000000 incr-chng
--edit test.db
SELECT x FROM t1;
--result 123

--testcase 125
seek :ofst write 9080808000010200 incr-chng
--edit test.db
/* There are two possible answers, depending on whether or not
** SQLITE_ENABLE_OVERSIZE_CELL_CHECK is defined at compile-time */
SELECT x FROM t1;
--store $res2
SELECT $res2 IN (
  'SQLITE_CORRUPT {database disk image is malformed}',
  'SQLITE_TOOBIG {string or blob too big}'
) AND $res1=$res2;
--result 1
--testcase 126
seek :ofst write 814d010401831c7b00000000 incr-chng
--edit test.db
SELECT x FROM t1;
--result 123
