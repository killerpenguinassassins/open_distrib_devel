/*
** This module contains tests of resolve.c source module.
**
** SCRIPT_MODULE_NAME:        resolve09
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a);
INSERT INTO t1 VALUES(1);
INSERT INTO t1 VALUES(2);
INSERT INTO t1 SELECT a+2 FROM t1;
INSERT INTO t1 SELECT a+4 FROM t1;
INSERT INTO t1 SELECT a+8 FROM t1;
SELECT a FROM t1 ORDER BY a LIMIT 3 OFFSET 3
--result 4 5 6
--testcase 101
SELECT a FROM t1 ORDER BY a LIMIT 1+2 OFFSET 4-1
--result 4 5 6
--testcase 102
SELECT a FROM t1 ORDER BY a LIMIT (SELECT count(a)/2 FROM t1);
--result 1 2 3 4 5 6 7 8

--testcase 200
CREATE TABLE t2(x,y);
INSERT INTO t2 VALUES(1,5);
INSERT INTO t2 VALUES(2,10);

SELECT a FROM t1 ORDER BY a LIMIT (SELECT y FROM t2 WHERE x=1);
--result 1 2 3 4 5
--testcase 201
SELECT a FROM t1 ORDER BY a LIMIT (SELECT y FROM t2 WHERE x=t1.a);
--result SQLITE_ERROR {no such column: t1.a}
--testcase 202
SELECT a FROM t1 ORDER BY a LIMIT 1 OFFSET (SELECT y FROM t2 WHERE x=t1.a);
--result SQLITE_ERROR {no such column: t1.a}


--testcase 300
SELECT * FROM t1 WHERE a<(SELECT max(y)+a FROM t2) ORDER BY a;
--result 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16

--testcase 310
--oom
DELETE FROM t1 WHERE a>(SELECT max(a)/2 FROM t1 ORDER BY 1);
--result
--testcase 311
SELECT * FROM t1 ORDER BY 1;
--result 1 2 3 4 5 6 7 8


--testcase 400
SELECT * FROM t1 JOIN (SELECT x, y FROM t2 UNION SELECT y, x FROM t2) AS P
 WHERE t1.a=P.x
 ORDER BY 1, 2;
--result 1 1 5 2 2 10 5 5 1
--testcase 401
SELECT * FROM t1 JOIN (SELECT x, y FROM t2 UNION SELECT y, z FROM t2) AS P
 WHERE t1.a=P.x
 ORDER BY 1, 2;
--result SQLITE_ERROR {no such column: z}
--testcase 400
--oom
SELECT * FROM t1 JOIN 
    (SELECT x, y FROM t2 UNION SELECT y, x FROM t2 ORDER BY 1 LIMIT 5) AS P
 WHERE t1.a=P.x
 ORDER BY 1, 2;
--result 1 1 5 2 2 10 5 5 1

--testcase 500
SELECT th3_load_normal_extension();
SELECT th3_sqlite3_limit('SQLITE_LIMIT_EXPR_DEPTH', 5);
--run 0
SELECT * FROM t1 JOIN (SELECT x, y FROM t2 UNION SELECT y, x FROM t2) AS P
 WHERE t1.a=P.x
 ORDER BY t1.a
--result 1 1 5 2 2 10 5 5 1
--testcase 501
SELECT t1.a+0+y-y, P.y
  FROM t1
  JOIN (SELECT x AS x, y FROM t2 UNION SELECT y, x FROM t2) AS P
 WHERE t1.a=P.x
 ORDER BY t1.a
--result 1 5 2 10 5 1
--testcase 502
SELECT t1.a+0+y-y-0, P.y
  FROM t1
  JOIN (SELECT x, y FROM t2 UNION SELECT y, x FROM t2) AS P
 WHERE t1.a=P.x
 ORDER BY t1.a
--result SQLITE_ERROR {Expression tree is too large (maximum depth 5)}
