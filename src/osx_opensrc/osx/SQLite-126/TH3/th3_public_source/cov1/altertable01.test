/*
** Module for testing the ALTER TABLE statement.  Tests are written by hand
** from the documentation in lang_altertable.html
**
** MIXED_MODULE_NAME:         altertable01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/*
** Overload some of the SQL functions used internally by ALTER TABLE with
** an application-defined function that always fails.  This will verify
** that ALTER TABLE always uses the built-in form of the function rather
** that the application-defined overload.
*/
void altertable01_fail(sqlite3_context *p, int argc, sqlite3_value **argv){
  sqlite3_result_error(p, "bad function", -1);
}
void altertable01_overload_functions(th3state *p, int iDb, char *zArg){
  sqlite3 *db = th3dbPointer(p, iDb);
  sqlite3_create_function(db, "substr", -1, SQLITE_UTF8, 0,
                          altertable01_fail, 0, 0);
  sqlite3_create_function(db, "like", -1, SQLITE_UTF8, 0,
                          altertable01_fail, 0, 0);
  sqlite3_create_function(db, "sqlite_rename_table", -1, SQLITE_UTF8, 0,
                          altertable01_fail, 0, 0);
}
--call altertable01_overload_functions


--testcase 1a
CREATE TABLE n1(x);
SELECT name FROM sqlite_master;
--result n1

--testcase 1b
--oom
ALTER TABLE n1 RENAME TO n2;
SELECT name FROM sqlite_master;
--result n2

--testcase 2
BEGIN;
ALTER TABLE n2 RENAME TO n1;
SELECT name FROM sqlite_master;
ROLLBACK;
SELECT name FROM sqlite_master;
--result n1 n2

--testcase 3
--oom
ALTER /* comment */ TABLE main.n2 RENAME TO n3;
ALTER TABLE /* comment */ main.n3 RENAME TO n4;
ALTER TABLE main.n4 RENAME TO n1;
INSERT INTO n1 VALUES(1);
SELECT * FROM n1;
--result 1

--testcase 4
CREATE TABLE n2 AS SELECT * FROM n1;
ALTER TABLE main.n2 RENAME TO n3;
SELECT * FROM n3;
SELECT name FROM sqlite_master ORDER BY 1;
--result 1 n1 n3

--testcase 5
ALTER TABLE n6 RENAME TO n7;
--result SQLITE_ERROR {no such table: n6}

--testcase 6
ALTER TABLE main.n6 RENAME TO n7;
--result SQLITE_ERROR {no such table: main.n6}

--testcase 7
--oom
ALTER TABLE aux.n6 RENAME TO n7;
--result SQLITE_ERROR {no such table: aux.n6}

--testcase 8
ALTER TABLE n1 RENAME TO n1;
--result SQLITE_ERROR {there is already another table or index with this name: n1}

--testcase 9
--oom
ALTER TABLE main.n1 RENAME TO n1;
--result SQLITE_ERROR {there is already another table or index with this name: n1}

--testcase 10
ALTER TABLE main.n1 RENAME TO n3;
--result SQLITE_ERROR {there is already another table or index with this name: n3}

--testcase 11
CREATE INDEX n2 ON n1(x);
ALTER TABLE n1 RENAME TO n2;
--result SQLITE_ERROR {there is already another table or index with this name: n2}

--testcase 12
ALTER TABLE n2 RENAME TO n9;
--result SQLITE_ERROR {no such table: n2}

--testcase 13
CREATE VIEW v1 AS SELECT x+1 FROM n1;
SELECT * FROM v1;
--result 2

--testcase 14
--oom
ALTER TABLE v1 RENAME TO v2;
--result SQLITE_ERROR {view v1 may not be altered}

--testcase 15
ALTER TABLE n1 RENAME TO v1;
--result SQLITE_ERROR {there is already another table or index with this name: v1}

--testcase 16
CREATE TEMP TABLE t1(a,b);
INSERT INTO t1 VALUES('x','y');
ALTER TABLE t1 RENAME TO t1b;
SELECT * FROM t1b;
--result x y

--testcase 17
CREATE TEMPORARY TABLE t2 AS SELECT x, x+1 FROM n1;
ALTER TABLE t2 RENAME TO t2b;
SELECT * FROM t2b;
--result 1 2

--testcase 18
CREATE TABLE temp.t3(p,q);
INSERT INTO t3 SELECT * FROM t2b;
ALTER TABLE t3 RENAME TO t3b;
SELECT * FROM t3b;
--result 1 2

--testcase 19
CREATE TABLE temp.t4 AS SELECT x, x+1 FROM n1;
ALTER TABLE t4 RENAME TO t4b;
SELECT * FROM t4b;
--result 1 2
