/*
** Test module for pager.c.
**
** Read-only files.
**
** SCRIPT_MODULE_NAME:        pager11
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     JOURNAL_OFF JOURNAL_MEMORY MEMDB ALT_PCACHE
** DISALLOWED_PROPERTIES:     JOURNAL_WAL
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a,b);
INSERT INTO t1 VALUES(1,2);
INSERT INTO t1 VALUES(3,4);
SELECT * FROM t1 ORDER BY a;
--result 1 2 3 4

--readonly test*
--open test.db
--testcase 110
SELECT * FROM t1 ORDER BY a;
--result 1 2 3 4

--testcase 120
INSERT INTO t1 VALUES(5,6);
--result SQLITE_READONLY {attempt to write a readonly database}

--testcase 130
CREATE TEMP TABLE t2(x,y);
INSERT INTO t2 VALUES(5,6);
SELECT * FROM t2;
--result 5 6

--readonly
--open test.db
--testcase 200
PRAGMA cache_size=2;
INSERT INTO t1 VALUES(5, zeroblob(100000));
BEGIN;
UPDATE t1 SET b=th3randomBlob(90000) WHERE a=5;
--result

--snapshot
--testcase 210
COMMIT;
--result

--revert
--readonly test*
--testcase 220
--open test.db
SELECT a, length(b) FROM t1; PRAGMA wal_checkpoint;
--result SQLITE_CANTOPEN {unable to open database file}
--cklog SQLITE_CANTOPEN {cannot open file at line # of [SOURCEID]} SQLITE_CANTOPEN {unable to open database file} SQLITE_CANTOPEN {cannot open file at line # of [SOURCEID]} SQLITE_CANTOPEN {unable to open database file}

/* Attempt to open a file with a name that is too long */
--readonly
--new test.db
--testcase 300
ATTACH 'a123456789b123456789c123456789d12' AS aux1;
--result SQLITE_CANTOPEN {unable to open database: a123456789b123456789c123456789d12}
--testcase 301
ATTACH 'a123456789b123456789c123456789d1' AS aux1;
--result
