/*
** This module contains tests of the sqlite3_initialize() interface for
** structural test coverage.
**
** MODULE_NAME:               main00
** REQUIRED_PROPERTIES:       TEST_VFS ALT_PCACHE TEST_MUTEX INITIALIZE_OK
** DISALLOWED_PROPERTIES:     THREADS NO_OOM
** MINIMUM_HEAPSIZE:          100000
*/
int main00(th3state *p){
  int rc;
  sqlite3_vfs *pVfs;
  sqlite3_vfs noopVfs;
  sqlite3_mutex *pMutex;

  memcpy(&noopVfs, sqlite3_vfs_find(0), sizeof(noopVfs));
  noopVfs.zName = "main00_vfs";

  /* It is safe to call sqlite3_shutdown() multiple times in a row, as
  ** long as it is called from the same thread. 
  */
  th3testBegin(p, "1");
  sqlite3_shutdown();
  sqlite3_shutdown();
  sqlite3_shutdown();
  rc = sqlite3_initialize();
  th3testCheckInt(p, SQLITE_OK, rc);

  /* Verify that initialization does not occur if the mutex initializer
  ** fails.
  */
  th3testBegin(p, "101");
  sqlite3_shutdown();
  th3mutexGlobal.mutexInitReturn = SQLITE_INTERNAL;
  rc = sqlite3_initialize();
  th3testCheckInt(p, SQLITE_INTERNAL, rc);
#ifndef SQLITE_OMIT_AUTOINIT
  th3testBegin(p, "102");
  pVfs = sqlite3_vfs_find(0);
  th3testCheckInt(p, 1, pVfs==0 );
  th3testBegin(p, "103");
  pMutex = sqlite3_mutex_alloc(SQLITE_MUTEX_FAST);
  th3testCheckInt(p, 1, pMutex==0);
#endif
  th3mutexGlobal.mutexInitReturn = SQLITE_OK;

  /* Verify that initialization does not occur if the malloc initializer
  ** fails.
  */
  th3testBegin(p, "201");
  sqlite3_shutdown();
  th3oom.initReturn = SQLITE_INTERNAL;
  rc = sqlite3_initialize();
  th3testCheckInt(p, SQLITE_INTERNAL, rc);
#ifndef SQLITE_OMIT_AUTOINIT
  th3testBegin(p, "202");
  rc = sqlite3_vfs_register(&noopVfs, 0);
  th3testCheckInt(p, SQLITE_INTERNAL, rc);
#endif
  th3oom.initReturn = SQLITE_OK;

  /* Verify that initialization does not occur if the pcache initializer
  ** fails.
  */
  th3testBegin(p, "301");
  sqlite3_shutdown();
  th3testpcacheGlobal.initReturn = SQLITE_INTERNAL;
  rc = sqlite3_initialize();
  th3testCheckInt(p, SQLITE_INTERNAL, rc);
  th3testpcacheGlobal.initReturn = SQLITE_OK;

  /* Test OOM handling during initialization. */
  th3oomBegin(p, "400");
  while( th3oomNext(p) ){
    int rc;
    sqlite3_shutdown();
    th3oomEnable(p, 1);
    rc = sqlite3_initialize();
    th3oomEnable(p, 0);
    sqlite3_initialize();
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK );
    }
  }
  th3oomEnd(p);

  /* Arrange for sqlite3_initialize() to be called recursively. */
  th3testBegin(p, "500");
  sqlite3_shutdown();
  th3mutexGlobal.callInitialize = 1;
  rc = sqlite3_initialize();
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "501");
  th3testCheckInt(p, 0, th3mutexGlobal.callInitialize);

  return 0;
}
