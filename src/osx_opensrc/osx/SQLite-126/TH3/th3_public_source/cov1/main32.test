/*
** Create a no-op VFS that has a very small sqlite3_file size - smaller than
** the one used by the MemJournal.  This provides coverage for a max() 
** function at the top of sqlite3PagerOpen().
**
** MODULE_NAME:               main32
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
*/

/* An implementation of the xOpen VFS method that always fails with
** SQLITE_FULL.
*/
static int main32_xOpen(
  sqlite3_vfs *pVfs,        /* The VFS */
  const char *zFilename,    /* Name of file to open */
  sqlite3_file *pSFile,     /* Fill in this file descriptor */
  int flags,                /* Mode flags */
  int *pOutFlags            /* Return flags set by this method */
){
  pSFile->pMethods = 0;
  return SQLITE_FULL;
}


int main32(th3state *p){
  int rc;
  sqlite3 *db;
  sqlite3_vfs *pDefaultVfs;
  sqlite3_vfs noopVfs;

  th3testBegin(p, "100");
  pDefaultVfs = sqlite3_vfs_find(0);
  memcpy(&noopVfs, pDefaultVfs, sizeof(noopVfs));
  noopVfs.zName = "main32_vfs";
  noopVfs.xOpen = main32_xOpen;
  noopVfs.szOsFile = sizeof(sqlite3_file);
  sqlite3_vfs_register(&noopVfs, 0);
  rc = sqlite3_open_v2("whatever.db", &db,
              SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE,
              "main32_vfs");
  sqlite3_close(db);
  th3testCheckInt(p, SQLITE_FULL, rc);

  sqlite3_vfs_unregister(&noopVfs);

  return 0;
}
