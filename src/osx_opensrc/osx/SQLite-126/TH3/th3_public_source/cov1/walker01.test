/*
** This module contains tests of resolve.c source module.
**
** SCRIPT_MODULE_NAME:        walker01
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 99
/* Ticket [2d401a94287b5] */
CREATE TABLE t1(x, y DEFAULT(datetime_x('now')));
INSERT INTO t1(x) VALUES(1);
SELECT x, y FROM t1;
--result SQLITE_ERROR {unknown function: datetime_x()}

#if !defined(SQLITE_OMIT_FLOATING_POINT) && !defined(SQLITE_OMIT_DATETIME)
--date 1245091771
--testcase 100
DROP TABLE IF EXISTS t1;
CREATE TABLE t1(x, y DEFAULT(datetime('now')));
INSERT INTO t1(x) VALUES(1);
SELECT x, y FROM t1;
--result 1 {2009-06-15 18:49:31}

--testcase 101
DROP TABLE t1;
CREATE TABLE t1(x, y DEFAULT(datetime('n'||'ow')));
INSERT INTO t1(x) VALUES(2);
SELECT x, y FROM t1;
--result 2 {2009-06-15 18:49:31}
#endif

--testcase 110
DROP TABLE IF EXISTS t1;
CREATE TABLE t1(x, y DEFAULT(typeof('abc')));
INSERT INTO t1(x) VALUES(3);
SELECT x, y FROM t1;
--result 3 text

--testcase 111
DROP TABLE IF EXISTS t1;
CREATE TABLE t1(x, y DEFAULT(typeof('abc'||'xyz')));
INSERT INTO t1(x) VALUES(3);
SELECT x, y FROM t1;
--result 3 text


--testcase 200
CREATE TABLE t2(x, y, CHECK( x IN (1,2,3) OR y>10 ));
INSERT INTO t2 VALUES(1,2);
INSERT INTO t2 VALUES(4,11);
--result
--testcase 201
INSERT INTO t2 VALUES(5,6);
--result SQLITE_CONSTRAINT {constraint failed}

--testcase 210
DROP TABLE t2;
CREATE TABLE t2(x, y, CHECK( x IN (1,2,z) OR y>10 ));
--result SQLITE_ERROR {no such column: z}

--testcase 300
CREATE TABLE t3(a, b);
INSERT INTO t3 VALUES(2,'two');
INSERT INTO t3 VALUES(3,'three');
DELETE FROM t1;
INSERT INTO t1 VALUES(1,'uno');
INSERT INTO t1 VALUES(2,'dos');
INSERT INTO t1 VALUES(3,'tres');
SELECT x, y, (SELECT b FROM t3 WHERE a=x) FROM t1 ORDER BY x;
--result 1 uno nil 2 dos two 3 tres three
--testcase 301
SELECT x, y, (SELECT b FROM t3 INDEXED BY t3i1 WHERE a=x) FROM t1 ORDER BY x;
--result SQLITE_ERROR {no such index: t3i1}
--testcase 302
SELECT x, y FROM t1
 WHERE EXISTS(SELECT b FROM t3 INDEXED BY t3i1 WHERE a=x);
--result SQLITE_ERROR {no such index: t3i1}
--testcase 303
SELECT x, y FROM t1
 GROUP BY (SELECT b FROM t3 INDEXED BY t3i1 WHERE a=x);
--result SQLITE_ERROR {no such index: t3i1}
--testcase 304
SELECT x, y FROM t1
 GROUP BY x HAVING EXISTS(SELECT b FROM t3 INDEXED BY t3i1 WHERE a=x);
--result SQLITE_ERROR {no such index: t3i1}
--testcase 305
SELECT x, y FROM t1
 ORDER BY (SELECT b FROM t3 INDEXED BY t3i1 WHERE a=x);
--result SQLITE_ERROR {no such index: t3i1}
--testcase 306
SELECT x, y FROM t1
 LIMIT (SELECT b FROM t3 INDEXED BY t3i1 WHERE a=x);
--result SQLITE_ERROR {no such index: t3i1}
--testcase 307
SELECT x, y FROM t1
 LIMIT 10 OFFSET (SELECT b FROM t3 INDEXED BY t3i1 WHERE a=x);
--result SQLITE_ERROR {no such index: t3i1}
