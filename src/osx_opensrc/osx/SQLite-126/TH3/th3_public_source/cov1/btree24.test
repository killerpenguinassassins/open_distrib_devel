/*
** Test module for btree.c.
**
** incremental blob I/O.
**
** MODULE_NAME:               btree24
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     NO_OOM
** MINIMUM_HEAPSIZE:          100000
*/

/* Test buffer size */
#define BTREE24_SZ 3000

int btree24(th3state *p){
  sqlite3 *db;
  sqlite3_blob *pBlob = 0;
  int i;
  int rc;
  unsigned char *pBuf;

  pBuf = th3malloc(p, BTREE24_SZ);

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  th3dbEval(p, 0,
    "CREATE TABLE t1(x INTEGER PRIMARY KEY, y BLOB, z BLOB);"
    "INSERT INTO t1 VALUES(1, zeroblob(5000), zeroblob(25000));"
    "INSERT INTO t1 VALUES(234, null, zeroblob(10));"
    "SELECT count(*) FROM t1;"
  );
  th3testCheck(p, "2");

  th3oomBegin(p, "110");
  while( th3oomNext(p) ){
    sqlite3_blob_close(pBlob);
    rc = sqlite3_blob_open(db, "main", "t1", "z", 1, 1, &pBlob);
    assert( rc==SQLITE_OK );
    th3oomEnable(p, 1);
    rc = sqlite3_blob_read(pBlob, pBuf, BTREE24_SZ, 25000-BTREE24_SZ);
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM);
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK);
      for(i=0; i<BTREE24_SZ && pBuf[i]==0; i++){}
      th3oomCheck(p, 3, i==BTREE24_SZ);
    }
  }
  th3oomEnd(p);

  th3testBegin(p, "120");
  for(i=0; i<256; i++) pBuf[i] = i;
  rc = sqlite3_blob_write(pBlob, pBuf, 256, 25000-256);
  th3testCheckInt(p, SQLITE_OK, rc);

  th3testBegin(p, "130");
  rc = sqlite3_blob_read(pBlob, pBuf, 256, 25000-256);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "131");
  for(i=0; i<256 && pBuf[i]==i; i++){}
  th3testCheckInt(p, 256, i);

  th3testBegin(p, "140");
  th3dbEval(p, 0, 
     "INSERT INTO t1 VALUES(0, zeroblob(300), null);"
     "INSERT INTO t1 VALUES(2, zeroblob(300), null);"
     "INSERT INTO t1 VALUES(3, zeroblob(300), null);"
     "INSERT INTO t1 VALUES(4, zeroblob(300), null);"
     "SELECT count(*) FROM t1;"
  );
  th3testCheck(p, "6");

  th3testBegin(p, "150");
  rc = sqlite3_blob_read(pBlob, pBuf, 256, 25000-256);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "151");
  for(i=0; i<256 && pBuf[i]==i; i++){}
  th3testCheckInt(p, 256, i);

  th3oomBegin(p, "160");
  while( th3oomNext(p) ){
    sqlite3_blob_close(pBlob);
    rc = sqlite3_blob_open(db, "main", "t1", "z", 234, 1, &pBlob);
    assert( rc==SQLITE_OK );
    th3oomEnable(p, 1);
    rc = sqlite3_blob_read(pBlob, pBuf, 10, 0);
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM);
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK);
      for(i=0; i<10 && pBuf[i]==0; i++){}
      th3oomCheck(p, 3, i==10);
    }
  }
  th3oomEnd(p);

  th3testBegin(p, "170");
  for(i=0; i<10; i++) pBuf[i] = i;
  rc = sqlite3_blob_write(pBlob, pBuf, 10, 0);
  th3testCheckInt(p, SQLITE_OK, rc);

  th3testBegin(p, "180");
  rc = sqlite3_blob_read(pBlob, pBuf, 10, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "181");
  for(i=0; i<10 && pBuf[i]==i; i++){}
  th3testCheckInt(p, 10, i);
  sqlite3_blob_close(pBlob);  


  th3testBegin(p, "200");
  th3dbEval(p, 0,
    "SELECT substr(hex(z), -10) FROM t1 WHERE x=1"
  );
  th3testCheck(p, "FBFCFDFEFF");


  th3testBegin(p, "300");
  th3dbEval(p, 0,
    "CREATE TABLE t3(x BLOB);"
    "INSERT INTO t3(rowid,x) VALUES(8, x'30313233343536373839');"
  );
  rc = sqlite3_blob_open(db, "main", "t3", "x", 8, 0, &pBlob);
  assert( rc==SQLITE_OK );
  assert( pBlob!=0 );
  rc = sqlite3_blob_read(pBlob, pBuf, 5, 0);
  assert( rc==SQLITE_OK );
  pBuf[5] = 0;
  th3testAppendResult(p, (char*)pBuf);
  th3testCheck(p, "01234");

  th3testBegin(p, "310");
  th3dbEval(p, 0, "DELETE FROM t3;");
  rc = sqlite3_blob_read(pBlob, pBuf, 5, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  sqlite3_blob_close(pBlob);

  th3free(p, pBuf);
  return 0;
}
