/*
** Module for testing secure_delete pragma. 
**
** SCRIPT_MODULE_NAME:        pragma20
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB UTF16LE UTF16BE
** MINIMUM_HEAPSIZE:          100000
*/
/* EVIDENCE-OF: R-33207-46321 PRAGMA secure_delete; PRAGMA
** database.secure_delete; PRAGMA secure_delete = boolean PRAGMA
** database.secure_delete = boolean Query or change the secure-delete
** setting.
*/
--testcase 100
PRAGMA secure_delete=ON;
PRAGMA secure_delete;
--result 1 1
--testcase 101
PRAGMA secure_delete=OFF;
PRAGMA secure_delete;
--result 0 0
--testcase 102
PRAGMA main.secure_delete=ON;
PRAGMA main.secure_delete;
--result 1 1
--testcase 103
PRAGMA main.secure_delete=OFF;
PRAGMA main.secure_delete;
--result 0 0
--testcase 104
PRAGMA temp.secure_delete=ON;
PRAGMA temp.secure_delete;
--result 1 1
--testcase 105
PRAGMA temp.secure_delete=OFF;
PRAGMA temp.secure_delete;
--result 0 0

/* EVIDENCE-OF: R-11848-39326 When secure-delete on, SQLite overwrites
** deleted content with zeros.
*/
--testcase 200
PRAGMA secure_delete=OFF;
CREATE TABLE t1(a);
INSERT INTO t1 VALUES('abcdefg');
DELETE FROM t1;
--result 0
--checkpoint
--testcase 201
search 61626364656667 store $loc
--edit test.db
SELECT ($loc+0)>512;
--result 1

--new test.db
--testcase 210
PRAGMA secure_delete=ON;
CREATE TABLE t1(a);
INSERT INTO t1 VALUES('abcdefg');
DELETE FROM t1;
--result 1
--checkpoint
--testcase 211
search 61626364656667
--edit test.db
--result -1

--autocheckpoint 1
--testcase 220
PRAGMA page_size;
--store $pgsz
DROP TABLE t1;
CREATE TABLE t1(a INTEGER PRIMARY KEY, b, c);
INSERT INTO t1 VALUES(1, null, zeroblob($pgsz/2));
INSERT INTO t1 VALUES(2, zeroblob($pgsz), 'abcdefg');
INSERT INTO t1 VALUES(3, 'abcdefg', zeroblob($pgsz/2));
CREATE INDEX i1b ON t1(b);
CREATE INDEX i1c ON t1(c);
--result
--testcase 221
search 616263646566 store $loc
--edit test.db
SELECT ($loc+0)>512
--result 1
--testcase 222
DELETE FROM t1;
--result
--if NOT $property_NO_TRUNCATE
--testcase 223
search 616263646566
--edit test.db
--result -1
--endif

/* EVIDENCE-OF: R-28054-53927 The default setting is determined by the
** SQLITE_SECURE_DELETE compile-time option.
*/
--raw-new test.db
#ifdef SQLITE_SECURE_DELETE
--testcase 300sd
PRAGMA secure_delete;
--result 1
#else
--testcase 300nsd
PRAGMA secure_delete;
--result 0
#endif

/* EVIDENCE-OF: R-43448-12354 When there are attached databases and no
** database is specified in the pragma, all databases have their
** secure-delete setting altered.
*/
--new test.db
--new-filename aux2.db
--testcase 300
PRAGMA secure_delete=OFF;
--run 0
ATTACH :filename AS aux2;
PRAGMA aux2.secure_delete;
PRAGMA secure_delete=ON;
PRAGMA aux2.secure_delete;
--result 0 1 1
--testcase 301
PRAGMA secure_delete=OFF;
PRAGMA aux2.secure_delete;
--result 0 0
--testcase 302
PRAGMA main.secure_delete=ON;
PRAGMA aux2.secure_delete;
--result 1 0

/* EVIDENCE-OF: R-36620-34941 The secure-delete setting for newly
** attached databases is the setting of the main database at the time the
** ATTACH command is evaluated.
*/
--new-filename aux3.db
--testcase 400
ATTACH :filename AS aux3;
PRAGMA main.secure_delete;
PRAGMA aux2.secure_delete;
PRAGMA aux3.secure_delete;
--result 1 0 1
--testcase 401
DETACH aux3;
PRAGMA main.secure_delete=OFF;
PRAGMA aux2.secure_delete=ON;
ATTACH :filename AS aux3;
PRAGMA main.secure_delete;
PRAGMA aux2.secure_delete;
PRAGMA aux3.secure_delete;
--result 0 1 0 1 0
