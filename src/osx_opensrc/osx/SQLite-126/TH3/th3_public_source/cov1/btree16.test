/*
** Test module for btree.c.  allocateSpace()
**
** SCRIPT_MODULE_NAME:        btree16
** REQUIRED_PROPERTIES:       CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** This test is designed for a 1024 byte page.  The test work for
** other page sizes - but for other page sizes it does not exercise
** the paths through allocateSpace() that it is intended to exercise.
**
** The comments assume a 1024 byte page and are not necessarily correct
** otherwise.
*/
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY,y,z);
INSERT INTO t1 VALUES(0, 9, zeroblob(57));  -- should be a 64 byte cell.
INSERT INTO t1 VALUES(1, 9, zeroblob(57));
INSERT INTO t1 SELECT x+2, 9, zeroblob(57) FROM t1;
INSERT INTO t1 SELECT x+4, 9, zeroblob(57) FROM t1;
INSERT INTO t1 SELECT x+8, 9, zeroblob(57) FROM t1 WHERE x+8<=14;
UPDATE t1 SET y=NULL WHERE x=0;  -- convert to 63-bit.  Leave 1-byte fragment.
INSERT INTO t1 VALUES(99, null, zeroblob(19));
SELECT count(*), min(x), max(x) FROM t1;
PRAGMA integrity_check;
--result 16 0 99 ok

/* Verify that the page did not split when the page size is 1024 or larger */
--testcase 110
PRAGMA page_size;
--store $pgsz
PRAGMA auto_vacuum;
--store $autovac
PRAGMA page_count
--store $pgcnt
SELECT CASE WHEN $pgsz+0>=1024 THEN $pgcnt-($autovac+0>0) ELSE 2 END;
--result 2

--testcase 120
CREATE TABLE IF NOT EXISTS t1(x INTEGER PRIMARY KEY,y,z);
DELETE FROM t1;
INSERT INTO t1 VALUES(0, 9, zeroblob(57));  -- should be a 64 byte cell.
INSERT INTO t1 VALUES(1, 9, zeroblob(57));
INSERT INTO t1 SELECT x+2, 9, zeroblob(57) FROM t1;
INSERT INTO t1 SELECT x+4, 9, zeroblob(57) FROM t1;
INSERT INTO t1 SELECT x+8, 9, zeroblob(57) FROM t1 WHERE x+8<=13;
INSERT INTO t1 VALUES(14, NULL, zeroblob(57));
UPDATE t1 SET y=NULL, z=NULL WHERE x<14;
INSERT INTO t1 SELECT x+100, NULL, NULL FROM t1;
SELECT count(*), min(x), max(x) FROM t1;
PRAGMA integrity_check;
--result 30 0 114 ok

--testcase 130
CREATE TABLE IF NOT EXISTS t1(x INTEGER PRIMARY KEY,y,z);
DELETE FROM t1;
INSERT INTO t1 VALUES(0, 9, zeroblob(57));  -- should be a 64 byte cell.
INSERT INTO t1 VALUES(1, 9, zeroblob(57));
INSERT INTO t1 SELECT x+2, 9, zeroblob(57) FROM t1;
INSERT INTO t1 SELECT x+4, 9, zeroblob(57) FROM t1;
INSERT INTO t1 SELECT x+8, 9, zeroblob(57) FROM t1 WHERE x+8<=13;
INSERT INTO t1 VALUES(14, 9, zeroblob(57));
UPDATE t1 SET y=NULL, z=NULL WHERE x<14;
INSERT INTO t1 SELECT x+100, NULL, NULL FROM t1;
SELECT count(*), min(x), max(x) FROM t1;
PRAGMA integrity_check;
--result 30 0 114 ok

--testcase 140
CREATE TABLE IF NOT EXISTS t1(x INTEGER PRIMARY KEY,y,z);
DELETE FROM t1;
INSERT INTO t1 VALUES(0, 9, zeroblob(57));  -- should be a 64 byte cell.
INSERT INTO t1 VALUES(1, 9, zeroblob(57));
INSERT INTO t1 SELECT x+2, 9, zeroblob(57) FROM t1;
INSERT INTO t1 SELECT x+4, 9, zeroblob(57) FROM t1;
INSERT INTO t1 SELECT x+8, 9, zeroblob(57) FROM t1 WHERE x+8<=13;
INSERT INTO t1 VALUES(14, 9, zeroblob(57));
UPDATE t1 SET y=NULL WHERE x<14;          /* 14 fragments */
UPDATE t1 SET z=zeroblob(56) WHERE x<14;  /* 28 fragments */
UPDATE t1 SET z=zeroblob(55) WHERE x<14;  /* 42 fragments */
UPDATE t1 SET z=zeroblob(54) WHERE x<14;  /* 56 fragments */
UPDATE t1 SET z=zeroblob(53) WHERE x<14;  /* Forced defragmentation */
SELECT count(*), min(x), max(x) FROM t1;
PRAGMA integrity_check;
--result 15 0 14 ok
