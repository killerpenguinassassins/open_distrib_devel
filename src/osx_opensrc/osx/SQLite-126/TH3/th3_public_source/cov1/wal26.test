/*
** Test the operation of WAL on large files - files larger than 4GiB
**
** SCRIPT_MODULE_NAME:        wal26
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB SHARED_CACHE AUTOVACUUM
** MINIMUM_HEAPSIZE:          100000
*/

/* File that are exactly 4GiB in size */
--testcase 100
CREATE TABLE t1(x);
PRAGMA journal_mode=DELETE;
INSERT INTO t1 VALUES(1);
SELECT x FROM t1;
--result delete 1
--testcase 110
seek 28 write32 0
extend 4294967296
incr-chng
--edit test.db
PRAGMA journal_mode=WAL;
CREATE TABLE t2(x);
INSERT INTO t2 VALUES(2);
SELECT x FROM t2;
--result wal 2
--testcase 111
PRAGMA wal_checkpoint;
SELECT * FROM t1, t2;
--glob # # # 1 2

/* File slightly smaller than 4GiB */
--new test.db
--testcase 200
CREATE TABLE t1(x);
PRAGMA journal_mode=DELETE;
INSERT INTO t1 VALUES(1);
SELECT x FROM t1;
--result delete 1
--store $pgsz PRAGMA page_size;
--store $size SELECT 4294967296 - max($sector_size,$pgsz+0)
--testcase 210
seek 28 write32 0
extend $size
incr-chng
--edit test.db
PRAGMA journal_mode=WAL;
CREATE TABLE t2(x);
INSERT INTO t2 VALUES(2);
SELECT x FROM t2;
--result wal 2
--testcase 211
PRAGMA wal_checkpoint;
SELECT * FROM t1, t2;
--glob # # # 1 2

/* File slightly larger than 4GiB */
--new test.db
--testcase 300
CREATE TABLE t1(x);
PRAGMA journal_mode=DELETE;
INSERT INTO t1 VALUES(1);
SELECT x FROM t1;
--result delete 1
--store $pgsz PRAGMA page_size;
--store $size SELECT 4294967296 + max($sector_size,$pgsz+0)
--testcase 310
seek 28 write32 0
extend $size
incr-chng
--edit test.db
PRAGMA journal_mode=WAL;
CREATE TABLE t2(x);
INSERT INTO t2 VALUES(2);
SELECT x FROM t2;
--result wal 2
--testcase 311
PRAGMA wal_checkpoint;
SELECT * FROM t1, t2;
--glob # # # 1 2
