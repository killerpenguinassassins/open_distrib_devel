/*
** Tests showing recovery from a corrupt WAL file.
**
** SCRIPT_MODULE_NAME:        wal10
** REQUIRED_PROPERTIES:       JOURNAL_WAL TEST_VFS
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/

/* Create a database holding some content and a WAL file holding
** a few changes to that content.
*/
--testcase 100
--ioerr
--open test.db
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(1);
INSERT INTO t1 VALUES(2);
INSERT INTO t1 VALUES(zeroblob(25000));
CREATE TABLE t2(y);
INSERT INTO t2 VALUES(1);
PRAGMA wal_checkpoint;
UPDATE t2 SET y=y+100;
INSERT INTO t2 VALUES(200);
SELECT y FROM t2 ORDER BY y;
--glob # # # 101 200
--snapshot
--store-raw $pgsz PRAGMA page_size

/* Verify correct reading of the snapshot
*/
--revert
--raw-open test.db
--testcase 200
SELECT y FROM t2 ORDER BY y;
--result 101 200

/* Modify the header of the WAL file.  This causes the WAL file to be
** ignored as corrupt and the last two transactions will rollback.
*/
--revert
--raw-open test.db
--testcase 300
seek 0 write 38
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1
--revert
--raw-open test.db
--testcase 310
seek 3 xor 01
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1

/* Corruptions to the database page size in the WAL header.
*/
--revert
--raw-open test.db
--testcase 400
seek 8 write32 2049
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1
--revert
--raw-open test.db
--testcase 410
seek 8 write32 256
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1
--revert
--raw-open test.db
--testcase 420
seek 8 write32 131072
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1

/* Differing page sizes on database and in WAL
*/
--store-raw $new_pgsz SELECT $pgsz/2
#ifndef SQLITE_HAS_CODEC
--revert
--open :memory:
--testcase 450
seek 16 write16 $new_pgsz incr-chng
--edit test.db
--raw-open test.db
SELECT y FROM t2 ORDER BY y;
--result SQLITE_CORRUPT {database disk image is malformed}
#endif

/* Corruptions of checksums and salt values.
*/
--revert
--raw-open test.db
--testcase 500
seek 40 add8 1
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1
--revert
--raw-open test.db
--testcase 510
seek 44 add8 1
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1
--revert
--raw-open test.db
--testcase 520
seek 48 add8 1
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1
--revert
--raw-open test.db
--testcase 530
seek 52 add8 1
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1

/* Change page numbers to zero.
*/
--revert
--raw-open test.db
--testcase 600
seek 32 write32 0
--edit test.db-wal
SELECT y FROM t2 ORDER BY y;
--result 1

/* Change the WAL header checksum values.  Test 700 runs without
** messing with the header, just to show that we get a different answer
** if the WAL is intact.  Tests 700+x for x>0 modify the header which
** causes the WAL to be ignored.
*/
--revert
--testcase 700
--raw-open test.db
SELECT y FROM t2 ORDER BY y;
--result 101 200
--revert
--open :memory:
--testcase 701
seek 24 xor 01
--edit test.db-wal
--raw-open test.db
SELECT y FROM t2 ORDER BY y;
--result 1
--revert
--open :memory:
--testcase 702
seek 28 xor 01
--edit test.db-wal
--raw-open test.db
SELECT y FROM t2 ORDER BY y;
--result 1

/* Change the WAL header so that it contains an invalid version number
** (3007001 in this case).  The checksum is validated before testing the
** checksum so we have to put in a checksum that matches the rest of the
** header in order for this to work.  When the version number is wrong,
** SQLite should return SQLITE_CANTOPEN.
*/
--revert
--open :memory:
--testcase 800
write 377f0682002de2190000080000000001000000017dcfbaf8d9d703138d71aa35
--edit test.db-wal
--raw-open test.db
SELECT y FROM t2 ORDER BY y;
--result SQLITE_CANTOPEN {unable to open database file}
