/*
** This module contains tests of the SELECT statement.
**
** The focus of this module the sqlite3SelectExpand() function and
** its helper functions selectExpand().
**
** SCRIPT_MODULE_NAME:        select25
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a,b,c);
INSERT INTO t1 VALUES(1,2,3);
INSERT INTO t1 VALUES(4,5,6);
INSERT INTO t1 VALUES(7,8,9);
INSERT INTO t1 VALUES(10,11,12);
CREATE VIEW v1 AS SELECT a+b AS x, b+c AS y FROM t1;
CREATE VIEW v2 AS SELECT x+y AS p FROM v1;
--result

--testcase 101
PRAGMA full_column_names;
PRAGMA short_column_names;
--result 0 1

--column-names 1
--testcase 110
--oom
SELECT * FROM v2;
--result p 8 p 20 p 32 p 44

--column-names 1
--testcase 200
--oom
SELECT * FROM (SELECT * FROM (SELECT 1 AS w, 2 AS x), (SELECT 3 AS y, 4 AS z));
--result w 1 x 2 y 3 z 4
--testcase 201
SELECT * FROM (SELECT * FROM 
   (SELECT 1 AS w, 2 AS x UNION ALL SELECT 8, 9), (SELECT 3 AS y, 4 AS z));
--result w 1 x 2 y 3 z 4 w 8 x 9 y 3 z 4

--testcase 210
SELECT * FROM t1 INDEXED BY t1a WHERE a=1;
--result SQLITE_ERROR {no such index: t1a}

--testcase 300
DROP TABLE t1;
SELECT * FROM v2;
--result SQLITE_ERROR {no such table: main.t1}

--testcase 400
CREATE TABLE t41(a,b);
INSERT INTO t41 VALUES(1,2);
INSERT INTO t41 VALUES(3,4);
CREATE TABLE t42(b,c);
INSERT INTO t42 VALUES(1,2);
INSERT INTO t42 VALUES(2,3);
SELECT * FROM t41 NATURAL JOIN t42;
--result a 1 b 2 c 3
--testcase 401
SELECT * FROM t41 JOIN t42 USING(a,b);
--result SQLITE_ERROR {cannot join using column a - column not present in both tables}

--testcase 410
SELECT t41.*, t42.*, t41.rowid FROM t41 NATURAL JOIN t42;
--result a 1 b 2 b 2 c 3 rowid 1
--testcase 411
SELECT t41.rowid, t42.* FROM t41 NATURAL JOIN t42;
--result rowid 1 b 2 c 3
--testcase 412
SELECT main.t41.rowid, t42.* FROM t41 NATURAL JOIN t42;
--result rowid 1 b 2 c 3
--testcase 413
--oom
SELECT t41.*, c, * FROM t41 NATURAL JOIN t42;
--result a 1 b 2 c 3 a 1 b 2 c 3

--testcase 500
PRAGMA full_column_names=ON;
SELECT *, t41.* FROM t41 JOIN t42 USING(b);
--result a 1 b 2 c 3 a 1 b 2
--testcase 510
PRAGMA short_column_names=OFF;
SELECT *, t41.* FROM t41 JOIN t42 USING(b);
--result t41.a 1 t41.b 2 t42.c 3 t41.a 1 t41.b 2
--testcase 520
SELECT *, P.* FROM t41 AS P JOIN t42 AS Q USING(b);
--result P.a 1 P.b 2 Q.c 3 P.a 1 P.b 2
--testcase 521
SELECT *, P.* FROM t41 AS P JOIN t42 AS '' USING(b);
--result P.a 1 P.b 2 .c 3 P.a 1 P.b 2

--testcase 600
SELECT *, t43.* FROM t41 NATURAL JOIN t42;
--result SQLITE_ERROR {no such table: t43}

--testcase 700
PRAGMA full_column_names=OFF;
SELECT th3_load_normal_extension();
SELECT th3_sqlite3_limit('SQLITE_LIMIT_COLUMN',20);
--run 0
SELECT *,*,*,*,*,*,a,b FROM t41 NATURAL JOIN t42;
--result a 1 b 2 c 3 a 1 b 2 c 3 a 1 b 2 c 3 a 1 b 2 c 3 a 1 b 2 c 3 a 1 b 2 c 3 a 1 b 2

--testcase 701
SELECT *,*,*,*,*,*,* FROM t41 NATURAL JOIN t42;
--result SQLITE_ERROR {too many columns in result set}
