/*
** This module contains tests of vdbe.c source module.
**
** SCRIPT_MODULE_NAME:        vdbe18
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** The OP_SeekLt, OP_SeekLe, OP_SeekGt, and OP_SeekGe opcodes
**
** When constructing the test database, add zeroblobs() that are half the
** page size.  This will force each row to be on its own page.  With each
** row on its own page, we can force btree-routine failures using OOM faults.
*/
--testcase 100
CREATE TABLE t1(a INTEGER PRIMARY KEY, b, c);
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(1,'one', zeroblob($pgsz/2));
INSERT INTO t1 VALUES(99,'ninety-nine', zeroblob($pgsz/2));
INSERT INTO t1 VALUES(-9223372036854775808, 'small', zeroblob($pgsz/2));
INSERT INTO t1 VALUES(9223372036854775807, 'big', zeroblob($pgsz/2));
SELECT b FROM t1 ORDER BY a;
--result small one ninety-nine big

--testcase 110
SELECT b FROM t1 WHERE a<-1000;
--result small
--testcase 111
SELECT b FROM t1 WHERE a<=-1000;
--result small
--testcase 112
SELECT b FROM t1 WHERE a>-1000 ORDER BY a;
--result one ninety-nine big
--testcase 113
SELECT b FROM t1 WHERE a>=-1000 ORDER BY a;
--result one ninety-nine big

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 120
--oom
SELECT b FROM t1 WHERE a>-9e+20 ORDER BY a;
--result small one ninety-nine big
--testcase 120rid
--oom
SELECT a FROM t1 WHERE a>-9e+20 ORDER BY a;
--result -9223372036854775808 1 99 9223372036854775807
--testcase 121
SELECT b FROM t1 WHERE a>=-9e+20 ORDER BY a;
--result small one ninety-nine big
--testcase 122
SELECT b FROM t1 WHERE a<-9e+20 ORDER BY a;
--result
--testcase 123
SELECT b FROM t1 WHERE a<=-9e+20 ORDER BY a;
--result
--testcase 124
SELECT b FROM t1 WHERE a<9e+20 ORDER BY a;
--result small one ninety-nine big
--testcase 125
SELECT b FROM t1 WHERE a<=9e+20 ORDER BY a;
--result small one ninety-nine big
--testcase 126
SELECT b FROM t1 WHERE a>9e+20 ORDER BY a;
--result
--testcase 127
SELECT b FROM t1 WHERE a>=9e+20 ORDER BY a;
--result

--testcase 130
SELECT b FROM t1 WHERE a>-9e+20 ORDER BY a DESC;
--result big ninety-nine one small
--testcase 131
SELECT b FROM t1 WHERE a>=-9e+20 ORDER BY a DESC;
--result big ninety-nine one small
--testcase 132
SELECT b FROM t1 WHERE a<-9e+20 ORDER BY a DESC;
--result
--testcase 133
SELECT b FROM t1 WHERE a<=-9e+20 ORDER BY a DESC;
--result
--testcase 134
--oom
SELECT b FROM t1 WHERE a<9e+20 ORDER BY a DESC;
--result big ninety-nine one small
--testcase 135
SELECT b FROM t1 WHERE a<=9e+20 ORDER BY a DESC;
--result big ninety-nine one small
--testcase 136
SELECT b FROM t1 WHERE a>9e+20 ORDER BY a DESC;
--result
--testcase 137
SELECT b FROM t1 WHERE a>=9e+20 ORDER BY a DESC;
--result

--testcase 140
--oom
SELECT b FROM t1 WHERE a BETWEEN 0.5 AND 1.5;
--result one
--testcase 141
SELECT b FROM t1 WHERE a BETWEEN 0.5 AND 1.0;
--result one
--testcase 142
SELECT b FROM t1 WHERE a BETWEEN 1.0 AND 1.5;
--result one
#endif /* SQLITE_OMIT_FLOATING_POINT */

--testcase 150
SELECT b FROM t1 WHERE a>'abc';
--result

--testcase 160
--oom
SELECT b FROM t1 WHERE a>-9223372036854775808 ORDER BY a;
--result one ninety-nine big
--testcase 161
SELECT b FROM t1 WHERE a>=-9223372036854775808 ORDER BY a;
--result small one ninety-nine big
--testcase 162
SELECT b FROM t1 WHERE a<-9223372036854775808 ORDER BY a;
--result
--testcase 163
SELECT b FROM t1 WHERE a<=-9223372036854775808 ORDER BY a;
--result small
--testcase 164
SELECT b FROM t1 WHERE a>-9223372036854775808 ORDER BY a DESC;
--result big ninety-nine one
--testcase 165
SELECT b FROM t1 WHERE a>=-9223372036854775808 ORDER BY a DESC;
--result big ninety-nine one small
--testcase 166
SELECT b FROM t1 WHERE a<-9223372036854775808 ORDER BY a DESC;
--result
--testcase 167
SELECT b FROM t1 WHERE a<=-9223372036854775808 ORDER BY a DESC;
--result small
--testcase 168

--testcase 170
SELECT b FROM t1 WHERE a>=CAST(-9223372036854775808 AS real) ORDER BY a;
--result small one ninety-nine big

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 180
SELECT b FROM t1 WHERE a>4.5 ORDER BY a;
--result ninety-nine big
--testcase 181
SELECT b FROM t1 WHERE a>=4.5 ORDER BY a;
--result ninety-nine big
--testcase 182
SELECT b FROM t1 WHERE a<4.5 ORDER BY a;
--result small one
--testcase 183
SELECT b FROM t1 WHERE a<=4.5 ORDER BY a;
--result small one
--testcase 184
SELECT b FROM t1 WHERE a<99.0 ORDER BY a;
--result small one
--testcase 185
SELECT b FROM t1 WHERE a<=99.0 ORDER BY a;
--result small one ninety-nine
--testcase 186
SELECT b FROM t1 WHERE a>99.0 ORDER BY a;
--result big
--testcase 187
SELECT b FROM t1 WHERE a>=99.0 ORDER BY a;
--result ninety-nine big

--testcase 190
SELECT b FROM t1 WHERE a>4.5 ORDER BY a DESC;
--result big ninety-nine
--testcase 191
SELECT b FROM t1 WHERE a>=4.5 ORDER BY a DESC;
--result big ninety-nine
--testcase 192
SELECT b FROM t1 WHERE a<4.5 ORDER BY a DESC;
--result one small
--testcase 193
SELECT b FROM t1 WHERE a<=4.5 ORDER BY a DESC;
--result one small
--testcase 194
SELECT b FROM t1 WHERE a<99.0 ORDER BY a DESC;
--result one small
--testcase 195
SELECT b FROM t1 WHERE a<=99.0 ORDER BY a DESC;
--result ninety-nine one small
--testcase 196
SELECT b FROM t1 WHERE a>99.0 ORDER BY a DESC;
--result big
--testcase 197
SELECT b FROM t1 WHERE a>=99.0 ORDER BY a DESC;
--result big ninety-nine

--testcase 200
SELECT b FROM t1 WHERE a>-4.5 ORDER BY a;
--result one ninety-nine big
--testcase 201
SELECT b FROM t1 WHERE a>=-4.5 ORDER BY a;
--result one ninety-nine big
--testcase 202
SELECT b FROM t1 WHERE a<-4.5 ORDER BY a;
--result small
--testcase 203
SELECT b FROM t1 WHERE a<=-4.5 ORDER BY a;
--result small
#endif /* SQLITE_OMIT_FLOATING_POINT */

--testcase 210
SELECT b FROM t1 WHERE a>99 ORDER BY a;
--result big
--testcase 211
SELECT b FROM t1 WHERE a>=99 ORDER BY a;
--result ninety-nine big
--testcase 212
SELECT b FROM t1 WHERE a<99 ORDER BY a;
--result small one
--testcase 213
SELECT b FROM t1 WHERE a<=99 ORDER BY a;
--result small one ninety-nine
--testcase 214
SELECT b FROM t1 WHERE a>99 ORDER BY a DESC;
--result big
--testcase 215
SELECT b FROM t1 WHERE a>=99 ORDER BY a DESC;
--result big ninety-nine
--testcase 216
--oom
SELECT b FROM t1 WHERE a<99 ORDER BY a DESC;
--result one small
--testcase 217
SELECT b FROM t1 WHERE a<=99 ORDER BY a DESC;
--result ninety-nine one small
--testcase 218


--new test.db
--testcase 300
SELECT * FROM sqlite_master WHERE rowid>=5;
--result
