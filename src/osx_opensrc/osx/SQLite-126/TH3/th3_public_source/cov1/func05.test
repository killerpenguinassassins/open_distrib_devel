/*
** Module for testing functions.  Tests are written by hand from
** the documentation in lang_corefunc.html
**
** SCRIPT_MODULE_NAME:        func05
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
/****************************************************************************
** total_changes(): Return the total number of database rows modified, 
** inserted, or deleted since the current database connection was opened.
**
** EVIDENCE-OF: R-38914-26427 The total_changes() function returns the
** number of row changes caused by INSERT, UPDATE or DELETE statements
** since the current database connection was opened.
*/
--testcase total-1
SELECT total_changes();
--result 0

/* Creating a new table does not modify the total-changes count */
--testcase total-2
CREATE TABLE t1(x);
SELECT total_changes();
--result 0

/* Adding rows to a table does modify the count. */
--testcase total-3
INSERT INTO t1 VALUES(1);
SELECT total_changes();
--result 1
--testcase total-4
INSERT INTO t1 SELECT x+1 FROM t1;
SELECT total_changes();
--result 2
--testcase total-5
INSERT INTO t1 SELECT x+2 FROM t1;
SELECT total_changes();
--result 4
--testcase total-6
INSERT INTO t1 SELECT x+4 FROM t1;
SELECT total_changes();
--result 8
--testcase total-7
INSERT INTO t1 SELECT x+8 FROM t1;
SELECT total_changes();
--result 16

/* Modifying rows updates the count */
--testcase total-8
UPDATE t1 SET x=x+1 WHERE x>8;
SELECT total_changes();
--result 24

/* Deleting rows updates the count */
--testcase total-9
DELETE FROM t1 WHERE rowid IN (1,2);
SELECT total_changes();
--result 26

/* Dropping a table does not modify the count, even if the table contains
** one or more rows of data */
--testcase total-10
DROP TABLE t1;
SELECT total_changes();
--result 26

/* The total_changes() function takes no parameters */
--testcase total-11
SELECT total_changes(1);
--result SQLITE_ERROR {wrong number of arguments to function total_changes()}

/****************************************************************************
** changes(): Return the number of database rows modified, inserted, or
** deleted by the most recently SQL statement.
*/
/* Creating a new table does not modify the changes count */
--testcase changes-1
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(1);
SELECT changes();
--result 1

/* Adding rows to a table does modify the count. */
--testcase total-4
INSERT INTO t1 SELECT x+1 FROM t1;
SELECT changes();
--result 1
--testcase total-5
INSERT INTO t1 SELECT x+2 FROM t1;
SELECT changes();
--result 2
--testcase total-6
INSERT INTO t1 SELECT x+4 FROM t1;
SELECT changes();
--result 4
--testcase total-7
INSERT INTO t1 SELECT x+8 FROM t1;
SELECT changes();
--result 8

/* Modifying rows updates the count */
--testcase total-8
UPDATE t1 SET x=x+1 WHERE x>8;
SELECT changes();
--result 8

/* Deleting rows updates the count */
--testcase total-9
DELETE FROM t1 WHERE rowid IN (1,2);
SELECT changes();
--result 2

/* Dropping a table does not modify the count.  The value 2 from the
** previous test is retained */
--testcase total-10
DROP TABLE t1;
SELECT changes();
--result 2

/* The changes() function takes no parameters */
--testcase changes-11
SELECT changes(1);
--result SQLITE_ERROR {wrong number of arguments to function changes()}
