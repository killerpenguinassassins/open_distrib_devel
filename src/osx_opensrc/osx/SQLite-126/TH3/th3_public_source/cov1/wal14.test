/*
** Verify that the unexpected crash of a writer process does not leave
** the wal-index hash table in an unrecoverable state.
**
** MIXED_MODULE_NAME:         wal14
** REQUIRED_PROPERTIES:       TEST_VFS JOURNAL_WAL
** DISALLOWED_PROPERTIES:     SHARED_CACHE MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
/*
** Invoke the TH3_FCNTL_SET_IGNOREWRITE file control on the main
** database of connection iDb
*/
void wal14_ignorewrite_1(th3state *p, int iDb, char *zArg){
  sqlite3 *db = th3dbPointer(p, iDb);
  sqlite3_file_control(db, "main", TH3_FCNTL_SET_IGNOREWRITE, 0);
}
/********* SCRIPT ********/

--raw-new test.db
--testcase 100
PRAGMA page_size=1024;
PRAGMA journal_mode=WAL;
CREATE TABLE t1(x INTEGER PRIMARY KEY, y);
INSERT INTO t1 VALUES(1, zeroblob(11000));
INSERT INTO t1 VALUES(2, zeroblob(11000));
INSERT INTO t1 VALUES(3, zeroblob(11000));
SELECT x FROM t1 ORDER BY x;
--result wal 1 2 3

--db 1
--open test.db
--testcase 110
PRAGMA cache_size=10;
BEGIN;
UPDATE t1 SET y=zeroblob(11001), x=12 WHERE x=2;
SELECT x, length(y) FROM t1 ORDER BY x;
--result 1 11000 3 11000 12 11001

--call wal14_ignorewrite_1
--close 1
--db 0
--testcase 120
SELECT x, length(y) FROM t1 ORDER BY x;
--result 1 11000 2 11000 3 11000

--testcase 130
INSERT INTO t1 VALUES(5, null);
--result
