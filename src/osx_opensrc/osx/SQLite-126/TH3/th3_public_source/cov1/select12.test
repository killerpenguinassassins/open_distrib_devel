/*
** This module contains tests of the SELECT statement.
**
** The focus of this module is the selectColumnsFromExprList() routine.
**
** SCRIPT_MODULE_NAME:        select12
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a,b,c);
CREATE TABLE t2(x,y,z);
INSERT INTO t1 VALUES(1,2,3);
INSERT INTO t2 VALUES(7,8,9);
--result

--testcase 110
--oom
CREATE TABLE t3 AS SELECT * FROM t1;
--result
--testcase 111
--column-names 1
SELECT * FROM t3
--result a 1 b 2 c 3

--testcase 120
DROP TABLE t3;
CREATE TABLE t3 AS SELECT rowid, t1.a, b AS x FROM t1;
--result
--testcase 121
--column-names 1
SELECT * FROM t3
--result rowid 1 a 1 x 2

--testcase 130
DROP TABLE t3;
--run 0
--oom
CREATE TABLE t3 AS SELECT b, a, b, c, b, b AS x FROM t1;
--result
--testcase 131
--column-names 1
SELECT * FROM t3
--result b 2 a 1 b:1 2 c 3 b:2 2 x 2

--testcase 140
SELECT * FROM (
   SELECT a.cnt, b.cnt
     FROM (SELECT count(*) AS cnt FROM t1) AS a JOIN
          (SELECT count(*) AS cnt FROM t2) AS b
);
--result cnt 1 cnt:1 1

/* Repeat the flattening above but this time inside a trigger */
--testcase 141
--column-names 0
CREATE TABLE t141a(x);
CREATE TABLE t141b(a,b);
CREATE TRIGGER t141r1 AFTER INSERT ON t141a BEGIN
  INSERT INTO t141b
    SELECT * FROM (
      SELECT a.cnt, b.cnt
        FROM (SELECT count(*) AS cnt FROM t1) AS a JOIN
             (SELECT count(*) AS cnt FROM t2) AS b
    );
END;
INSERT INTO t141a VALUES(1);
SELECT * FROM t141b;
--result 1 1

--testcase 150
CREATE VIEW v1 AS SELECT t1.a FROM t1;
CREATE TRIGGER v1r1 INSTEAD OF DELETE on v1 BEGIN SELECT 'no-op'; END;
DELETE FROM v1 WHERE a=11;
--result
