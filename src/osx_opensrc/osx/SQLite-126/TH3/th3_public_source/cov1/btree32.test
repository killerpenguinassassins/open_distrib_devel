/*
** Test module for btree.c.  DROP TABLE while another thread is doing a
** query on the same shared cache.
**
** MODULE_NAME:               btree32
** REQUIRED_PROPERTIES:       TEST_VFS SHARED_CACHE
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
int btree32(th3state *p){
  int rc;
  sqlite3_stmt *pStmt;
  const char *z;

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
     "CREATE TABLE t1(x); INSERT INTO t1 VALUES(1);"
     "CREATE TABLE t2(x);"
     "CREATE TABLE t3(x); INSERT INTO t3 VALUES(3);"
     "CREATE TABLE t4(x); INSERT INTO t4 VALUES(4);"
     "CREATE TABLE t5(x); INSERT INTO t5 VALUES(5);"
     "INSERT INTO t4 VALUES(6);"
     "PRAGMA integrity_check;"
     "SELECT name FROM sqlite_master ORDER BY name"
  );
  th3testCheck(p, "ok t1 t2 t3 t4 t5");

  th3testBegin(p, "101");
  pStmt = th3dbPrepare(p, 0, "SELECT * FROM t4;");
  assert( pStmt!=0 );
  rc = sqlite3_step(pStmt);
  assert( rc==SQLITE_ROW );
  th3dbOpen(p, 1, "test.db", 0);
  th3dbEval(p, 1, "DROP TABLE t2;");
  z = p->zResult;
  th3testCheckInt(p, 1,
     th3strcmp(z, "SQLITE_BUSY {database is locked}")==0 ||
     th3strcmp(z, "SQLITE_LOCKED {database table is locked: sqlite_master}")==0
  );
  sqlite3_finalize(pStmt);

  return 0;
}
