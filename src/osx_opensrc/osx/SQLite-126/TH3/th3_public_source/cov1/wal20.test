/*
** Tests for ROLLBACK across the 4096-frame boundaries in the wal-index.
**
** SCRIPT_MODULE_NAME:        wal20
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     TEST_VFS ALT_PCACHE MEMDB EXCLUSIVE_ONLY
** DISALLOWED_PROPERTIES:     ZIPVFS
** MINIMUM_HEAPSIZE:          100000
** INCOMPATIBLE_WITH:         dotlock nolock
*/
--raw-new test.db
--store $pgsz PRAGMA page_size
--testcase 100
PRAGMA synchronous=NORMAL;
PRAGMA journal_mode=WAL;
PRAGMA wal_autocheckpoint=0;
--result wal 0
--testcase 110
--oom
CREATE TABLE IF NOT EXISTS t1(a,b,c);
--result
--testcase 111
CREATE TABLE IF NOT EXISTS t2(y);
INSERT INTO t1 VALUES(1, zeroblob($pgsz/2), 10001);
INSERT INTO t1 VALUES(2, zeroblob($pgsz/2), 10002);
INSERT INTO t1 SELECT a+2, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+4, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+8, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+16, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+32, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+64, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+128, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+256, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+512, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+1024, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+2048, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+4096, b, a+10000 FROM t1 WHERE a+4096<=4100;
SELECT count(a), sum(a) FROM t1;
PRAGMA wal_checkpoint;
--glob 4100 8407050 # # #

--testcase 120
UPDATE t1 SET c=c+1 WHERE a<4062;
BEGIN;
UPDATE t1 SET c=a+10000 WHERE a<20;
ROLLBACK;
SELECT count(a), sum(a) FROM t1;
PRAGMA wal_checkpoint;
--glob 4100 8407050 # # #

--testcase 130
UPDATE t1 SET c=c+1 WHERE a<4063;
BEGIN;
UPDATE t1 SET c=a+10000 WHERE a<20;
ROLLBACK;
SELECT count(a), sum(a) FROM t1;
PRAGMA wal_checkpoint;
--glob 4100 8407050 # # #

--testcase 140
UPDATE t1 SET c=c+1 WHERE a<4064;
BEGIN;
UPDATE t1 SET c=a+10000 WHERE a<20;
ROLLBACK;
SELECT count(a), sum(a) FROM t1;
--result 4100 8407050

--testcase 150
PRAGMA cache_size=10;
UPDATE t1 SET c=a+20000;
CREATE TABLE t3(x UNIQUE, y);
INSERT INTO t3 VALUES(20, null);
SELECT count(c), sum(c) FROM t1;
--result 4100 90407050
--testcase 151
INSERT INTO t3 SELECT a%30, b FROM t1;
--result SQLITE_CONSTRAINT {column x is not unique}
