/*
** This module contains tests of prepare.c source module.
**
** MODULE_NAME:               prepare02
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
int prepare02(th3state *p){
  char *zAux;


  /*
  ** The only way to enter sqlite3InitCallback() with db->mallocFailed set
  ** is to fail a malloc during an ATTACH.  The --oom script command cannot
  ** accomplish that because --oom uses a transaction and ATTACH does not
  ** work within a transaction.
  */
  zAux = th3malloc(p, 200);
  th3convertFilename(p, "aux.db", zAux);
  th3oomBegin(p, "1");
  while( th3oomNext(p) ){
    int rc;
    th3dbNew(p, 0, "test.db");
    th3fileDelete(p, "aux.db");
    th3fileDelete(p, "aux.db-journal");
    th3oomEnable(p, 1);
    rc = th3dbEval(p, 0,
         th3format(p, "ATTACH '%s' AS aux; CREATE TABLE aux.t2(x);", zAux)
    );
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK );
      th3oomResult(p,3, "");
    }
  }
  th3oomEnd(p);
  th3free(p, zAux);

  /* 
  ** For an interrupt while parsing the schema
  */
  th3dbNew(p, 0, "test.db");
  th3oomBegin(p, "2");
  th3oom.interruptDb = th3dbPointer(p, 0);
  while( th3oomNext(p) ){
    th3dbEval(p, 0, "BEGIN");
    th3oomEnable(p, 1);
    th3dbEval(p, 0, "CREATE TABLE t1(a, b UNIQUE, c);");
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      if( p->zResult[0] ){
        th3oomCheck(p, 1, strcmp(p->zResult, "SQLITE_INTERRUPT interrupted")==0
                       || strcmp(p->zResult, "SQLITE_INTERRUPT interrupt")==0 );
      }else{
        th3oomCheck(p, 1, 1);
      }
    }else{
      th3dbEval(p, 0, "SELECT name FROM sqlite_master ORDER BY rowid");
      th3oomResult(p, 3, "t1 sqlite_autoindex_t1_1");
    }
    th3dbEval(p, 0, "ROLLBACK");
  }
  th3oomEnd(p);

  /* 
  ** OOM while parsing the schema
  */
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
    "CREATE TABLE t1(a,b,c);"
    "CREATE INDEX t1a ON t1(a);"
    "CREATE INDEX t1b ON t1(b);"
    "CREATE INDEX t1c ON t1(c);"
    "INSERT INTO t1 VALUES(1,2,3);"
    "INSERT INTO t1 VALUES(1,2,4);"
    "INSERT INTO t1 VALUES(1,3,5);"
    "INSERT INTO t1 VALUES(6,7,8);"
    "ANALYZE;"
  );
  th3oomBegin(p, "3");
  th3oom.interruptDb = 0;
  while( th3oomNext(p) ){
    int rc;
    th3dbOpen(p, 0, "test.db", 0);
    th3oomEnable(p, 1);
    rc = th3dbEval(p, 0, "SELECT b FROM t1 ORDER BY b");
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM);
    }else{
      th3oomResult(p, 2, "2 2 3 7");
    }
  }
  th3oomEnd(p);


  return 0;
}
