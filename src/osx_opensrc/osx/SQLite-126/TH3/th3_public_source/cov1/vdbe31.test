/*
** This module contains tests of vdbe.c source module.
**
** MODULE_NAME:               vdbe31
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** Return an error from the step function of an aggregate.  None of
** the built-in aggregates do this - they all error out in the finalizer.
**
** Also test returning over-sized strings from an aggregate finalizer.
*/

/*
** This 3-argument aggregate function returns the 32-bit integer sum
** of the first argument.  Or if the second argument is ever non-zero,
** it errors out immediately with an error code of the second argument
** and error text of the third argument.
*/
static void vdbe31Step(sqlite3_context *context, int argc,sqlite3_value **argv){
  int *p;
  int rc;
  assert( argc==3 );
  p = sqlite3_aggregate_context(context, sizeof(*p));
  rc = sqlite3_value_int(argv[1]);
  if( rc ){
    sqlite3_result_error(context, (const char*)sqlite3_value_text(argv[2]), -1);
    sqlite3_result_error_code(context, rc);
    return;
  }
  *p += sqlite3_value_int(argv[0]);
}
static void vdbe31Finalize(sqlite3_context *context){
  int *p;
  p = sqlite3_aggregate_context(context, 0);
  sqlite3_result_int(context, *p);
}

/*
** This is an alternative finalizer for the vdbe31Step() function above.
** Instead of returning the integer value, it returns a string of "*"
** characters that is N characters in length.  N might be larger than
** SQLITE_LIMIT_LENGTH.  This routine is to verify that the OP_AggFinal
** operator checks for oversize returns.
*/
static void vdbe31Str(sqlite3_context *context){
  int *p;
  char *z;
  int i, n;
  p = sqlite3_aggregate_context(context, 0);
  n = *p;
  if( n<0 ) n = -n;
  z = sqlite3_malloc(n+1);
  if( z ){
    for(i=0; i<n; i++) z[i] = '*';
    z[n] = 0;
    sqlite3_result_text(context, z, *p, sqlite3_free);
  }
}
static void vdbe31Str16(sqlite3_context *context){
  int *p;
  short int *z;
  int i, n;
  p = sqlite3_aggregate_context(context, 0);
  n = *p;
  if( n<0 ) n = -n;
  z = sqlite3_malloc((n+1)*sizeof(z[0]));
  if( z ){
    for(i=0; i<n; i++) z[i] = '*';
    z[n] = 0;
    sqlite3_result_text16(context, z, (*p)<0 ? -1 : n*2, sqlite3_free);
  }
}

/*
** The same as vdbe31Str except that the sqlite3_result is transient.
*/
static void vdbe31StrTrans(sqlite3_context *context){
  int *p;
  char *z;
  int i, n;
  p = sqlite3_aggregate_context(context, 0);
  n = *p;
  if( n<0 ) n = -n;
  z = sqlite3_malloc(n+1);
  if( z ){
    for(i=0; i<n; i++) z[i] = '*';
    z[n] = 0;
    sqlite3_result_text(context, z, *p, SQLITE_TRANSIENT);
    sqlite3_free(z);
  }
}

/*
** The same as vdbe31Str except that the sqlite3_result_zeroblob is used.
*/
static void vdbe31Zero(sqlite3_context *context){
  int *p;
  int n;
  p = sqlite3_aggregate_context(context, 0);
  n = *p;
  if( n<0 ) n = -n;
  sqlite3_result_zeroblob(context, n);
}


/* The test module */
int vdbe31(th3state *p){
  sqlite3 *db;

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  sqlite3_create_function(db, "vdbe31", 3, SQLITE_UTF8, 0,
                          0, vdbe31Step, vdbe31Finalize);
  th3dbEval(p, 0,
    "CREATE TABLE t1(a,b,c);"
    "INSERT INTO t1 VALUES(1,0,'');"
    "INSERT INTO t1 VALUES(2,0,'not used');"
    "SELECT vdbe31(a,b,c) FROM t1;"
  );
  th3testCheck(p, "3");

  th3testBegin(p, "110");
  th3dbEval(p, 0,
    "INSERT INTO t1 VALUES(3,1,'this is the error text');"
    "SELECT vdbe31(a,b,c) FROM t1;"
  );
  th3testCheck(p, "SQLITE_ERROR {this is the error text}");

  th3testBegin(p, "200");
  sqlite3_create_function(db, "vdbe31str", 3, SQLITE_UTF8, 0,
                          0, vdbe31Step, vdbe31Str);
  th3dbEval(p, 0, 
    "UPDATE t1 SET b=0, a=500 WHERE a=3;"
    "SELECT length(vdbe31str(a,b,c)) FROM t1;"
  );
  th3testCheck(p, "503");
  th3testBegin(p, "201");
  sqlite3_create_function(db, "vdbe31str16", 3, SQLITE_UTF8, 0,
                          0, vdbe31Step, vdbe31Str16);
  th3dbEval(p, 0, 
    "SELECT length(vdbe31str16(a,b,c)) FROM t1;"
  );
  th3testCheck(p, "503");
  th3testBegin(p, "202");
  th3dbEval(p, 0, 
    "SELECT length(vdbe31str(-a,b,c)) FROM t1;"
  );
  th3testCheck(p, "503");
  th3testBegin(p, "203");
  th3dbEval(p, 0, 
    "SELECT length(vdbe31str16(-a,b,c)) FROM t1;"
  );
  th3testCheck(p, "503");

  th3testBegin(p, "210");
  sqlite3_limit(db, SQLITE_LIMIT_LENGTH, 400);
  th3dbEval(p, 0, 
    "SELECT length(vdbe31str(a,b,c)) FROM t1;"
  );
  th3testCheck(p, "SQLITE_TOOBIG {string or blob too big}");
  th3testBegin(p, "211");
  th3dbEval(p, 0, 
    "SELECT length(vdbe31str16(a,b,c)) FROM t1;"
  );
  th3testCheck(p, "SQLITE_TOOBIG {string or blob too big}");
  th3testBegin(p, "211");
  th3dbEval(p, 0, 
    "SELECT length(vdbe31str(-a,b,c)) FROM t1;"
  );
  th3testCheck(p, "SQLITE_TOOBIG {string or blob too big}");
  th3testBegin(p, "212");
  th3dbEval(p, 0, 
    "SELECT length(vdbe31str16(-a,b,c)) FROM t1;"
  );
  th3testCheck(p, "SQLITE_TOOBIG {string or blob too big}");

  th3testBegin(p, "220");
  sqlite3_create_function(db, "vdbe31strtrans", 3, SQLITE_UTF8, 0,
                          0, vdbe31Step, vdbe31StrTrans);
  th3dbEval(p, 0, 
    "SELECT length(vdbe31strtrans(a,b,c)) FROM t1;"
  );
  th3testCheck(p, "SQLITE_TOOBIG {string or blob too big}");
  th3testBegin(p, "221");
  th3dbEval(p, 0, 
    "SELECT length(vdbe31strtrans(-a,b,c)) FROM t1;"
  );
  th3testCheck(p, "SQLITE_TOOBIG {string or blob too big}");

  th3testBegin(p, "230");
  sqlite3_create_function(db, "vdbe31zero", 3, SQLITE_UTF8, 0,
                          0, vdbe31Step, vdbe31Zero);
  th3dbEval(p, 0, 
    "SELECT length(vdbe31zero(a,b,c)) FROM t1;"
  );
  th3testCheck(p, "SQLITE_TOOBIG {string or blob too big}");

  return 0;
}
