/*
** Tests for WAL files holding more than 4096 frames
**
** MIXED_MODULE_NAME:         wal21
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     ALT_PCACHE MEMDB EXCLUSIVE_ONLY ZIPVFS
** MINIMUM_HEAPSIZE:          100000
*/

/*
** Run I/O error tests on WAL recovery.  Only xShmMap will fail.
*/
static void wal21_shmmap_error(th3state *p, int iDb, char *zArg){
  th3ioerrBegin(p, "200");
  th3filesystemSnapshot(p);
  p->fsysTest.ioerr.mask = TH3VFS_IOERR_SHMMAP;
  while( th3ioerrNext(p) ){
    int rc;
    th3dbCloseAll(p);
    th3filesystemRevert(p);
    th3dbOpen(p, iDb, "test.db", TH3_OPEN_RAW);
    th3ioerrEnable(p, 1);
    rc = th3dbEval(p, iDb, "SELECT count(c), sum(c) FROM t1");
    th3ioerrEnable(p, 0);
    if( th3ioerrHit(p) && rc!=SQLITE_OK ){
      th3ioerrCheck(p, 1, rc!=SQLITE_OK );
    }else{
      th3ioerrResult(p, 2, "1024 529920");
    }
  }
  th3ioerrEnd(p);
}
--store $pgsz PRAGMA page_size
--if ($sector_size+0)<=1024 AND ($pgsz+0)<=4096
--testcase 100
PRAGMA synchronous=NORMAL;
PRAGMA journal_mode=WAL;
PRAGMA wal_autocheckpoint=0;
--result wal 0
--testcase 110
CREATE TABLE t1(a,b,c);
INSERT INTO t1 VALUES(1, randomblob($pgsz/2), 10001);
INSERT INTO t1 VALUES(2, randomblob($pgsz/2), 10002);
INSERT INTO t1 SELECT a+2, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+4, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+8, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+16, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+32, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+64, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+128, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+256, b, a+10000 FROM t1;
INSERT INTO t1 SELECT a+512, b, a+10000 FROM t1;
SELECT count(a), sum(a) FROM t1;
PRAGMA wal_checkpoint;
--glob 1024 524800 # # #

--testcase 120
UPDATE t1 SET c=a;
UPDATE t1 SET c=c+1;
UPDATE t1 SET c=c+1;
UPDATE t1 SET c=c+1;
UPDATE t1 SET c=c+1;
UPDATE t1 SET c=c+1;
SELECT count(c), sum(c) FROM t1;
--result 1024 529920

--call wal21_shmmap_error
--endif
