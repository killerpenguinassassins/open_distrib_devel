/*
** Module for testing the ANALYZE statement.  This is black-box testing
** of the new "range scan" enhancement that is enabled by the
** SQLITE_ENABLE_STAT2 compile-time option.
**
** SCRIPT_MODULE_NAME:        analyze04
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
#ifdef SQLITE_ENABLE_STAT2
--testcase 100
CREATE TABLE t1(a INTEGER PRIMARY KEY, b TEXT, c TEXT);
INSERT INTO t1 VALUES(1,'apple','able');
INSERT INTO t1 VALUES(2,'apricot','about');
INSERT INTO t1 VALUES(3,'avocado','above');
INSERT INTO t1 VALUES(4,'banana','according');
INSERT INTO t1 VALUES(5,'blackberry','account');
INSERT INTO t1 VALUES(6,'blueberry','across');
INSERT INTO t1 VALUES(7,'cherry','act');
INSERT INTO t1 VALUES(8,'coconut','action');
INSERT INTO t1 VALUES(9,'cranberry','activities');
INSERT INTO t1 VALUES(10,'fig','activity');
INSERT INTO t1 VALUES(11,'grapefruit','actually');
INSERT INTO t1 VALUES(12,'grape','added');
INSERT INTO t1 VALUES(13,'guava','addition');
INSERT INTO t1 VALUES(14,'kiwi','additional');
INSERT INTO t1 VALUES(15,'lemon','administration');
INSERT INTO t1 VALUES(16,'lime','after');
INSERT INTO t1 VALUES(17,'mango','again');
INSERT INTO t1 VALUES(18,'nectarine','against');
INSERT INTO t1 VALUES(19,'orange','age');
INSERT INTO t1 VALUES(20,'peach','ago');
INSERT INTO t1 VALUES(21,'pear','ahead');
INSERT INTO t1 VALUES(22,'persimmon','aid');
INSERT INTO t1 VALUES(23,'pinapple','air');
INSERT INTO t1 VALUES(24,'watermelon','all');
CREATE INDEX t1b ON t1(b);
CREATE INDEX t1c ON t1(c);
SELECT a FROM t1 WHERE b BETWEEN 'cherry' AND 'fig';
--result 7 8 9 10

--testcase 110
ANALYZE;
SELECT name FROM sqlite_master WHERE name LIKE 'sqlite_%' ORDER BY name;
--result sqlite_stat1 sqlite_stat2

--testcase 120
EXPLAIN QUERY PLAN
SELECT a FROM t1
 WHERE b BETWEEN 'cherry' AND 'pear'
   AND c BETWEEN 'a' AND 'z';
--glob * INDEX t1b *

--testcase 130
EXPLAIN QUERY PLAN
SELECT a FROM t1
 WHERE b GLOB 'a*' AND c GLOB 'a*';
--glob * INDEX t1b *
--testcase 131
SELECT 'a*';
--store $pattern
--prepare_v2 0
EXPLAIN QUERY PLAN
SELECT a FROM t1
 WHERE b GLOB $pattern AND c GLOB $pattern;
--glob 0 0 0 {SCAN TABLE t1 (~* rows)}
--testcase 132
--prepare_v2 1
EXPLAIN QUERY PLAN
SELECT a FROM t1
 WHERE b GLOB $pattern AND c GLOB $pattern;
--result 0 0 0 {SEARCH TABLE t1 USING INDEX t1b (b>? AND b<?) (~2 rows)}


--testcase 200
CREATE TABLE t2(a INTEGER PRIMARY KEY, b TEXT, c TEXT);
INSERT INTO t2 VALUES(1,'able','name');
INSERT INTO t2 VALUES(2,'about','nation');
INSERT INTO t2 VALUES(3,'above','national');
INSERT INTO t2 VALUES(4,'according','nations');
INSERT INTO t2 VALUES(5,'account','natural');
INSERT INTO t2 VALUES(6,'across','nature');
INSERT INTO t2 VALUES(7,'act','near');
INSERT INTO t2 VALUES(8,'action','nearly');
INSERT INTO t2 VALUES(9,'activities','necessary');
INSERT INTO t2 VALUES(10,'activity','need');
INSERT INTO t2 VALUES(11,'actually','needed');
INSERT INTO t2 VALUES(12,'added','needs');
INSERT INTO t2 VALUES(13,'addition','negro');
INSERT INTO t2 VALUES(14,'additional','neither');
INSERT INTO t2 VALUES(15,'administration','never');
INSERT INTO t2 VALUES(16,'after','new');
INSERT INTO t2 VALUES(17,'again','next');
INSERT INTO t2 VALUES(18,'against','night');
INSERT INTO t2 VALUES(19,'age','no');
INSERT INTO t2 VALUES(20,'ago','non');
INSERT INTO t2 VALUES(21,'ahead','nor');
INSERT INTO t2 VALUES(22,'aid','normal');
INSERT INTO t2 VALUES(23,'air','north');
INSERT INTO t2 VALUES(24,'all','not');
INSERT INTO t2 VALUES(25,'almost','note');
INSERT INTO t2 VALUES(26,'alone','nothing');
INSERT INTO t2 VALUES(27,'along','now');
INSERT INTO t2 VALUES(28,'already','nuclear');
INSERT INTO t2 VALUES(29,'also','number');
INSERT INTO t2 VALUES(30,'although','numbers');
INSERT INTO t2 VALUES(31,'always','obtained');
INSERT INTO t2 VALUES(32,'am','obviously');
INSERT INTO t2 VALUES(33,'america','of');
INSERT INTO t2 VALUES(34,'american','off');
INSERT INTO t2 VALUES(35,'among','office');
INSERT INTO t2 VALUES(36,'amount','often');
INSERT INTO t2 VALUES(37,'an','oh');
INSERT INTO t2 VALUES(38,'analysis','old');
INSERT INTO t2 VALUES(39,'and','on');
INSERT INTO t2 VALUES(40,'another','once');
INSERT INTO t2 VALUES(41,'answer','one');
INSERT INTO t2 VALUES(42,'anti','ones');
INSERT INTO t2 VALUES(43,'any','only');
INSERT INTO t2 VALUES(44,'anyone','open');
INSERT INTO t2 VALUES(45,'anything','opened');
INSERT INTO t2 VALUES(46,'apparently','operation');
INSERT INTO t2 VALUES(47,'appear','opportunity');
INSERT INTO t2 VALUES(48,'appeared','or');
INSERT INTO t2 VALUES(49,'approach','order');
INSERT INTO t2 VALUES(50,'are','organization');
INSERT INTO t2 VALUES(51,'area','other');
INSERT INTO t2 VALUES(52,'areas','others');
INSERT INTO t2 VALUES(53,'arms','our');
INSERT INTO t2 VALUES(54,'army','out');
INSERT INTO t2 VALUES(55,'around','outside');
INSERT INTO t2 VALUES(56,'art','over');
INSERT INTO t2 VALUES(57,'as','own');
INSERT INTO t2 VALUES(58,'ask','p');
INSERT INTO t2 VALUES(59,'asked','paid');
INSERT INTO t2 VALUES(60,'association','paper');
INSERT INTO t2 VALUES(61,'at','part');
INSERT INTO t2 VALUES(62,'attack','particular');
INSERT INTO t2 VALUES(63,'attention','particularly');
INSERT INTO t2 VALUES(64,'audience','parts');
INSERT INTO t2 VALUES(65,'available','party');
INSERT INTO t2 VALUES(66,'average','passed');
INSERT INTO t2 VALUES(67,'away','past');
INSERT INTO t2 VALUES(68,'b','pattern');
INSERT INTO t2 VALUES(69,'back','pay');
INSERT INTO t2 VALUES(70,'bad','peace');
INSERT INTO t2 VALUES(71,'ball','people');
INSERT INTO t2 VALUES(72,'based','per');
INSERT INTO t2 VALUES(73,'basic','performance');
INSERT INTO t2 VALUES(74,'basis','perhaps');
INSERT INTO t2 VALUES(75,'be','period');
INSERT INTO t2 VALUES(76,'beautiful','person');
INSERT INTO t2 VALUES(77,'became','personal');
INSERT INTO t2 VALUES(78,'because','persons');
INSERT INTO t2 VALUES(79,'become','physical');
INSERT INTO t2 VALUES(80,'bed','picture');
INSERT INTO t2 VALUES(81,'been','piece');
INSERT INTO t2 VALUES(82,'before','place');
INSERT INTO t2 VALUES(83,'began','placed');
INSERT INTO t2 VALUES(84,'beginning','plan');
INSERT INTO t2 VALUES(85,'behind','plane');
INSERT INTO t2 VALUES(86,'being','planning');
INSERT INTO t2 VALUES(87,'believe','plans');
INSERT INTO t2 VALUES(88,'below','plant');
INSERT INTO t2 VALUES(89,'best','play');
INSERT INTO t2 VALUES(90,'better','point');
INSERT INTO t2 VALUES(91,'between','points');
INSERT INTO t2 VALUES(92,'beyond','police');
INSERT INTO t2 VALUES(93,'big','policy');
INSERT INTO t2 VALUES(94,'bill','political');
INSERT INTO t2 VALUES(95,'black','pool');
INSERT INTO t2 VALUES(96,'blood','poor');
INSERT INTO t2 VALUES(97,'blue','population');
INSERT INTO t2 VALUES(98,'board','position');
INSERT INTO t2 VALUES(99,'body','possible');
INSERT INTO t2 VALUES(100,'book','post');
INSERT INTO t2 VALUES(101,'born','power');
INSERT INTO t2 VALUES(102,'both','present');
INSERT INTO t2 VALUES(103,'boy','president');
INSERT INTO t2 VALUES(104,'boys','press');
INSERT INTO t2 VALUES(105,'bring','pressure');
INSERT INTO t2 VALUES(106,'british','price');
INSERT INTO t2 VALUES(107,'brought','principle');
INSERT INTO t2 VALUES(108,'brown','private');
INSERT INTO t2 VALUES(109,'building','probably');
INSERT INTO t2 VALUES(110,'built','problem');
INSERT INTO t2 VALUES(111,'business','problems');
INSERT INTO t2 VALUES(112,'but','process');
INSERT INTO t2 VALUES(113,'by','production');
INSERT INTO t2 VALUES(114,'c','products');
INSERT INTO t2 VALUES(115,'call','program');
INSERT INTO t2 VALUES(116,'called','programs');
INSERT INTO t2 VALUES(117,'came','progress');
INSERT INTO t2 VALUES(118,'can','property');
INSERT INTO t2 VALUES(119,'cannot','provide');
INSERT INTO t2 VALUES(120,'cant','provided');
INSERT INTO t2 VALUES(121,'car','public');
INSERT INTO t2 VALUES(122,'care','purpose');
INSERT INTO t2 VALUES(123,'carried','put');
INSERT INTO t2 VALUES(124,'cars','quality');
INSERT INTO t2 VALUES(125,'case','question');
INSERT INTO t2 VALUES(126,'cases','questions');
INSERT INTO t2 VALUES(127,'cause','quite');
INSERT INTO t2 VALUES(128,'cent','r');
INSERT INTO t2 VALUES(129,'center','race');
INSERT INTO t2 VALUES(130,'central','radio');
INSERT INTO t2 VALUES(131,'century','ran');
INSERT INTO t2 VALUES(132,'certain','range');
INSERT INTO t2 VALUES(133,'certainly','rate');
INSERT INTO t2 VALUES(134,'chance','rather');
INSERT INTO t2 VALUES(135,'change','reached');
INSERT INTO t2 VALUES(136,'changes','reaction');
INSERT INTO t2 VALUES(137,'character','read');
INSERT INTO t2 VALUES(138,'charge','reading');
INSERT INTO t2 VALUES(139,'chief','ready');
INSERT INTO t2 VALUES(140,'child','real');
INSERT INTO t2 VALUES(141,'children','really');
INSERT INTO t2 VALUES(142,'choice','reason');
INSERT INTO t2 VALUES(143,'christian','received');
INSERT INTO t2 VALUES(144,'church','recent');
INSERT INTO t2 VALUES(145,'city','recently');
INSERT INTO t2 VALUES(146,'class','record');
INSERT INTO t2 VALUES(147,'clear','red');
INSERT INTO t2 VALUES(148,'clearly','religion');
INSERT INTO t2 VALUES(149,'close','religious');
INSERT INTO t2 VALUES(150,'closed','remember');
INSERT INTO t2 VALUES(151,'club','report');
INSERT INTO t2 VALUES(152,'co','reported');
INSERT INTO t2 VALUES(153,'cold','required');
INSERT INTO t2 VALUES(154,'college','research');
INSERT INTO t2 VALUES(155,'color','respect');
INSERT INTO t2 VALUES(156,'come','responsibility');
INSERT INTO t2 VALUES(157,'comes','rest');
INSERT INTO t2 VALUES(158,'coming','result');
INSERT INTO t2 VALUES(159,'committee','results');
INSERT INTO t2 VALUES(160,'common','return');
INSERT INTO t2 VALUES(161,'communist','returned');
INSERT INTO t2 VALUES(162,'community','right');
INSERT INTO t2 VALUES(163,'company','river');
INSERT INTO t2 VALUES(164,'complete','road');
INSERT INTO t2 VALUES(165,'completely','room');
INSERT INTO t2 VALUES(166,'concerned','run');
INSERT INTO t2 VALUES(167,'conditions','running');
INSERT INTO t2 VALUES(168,'congress','s');
INSERT INTO t2 VALUES(169,'consider','said');
INSERT INTO t2 VALUES(170,'considered','sales');
INSERT INTO t2 VALUES(171,'continued','same');
INSERT INTO t2 VALUES(172,'control','sat');
INSERT INTO t2 VALUES(173,'corner','saw');
INSERT INTO t2 VALUES(174,'corps','say');
INSERT INTO t2 VALUES(175,'cost','saying');
INSERT INTO t2 VALUES(176,'costs','says');
INSERT INTO t2 VALUES(177,'could','school');
INSERT INTO t2 VALUES(178,'couldnt','schools');
INSERT INTO t2 VALUES(179,'countries','science');
INSERT INTO t2 VALUES(180,'country','season');
INSERT INTO t2 VALUES(181,'county','second');
INSERT INTO t2 VALUES(182,'couple','secretary');
INSERT INTO t2 VALUES(183,'course','section');
INSERT INTO t2 VALUES(184,'court','see');
INSERT INTO t2 VALUES(185,'covered','seem');
INSERT INTO t2 VALUES(186,'cut','seemed');
INSERT INTO t2 VALUES(187,'d','seems');
INSERT INTO t2 VALUES(188,'daily','seen');
INSERT INTO t2 VALUES(189,'dark','self');
INSERT INTO t2 VALUES(190,'data','sense');
INSERT INTO t2 VALUES(191,'day','sent');
INSERT INTO t2 VALUES(192,'days','series');
INSERT INTO t2 VALUES(193,'de','serious');
INSERT INTO t2 VALUES(194,'dead','served');
INSERT INTO t2 VALUES(195,'deal','service');
INSERT INTO t2 VALUES(196,'death','services');
INSERT INTO t2 VALUES(197,'decided','set');
INSERT INTO t2 VALUES(198,'decision','seven');
INSERT INTO t2 VALUES(199,'deep','several');
INSERT INTO t2 VALUES(200,'defense','shall');
INSERT INTO t2 VALUES(201,'degree','she');
INSERT INTO t2 VALUES(202,'democratic','short');
INSERT INTO t2 VALUES(203,'department','shot');
INSERT INTO t2 VALUES(204,'described','should');
INSERT INTO t2 VALUES(205,'design','show');
INSERT INTO t2 VALUES(206,'designed','showed');
INSERT INTO t2 VALUES(207,'determined','shown');
INSERT INTO t2 VALUES(208,'developed','side');
INSERT INTO t2 VALUES(209,'development','similar');
INSERT INTO t2 VALUES(210,'did','simple');
INSERT INTO t2 VALUES(211,'didnt','simply');
INSERT INTO t2 VALUES(212,'difference','since');
INSERT INTO t2 VALUES(213,'different','single');
--result
--testcase 201
INSERT INTO t2 VALUES(214,'difficult','situation');
INSERT INTO t2 VALUES(215,'direct','six');
INSERT INTO t2 VALUES(216,'direction','size');
INSERT INTO t2 VALUES(217,'directly','slowly');
INSERT INTO t2 VALUES(218,'distance','small');
INSERT INTO t2 VALUES(219,'district','so');
INSERT INTO t2 VALUES(220,'do','social');
INSERT INTO t2 VALUES(221,'does','society');
INSERT INTO t2 VALUES(222,'doing','some');
INSERT INTO t2 VALUES(223,'done','something');
INSERT INTO t2 VALUES(224,'dont','sometimes');
INSERT INTO t2 VALUES(225,'door','somewhat');
INSERT INTO t2 VALUES(226,'doubt','son');
INSERT INTO t2 VALUES(227,'down','soon');
INSERT INTO t2 VALUES(228,'dr','sort');
INSERT INTO t2 VALUES(229,'drive','sound');
INSERT INTO t2 VALUES(230,'due','south');
INSERT INTO t2 VALUES(231,'during','southern');
INSERT INTO t2 VALUES(232,'e','soviet');
INSERT INTO t2 VALUES(233,'each','space');
INSERT INTO t2 VALUES(234,'earlier','speak');
INSERT INTO t2 VALUES(235,'early','special');
INSERT INTO t2 VALUES(236,'earth','specific');
INSERT INTO t2 VALUES(237,'east','spirit');
INSERT INTO t2 VALUES(238,'easy','spring');
INSERT INTO t2 VALUES(239,'economic','square');
INSERT INTO t2 VALUES(240,'education','st');
INSERT INTO t2 VALUES(241,'effect','staff');
INSERT INTO t2 VALUES(242,'effective','stage');
INSERT INTO t2 VALUES(243,'effects','stand');
INSERT INTO t2 VALUES(244,'effort','standard');
INSERT INTO t2 VALUES(245,'efforts','start');
INSERT INTO t2 VALUES(246,'eight','started');
INSERT INTO t2 VALUES(247,'either','state');
INSERT INTO t2 VALUES(248,'elements','statements');
INSERT INTO t2 VALUES(249,'else','states');
INSERT INTO t2 VALUES(250,'end','stay');
INSERT INTO t2 VALUES(251,'england','step');
INSERT INTO t2 VALUES(252,'english','steps');
INSERT INTO t2 VALUES(253,'enough','still');
INSERT INTO t2 VALUES(254,'entire','stock');
INSERT INTO t2 VALUES(255,'equipment','stood');
INSERT INTO t2 VALUES(256,'especially','stop');
INSERT INTO t2 VALUES(257,'established','stopped');
INSERT INTO t2 VALUES(258,'europe','story');
INSERT INTO t2 VALUES(259,'even','straight');
INSERT INTO t2 VALUES(260,'evening','street');
INSERT INTO t2 VALUES(261,'ever','strength');
INSERT INTO t2 VALUES(262,'every','strong');
INSERT INTO t2 VALUES(263,'everything','student');
INSERT INTO t2 VALUES(264,'evidence','students');
INSERT INTO t2 VALUES(265,'example','study');
INSERT INTO t2 VALUES(266,'except','subject');
INSERT INTO t2 VALUES(267,'existence','such');
INSERT INTO t2 VALUES(268,'expect','suddenly');
INSERT INTO t2 VALUES(269,'expected','summer');
INSERT INTO t2 VALUES(270,'experience','sun');
INSERT INTO t2 VALUES(271,'extent','support');
INSERT INTO t2 VALUES(272,'eye','sure');
INSERT INTO t2 VALUES(273,'eyes','surface');
INSERT INTO t2 VALUES(274,'f','system');
INSERT INTO t2 VALUES(275,'face','systems');
INSERT INTO t2 VALUES(276,'fact','t');
INSERT INTO t2 VALUES(277,'faith','table');
INSERT INTO t2 VALUES(278,'fall','take');
INSERT INTO t2 VALUES(279,'family','taken');
INSERT INTO t2 VALUES(280,'far','taking');
INSERT INTO t2 VALUES(281,'farm','talk');
INSERT INTO t2 VALUES(282,'father','tax');
INSERT INTO t2 VALUES(283,'fear','technical');
INSERT INTO t2 VALUES(284,'federal','tell');
INSERT INTO t2 VALUES(285,'feed','temperature');
INSERT INTO t2 VALUES(286,'feel','ten');
INSERT INTO t2 VALUES(287,'feeling','term');
INSERT INTO t2 VALUES(288,'feet','terms');
INSERT INTO t2 VALUES(289,'felt','test');
INSERT INTO t2 VALUES(290,'few','th');
INSERT INTO t2 VALUES(291,'field','than');
INSERT INTO t2 VALUES(292,'figure','that');
INSERT INTO t2 VALUES(293,'figures','thats');
INSERT INTO t2 VALUES(294,'filled','the');
INSERT INTO t2 VALUES(295,'final','their');
INSERT INTO t2 VALUES(296,'finally','them');
INSERT INTO t2 VALUES(297,'find','themselves');
INSERT INTO t2 VALUES(298,'fine','then');
INSERT INTO t2 VALUES(299,'fire','theory');
INSERT INTO t2 VALUES(300,'firm','there');
INSERT INTO t2 VALUES(301,'first','therefore');
INSERT INTO t2 VALUES(302,'fiscal','theres');
INSERT INTO t2 VALUES(303,'five','these');
INSERT INTO t2 VALUES(304,'floor','they');
INSERT INTO t2 VALUES(305,'followed','thing');
INSERT INTO t2 VALUES(306,'following','things');
INSERT INTO t2 VALUES(307,'food','think');
INSERT INTO t2 VALUES(308,'foot','thinking');
INSERT INTO t2 VALUES(309,'for','third');
INSERT INTO t2 VALUES(310,'force','thirty');
INSERT INTO t2 VALUES(311,'forces','this');
INSERT INTO t2 VALUES(312,'foreign','those');
INSERT INTO t2 VALUES(313,'form','thought');
INSERT INTO t2 VALUES(314,'former','three');
INSERT INTO t2 VALUES(315,'forms','through');
INSERT INTO t2 VALUES(316,'forward','through');
INSERT INTO t2 VALUES(317,'found','throughout');
INSERT INTO t2 VALUES(318,'four','thus');
INSERT INTO t2 VALUES(319,'free','time');
INSERT INTO t2 VALUES(320,'freedom','times');
INSERT INTO t2 VALUES(321,'french','to');
INSERT INTO t2 VALUES(322,'friend','today');
INSERT INTO t2 VALUES(323,'friends','together');
INSERT INTO t2 VALUES(324,'from','told');
INSERT INTO t2 VALUES(325,'front','too');
INSERT INTO t2 VALUES(326,'full','took');
INSERT INTO t2 VALUES(327,'function','top');
INSERT INTO t2 VALUES(328,'further','total');
INSERT INTO t2 VALUES(329,'future','toward');
INSERT INTO t2 VALUES(330,'g','town');
INSERT INTO t2 VALUES(331,'game','trade');
INSERT INTO t2 VALUES(332,'gave','training');
INSERT INTO t2 VALUES(333,'general','treatment');
INSERT INTO t2 VALUES(334,'generally','trial');
INSERT INTO t2 VALUES(335,'george','tried');
INSERT INTO t2 VALUES(336,'get','trouble');
INSERT INTO t2 VALUES(337,'getting','true');
INSERT INTO t2 VALUES(338,'girl','truth');
INSERT INTO t2 VALUES(339,'girls','try');
INSERT INTO t2 VALUES(340,'give','trying');
INSERT INTO t2 VALUES(341,'given','turn');
INSERT INTO t2 VALUES(342,'gives','turned');
INSERT INTO t2 VALUES(343,'glass','twenty');
INSERT INTO t2 VALUES(344,'go','two');
INSERT INTO t2 VALUES(345,'god','type');
INSERT INTO t2 VALUES(346,'going','types');
INSERT INTO t2 VALUES(347,'gone','u');
INSERT INTO t2 VALUES(348,'good','under');
INSERT INTO t2 VALUES(349,'got','understand');
INSERT INTO t2 VALUES(350,'government','understanding');
INSERT INTO t2 VALUES(351,'great','union');
INSERT INTO t2 VALUES(352,'greater','united');
INSERT INTO t2 VALUES(353,'green','university');
INSERT INTO t2 VALUES(354,'ground','until');
INSERT INTO t2 VALUES(355,'group','up');
INSERT INTO t2 VALUES(356,'groups','upon');
INSERT INTO t2 VALUES(357,'growing','us');
INSERT INTO t2 VALUES(358,'growth','use');
INSERT INTO t2 VALUES(359,'gun','used');
INSERT INTO t2 VALUES(360,'h','using');
INSERT INTO t2 VALUES(361,'had','usually');
INSERT INTO t2 VALUES(362,'hair','value');
INSERT INTO t2 VALUES(363,'half','values');
INSERT INTO t2 VALUES(364,'hall','various');
INSERT INTO t2 VALUES(365,'hand','very');
INSERT INTO t2 VALUES(366,'hands','view');
INSERT INTO t2 VALUES(367,'happened','voice');
INSERT INTO t2 VALUES(368,'hard','volume');
INSERT INTO t2 VALUES(369,'has','waiting');
INSERT INTO t2 VALUES(370,'have','walked');
INSERT INTO t2 VALUES(371,'having','wall');
INSERT INTO t2 VALUES(372,'he','want');
INSERT INTO t2 VALUES(373,'head','wanted');
INSERT INTO t2 VALUES(374,'hear','war');
INSERT INTO t2 VALUES(375,'heard','was');
INSERT INTO t2 VALUES(376,'heart','washington');
INSERT INTO t2 VALUES(377,'heavy','wasnt');
INSERT INTO t2 VALUES(378,'held','water');
INSERT INTO t2 VALUES(379,'hell','way');
INSERT INTO t2 VALUES(380,'help','ways');
INSERT INTO t2 VALUES(381,'her','we');
INSERT INTO t2 VALUES(382,'here','week');
INSERT INTO t2 VALUES(383,'herself','weeks');
INSERT INTO t2 VALUES(384,'hes','well');
INSERT INTO t2 VALUES(385,'high','went');
INSERT INTO t2 VALUES(386,'higher','were');
INSERT INTO t2 VALUES(387,'him','west');
INSERT INTO t2 VALUES(388,'himself','western');
INSERT INTO t2 VALUES(389,'his','what');
INSERT INTO t2 VALUES(390,'history','whatever');
INSERT INTO t2 VALUES(391,'hit','when');
INSERT INTO t2 VALUES(392,'hold','where');
INSERT INTO t2 VALUES(393,'home','whether');
INSERT INTO t2 VALUES(394,'hope','which');
INSERT INTO t2 VALUES(395,'horse','while');
INSERT INTO t2 VALUES(396,'hospital','white');
INSERT INTO t2 VALUES(397,'hot','who');
INSERT INTO t2 VALUES(398,'hotel','whole');
INSERT INTO t2 VALUES(399,'hour','whom');
INSERT INTO t2 VALUES(400,'hours','whose');
INSERT INTO t2 VALUES(401,'house','why');
INSERT INTO t2 VALUES(402,'how','wide');
INSERT INTO t2 VALUES(403,'however','wife');
INSERT INTO t2 VALUES(404,'human','will');
INSERT INTO t2 VALUES(405,'hundred','william');
INSERT INTO t2 VALUES(406,'husband','window');
INSERT INTO t2 VALUES(407,'i','wish');
INSERT INTO t2 VALUES(408,'idea','with');
INSERT INTO t2 VALUES(409,'ideas','within');
INSERT INTO t2 VALUES(410,'if','without');
INSERT INTO t2 VALUES(411,'ill','woman');
INSERT INTO t2 VALUES(412,'im','women');
INSERT INTO t2 VALUES(413,'image','word');
INSERT INTO t2 VALUES(414,'immediately','words');
INSERT INTO t2 VALUES(415,'important','work');
INSERT INTO t2 VALUES(416,'in','worked');
INSERT INTO t2 VALUES(417,'include','working');
INSERT INTO t2 VALUES(418,'including','works');
INSERT INTO t2 VALUES(419,'income','world');
INSERT INTO t2 VALUES(420,'increase','would');
INSERT INTO t2 VALUES(421,'increased','wouldnt');
INSERT INTO t2 VALUES(422,'indeed','writing');
INSERT INTO t2 VALUES(423,'individual','written');
INSERT INTO t2 VALUES(424,'industrial','wrong');
INSERT INTO t2 VALUES(425,'industry','wrote');
INSERT INTO t2 VALUES(426,'influence','year');
INSERT INTO t2 VALUES(427,'information','years');
INSERT INTO t2 VALUES(428,'inside','yes');
INSERT INTO t2 VALUES(429,'instead','yet');
INSERT INTO t2 VALUES(430,'interest','york');
INSERT INTO t2 VALUES(431,'international','you');
INSERT INTO t2 VALUES(432,'into','young');
INSERT INTO t2 VALUES(433,'involved','your');
CREATE INDEX t2b ON t2(b);
CREATE INDEX t2c ON t2(c);
ANALYZE;
--result

--testcase 210
EXPLAIN QUERY PLAN
SELECT a FROM t2 WHERE b>='into' AND c<='your';
--result 0 0 0 {SEARCH TABLE t2 USING INDEX t2b (b>?) (~7 rows)}

--testcase 220
EXPLAIN QUERY PLAN
SELECT a FROM t2 WHERE b>='able' AND c<='nation';
--result 0 0 0 {SEARCH TABLE t2 USING INDEX t2c (c<?) (~7 rows)}

--prepare_v2 1
--testcase 230
CREATE VIRTUAL TABLE syms USING bvs;
INSERT INTO syms(name,value) VALUES('$b', 'into');
INSERT INTO syms(name,value) VALUES('$c', 'your');
EXPLAIN QUERY PLAN
SELECT a FROM t2 WHERE b>=$b AND c<=$c;
--result 0 0 0 {SEARCH TABLE t2 USING INDEX t2b (b>?) (~7 rows)}
--testcase 231
REPLACE INTO syms(name,value) VALUES('$b', 'able');
REPLACE INTO syms(name,value) VALUES('$c', 'nation');
EXPLAIN QUERY PLAN
SELECT a FROM t2 WHERE b>=$b AND c<=$c;
--result 0 0 0 {SEARCH TABLE t2 USING INDEX t2c (c<?) (~7 rows)}

--stmt-cache 4
--testcase 240
DELETE FROM syms;
INSERT INTO syms(name,value) VALUES(1, 'into');
INSERT INTO syms(name,value) VALUES(2, 'your');
EXPLAIN QUERY PLAN
SELECT a FROM t2 WHERE b>=? AND c<=?;
--result 0 0 0 {SEARCH TABLE t2 USING INDEX t2b (b>?) (~7 rows)}
--testcase 241
REPLACE INTO syms(name,value) VALUES(1, 'able');
REPLACE INTO syms(name,value) VALUES(2, 'nation');
EXPLAIN QUERY PLAN
SELECT a FROM t2 WHERE b>=? AND c<=?;
--result 0 0 0 {SEARCH TABLE t2 USING INDEX t2c (c<?) (~7 rows)}
--stmt-cache 0


--testcase 300
CREATE TEMP TABLE tt1(x);
INSERT INTO tt1 SELECT b FROM t2 ORDER BY random() LIMIT 1000;
CREATE TEMP TABLE tt2(y);
INSERT INTO tt2 SELECT c FROM t2 ORDER BY random() LIMIT 1000;
CREATE TABLE t3(a INTEGER PRIMARY KEY, b TEXT, c TEXT, d INT);
INSERT INTO t3
   SELECT tt1.rowid, x, y, 123
     FROM tt1 JOIN tt2 ON tt1.rowid=tt2.rowid;
CREATE INDEX t3b ON t3(b);
CREATE INDEX t3c ON t3(c);
DROP TABLE tt1;
DROP TABLE tt2;
ANALYZE;
--result

--testcase 310
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>='into' AND c<='your';
--result 0 0 0 {SEARCH TABLE t3 USING INDEX t3b (b>?) (~7 rows)}
--testcase 311
SELECT b FROM t3 WHERE b>'into' AND c<='your';
--result involved

--testcase 320
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>='able' AND c<='nation';
--result 0 0 0 {SEARCH TABLE t3 USING INDEX t3c (c<?) (~7 rows)}
--testcase 321
SELECT c FROM t3 WHERE b>='able' AND c<'nation';
--result name

/* I started peeking at the code here.  The stuff above is black-box.
** But below is open-box. */

--prepare_v2 0
--testcase 330
SELECT 'into';
--store $b1
SELECT 'able';
--store $b2
SELECT 'your';
--store $c1
SELECT 'nation';
--store $c2
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>=$b1 AND c<=$c1;
--store $plan1
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>=$b2 AND c<=$c2;
--store $plan2
SELECT $plan1==$plan2
--result 1
#ifndef SQLITE_ENABLE_STAT2
--testcase 331
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>=$b2 AND c<='nation';
--store $plan2
SELECT substr($plan1,1,40)=substr($plan2,1,40);
--result 1
--testcase 332
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>='able' AND c<=$c2;
--store $plan2
SELECT $plan1==$plan2
--result 1
#endif

--prepare_v2 1
--testcase 340
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>=$b1 AND c<=$c1;
--store $plan1
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>=$b2 AND c<=$c2;
--store $plan2
SELECT $plan1==$plan2
--result 0
--testcase 341
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>=$b2 AND c<='nation';
--store $plan2
SELECT $plan1==$plan2
--result 0
--testcase 342
EXPLAIN QUERY PLAN
SELECT a FROM t3 WHERE b>='able' AND c<=$c2;
--store $plan2
SELECT $plan1==$plan2
--result 0

--testcase 400
CREATE TABLE t4(a INTEGER PRIMARY KEY, b, c);
INSERT INTO t4(b,c)
  SELECT abs(random())%100 + 100 + (random()>=0)*300,/* 100..199 and 400..499 */
         abs(random())%200 + 200                     /* 200..399 */
    FROM t3;
CREATE INDEX t4b ON t4(b);
CREATE INDEX t4c ON t4(c);
ANALYZE;
--result

--testcase 410
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 190 AND 390
   AND c BETWEEN 190 AND 390;
--glob * INDEX t4b *
--testcase 411
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 110 AND 210
   AND c BETWEEN 110 AND 210;
--glob * INDEX t4c *
--testcase 412
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 390 AND 500
   AND c BETWEEN 390 AND 500;
--glob * INDEX t4c *
--testcase 413
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 210 AND 410
   AND c BETWEEN 210 AND 410;
--glob * INDEX t4b *

--testcase 420
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 190.0 AND 390.0
   AND c BETWEEN 190.0 AND 390.0;
--glob * INDEX t4b *
--testcase 420b
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b>=190.0 AND b<=390.0
   AND c>=190.0 AND c<=390.0;
--glob * INDEX t4b *
--testcase 421
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 110.0 AND 210.0
   AND c BETWEEN 110.0 AND 210.0;
--glob * INDEX t4c *
--testcase 422
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 390.0 AND 500.0
   AND c BETWEEN 390.0 AND 500.0;
--glob * INDEX t4c *
--testcase 423
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 210.0 AND 410.0
   AND c BETWEEN 210.0 AND 410.0;
--glob * INDEX t4b *

/* Introduce some NULL and TEXT values into the mix */
--testcase 430
INSERT INTO t4(b,c) SELECT NULL, 'xyz' || rowid FROM t4 LIMIT 100;
ANALYZE;
SELECT DISTINCT typeof(sample) FROM sqlite_stat2 WHERE idx='t4b' ORDER BY 1;
--result integer null
--testcase 431
SELECT DISTINCT typeof(sample) FROM sqlite_stat2 WHERE idx='t4c' ORDER BY 1;
--result integer text

--testcase 440
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 190.0 AND 390.0
   AND c BETWEEN 190.0 AND 390.0;
--glob * INDEX t4b *
--testcase 441
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 110 AND 210
   AND c BETWEEN 110.0 AND 210.0;
--glob * INDEX t4c *
--testcase 442
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 390.0 AND 500.0
   AND c BETWEEN 390 AND 500;
--glob * INDEX t4c *
--testcase 443
EXPLAIN QUERY PLAN
SELECT a FROM t4
 WHERE b BETWEEN 210 AND 410
   AND c BETWEEN 210 AND 410;
--glob * INDEX t4b *




#endif /* SQLITE_ENABLE_STAT2 */
