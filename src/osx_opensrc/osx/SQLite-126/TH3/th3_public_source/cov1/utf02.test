/*
** A module for testing conversions between UTF8, UTF16le, and UTF16be.
**
** SCRIPT_MODULE_NAME:       utf02
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

--testcase 1.0
SELECT th3_load_normal_extension();
--result nil

--testcase 1.1
--oom
SELECT utf16be_to_hex(hex_to_utf8('e288bc ce95 7f c280'));
--result 223c0395007f0080

--testcase 1.2
--oom
SELECT utf16le_to_hex(hex_to_utf8('e288bc ce95 7f c280'));
--result 3c2295037f008000

--testcase 1.3
--oom
SELECT utf8_to_hex(hex_to_utf16be('223c 0395 007f 0080'));
--result e288bcce957fc280

/* If we insert UTF text that as little-endian but which contains
** a big-endian byte-order-mark (BOM) then treat it as big-endian.
*/
--testcase 1.4
--oom
SELECT utf8_to_hex(hex_to_utf16le('FEFF 223c 0395 007f 0080'));
--result e288bcce957fc280

/* If we insert UTF text that as big-endian but which contains
** a little-endian (BOM) then treat it as little-endian.
*/
--testcase 1.5
--oom
SELECT utf8_to_hex(hex_to_utf16be('FFFE 3c22 9503 7f00 8000'));
--result e288bcce957fc280

/* BOMs are harmlessly removed if the encoding is correct in the first place.
*/
--testcase 1.6
SELECT utf8_to_hex(hex_to_utf16be('FEFF 223c 0395 007f 0080'));
--result e288bcce957fc280
--testcase 1.7
SELECT utf8_to_hex(hex_to_utf16le('FFFE 3c22 9503 7f00 8000'));
--result e288bcce957fc280

/* FEFE is not a BOM and is passed through unchanged.
*/
--testcase 1.8
SELECT utf8_to_hex(hex_to_utf16be('FEFE 223c'));
--result efbbbee288bc
--testcase 1.9
SELECT utf8_to_hex(hex_to_utf16le('FEFE 3c22'));
--result efbbbee288bc

/**** Invalid UTF-8 encodings ****
**
** Invalid UTF-8 encodings are converted into FFFD
*/
--testcase 2.1
SELECT utf16be_to_hex(hex_to_utf8('C1BF'));  /* 7f as a two-byte encoding */
--result fffd
--testcase 2.2
SELECT utf16be_to_hex(hex_to_utf8('7f'));    /* Correct encoding of 7f */
--result 007f

--testcase 2.3
SELECT utf16be_to_hex(hex_to_utf8('ED9FBF'));  /* Valid code point */
--result d7ff
--testcase 2.4
SELECT utf16be_to_hex(hex_to_utf8('EDA080'));  /* Surrogate pair range */
--result fffd
--testcase 2.5
SELECT utf16be_to_hex(hex_to_utf8('EDBFBF'));  /* Surrogate pair range */
--result fffd
--testcase 2.6
SELECT utf16be_to_hex(hex_to_utf8('EE8080'));  /* Valid code point */
--result e000

--testcase 2.7
SELECT utf16be_to_hex(hex_to_utf8('EFBFBE'));  /* BOM is invalid */
--result fffd

/******************** Structure Coverage ******************************/
/* The LIKE and GLOB functions call the internal routine sqlite3Utf8Read().
** These are the only callers of that routine.  Make sure the UTF8 conversions
** within that routine work as expected.
*/
--testcase 3.1
SELECT hex_to_utf8('61 e288bc ce95 7a c280')
       LIKE hex_to_utf8('41 e288bc ce95 5a c280');
--result 1

/* In these tests, the middle character converts to fffd */
--testcase 3.2
SELECT hex_to_utf8('61 c1bf 7a') LIKE hex_to_utf8('41 eda080 5a');
--result 1
--testcase 3.3
SELECT hex_to_utf8('61 edbfbf 7a') LIKE hex_to_utf8('41 efbfbe 5a');
--result 1

/*
** The LIKE method also calls sqlite3Utf8CharLen() when processing ESCAPE
*/
--testcase 3.11
SELECT 'abc' LIKE 'ABC' ESCAPE hex_to_utf8('c280');
--result 1
--testcase 3.12
SELECT 'abc' LIKE 'ABC' ESCAPE hex_to_utf8('e288bc');
--result 1
--testcase 3.13
SELECT 'abc' LIKE 'ABC' ESCAPE hex_to_utf8('e288bc ce95 7a');
--result SQLITE_ERROR {ESCAPE expression must be a single character}
