/*
** Test module for btree.c.  Tests for lockBtree().
** OOM failure on a page size changes
**
** MODULE_NAME:               btree21
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB NO_OOM ZIPVFS
** MINIMUM_HEAPSIZE:          100000
*/
int btree21(th3state *p){
  int rc;

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, 
     "CREATE TABLE t1(x);"
     "SELECT name FROM sqlite_master;"
  );
  th3testCheck(p, "t1");

  th3testBegin(p, "110");
  th3dbOpen(p, 1, "test.db", 0);
  th3dbEval(p, 1, 
     "SELECT name FROM sqlite_master;"
  );
  th3testCheck(p, "t1");

  th3filesystemSnapshot(p);
  th3oomBegin(p, "120");
  while( th3oomNext(p) ){
    th3dbCloseAll(p);
    th3filesystemRevert(p);
    th3dbOpen(p, 1, "test.db", 0);
    th3dbEval(p, 1, "SELECT name FROM sqlite_master");
    th3testResetResult(p);
    th3dbOpen(p, 0, "test.db", 0);
#ifdef SQLITE_ENABLE_MEMSYS5
    /* Less memory is available with MEMSYS5 (due to power-of-two roundup)
    ** so don't make the ALT_PCACHE quite as big. */
    th3dbEval(p, 0, "PRAGMA page_size=16384; VACUUM;");
#else
    th3dbEval(p, 0, "PRAGMA page_size=32768; VACUUM;");
#endif
    th3oomEnable(p, 1);
    rc = th3dbEval(p, 1, "SELECT name FROM sqlite_master");
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM);
    }else{
      th3oomResult(p, 2, "t1");
    }
  }
  th3oomEnd(p);

  return 0;
}
