/*
** This module contains tests of the SELECT statement.
**
** The focus of this module is column naming for SELECT statements.
**
** SCRIPT_MODULE_NAME:        select10
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a,b,c);
CREATE TABLE t2(x,y,z);
INSERT INTO t1 VALUES(1,2,3);
INSERT INTO t2 VALUES(7,8,9);
--result

--column-names 1
--testcase 101
--oom
SELECT * FROM t1, t2;
--result a 1 b 2 c 3 x 7 y 8 z 9

--testcase 102
PRAGMA short_column_names=OFF;
PRAGMA full_column_names=OFF;
SELECT * FROM t1, t2;
--result a 1 b 2 c 3 x 7 y 8 z 9

--testcase 103
SELECT P.*, Q.* FROM t1 P, t2 Q;
--result a 1 b 2 c 3 x 7 y 8 z 9

--testcase 104
PRAGMA full_column_names=ON;
SELECT * FROM t1, t2;
--result t1.a 1 t1.b 2 t1.c 3 t2.x 7 t2.y 8 t2.z 9

--testcase 105
SELECT * FROM t1 AS P, t2 AS Q;
--result P.a 1 P.b 2 P.c 3 Q.x 7 Q.y 8 Q.z 9
--testcase 106
SELECT a,x,b,y FROM t1 AS P, t2 AS Q;
--result t1.a 1 t2.x 7 t1.b 2 t2.y 8
--testcase 107
SELECT P.oid,Q._rowid_,a,x,b,y FROM t1 AS P, t2 AS Q;
--result t1.rowid 1 t2.rowid 1 t1.a 1 t2.x 7 t1.b 2 t2.y 8

--testcase 110
SELECT * FROM t1 UNION ALL SELECT * FROM t2;
--result t1.a 1 t1.b 2 t1.c 3 t1.a 7 t1.b 8 t1.c 9

--testcase 120
PRAGMA short_column_names=OFF;
PRAGMA full_column_names=OFF;
SELECT a + x, b + y FROM t1, t2;
--result {a + x} 8 {b + y} 10
--testcase 121
SELECT a, x FROM t1, t2;
--result a 1 x 7
--testcase 122
SELECT t1 . a, t2 . x FROM t1, t2;
--result {t1 . a} 1 {t2 . x} 7

--testcase 200
PRAGMA full_column_names=OFF;
PRAGMA short_column_names=ON;
CREATE TABLE out(rownum,colname,value);
--result
--testcase 201
EXPLAIN SELECT * FROM t1;
--table out
--column-names 0
SELECT DISTINCT colname FROM out ORDER BY colname;
--result addr comment opcode p1 p2 p3 p4 p5

--testcase 300
--column-names 1
SELECT count(*) FROM t1;
--result count(*) 1
--testcase 301
SELECT count(*) AS cnt FROM t1;
--result cnt 1

--testcase 310
SELECT 'hello','world';
--result 'hello' hello 'world' world

--testcase 320
SELECT a FROM t1 UNION SELECT a FROM t1;
--result a 1
