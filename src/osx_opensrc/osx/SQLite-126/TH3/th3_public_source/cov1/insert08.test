/*
** This module contains tests for the INSERT statement.
**
** Structural testing.  Tests of the xfer optimization.
**
** SCRIPT_MODULE_NAME:        insert08
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
/* Prepare raw data to be used for all transfer tests */
--testcase 1
CREATE TABLE rawdata(a,b,c,d);
INSERT INTO rawdata VALUES(1, 1, 1, 1);
INSERT INTO rawdata VALUES(2, 4, 8, 16);
INSERT INTO rawdata VALUES(3, 9, 27, 81);
INSERT INTO rawdata VALUES(4, 16, 256, 4096);
INSERT INTO rawdata VALUES(5, 'squared', 'cubed', NULL);
SELECT * FROM rawdata ORDER BY rowid;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* Trigger on destination */
--testcase 100
CREATE TABLE dest1(w,x,y,z);
CREATE TRIGGER dest1_r1 AFTER INSERT ON dest1
WHEN typeof(new.w)!='integer' BEGIN
  SELECT raise(abort, 'w must be an integer');
END;
INSERT INTO dest1 SELECT * FROM rawdata;
SELECT * FROM dest1 ORDER BY w;
DROP TABLE dest1;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* INSERT OR ROLLBACK as the destination. */
--testcase 200
CREATE TABLE src2(a INTEGER PRIMARY KEY, b, c, d);
INSERT INTO src2 SELECT * FROM rawdata;
CREATE TABLE dest2(w INTEGER PRIMARY KEY,x,y,z);
INSERT INTO dest2 VALUES(3,3,3,3);
BEGIN;
INSERT OR ROLLBACK INTO dest2 SELECT * FROM src2;
--result SQLITE_CONSTRAINT {PRIMARY KEY must be unique}

--testcase 201
COMMIT;
--result SQLITE_ERROR {cannot commit - no transaction is active}

--testcase 202
SELECT * FROM dest2 ORDER BY w;
--result 3 3 3 3

--testcase 204
DELETE FROM dest2;
BEGIN;
INSERT OR ROLLBACK INTO dest2 SELECT * FROM src2;
COMMIT;
SELECT * FROM dest2 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* INSERT OR IGNORE as the destination. */
--testcase 210
DELETE FROM dest2;
INSERT INTO dest2 VALUES(3,3,3,3);
INSERT OR IGNORE INTO dest2 SELECT * FROM src2;
SELECT * FROM dest2 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 3 3 3 4 16 256 4096 5 squared cubed nil

/* INSERT OR REPLACE as the destination. */
--testcase 220
DELETE FROM dest2;
INSERT INTO dest2 VALUES(3,3,3,3);
INSERT OR REPLACE INTO dest2 SELECT * FROM src2;
SELECT * FROM dest2 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* INSERT OR ABORT as the destination. */
--testcase 230
DELETE FROM dest2;
INSERT INTO dest2 VALUES(3,3,3,3);
INSERT OR ABORT INTO dest2 SELECT * FROM src2;
--result SQLITE_CONSTRAINT {PRIMARY KEY must be unique}

--testcase 231
SELECT * FROM dest2 ORDER BY w;
--result 3 3 3 3

/* INSERT OR FAIL as the destination. */
--testcase 240
DELETE FROM dest2;
INSERT INTO dest2 VALUES(3,3,3,3);
INSERT OR FAIL INTO dest2 SELECT * FROM src2;
--result SQLITE_CONSTRAINT {PRIMARY KEY must be unique}

--testcase 241
SELECT * FROM dest2 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 3 3 3

--testcase 299
DROP TABLE src2;
DROP TABLE dest2;
--result

/* Transfer from a join */
--testcase 300
CREATE TABLE dest3(w,x,y,z);
CREATE TABLE src3a AS SELECT a, b FROM rawdata ORDER BY a LIMIT 2;
CREATE TABLE src3b AS SELECT c, d FROM rawdata ORDER BY a LIMIT 2;
INSERT INTO dest3 SELECT * FROM src3a, src3b;
SELECT * FROM dest3 ORDER BY w;
--result 1 1 1 1 1 1 8 16 2 4 1 1 2 4 8 16

/* FROM clause does not contain a subquery */
--testcase 310
DELETE FROM dest3;
INSERT INTO dest3
   SELECT * FROM (SELECT * FROM rawdata ORDER BY a LIMIT 2) LIMIT 3;
SELECT * FROM dest3 ORDER BY w;
--result 1 1 1 1 2 4 8 16

/* WHERE clause on RHS */
--testcase 320
DELETE FROM dest3;
INSERT INTO dest3 SELECT * FROM rawdata WHERE a>0;
SELECT * FROM dest3 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* ORDER BY on the RHS */
--testcase 330
DELETE FROM dest3;
INSERT INTO dest3 SELECT * FROM rawdata ORDER BY b DESC;
SELECT * FROM dest3;
--result 5 squared cubed nil 4 16 256 4096 3 9 27 81 2 4 8 16 1 1 1 1

/* GROUP BY and HAVING clauses on the RHS */
--testcase 340
DELETE FROM dest3;
INSERT INTO dest3 SELECT * FROM rawdata GROUP BY a HAVING a>0;
SELECT * FROM dest3 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* LIMIT on the RHS */
--testcase 350
DELETE FROM dest3;
INSERT INTO dest3 SELECT * FROM rawdata LIMIT 3;
SELECT * FROM dest3 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81

/* Compound query on the RHS */
--testcase 360
DELETE FROM dest3;
INSERT INTO dest3
   SELECT * FROM rawdata
   UNION ALL
   SELECT * FROM rawdata;
SELECT count(*) FROM dest3;
SELECT DISTINCT * FROM dest3 ORDER BY w;
--result 10 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* DISTINCT on the RHS */
--testcase 370
DELETE FROM dest3;
INSERT INTO dest3 SELECT DISTINCT * FROM rawdata;
SELECT * FROM dest3 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* Multiple columns on the RHS */
--testcase 380
DELETE FROM dest3;
INSERT INTO dest3 SELECT a, b, c, d FROM rawdata;
SELECT * FROM dest3 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 399
DROP TABLE dest3;
DROP TABLE src3a;
DROP TABLE src3b;
SELECT count(*) FROM sqlite_master;
--result 1

/* Single columns on the RHS that is not "*" */
--testcase 400
CREATE TABLE dest4(x);
INSERT INTO dest4 SELECT a FROM rawdata;
SELECT * FROM dest4 ORDER BY x;
DROP TABLE dest4;
--result 1 2 3 4 5

/* Table on the RHS does not exist */
--testcase 500
CREATE TABLE dest5(w,x,y,z);
INSERT INTO dest5 SELECT * FROM nosuchtable;
--result SQLITE_ERROR {no such table: nosuchtable}

/* Tables on the left and right are the same */
--testcase 510
INSERT INTO dest5 SELECT * FROM rawdata;
INSERT INTO dest5 SELECT * FROM dest5;
SELECT count(*) FROM dest5;
SELECT DISTINCT * FROM dest5 ORDER BY w;
--result 10 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* RHS is a view */
--testcase 520
CREATE VIEW v5 AS SELECT * FROM rawdata;
DELETE FROM dest5;
INSERT INTO dest5 SELECT * FROM v5;
SELECT * FROM dest5 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 599
PRAGMA integrity_check;
DROP VIEW v5;
DROP TABLE dest5;
--result ok

/* Wrong number of terms on the RHS */
--testcase 600
CREATE TABLE dest6a(w,x,y);
INSERT INTO dest6a SELECT * FROM rawdata;
--result SQLITE_ERROR {table dest6a has 3 columns but 4 values were supplied}

--testcase 601
CREATE TABLE dest6b(w,x,y,z,_);
INSERT INTO dest6b SELECT * FROM rawdata;
--result SQLITE_ERROR {table dest6b has 5 columns but 4 values were supplied}

--testcase 699
DROP TABLE dest6a;
DROP TABLE dest6b;
SELECT name FROM sqlite_master;
--result rawdata

/* INTEGER PRIMARY KEY different on left an dright */
--testcase 700
CREATE TABLE dest7(w INTEGER, x INTEGER, y, z, PRIMARY KEY(x));
CREATE TABLE src7a(a INTEGER, b INTTEER, y, z, PRIMARY KEY(a));
CREATE TABLE src7b(a INTEGER, b INTTEER, y, z, PRIMARY KEY(b));
INSERT INTO src7a SELECT a, b+0, c, d FROM rawdata;
INSERT INTO src7b SELECT * FROM src7a;
SELECT * FROM src7a EXCEPT SELECT * FROM src7b;
SELECT * FROM src7a ORDER BY a;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 0 cubed nil

--testcase 701
INSERT INTO dest7 SELECT * FROM src7a;
SELECT * FROM dest7 ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 0 cubed nil

--testcase 702
DELETE FROM dest7;
INSERT INTO dest7 SELECT * FROM src7b;
SELECT * FROM dest7 ORDER BY w;
DROP TABLE dest7;
DROP TABLE src7a;
DROP TABLE src7b;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 0 cubed nil

/* INTEGER PRIMARY KEY and UNIQUE both on LHS */
--testcase 750
CREATE TABLE dest75(a INTEGER PRIMARY KEY, b UNIQUE, c, d);
CREATE TABLE src75(a INTEGER PRIMARY KEY, b UNIQUE, c, d);
INSERT INTO src75 SELECT * FROM rawdata;
INSERT INTO dest75 SELECT * FROM src75;
PRAGMA integrity_check;
SELECT * FROM dest75 ORDER BY a;
--result ok 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 751
DELETE FROM dest75;
INSERT INTO dest75 VALUES(0,0,0,0);
INSERT INTO dest75 SELECT * FROM src75;
PRAGMA integrity_check;
SELECT * FROM dest75 ORDER BY a;
DROP TABLE src75;
DROP TABLE dest75;
--result ok 0 0 0 0 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

/* LHS and RHS have different column affinities */
--testcase 800
CREATE TABLE src8a(a INTEGER, b INT, c TEXT, d BLOB);
CREATE TABLE dest8(a INT, b FLOAT, c VARCHAR, d BLOB);
INSERT INTO src8a SELECT * FROM rawdata;
INSERT INTO dest8 SELECT * FROM src8a;
SELECT distinct typeof(c) FROM dest8;
SELECT * FROM dest8;
DROP TABLE dest8;
DROP TABLE src8a;
#ifndef SQLITE_OMIT_FLOATING_POINT
--result text 1 1.0 1 1 2 4.0 8 16 3 9.0 27 81 4 16.0 256 4096 5 squared cubed nil
#else
--result text 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil
#endif

/* LHS and RHS have different collating sequences */
--testcase 900
CREATE TABLE src9a(a TEXT COLLATE BINARY, b TEXT COLLATE NOCASE);
INSERT INTO src9a VALUES('one','one');
INSERT INTO src9a VALUES('One','One');
INSERT INTO src9a VALUES('Two','Two');
CREATE TABLE dest9a(a TEXT, b TEXT);
INSERT INTO dest9a SELECT * FROM src9a;
SELECT * FROM dest9a ORDER BY a;
--result One One Two Two one one

--testcase 910
CREATE TABLE dest9b(a TEXT, b TEXT COLLATE NOCASE);
INSERT INTO dest9b SELECT * FROM src9a;
SELECT * FROM dest9b ORDER BY a;
--result One One Two Two one one

--testcase 920
CREATE TABLE dest9c(a TEXT COLLATE NOCASE, b TEXT COLLATE NOCASE);
INSERT INTO dest9c SELECT * FROM src9a;
SELECT * FROM dest9c ORDER BY a, a COLLATE BINARY;
--result One One one one Two Two

--testcase 930
DELETE FROM dest9b;
INSERT INTO dest9b SELECT * FROM dest9a;
SELECT * FROM dest9b ORDER BY a;
--result One One Two Two one one

--testcase 940
DELETE FROM dest9c;
INSERT INTO dest9c SELECT * FROM dest9a;
SELECT * FROM dest9c ORDER BY a, a COLLATE BINARY;
--result One One one one Two Two

--testcase 999
PRAGMA integrity_check;
DROP TABLE src9a;
DROP TABLE dest9a;
DROP TABLE dest9b;
DROP TABLE dest9c;
PRAGMA integrity_check;
SELECT name FROM sqlite_master;
--result ok ok rawdata

/* Compatible NOT NULL constraints */
--testcase 1000
CREATE TABLE src10(a, b NOT NULL, c, d);
INSERT INTO src10 SELECT * FROM rawdata;
SELECT * FROM src10 ORDER BY a;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 1010
CREATE TABLE dest10a(w, x NOT NULL, c, d);
INSERT INTO dest10a SELECT * FROM src10;
SELECT * FROM dest10a ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 1020
CREATE TABLE dest10b(w, x, c, d);
INSERT INTO dest10b SELECT * FROM src10;
SELECT * FROM dest10b ORDER BY w;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 1099
PRAGMA integrity_check;
DROP TABLE src10;
DROP TABLE dest10a;
DROP TABLE dest10b;
SELECT name FROM sqlite_master;
--result ok rawdata

/* Compatible CHECK constraints */
--testcase 1100
CREATE TABLE src11(a,b,c,d,CHECK(b>='' || a<=b));
INSERT INTO src11 SELECT * FROM rawdata;
SELECT * FROM src11 ORDER BY a;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil


--testcase 1110
CREATE TABLE dest11(a,b,c,d,CHECK(b>='' || a<=b));
INSERT INTO dest11 SELECT * FROM src11;
SELECT * FROM dest11 ORDER BY a;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 1120
CREATE TABLE dest11b(a,b,c,d,
   CHECK(CASE WHEN b<'' THEN a<=b ELSE 1 END)
);
INSERT INTO dest11b SELECT * FROM src11;
SELECT * FROM dest11b ORDER BY a;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 1130
CREATE TABLE dest11c(a,b,c,d);
INSERT INTO dest11c SELECT * FROM src11;
SELECT * FROM dest11c ORDER BY a;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 1199
PRAGMA integrity_check;
DROP TABLE src11;
DROP TABLE dest11;
DROP TABLE dest11b;
DROP TABLE dest11c;
SELECT rowid FROM sqlite_master WHERE name!='rawdata';
--result ok 

/* For every index on the LHS there must be a compatible index
** on the RHS in order for the xfer optimization to run. */
--testcase 1200
CREATE TABLE src12(a,b,c,d);
CREATE TABLE dest12(a,b,c,d);
CREATE INDEX sidx ON src12(a,b);
INSERT INTO src12 SELECT * FROM rawdata;
SELECT * FROM src12 ORDER BY a;
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil

--testcase 1201
CREATE INDEX didx ON dest12(a,b);
INSERT INTO dest12 SELECT * FROM src12;
SELECT * FROM dest12 ORDER BY a;
PRAGMA integrity_check
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil ok

--testcase 1202
DELETE FROM dest12;
DROP INDEX sidx;
DROP INDEX didx;
CREATE UNIQUE INDEX sidx ON src12(b,c);
CREATE UNIQUE INDEX didx ON dest12(b,c);
INSERT INTO dest12 SELECT * FROM src12;
SELECT * FROM dest12 ORDER BY a;
PRAGMA integrity_check
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil ok

--testcase 1203
DELETE FROM dest12;
DROP INDEX sidx;
INSERT INTO dest12 SELECT * FROM src12;
SELECT * FROM dest12 ORDER BY a;
PRAGMA integrity_check
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil ok

--testcase 1204
DELETE FROM dest12;
CREATE INDEX sidx ON src12(b,c);
INSERT INTO dest12 SELECT * FROM src12;
SELECT * FROM dest12 ORDER BY a;
PRAGMA integrity_check
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil ok

--testcase 1205
DELETE FROM dest12;
DROP INDEX sidx;
CREATE UNIQUE INDEX sidx ON src12(b,d);
INSERT INTO dest12 SELECT * FROM src12;
SELECT * FROM dest12 ORDER BY a;
PRAGMA integrity_check
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil ok

--testcase 1206
DROP INDEX didx;
CREATE INDEX sidx_a ON src12(a);
CREATE INDEX sidx_cd ON src12(c,d);
CREATE UNIQUE INDEX sidx_b1 ON src12(b DESC);
CREATE INDEX sidx_b2 ON src12(b COLLATE NOCASE);
CREATE INDEX sidx_b3 ON src12(b COLLATE RTRIM);
CREATE INDEX didx_cd ON dest12(c,d);
CREATE UNIQUE INDEX didx_b1 ON dest12(b DESC);
CREATE INDEX didx_b2 ON dest12(b COLLATE NOCASE);
CREATE INDEX didx_a ON dest12(a);
CREATE INDEX didx_b3 ON dest12(b COLLATE RTRIM);
DELETE FROM dest12;
INSERT INTO dest12 SELECT * FROM src12;
SELECT * FROM dest12 ORDER BY a;
PRAGMA integrity_check
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil ok

--testcase 1207
DROP INDEX sidx_b2;
DELETE FROM dest12;
INSERT INTO dest12 SELECT * FROM src12;
SELECT * FROM dest12 ORDER BY a;
PRAGMA integrity_check
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil ok

--testcase 1208
CREATE INDEX sidx_b2 ON src12(b COLLATE NOCASE);
DROP INDEX didx_b1;
CREATE INDEX didx_b1 ON dest12(b DESC);
DELETE FROM dest12;
INSERT INTO dest12 SELECT * FROM src12;
SELECT * FROM dest12 ORDER BY a;
PRAGMA integrity_check
--result 1 1 1 1 2 4 8 16 3 9 27 81 4 16 256 4096 5 squared cubed nil ok

/* Destination contains a foreign key constraint. */
--testcase 1300
CREATE TABLE t1300a(x INT PRIMARY KEY); INSERT INTO t1300a VALUES(123);
CREATE TABLE t1300b(a,b INT REFERENCES t1300a);
CREATE TABLE t1300c(a,b INT);
INSERT INTO t1300c SELECT a, b FROM rawdata WHERE typeof(b)='integer';
SELECT * FROM t1300c;
--result 1 1 2 4 3 9 4 16

--if $property_FOREIGN_KEYS
--testcase 1301a
INSERT INTO t1300b SELECT * FROM t1300c;
SELECT * FROM t1300b ORDER BY a;
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--else
--testcase 1301b
INSERT INTO t1300b SELECT * FROM t1300c;
SELECT * FROM t1300b ORDER BY a;
--result 1 1 2 4 3 9 4 16
--endif
