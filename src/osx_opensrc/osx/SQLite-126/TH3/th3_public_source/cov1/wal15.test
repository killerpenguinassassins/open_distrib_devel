/*
** Multiple concurrent readers on different transactions and a concurrent
** writer.
**
** SCRIPT_MODULE_NAME:        wal15
** REQUIRED_PROPERTIES:       JOURNAL_WAL
** DISALLOWED_PROPERTIES:     SHARED_CACHE MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
--db 0
--new test.db
--testcase 100
PRAGMA wal_autocheckpoint=0;
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(1);
SELECT count(x), sum(x) FROM t1;
--result 0 1 1

--db 1
--open test.db
--testcase 110
BEGIN;
SELECT count(x), sum(x) FROM t1;
--result 1 1

--db 0
--testcase 120
INSERT INTO t1 VALUES(2);
SELECT count(x), sum(x) FROM t1;
--result 2 3
--db 1
--testcase 121
SELECT count(x), sum(x) FROM t1;
--result 1 1
--db 2
--open test.db
--testcase 122
BEGIN;
SELECT count(x), sum(x) FROM t1;
--result 2 3

--db 0
--testcase 130
INSERT INTO t1 VALUES(3);
SELECT count(x), sum(x) FROM t1;
--result 3 6
--db 1
--testcase 131
SELECT count(x), sum(x) FROM t1;
--result 1 1
--db 2
--testcase 132
SELECT count(x), sum(x) FROM t1;
--result 2 3
--db 3
--open test.db
--testcase 133
BEGIN;
SELECT count(x), sum(x) FROM t1;
--result 3 6

--db 0
--testcase 140
INSERT INTO t1 VALUES(4);
SELECT count(x), sum(x) FROM t1;
--result 4 10
--db 1
--testcase 141
SELECT count(x), sum(x) FROM t1;
--result 1 1
--db 2
--testcase 142
SELECT count(x), sum(x) FROM t1;
--result 2 3
--db 3
--testcase 143
SELECT count(x), sum(x) FROM t1;
--result 3 6
--db 4
--open test.db
--testcase 144
BEGIN;
SELECT count(x), sum(x) FROM t1;
--result 4 10

--db 0
--testcase 150
INSERT INTO t1 VALUES(5);
SELECT count(x), sum(x) FROM t1;
--result 5 15
--db 1
--testcase 151
SELECT count(x), sum(x) FROM t1;
--result 1 1
--db 2
--testcase 152
SELECT count(x), sum(x) FROM t1;
--result 2 3
--db 3
--testcase 153
SELECT count(x), sum(x) FROM t1;
--result 3 6
--db 4
--testcase 154
SELECT count(x), sum(x) FROM t1;
--result 4 10
--db 5
--open test.db
--testcase 155
BEGIN;
SELECT count(x), sum(x) FROM t1;
--result 5 15

--db 0
--testcase 160
INSERT INTO t1 VALUES(6);
SELECT count(x), sum(x) FROM t1;
--result 6 21
--db 1
--testcase 161
SELECT count(x), sum(x) FROM t1;
--result 1 1
--db 2
--testcase 162
SELECT count(x), sum(x) FROM t1;
--result 2 3
--db 3
--testcase 163
SELECT count(x), sum(x) FROM t1;
--result 3 6
--db 4
--testcase 164
SELECT count(x), sum(x) FROM t1;
--result 4 10
--db 5
--testcase 165
SELECT count(x), sum(x) FROM t1;
--result 5 15
--db 6
--open test.db
--testcase 166
BEGIN;
SELECT count(x), sum(x) FROM t1;
--result 6 21

--db 0
--testcase 170
INSERT INTO t1 VALUES(7);
SELECT count(x), sum(x) FROM t1;
--result 7 28
--db 1
--testcase 171
SELECT count(x), sum(x) FROM t1;
--result 1 1
--db 2
--testcase 172
SELECT count(x), sum(x) FROM t1;
--result 2 3
--db 3
--testcase 173
SELECT count(x), sum(x) FROM t1;
--result 3 6
--db 4
--testcase 174
SELECT count(x), sum(x) FROM t1;
--result 4 10
--db 5
--testcase 175
SELECT count(x), sum(x) FROM t1;
--result 5 15
--db 6
--testcase 176
SELECT count(x), sum(x) FROM t1;
--result 6 21

--db 0
--testcase 180
PRAGMA wal_autocheckpoint=5;
INSERT INTO t1 VALUES(8);
SELECT count(x), sum(x) FROM t1;
--result 5 8 36
--db 1
--testcase 181
SELECT count(x), sum(x) FROM t1;
--result 1 1
--db 2
--testcase 182
SELECT count(x), sum(x) FROM t1;
--result 2 3
--db 3
--testcase 183
SELECT count(x), sum(x) FROM t1;
--result 3 6
--db 4
--testcase 184
SELECT count(x), sum(x) FROM t1;
--result 4 10
--db 5
--testcase 185
SELECT count(x), sum(x) FROM t1;
--result 5 15
--db 6
--testcase 186
SELECT count(x), sum(x) FROM t1;
--result 6 21

--db 0
--testcase 190
INSERT INTO t1 VALUES(9);
SELECT count(x), sum(x) FROM t1;
--result 9 45
--db 1
--testcase 191
COMMIT; BEGIN;
INSERT INTO t1 VALUES(10);
SELECT count(x), sum(x) FROM t1;
--result 10 55
--db 2
--testcase 192
SELECT count(x), sum(x) FROM t1;
--result 2 3
--db 3
--testcase 193
SELECT count(x), sum(x) FROM t1;
--result 3 6
--db 4
--testcase 194
SELECT count(x), sum(x) FROM t1;
--result 4 10
--db 5
--testcase 195
SELECT count(x), sum(x) FROM t1;
--result 5 15
--db 6
--testcase 196
SELECT count(x), sum(x) FROM t1;
--result 6 21
