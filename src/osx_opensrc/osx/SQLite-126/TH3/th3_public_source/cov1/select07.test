/*
** This module contains tests of the SELECT statement.
**
** The focus of this module is SELECT statements within TRIGGERs and VIEWs.
** The result of such selects is ignored.  They are used for their
** side-effects only.
**
** SCRIPT_MODULE_NAME:        select07
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x);
CREATE TABLE t2(x, y);
INSERT INTO t2 VALUES(1, 2);
INSERT INTO t2 VALUES(3, 4);
INSERT INTO t2 SELECT x+4, y+4 FROM t2;
INSERT INTO t2 SELECT x+8, y+8 FROM t2;
CREATE TABLE log(y);
CREATE TRIGGER r1 AFTER INSERT ON t1 BEGIN
  SELECT th3puts(x) FROM t2;
END;
INSERT INTO t1 VALUES(5);
--result 1 3 5 7 9 11 13 15

--testcase 110
CREATE TRIGGER r2 AFTER UPDATE ON t2 BEGIN
  SELECT th3puts(x) FROM t2 ORDER BY x DESC LIMIT 5;
END;
UPDATE t2 SET x=y, y=x WHERE x=1;
UPDATE t2 SET x=y, y=x WHERE y=1;
--result 2 3 5 7 9 1 3 5 7 9

--testcase 200
CREATE VIEW v1 AS SELECT x+y AS m, y*2-x AS n FROM t2;
SELECT * FROM v1 ORDER BY m;
--result 3 3 7 5 11 7 15 9 19 11 23 13 27 15 31 17
--testcase 201
CREATE TRIGGER v1r1 INSTEAD OF DELETE ON v1 BEGIN
  INSERT INTO log VALUES(old.m);
END;
DELETE FROM v1 WHERE n=15;
SELECT * FROM log;
--result 27

--testcase 210
CREATE VIEW v2 AS SELECT x+y AS m, y*2-x AS n FROM t2 ORDER BY m LIMIT 3;
SELECT * FROM v2 ORDER BY m;
--result 3 3 7 5 11 7
--testcase 211
CREATE TRIGGER v2r1 INSTEAD OF DELETE ON v2 BEGIN
  INSERT INTO log VALUES(old.m);
END;
DELETE FROM log;
DELETE FROM v2 WHERE n=15;
SELECT * FROM log;
--result
--testcase 212
DELETE FROM v2 WHERE n=5;
SELECT * FROM log;
--result 7

--testcase 220
CREATE VIEW v3 AS SELECT x+y AS m, y*2-x AS n FROM t2 ORDER BY m DESC LIMIT 3;
SELECT * FROM v3 ORDER BY m;
--result 23 13 27 15 31 17
--testcase 221
CREATE TRIGGER v3r1 INSTEAD OF DELETE ON v3 BEGIN
  INSERT INTO log VALUES(old.m);
END;
DELETE FROM log;
DELETE FROM v3 WHERE n=5;
SELECT * FROM log;
--result
--testcase 222
DELETE FROM v3 WHERE n=15;
SELECT * FROM log;
--result 27
