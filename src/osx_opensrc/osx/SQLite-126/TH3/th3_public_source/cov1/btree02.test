/*
** Test module for btree.c.
**
** Handling of corrupt databases
**
** SCRIPT_MODULE_NAME:        btree02
** REQUIRED_PROPERTIES:       TEST_VFS AUTOVACUUM CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x,y);
PRAGMA integrity_check;
--result ok

--testcase 110
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(1, zeroblob($pgsz));
INSERT INTO t1 VALUES(2, zeroblob($pgsz));
INSERT INTO t1 VALUES(3, zeroblob($pgsz));
INSERT INTO t1 VALUES(4, zeroblob($pgsz));
--result
--checkpoint

--testcase 120
SELECT rootpage*$pgsz FROM sqlite_master WHERE name='t1';
--store $ofst
seek $ofst move -4 write32 0 incr-chng
--edit test.db
--oom-ck
PRAGMA integrity_check;
--result "*** in database main ***\012On tree page 3 cell 0: Failed to read ptrmap key=0\012On tree page 3 cell 0: 1 of 1 pages missing from overflow list starting at 0\012Page 4 is never used"

--testcase 130
INSERT INTO t1 VALUES(5, zeroblob($pgsz*3/4));
--result SQLITE_CORRUPT {database disk image is malformed}

--new test.db
--testcase 200
CREATE TABLE t1(x,y);
INSERT INTO t1 VALUES(1, zeroblob($pgsz));
INSERT INTO t1 VALUES(2, zeroblob($pgsz));
INSERT INTO t1 VALUES(3, zeroblob($pgsz));
INSERT INTO t1 VALUES(4, zeroblob($pgsz));
--result
--checkpoint

--testcase 210
seek $ofst move -4 write32 2 incr-chng
--edit test.db
INSERT INTO t1 VALUES(5, zeroblob($pgsz*3/4));
--result SQLITE_CORRUPT {database disk image is malformed}

--new test.db
--testcase 300
CREATE TABLE t1(x,y);
INSERT INTO t1 VALUES(1, zeroblob($pgsz));
INSERT INTO t1 VALUES(2, zeroblob($pgsz));
INSERT INTO t1 VALUES(3, zeroblob($pgsz));
INSERT INTO t1 VALUES(4, zeroblob($pgsz));
PRAGMA page_count;
--result 7
--checkpoint

--testcase 310
seek $pgsz move 20 write8 0 incr-chng
--edit test.db
DELETE FROM t1 WHERE x=1;
--result SQLITE_CORRUPT {database disk image is malformed}

--new test.db
--testcase 400
CREATE TABLE t1(x,y);
INSERT INTO t1 VALUES(1, zeroblob($pgsz));
INSERT INTO t1 VALUES(2, zeroblob($pgsz));
INSERT INTO t1 VALUES(3, zeroblob($pgsz));
INSERT INTO t1 VALUES(4, zeroblob($pgsz));
PRAGMA page_count;
--result 7
--checkpoint

--testcase 410
seek $pgsz move 20 write8 6 incr-chng
--edit test.db
DELETE FROM t1 WHERE x=1;
--result SQLITE_CORRUPT {database disk image is malformed}
