/*
** This module contains tests of the C-language interfaces found in
** the main.c source file. 
**
** MODULE_NAME:               main13
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** The focus of this test module is the sqlite3_limit() interface.
*/
int main13(th3state *p){
  sqlite3 *db1;
  sqlite3 *db2;
  int i;
  int rc;

  th3dbNew(p, 0, "test1.db");
  db1 = th3dbPointer(p, 0);
  th3dbNew(p, 1, "test2.db");
  db2 = th3dbPointer(p, 1);

  /* Limit ID out of range */
  th3testBegin(p, "1");
  rc = sqlite3_limit(db1, -1, -1);
  th3testCheckInt(p, -1, rc);
  th3testBegin(p, "2");
  rc = sqlite3_limit(db1, 11, -1);
  th3testCheckInt(p, -1, rc);

  /* Manipulate each limit setting */
  for(i=SQLITE_LIMIT_LENGTH; i<=SQLITE_LIMIT_TRIGGER_DEPTH; i++){
    int origLimit;

    th3testBegin(p, th3format(p, "3.%d.1", i));
    origLimit = sqlite3_limit(db1, i, -1);
    th3testCheckInt(p, 1, origLimit>=0);

    th3testBegin(p, th3format(p, "3.%d.2", i));
    rc = sqlite3_limit(db1, i, origLimit/2);
    th3testCheckInt(p, origLimit, rc);

    th3testBegin(p, th3format(p, "3.%d.3", i));
    rc = sqlite3_limit(db1, i, 0x7fffffff);
    th3testCheckInt(p, origLimit/2, rc);

    th3testBegin(p, th3format(p, "3.%d.4", i));
    rc = sqlite3_limit(db1, i, origLimit);
    th3testCheckInt(p, 1, rc>=origLimit);

    th3testBegin(p, th3format(p, "3.%d.5", i));
    rc = sqlite3_limit(db1, i, -1);
    th3testCheckInt(p, origLimit, rc);

    th3testBegin(p, th3format(p, "3.%d.6", i));
    rc = sqlite3_limit(db2, i, -1);
    th3testCheckInt(p, origLimit, rc);
  }
  
  return 0;
}
