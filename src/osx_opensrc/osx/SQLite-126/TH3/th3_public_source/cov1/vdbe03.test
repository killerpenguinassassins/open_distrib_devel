/*
** This module contains tests of vdbe.c source module.
**
** SCRIPT_MODULE_NAME:        vdbe03
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB EXCLUSIVE_ONLY
** MINIMUM_HEAPSIZE:          100000
*/
--new test.db
#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 100
CREATE TABLE t1(x NOT NULL, y NOT NULL);
INSERT INTO t1 VALUES(1,2);
INSERT INTO t1 VALUES(34.5,789.0);
INSERT INTO t1 VALUES('hello','world');
INSERT INTO t1 VALUES(x'00',x'01');
SELECT quote(x), quote(y) FROM t1 ORDER BY rowid;
--result 1 2 34.5 789.0 'hello' 'world' X'00' X'01'
#endif
#ifdef SQLITE_OMIT_FLOATING_POINT
--testcase 100
CREATE TABLE t1(x NOT NULL, y NOT NULL);
INSERT INTO t1 VALUES(1,2);
INSERT INTO t1 VALUES('hello','world');
INSERT INTO t1 VALUES(x'00',x'01');
SELECT quote(x), quote(y) FROM t1 ORDER BY rowid;
--result 1 2 'hello' 'world' X'00' X'01'
#endif

--testcase 110
INSERT INTO t1 VALUES(3,null);
--result SQLITE_CONSTRAINT {t1.y may not be NULL}

--if $configuration NOT IN ('dotlock','nolock')
/* OP_Halt:  Busy on a commit attempt */
--testcase 200
BEGIN;
SELECT * FROM t1 WHERE typeof(x)=='integer';
--result 1 2

--db 1
--open test.db
--testcase 201
SELECT * FROM t1 WHERE typeof(x)=='integer';
--result 1 2
--testcase 202
UPDATE t1 SET x=x+1 WHERE y=2;
--store $res
SELECT CASE
  WHEN $property_SHARED_CACHE 
       THEN $res=='SQLITE_LOCKED {database table is locked: t1}'
  WHEN $property_JOURNAL_WAL THEN $res==''
  ELSE $res=='SQLITE_BUSY {database is locked}'
  END
END;
--result 1
--db 0
--testcase 203
COMMIT;
--result
--endif

/* OP_Int64 */
--testcase 300
CREATE TABLE t300(
  a INT DEFAULT 2147483647,
  b INT DEFAULT 2147483648,
  c INT DEFAULT +9223372036854775807,
  d INT DEFAULT -2147483647,
  e INT DEFAULT -2147483648,
  f INT DEFAULT -9223372036854775808,
  g INT DEFAULT (-(-9223372036854775808)),
  h INT DEFAULT (-(-9223372036854775807))
);
INSERT INTO t300 DEFAULT VALUES;
SELECT * FROM t300;
--result 2147483647 2147483648 9223372036854775807 -2147483647 -2147483648 -9223372036854775808 9.22337203685478e+18 9223372036854775807

/* OP_String8 */
--testcase 400
CREATE TABLE t400(x TEXT);
--result
--testcase 401
--oom
INSERT INTO t400 VALUES('this is a long string that will require a memory allocation or to in order to insert. it is being used to test the memory allocation faiure logic in the OP_String8 opcode.');
--result
--testcase 402
SELECT * FROM t400;
--result {this is a long string that will require a memory allocation or to in order to insert. it is being used to test the memory allocation faiure logic in the OP_String8 opcode.}

/* OP_Concat */
--testcase 500
SELECT 'abc'||null;
--result nil
--testcase 501
SELECT null||'abc';
--result nil
--if th3eval('PRAGMA encoding')=='UTF-8'
--testcase 502
SELECT x'616263' || x'313233';
--result abc123
--endif
--testcase 503
--oom
SELECT (1234567890123456780 + 9) || (1234567890123456780 + 0)
--result 12345678901234567891234567890123456780
--testcase 504
SELECT th3_load_normal_extension();
SELECT th3_sqlite3_limit('SQLITE_LIMIT_LENGTH', 200);
--run 0
SELECT x||x||x||x||x FROM t400;
--result SQLITE_TOOBIG {string or blob too big}
--testcase 505
SELECT th3_sqlite3_limit('SQLITE_LIMIT_LENGTH', 1000000);
--run 0
--oom
SELECT x||' (extra)' FROM t400;
--result {this is a long string that will require a memory allocation or to in order to insert. it is being used to test the memory allocation faiure logic in the OP_String8 opcode. (extra)}
--testcase 506
--oom
SELECT zeroblob(100)||zeroblob(100);
--result {}

/* OP_Function and the sqlite3VdbeMemTooBig() call therein.  The only way
** to make the result too big is to make it go over-large when translated
** from UTF8 into UTF16 */
--testcase 600
PRAGMA encoding;
--store $enc
SELECT th3_sqlite3_limit('SQLITE_LIMIT_LENGTH',
                CASE WHEN $enc='UTF-8' THEN 100 ELSE 200 END);
--result nil
--testcase 602
SELECT replace('aaaaaaaaaaa','a','0123456789');
--result SQLITE_TOOBIG {string or blob too big}
--testcase 603
SELECT th3_sqlite3_limit('SQLITE_LIMIT_LENGTH', 1000000);
--result nil


/* OP_MustBeInt with a jump. */
--testcase 700

CREATE TABLE t700(x);
INSERT INTO t700(rowid,x) VALUES(1,'one');
INSERT INTO t700(rowid,x) VALUES(2,'two');
INSERT INTO t700(rowid,x) VALUES(3,'three');
SELECT x FROM t700 WHERE rowid IN (2,null,'hello',3) ORDER BY x;
--result three two
