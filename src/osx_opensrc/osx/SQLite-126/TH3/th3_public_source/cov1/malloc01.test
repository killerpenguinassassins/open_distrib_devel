/*
** Tests for the malloc.c source module.
**
** MODULE_NAME:               malloc01
** REQUIRED_PROPERTIES:       INITIALIZE_OK
** DISALLOWED_PROPERTIES:     THREADS NO_OOM NO_MEMSTATUS ZIPVFS
** MINIMUM_HEAPSIZE:          100000
*/
int malloc01(th3state *p){
  void *p1, *p2;
  int sz1;
  int sz2;

  sqlite3_shutdown();
  th3testBegin(p, "10");
  th3testCheckInt(p, 0, sqlite3_memory_used());

  th3testBegin(p, "20");
  sqlite3_memory_highwater(1);
  th3testCheckInt(p, 0, sqlite3_memory_highwater(0));

#ifndef SQLITE_OMIT_AUTOINIT
  th3testBegin(p, "30");
  th3oom.initReturn = SQLITE_INTERNAL;
  p1 = sqlite3_realloc(0, 100);
  sqlite3_free(p1);
  th3testCheckInt(p, 1, p1==0);
#else
  sqlite3_initialize();
#endif

  th3testBegin(p, "40");
  th3oom.initReturn = SQLITE_OK;
  p1 = sqlite3_realloc(0, 100);
  th3testCheckInt(p, 1, p1!=0 );
  th3testBegin(p, "41");
  sz1 = sqlite3_memory_used();
  th3testCheckInt(p, 1, sz1>=40);
  th3testBegin(p, "42");
  th3testCheckInt(p, 1, sqlite3_memory_highwater(0)>=sz1);

  th3testBegin(p, "50");
  p2 = sqlite3_realloc(p1, 0);
  sz2 = sqlite3_memory_used();
  th3testCheckInt(p, 1, p2==0);
  th3testBegin(p, "51");
  th3testCheckInt(p, 1, sz2<sz1);
  th3testBegin(p, "52");
  th3testCheckInt(p, 1, sqlite3_memory_highwater(1)>=sz1);
  th3testBegin(p, "53");
  th3testCheckInt(p, 1, sqlite3_memory_highwater(1)>=sz2);

  th3testBegin(p, "60");
  p1 = sqlite3_malloc(100);
  p2 = sqlite3_realloc(p1, 0x7fffffff);
  th3testCheckInt(p, 1, p1!=0 && p2==0);
  sqlite3_free(p1);
  sqlite3_free(p2);

  th3testBegin(p, "70");
  sqlite3_shutdown();
  th3testCheckInt(p, 0, sqlite3_memory_used());
  th3testBegin(p, "71");
  sqlite3_memory_highwater(1);
  th3testCheckInt(p, 0, sqlite3_memory_highwater(0));

#ifndef SQLITE_OMIT_AUTOINIT
  th3testBegin(p, "100");
  th3oom.initReturn = SQLITE_INTERNAL;
  p1 = sqlite3_malloc(100);
  sqlite3_free(p1);
  th3testCheckInt(p, 1, p1==0);
#else
  sqlite3_initialize();
#endif

  th3testBegin(p, "110");
  th3oom.initReturn = SQLITE_OK;
  p1 = sqlite3_malloc(100);
  th3testCheckInt(p, 1, p1!=0 );
  th3testBegin(p, "111");
  sz1 = sqlite3_memory_used();
  th3testCheckInt(p, 1, sz1>=40);
  th3testBegin(p, "112");
  th3testCheckInt(p, 1, sqlite3_memory_highwater(0)>=sz1);

  th3testBegin(p, "120");
  p2 = sqlite3_realloc(p1, 2000);
  th3testCheckInt(p, 1, p2!=0);
  th3testBegin(p, "121");
  sz2 = sqlite3_memory_used();
  th3testCheckInt(p, 1, sz2>=2000);
  th3testBegin(p, "122");
  th3testCheckInt(p, sz2, sqlite3_memory_highwater(1));
  th3testBegin(p, "123");
  th3testCheckInt(p, 1, sqlite3_memory_highwater(1)==sqlite3_memory_used());

  th3testBegin(p, "130");
  sqlite3_free(p2);
  sz1 = sqlite3_memory_used();
  th3testCheckInt(p, 1, sz1<2000);
  th3testBegin(p, "131");
  th3testCheckInt(p, 1, sqlite3_memory_highwater(1)>sqlite3_memory_used());
  th3testBegin(p, "132");
  th3testCheckInt(p, 1, sqlite3_memory_highwater(1)==sqlite3_memory_used());

  th3testBegin(p, "135");
  p1 = sqlite3_malloc(0);
  th3testCheckInt(p, 1, p1==0);

  th3testBegin(p, "140");
  p1 = sqlite3_malloc(0x7fffffff);
  th3testCheckInt(p, 1, p1==0);

  th3testBegin(p, "150");
  sqlite3_shutdown();
  th3testCheckInt(p, 0, sqlite3_memory_used());
  th3testBegin(p, "151");
  sqlite3_memory_highwater(1);
  th3testCheckInt(p, 0, sqlite3_memory_highwater(0));

  sqlite3_shutdown();
  th3testBegin(p, "200");
  sqlite3_config(SQLITE_CONFIG_MEMSTATUS, 0);
#ifdef SQLITE_OMIT_AUTOINIT
  sqlite3_initialize();
#endif
  p1 = sqlite3_malloc(100);
  th3testCheckInt(p, 1, p1!=0 );
  th3testBegin(p, "201");
  th3testCheckInt(p, 0, sqlite3_memory_used());
  th3testBegin(p, "202");
  th3testCheckInt(p, 0, sqlite3_memory_highwater(0));

  th3testBegin(p, "210");
  p2 = sqlite3_realloc(p1, 2000);
  th3testCheckInt(p, 1, p2!=0);
  th3testBegin(p, "211");
  sz1 = sqlite3_memory_used();
  th3testCheckInt(p, 0, sz1);
  th3testBegin(p, "212");
  th3testCheckInt(p, 0, sqlite3_memory_highwater(1));

  th3testBegin(p, "220");
  sqlite3_free(p2);
  th3testCheckInt(p, 0, sqlite3_memory_used());
  th3testBegin(p, "221");
  th3testCheckInt(p, 0, sqlite3_memory_highwater(0));

  th3testBegin(p, "300");
  sqlite3_shutdown();
  sqlite3_config(SQLITE_CONFIG_MEMSTATUS, 1);
#ifdef SQLITE_OMIT_AUTOINIT
  sqlite3_initialize();
#endif
  sqlite3_soft_heap_limit(1000);
  p1 = sqlite3_malloc(500);
  p2 = sqlite3_malloc(1000);
  sqlite3_free(p1);
  sqlite3_free(p2);
  th3testCheckInt(p, 1, p1!=0 && p2!=0);

  th3testBegin(p, "310");
  sqlite3_shutdown();
  sqlite3_soft_heap_limit(-1);
  p1 = sqlite3_malloc(500);
  p2 = sqlite3_malloc(1000);
  sqlite3_free(p1);
  sqlite3_free(p2);
  th3testCheckInt(p, 1, p1!=0 && p2!=0);

#ifndef SQLITE_OMIT_DEPRECATED
  sqlite3_memory_alarm(0,0,0);
#endif

  return 0; 
}
