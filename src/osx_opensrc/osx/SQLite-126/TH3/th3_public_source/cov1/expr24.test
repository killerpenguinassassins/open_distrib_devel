/*
** This module contains tests of expression handling.
**
** The focus of this module is conditional jump logic.
**
** SCRIPT_MODULE_NAME:        expr24
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(t,f,n);
INSERT INTO t1 VALUES(1,0,null);
--result

--testcase 101
SELECT CASE WHEN t AND t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 102
SELECT CASE WHEN t AND f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 103
SELECT CASE WHEN t AND n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 104
SELECT CASE WHEN f AND t THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 105
SELECT CASE WHEN f AND f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 106
SELECT CASE WHEN f AND n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 107
SELECT CASE WHEN n AND t THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 108
SELECT CASE WHEN n AND f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 109
SELECT CASE WHEN n AND n THEN 'yes' ELSE 'no' END FROM t1;
--result no

--testcase 111
SELECT CASE WHEN NOT(t AND t) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 112
SELECT CASE WHEN NOT(t AND f) THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 113
SELECT CASE WHEN NOT(t AND n) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 114
SELECT CASE WHEN NOT(f AND t) THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 115
SELECT CASE WHEN NOT(f AND f) THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 116
SELECT CASE WHEN NOT(f AND n) THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 117
SELECT CASE WHEN NOT(n AND t) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 118
SELECT CASE WHEN NOT(n AND f) THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 119
SELECT CASE WHEN NOT(n AND n) THEN 'yes' ELSE 'no' END FROM t1;
--result no

--testcase 121
SELECT CASE WHEN t OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 122
SELECT CASE WHEN t OR f THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 123
SELECT CASE WHEN t OR n THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 124
SELECT CASE WHEN f OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 125
SELECT CASE WHEN f OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 126
SELECT CASE WHEN f OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 127
SELECT CASE WHEN n OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 128
SELECT CASE WHEN n OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 129
SELECT CASE WHEN n OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no

--testcase 131
SELECT CASE WHEN NOT(t OR t) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 132
SELECT CASE WHEN NOT(t OR f) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 133
SELECT CASE WHEN NOT(t OR n) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 134
SELECT CASE WHEN NOT(f OR t) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 135
SELECT CASE WHEN NOT(f OR f) THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 136
SELECT CASE WHEN NOT(f OR n) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 137
SELECT CASE WHEN NOT(n OR t) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 138
SELECT CASE WHEN NOT(n OR f) THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 139
SELECT CASE WHEN NOT(n OR n) THEN 'yes' ELSE 'no' END FROM t1;
--result no

--testcase 201
SELECT CASE WHEN (t AND t) OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 202
SELECT CASE WHEN (t AND f) OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 203
SELECT CASE WHEN (t AND n) OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 204
SELECT CASE WHEN (f AND t) OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 205
SELECT CASE WHEN (f AND f) OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 206
SELECT CASE WHEN (f AND n) OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 207
SELECT CASE WHEN (n AND t) OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 208
SELECT CASE WHEN (n AND f) OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 209
SELECT CASE WHEN (n AND n) OR t THEN 'yes' ELSE 'no' END FROM t1;
--result yes

--testcase 211
SELECT CASE WHEN (t AND t) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 212
SELECT CASE WHEN (t AND f) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 213
SELECT CASE WHEN (t AND n) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 214
SELECT CASE WHEN (f AND t) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 215
SELECT CASE WHEN (f AND f) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 216
SELECT CASE WHEN (f AND n) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 217
SELECT CASE WHEN (n AND t) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 218
SELECT CASE WHEN (n AND f) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 219
SELECT CASE WHEN (n AND n) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no

--testcase 221
SELECT CASE WHEN (t AND t) OR n THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 222
SELECT CASE WHEN (t AND f) OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 223
SELECT CASE WHEN (t AND n) OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 224
SELECT CASE WHEN (f AND t) OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 225
SELECT CASE WHEN (f AND f) OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 226
SELECT CASE WHEN (f AND n) OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 227
SELECT CASE WHEN (n AND t) OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 228
SELECT CASE WHEN (n AND f) OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 229
SELECT CASE WHEN (n AND n) OR n THEN 'yes' ELSE 'no' END FROM t1;
--result no

--testcase 231
SELECT CASE WHEN (NOT (t AND t)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 232
SELECT CASE WHEN (NOT (t AND f)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 233
SELECT CASE WHEN (NOT (t AND n)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 234
SELECT CASE WHEN (NOT (f AND t)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 235
SELECT CASE WHEN (NOT (f AND f)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 236
SELECT CASE WHEN (NOT (f AND n)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 237
SELECT CASE WHEN (NOT (n AND t)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 238
SELECT CASE WHEN (NOT (n AND f)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 239
SELECT CASE WHEN (NOT (n AND n)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no

--testcase 240
SELECT CASE WHEN NOT t THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 241
SELECT CASE WHEN NOT f THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 242
SELECT CASE WHEN NOT n THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 243
SELECT CASE WHEN NOT NOT t THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 244
SELECT CASE WHEN NOT NOT f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 245
SELECT CASE WHEN NOT NOT n THEN 'yes' ELSE 'no' END FROM t1;
--result no

--testcase 251
SELECT CASE WHEN (NOT (t OR t)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 252
SELECT CASE WHEN (NOT (t OR f)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 253
SELECT CASE WHEN (NOT (t OR n)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 254
SELECT CASE WHEN (NOT (f OR t)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 255
SELECT CASE WHEN (NOT (f OR f)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result yes
--testcase 256
SELECT CASE WHEN (NOT (f OR n)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 257
SELECT CASE WHEN (NOT (n OR t)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 258
SELECT CASE WHEN (NOT (n OR f)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 259
SELECT CASE WHEN (NOT (n OR n)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no

--testcase 260
SELECT CASE WHEN (NOT NOT(f AND t)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 261
SELECT CASE WHEN (NOT NOT(f AND NULL)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 262
SELECT CASE WHEN (NOT NOT(t AND NULL)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 263
SELECT CASE WHEN (NOT NOT(f OR NULL)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result no
--testcase 264
SELECT CASE WHEN (NOT NOT(t OR NULL)) OR f THEN 'yes' ELSE 'no' END FROM t1;
--result yes


--testcase 300
CREATE TABLE t3(x);
INSERT INTO t3 VALUES(8);
SELECT CASE WHEN NOT x=8 THEN 'yes' ELSE 'no' END FROM t3;
--result no
--testcase 301
SELECT CASE WHEN NOT x=9 THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 302
SELECT CASE WHEN NOT x<>8 THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 303
SELECT CASE WHEN NOT x<>9 THEN 'yes' ELSE 'no' END FROM t3;
--result no
--testcase 304
SELECT CASE WHEN NOT x<8 THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 305
SELECT CASE WHEN NOT x<9 THEN 'yes' ELSE 'no' END FROM t3;
--result no
--testcase 306
SELECT CASE WHEN NOT x<=7 THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 307
SELECT CASE WHEN NOT x<=8 THEN 'yes' ELSE 'no' END FROM t3;
--result no
--testcase 308
SELECT CASE WHEN NOT x>8 THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 309
SELECT CASE WHEN NOT x>7 THEN 'yes' ELSE 'no' END FROM t3;
--result no
--testcase 310
SELECT CASE WHEN NOT x>=9 THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 311
SELECT CASE WHEN NOT x>=8 THEN 'yes' ELSE 'no' END FROM t3;
--result no

--testcase 320
SELECT CASE WHEN x NOT BETWEEN 7 AND 9 THEN 'yes' ELSE 'no' END FROM t3;
--result no
--testcase 321
SELECT CASE WHEN x NOT BETWEEN 9 AND 11 THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 322
SELECT CASE WHEN (x BETWEEN 7 AND 9) OR x=0 THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 323
SELECT x,CASE WHEN (x BETWEEN 9 AND 11) OR x=0 THEN 'yes' ELSE 'no' END FROM t3;
--result 8 no
--testcase 324
SELECT CASE WHEN x BETWEEN 7 AND 9 THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 325
SELECT x, CASE WHEN x BETWEEN 9 AND 11 THEN 'yes' ELSE 'no' END FROM t3;
--result 8 no
--testcase 326
SELECT CASE WHEN (x NOT BETWEEN 7 AND 9) OR x=0 THEN 'yes' ELSE 'no' END
  FROM t3;
--result no
--testcase 327
SELECT CASE WHEN (x NOT BETWEEN 9 AND 11) OR x=0 THEN 'yes' ELSE 'no' END
  FROM t3;
--result yes
--testcase 328
SELECT x, CASE WHEN (x NOT BETWEEN 7 AND 9) OR x=0 THEN 'yes' ELSE 'no' END
  FROM t3;
--result 8 no
--testcase 329
SELECT x, CASE WHEN x NOT BETWEEN 7 AND 9 THEN 'yes' ELSE 'no' END FROM t3;
--result 8 no

--testcase 330
SELECT CASE WHEN NOT x IS NULL THEN 'yes' ELSE 'no' END FROM t3;
--result yes
--testcase 331
SELECT CASE WHEN NOT x NOT NULL THEN 'yes' ELSE 'no' END FROM t3;
--result no
--testcase 332
SELECT CASE WHEN (x IS NULL) OR x=0 THEN 'yes' ELSE 'no' END FROM t3;
--result no
--testcase 333
SELECT x, CASE WHEN (x NOT NULL) OR x=0 THEN 'yes' ELSE 'no' END FROM t3;
--result 8 yes
--testcase 334
SELECT CASE WHEN x IS NULL THEN 'yes' ELSE 'no' END FROM t3;
--result no
--testcase 335
SELECT x, CASE WHEN x NOT NULL THEN 'yes' ELSE 'no' END FROM t3;
--result 8 yes

--testcase 400
CREATE TABLE t4(x INTEGER PRIMARY KEY);
INSERT INTO t4 VALUES(8);
SELECT CASE WHEN x IN (5,6,7) THEN 'yes' ELSE 'no' END FROM t4;
--result no
--testcase 401
SELECT CASE WHEN x IN (NULL,6,7) THEN 'yes' ELSE 'no' END FROM t4;
--result no
--testcase 402
SELECT CASE WHEN x IN (6,7,8) THEN 'yes' ELSE 'no' END FROM t4;
--result yes
--testcase 403
SELECT CASE WHEN x NOT IN (5,6,7) THEN 'yes' ELSE 'no' END FROM t4;
--result yes
--testcase 404
SELECT CASE WHEN x NOT IN (NULL,6,7) THEN 'yes' ELSE 'no' END FROM t4;
--result no
--testcase 405
SELECT CASE WHEN x NOT IN (6,7,8) THEN 'yes' ELSE 'no' END FROM t4;
--result no

--testcase 410
SELECT CASE WHEN x IN (5,6,7) OR x=0 THEN 'yes' ELSE 'no' END FROM t4;
--result no
--testcase 411
SELECT CASE WHEN x IN (NULL,6,7) OR x=0 THEN 'yes' ELSE 'no' END FROM t4;
--result no
--testcase 412
SELECT CASE WHEN x IN (6,7,8) OR x=0 THEN 'yes' ELSE 'no' END FROM t4;
--result yes
--testcase 413
SELECT CASE WHEN x NOT IN (5,6,7) OR x=0 THEN 'yes' ELSE 'no' END FROM t4;
--result yes
--testcase 414
SELECT CASE WHEN x NOT IN (NULL,6,7) OR x=0 THEN 'yes' ELSE 'no' END FROM t4;
--result no
--testcase 415
SELECT CASE WHEN x NOT IN (6,7,8) OR x=0 THEN 'yes' ELSE 'no' END FROM t4;
--result no
