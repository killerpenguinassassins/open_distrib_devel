/*
** This module contains tests for the build.c source file.
**
** This particular file focuses on the CREATE TABLE statement and
** on coverage testing of accessory routines such as sqlite3AddNotNull()
** and sqlite3AddPrimaryKey().
** 
** SCRIPT_MODULE_NAME:        build08
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a);
CREATE TABLE IF NOT EXISTS t1(a TEXT NOT NULL DEFAULT 'hello');
INSERT INTO t1 VALUES(NULL);
SELECT * FROM t1;
--result nil

--testcase 110
CREATE TABLE IF NOT EXISTS t1(a PRIMARY KEY, b CHECK(a>b));
SELECT * FROM t1;
--result nil
--testcase 111
CREATE TABLE IF NOT EXISTS t1(a TEXT COLLATE NO, b INTEGER DEFAULT 123);
SELECT * FROM t1;
--result nil

--testcase 120
CREATE TABLE t120(a INTEGER, PRIMARY KEY(x));
--result SQLITE_ERROR {table t120 has no column named x}

--testcase 130
CREATE TABLE t130(a TEXT PRIMARY KEY AUTOINCREMENT);
--result SQLITE_ERROR {AUTOINCREMENT is only allowed on an INTEGER PRIMARY KEY}

--testcase 200
CREATE TABLE t2(a,b,c,PRIMARY KEY(b,x,d));
--result SQLITE_ERROR {table t2 has no column named x}

/* A DESCENDING integer primary key is not the rowid if the PRIMARY KEY
** keyword is on the column declaration.  But is is a rowid if the
** PRIMARY KEY declaration is separate.  */
--testcase 300
CREATE TABLE t3a(a INTEGER, PRIMARY KEY (a ASC));
CREATE TABLE t3b(a INTEGER, PRIMARY KEY (a DESC));
CREATE TABLE t3c(a INTEGER PRIMARY KEY ASC);
CREATE TABLE t3d(a INTEGER PRIMARY KEY DESC);
INSERT INTO t3a VALUES(123);
INSERT INTO t3b VALUES(123);
INSERT INTO t3c VALUES(123);
INSERT INTO t3d VALUES(123);
SELECT rowid, a FROM t3a;
SELECT rowid, a FROM t3b;
SELECT rowid, a FROM t3c;
SELECT rowid, a FROM t3d;
--result 123 123 123 123 123 123 1 123

--testcase 400
CREATE TABLE t4(x INTEGER UNIQUE, y TEXT PRIMARY KEY COLLATE xyzzy);
--result SQLITE_ERROR {no such collation sequence: xyzzy}

--testcase 410
--oom
CREATE TABLE t4(x INTEGER UNIQUE, y TEXT PRIMARY KEY COLLATE nocase);
--result

--testcase 411
INSERT INTO t4 VALUES(1, 'abc');
INSERT INTO t4 VALUES(2, 'BCD');
SELECT y FROM t4 ORDER BY y;
--result abc BCD
