/*
** Verify that changing the sqlite3_soft_heap_limit() in the middle of a
** statement does no harm.
**
** MODULE_NAME:               malloc08
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     THREADS NO_OOM NO_MEMSTATUS
** MINIMUM_HEAPSIZE:          100000
*/
int malloc08(th3state *p){
  sqlite3_stmt *pStmt;
  int sum1, sum2;

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
    "CREATE TABLE t1(x INTEGER PRIMARY KEY, y BLOB);"
    "INSERT INTO t1 VALUES(1, zeroblob(2000));"
    "INSERT INTO t1 VALUES(2, zeroblob(100));"
    "INSERT INTO t1 VALUES(3, zeroblob(4000));"
    "INSERT INTO t1 VALUES(4, zeroblob(200));"
    "INSERT INTO t1 SELECT x+4, zeroblob("
          "CASE x%2 WHEN 0 THEN 100*(x+4) ELSE 1000*(x+4) END) FROM t1;"
    "SELECT sum(x), sum(length(y)) FROM t1;"
  );
  th3testCheck(p, "36 19700");

  sqlite3_soft_heap_limit(1000);
  th3testBegin(p, "101");
  th3dbEval(p, 0, "SELECT sum(x), sum(length(y)) FROM t1");
  th3testCheck(p, "36 19700");
   
  sqlite3_soft_heap_limit(1000000);
  th3testBegin(p, "102");
  th3dbEval(p, 0, "SELECT sum(x), sum(length(y)) FROM t1");
  th3testCheck(p, "36 19700");

  sqlite3_soft_heap_limit(1000);
  th3testBegin(p, "103");
  th3dbEval(p, 0, "SELECT sum(x), sum(length(y)) FROM t1");
  th3testCheck(p, "36 19700");

  sqlite3_soft_heap_limit(0);
  th3testBegin(p, "104");
  th3dbEval(p, 0, "SELECT sum(x), sum(length(y)) FROM t1");
  th3testCheck(p, "36 19700");

  th3testBegin(p, "110");
  pStmt = th3dbPrepare(p, 0, "SELECT x, length(y) FROM t1");
  sum1 = sum2 = 0;
  while( sqlite3_step(pStmt)==SQLITE_ROW ){
    int x = sqlite3_column_int(pStmt, 0);
    sqlite3_int64 y = sqlite3_column_int64(pStmt, 1);
    sqlite3_soft_heap_limit64(y*10);
    if( y*10 != sqlite3_soft_heap_limit64(-1) ) break;   
    sum1 += x;
    sum2 += y;
  }
  sqlite3_finalize(pStmt);
  th3testCheckInt(p, 36, sum1);
  th3testBegin(p, "111");
  th3testCheckInt(p, 19700, sum2);

  return 0; 
}
