/*
** Tests against write-ahead logging logic
**
** MIXED_MODULE_NAME:         wal05
** REQUIRED_PROPERTIES:       JOURNAL_WAL
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
/*
** This is a wal_hook callback for testing.  It records the
** database name and nFrame parameters on the test result buffer.
** It returns the value of a variable whose name is derived from
** the zDb by prepending a '$'.  Hence is zDb is "main", the
** return value will be the integer value of bind variable "$main".
*/
static int wal05_hook(
  void *pClientData,     /* Argument */
  sqlite3 *db,           /* Connection */
  const char *zDb,       /* Database */
  int nFrame             /* Size of WAL */
){
  th3state *p = (th3state*)pClientData;
  char zBuf[20];
  th3testAppendResultTerm(p, zDb);
  sqlite3_snprintf(sizeof(zBuf), zBuf, "%d", nFrame);
  th3testAppendResultTerm(p, zBuf);
  sqlite3_snprintf(sizeof(zBuf), zBuf, "$%s", zDb);
  return (int)th3valueInt64(p, zBuf, SQLITE_OK);
}

/*
** Install the testing wal_hook on database 0.
*/
static void wal05_install_hook(th3state *p, int iDb, char *zArg){
  sqlite3 *db0 = th3dbPointer(p, iDb);
  sqlite3_wal_hook(db0, wal05_hook, (void*)p);
}

/* Begin test scripts.
**
** Construct a database connection with 3 different attached databases.
*/
--new-filename f2.db
--testcase 100
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(1);
ATTACH :filename AS f2;
PRAGMA f2.journal_mode=WAL;
CREATE TABLE f2.t2(y);
INSERT INTO t2 VALUES(2);
SELECT * FROM t1, t2;
--result wal 1 2

--new-filename f3.db
--testcase 101
ATTACH :filename AS f3;
PRAGMA f3.journal_mode=WAL;
CREATE TABLE f3.t3(z);
INSERT INTO t3 VALUES(3);
SELECT * FROM t1, t2, t3;
--result wal 1 2 3

--new-filename f4.db
--testcase 102
ATTACH :filename AS f4;
PRAGMA f4.journal_mode=WAL;
CREATE TABLE f4.t4(w);
INSERT INTO t4 VALUES(4);
SELECT * FROM t1, t2, t3, t4;
--result wal 1 2 3 4

/* Verify that all checkpoint hooks fire.
*/
--call wal05_install_hook
--testcase 110
PRAGMA wal_checkpoint;
BEGIN;
UPDATE t1 SET x=10;
UPDATE t2 SET y=20;
UPDATE t3 SET z=30;
UPDATE t4 SET w=40;
SELECT * FROM t1, t2, t3, t4;
--glob # # # 10 20 30 40

/* When the sector size is greater than the page size, some extra pages
** are appended to the WAL which throws off the counts in the following
** test.  So bypass this test if the sector size exceeds the page size.
*/
--testcase 111
PRAGMA page_size;
--store $pgsz
--result
--if ($pgsz*2) >= $sector_size
--testcase 112
COMMIT;
--result main 2 f2 2 f3 2 f4 2
--else
--testcase 112x
COMMIT;
--run 0
--result
--endif

/* Repeat, but this time fail the f2 wal_hook. */
--testcase 120
BEGIN;
UPDATE t1 SET x=100;
UPDATE t2 SET y=200;
UPDATE t3 SET z=300;
UPDATE t4 SET w=400;
SELECT * FROM t1, t2, t3, t4;
--result 100 200 300 400
--store $f2 SELECT 1
--if ($pgsz*2) >= $sector_size
--testcase 121
COMMIT;
--result main 4 f2 4 SQLITE_ERROR {SQL logic error or missing database}
--endif
