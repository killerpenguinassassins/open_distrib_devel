/*
** This module contains tests of the VIRTUAL TABLE.
**
** MODULE_NAME:               vtab02
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     SHARED_CACHE MEMDB
** MINIMUM_HEAPSIZE:          100000
*/

/*
** Client data for the custom virtual table.  The new virtual table
** module reuses many of the BVS virtual table routines and so the
** actual client data is a pointer to the th3state.  But th3state.pTestData
** will point to an instance of this structure.
*/
typedef struct vtab02_appdata vtab02_appdata;
struct vtab02_appdata {
  int iMagic;               /* Magic number for sanity */
  int nDestroy;             /* Number of destructor calls */
};

/* Allowed values for vtab02_appdata */
#define VTAB02_MAGIC_OPEN   0xc87145a2
#define VTAB02_MAGIC_CLOSED 0x9f584ca7

/* Destructor for the custom virtual table */
static void vtab02_destroy(void *pAppData){
  th3state *p = (th3state*)pAppData;
  vtab02_appdata *pData = (vtab02_appdata*)p->pTestData;
  assert( pData->iMagic==VTAB02_MAGIC_OPEN );
  pData->nDestroy++;
  pData->iMagic = VTAB02_MAGIC_CLOSED;
}

/*
** A function to return its first non-NULL argument.
*/
static void vtab02_ifnull(
  sqlite3_context *context,
  int argc,
  sqlite3_value **argv
){
  int i;
  for(i=0; i<argc; i++){
    if( SQLITE_NULL!=sqlite3_value_type(argv[i]) ){
      sqlite3_result_value(context, argv[i]);
      break;
    }
  }
}

/*
** An xFindFunction implementation.
*/
static int vtab02_findfunc(
  sqlite3_vtab *pVtab,
  int nArg,
  const char *zName,
  void (**pxFunc)(sqlite3_context*,int,sqlite3_value**),
  void **ppArg
){
  if( strcmp(zName,"vt_ifnull")==0 ){
    *pxFunc = vtab02_ifnull;
    *ppArg = 0;
    return 1;
  }
  return 0;
}



/* Definition of the custom virtual table */
static const sqlite3_module vtab02_module = {
  /* iVersion      */  1,
  /* xCreate       */  th3bvsvtabConnect,
  /* xConnect      */  th3bvsvtabConnect,
  /* xBestIndex    */  th3bvsvtabBestIndex,
  /* xDisconnect   */  th3bvsvtabDisconnect,
  /* xDestroy      */  th3bvsvtabDisconnect,
  /* xOpen         */  th3bvsvtabOpen,
  /* xClose        */  th3bvsvtabClose,
  /* xFilter       */  th3bvsvtabFilter,
  /* xNext         */  th3bvsvtabNext,
  /* xEof          */  th3bvsvtabEof,
  /* xColumn       */  th3bvsvtabColumn,
  /* xRowid        */  th3bvsvtabRowid,
  /* xUpdate       */  th3bvsvtabUpdate, 
  /* xBegin        */  0, 
  /* xSync         */  0,
  /* xCommit       */  0, 
  /* xRollback     */  0,
  /* xFindFunction */  vtab02_findfunc,
  /* Rename        */  th3bvsvtabRename
};

/* The test routine */
int vtab02(th3state *p){
  sqlite3 *db;
  int rc;
  vtab02_appdata sData;
  
  /* Verify that the module destructor is called when a database
  ** connection closes 
  */
  th3testBegin(p, "100");
  p->pTestData = (void*)&sData;
  memset(&sData, 0, sizeof(sData));
  sData.iMagic = VTAB02_MAGIC_OPEN;
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  rc = sqlite3_create_module_v2(db, "vtab02", &vtab02_module, (void*)p,
                                vtab02_destroy);
  th3dbClose(p, 0);
  th3testCheckInt(p, 1, sData.nDestroy);
  th3testBegin(p, "101");
  th3testCheckInt(p, VTAB02_MAGIC_CLOSED, sData.iMagic);

  /* Memory allocation failure during module creation */
  th3oomBegin(p, "200");
  while( th3oomNext(p) ){
    int rc;
    th3dbNew(p, 0, "test.db");
    memset(&sData, 0, sizeof(sData));
    sData.iMagic = VTAB02_MAGIC_OPEN;
    db = th3dbPointer(p, 0);
    th3oomEnable(p, 1);
    rc = sqlite3_create_module_v2(db, "vtab02", &vtab02_module, (void*)p,
                                  vtab02_destroy);
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK );
    }
  }
  th3oomEnd(p);

  /* Memory allocation failure during module creation */
  th3oomBegin(p, "201");
  while( th3oomNext(p) ){
    int rc;
    th3dbNew(p, 0, "test.db");
    db = th3dbPointer(p, 0);
    th3oomEnable(p, 1);
    rc = sqlite3_create_module(db, "vtab02", &vtab02_module, (void*)p);
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK );
    }
  }
  th3oomEnd(p);
  th3dbClose(p, 0);

  /* It is illegal to call sqlite3_declare_vtab() from outside of a
  ** virtual table constructor.  It should return SQLITE_MISUSE.
  */
  th3testBegin(p, "300");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  rc = sqlite3_declare_vtab(db, "CREATE TABLE x(a,b,c)");
  th3testCheckInt(p, SQLITE_MISUSE, rc);

  /* Create a new module and a VIRTUAL TABLE on connection 0.  Then
  ** try to open that same file on connection 1.  Because the module
  ** is not registered, it should throw an error.
  */
  th3testBegin(p, "400");
  sqlite3_create_module(db, "vtab02", &vtab02_module, (void*)p);
  th3dbEval(p, 0,
     "CREATE VIRTUAL TABLE vt1 USING vtab02;"
     "SELECT * FROM vt1;"
  );
  th3testCheck(p, "");
  th3testBegin(p, "401");
  th3dbOpen(p, 1, "test.db", 0);
  th3dbEval(p, 1, "SELECT * FROM vt1");
  th3testCheck(p, "SQLITE_ERROR {no such module: vtab02}");

  th3testBegin(p, "402");
  th3dbEval(p, 1, "DROP TABLE vt1");
  th3testCheck(p, "SQLITE_ERROR {no such module: vtab02}");
  th3dbClose(p, 1);

#ifndef SQLITE_OMIT_FLOATING_POINT
# define VTAB02_B 45.75
# define VTAB02_zB "45.75"
#else
# define VTAB02_B 45
# define VTAB02_zB "45"
#endif

  /* Function overloading
  */
  th3testBegin(p, "500");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  th3bindInt(p, "a", 123);
#ifndef SQLITE_OMIT_FLOATING_POINT
  th3bindDouble(p, "b", VTAB02_B);
#else
  th3bindInt(p, "b", VTAB02_B);
#endif
  th3bindNull(p, "c");
  th3dbEval(p, 0,
     "CREATE VIRTUAL TABLE vt1 USING bvs;"
     "SELECT * FROM vt1 ORDER BY name;"
  );
  th3testCheck(p, "a 123 b "VTAB02_zB" c nil");

  th3testBegin(p, "510");
  sqlite3_overload_function(db, "whatever", 2);
  sqlite3_overload_function(db, "vt_ifnull", 2);
  th3dbEval(p, 0, "SELECT whatever(name, 'hi'), value FROM vt1 ORDER BY name");
  th3testCheck(p, "SQLITE_ERROR {unable to use function whatever in the requested context}");

  th3testBegin(p, "511");
  th3dbEval(p, 0, "SELECT whatever(value+1, 'hi'), value FROM vt1");
  th3testCheck(p, "SQLITE_ERROR {unable to use function whatever in the requested context}");

  th3testBegin(p, "512");
  th3dbEval(p, 0, "CREATE TABLE t1(a,b,c); INSERT INTO t1 VALUES(11,22,33);");
  th3dbEval(p, 0, "SELECT whatever(a, 'hi'), value FROM t1, vt1");
  th3testCheck(p, "SQLITE_ERROR {unable to use function whatever in the requested context}");

  th3testBegin(p, "520");
  sqlite3_create_module(db, "vtab02", &vtab02_module, (void*)p);
  th3dbEval(p, 0,
     "CREATE VIRTUAL TABLE vt2 USING vtab02;"
     "SELECT name, vt_ifnull(value, 999) FROM vt2 ORDER BY name;"
  );
  th3testCheck(p, "a 123 b "VTAB02_zB" c 999");

  th3oomBegin(p, "521");
  while( th3oomNext(p) ){
    th3oomEnable(p, 1);
    rc = th3dbEval(p, 0,
       "SELECT name, VT_IfNull(value, 999) FROM vt2 ORDER BY name;"
    );
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM);
    }else{
      th3oomResult(p, 2, "a 123 b "VTAB02_zB" c 999");
    }
  }
  th3oomEnd(p);

  th3testBegin(p, "522");
  th3dbEval(p, 0,
     "SELECT name, Whatever(value, 999) FROM vt2 ORDER BY name;"
  );
  th3testCheck(p, "SQLITE_ERROR {unable to use function whatever in the requested context}");

  th3testBegin(p, "523");
  th3dbEval(p, 0,
     "SELECT name, vt_ifnull(value, 999) FROM vt1 ORDER BY name;"
  );
  th3testCheck(p, "SQLITE_ERROR {unable to use function vt_ifnull in the requested context}");

  return 0;
}
