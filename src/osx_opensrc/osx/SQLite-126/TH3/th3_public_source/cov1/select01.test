/*
** This module contains tests of the SELECT statement.
**
** The focus of this module is ORDER BY and LIMIT clauses on simple
** SELECT statements.
**
** SCRIPT_MODULE_NAME:        select01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--prepare_v2 0
--testcase 100
CREATE TABLE t1(a,b,c,d);
INSERT INTO t1 VALUES(1,'abc',9,5);
INSERT INTO t1 VALUES(2,'BCD',8,7);
INSERT INTO t1 VALUES(3,'cde',7,3);
INSERT INTO t1 VALUES(4,'DEF',6,6);
INSERT INTO t1 VALUES(5,'efg',5,2);
INSERT INTO t1 VALUES(6,'fgh',4,8);
SELECT b FROM t1 ORDER BY a LIMIT 2 OFFSET 2;
--result cde DEF

--testcase 101
SELECT b FROM t1 ORDER BY a DESC LIMIT 2 OFFSET 2;
--result DEF cde

/* LIMIT 0 means show nothing */
--testcase 102
SELECT b FROM t1 ORDER BY a LIMIT 0 OFFSET 2;
--result

/* LIMIT -1 means show everything */
--testcase 103
SELECT b FROM t1 ORDER BY a LIMIT -1 OFFSET 2;
--result cde DEF efg fgh

--testcase 104
--oom
SELECT * FROM t1 ORDER BY a LIMIT 2;
--result 1 abc 9 5 2 BCD 8 7

--testcase 105
SELECT DISTINCT a FROM t1 WHERE b LIKE 'abc';
--result 1

--testcase 110
SELECT 3
--store :N
SELECT b FROM t1 ORDER BY a LIMIT :N
--result abc BCD cde
--testcase 111
SELECT b FROM t1 ORDER BY a LIMIT :N-1 OFFSET :N
--result DEF efg
--testcase 112
SELECT b FROM t1 ORDER BY a LIMIT :N OFFSET :N-1
--result cde DEF efg
--testcase 113
SELECT b FROM t1 ORDER BY a LIMIT :N, :N-1
--result DEF efg
--testcase 114
SELECT b FROM t1 ORDER BY a LIMIT :N-1, :N
--result cde DEF efg

--testcase 120
SELECT b FROM t1 ORDER BY a*10 DESC LIMIT 2 OFFSET 2;
--result DEF cde

--testcase 130
SELECT b FROM t1 ORDER BY b LIMIT 2 OFFSET 2;
--result abc cde
--testcase 131
SELECT b FROM t1 ORDER BY b COLLATE nocase LIMIT 2 OFFSET 2;
--result cde DEF

--testcase 140
SELECT b FROM t1 ORDER BY a LIMIT -1 OFFSET -1;
--result abc BCD cde DEF efg fgh
--testcase 141
SELECT b FROM t1 ORDER BY a LIMIT -1 OFFSET 0;
--result abc BCD cde DEF efg fgh
--testcase 142
SELECT b FROM t1 ORDER BY a LIMIT -1 OFFSET 1;
--result BCD cde DEF efg fgh
--testcase 143
SELECT b FROM t1 ORDER BY a LIMIT -1 OFFSET 99;
--result

--testcase 150
SELECT ALL t1.* FROM t1 WHERE 1 ORDER BY main.t1.a LIMIT 2 OFFSET 3;
--result 4 DEF 6 6 5 efg 5 2
--testcase 151
SELECT DISTINCT t1.* FROM t1 WHERE 1 ORDER BY main.t1.a LIMIT 2 OFFSET 3;
--result 4 DEF 6 6 5 efg 5 2

--testcase 160
SELECT DISTINCT t1.*, * FROM t1 ORDER BY a LIMIT 1;
--result 1 abc 9 5 1 abc 9 5
--testcase 161
SELECT DISTINCT t1.*, t1.* FROM t1 ORDER BY a LIMIT 1;
--result 1 abc 9 5 1 abc 9 5
--testcase 162
SELECT DISTINCT x.*, * FROM t1 AS x ORDER BY x.a LIMIT 1;
--result 1 abc 9 5 1 abc 9 5
--testcase 163
SELECT DISTINCT x.*, * FROM t1 x ORDER BY x.a LIMIT 1;
--result 1 abc 9 5 1 abc 9 5

--testcase 200
SELECT * FROM (SELECT a, b FROM t1 UNION ALL SELECT c, b FROM t1)
 ORDER BY a, b DESC;
--result 1 abc 2 BCD 3 cde 4 fgh 4 DEF 5 efg 5 efg 6 fgh 6 DEF 7 cde 8 BCD 9 abc
--testcase 201
SELECT x, b FROM (SELECT a AS x, b FROM t1 UNION ALL SELECT c, b FROM t1)
 ORDER BY x, b DESC;
--result 1 abc 2 BCD 3 cde 4 fgh 4 DEF 5 efg 5 efg 6 fgh 6 DEF 7 cde 8 BCD 9 abc
--testcase 202
SELECT y.x, y.b FROM (SELECT a AS x, b FROM t1 UNION ALL SELECT c, b FROM t1) y
 ORDER BY y.x, y.b DESC;
--result 1 abc 2 BCD 3 cde 4 fgh 4 DEF 5 efg 5 efg 6 fgh 6 DEF 7 cde 8 BCD 9 abc
--testcase 203
SELECT y.* FROM (SELECT a AS x, b FROM t1 UNION ALL SELECT c, b FROM t1) AS y
 ORDER BY 1, 2 DESC;
--result 1 abc 2 BCD 3 cde 4 fgh 4 DEF 5 efg 5 efg 6 fgh 6 DEF 7 cde 8 BCD 9 abc
--testcase 204
SELECT DISTINCT y.*
  FROM (SELECT a AS x, b FROM t1 UNION ALL SELECT c, b FROM t1) AS y
 ORDER BY 1, 2 DESC;
--result 1 abc 2 BCD 3 cde 4 fgh 4 DEF 5 efg 6 fgh 6 DEF 7 cde 8 BCD 9 abc
--testcase 205
SELECT ALL y.*
  FROM (SELECT DISTINCT a AS x, b FROM t1
        UNION ALL SELECT DISTINCT c, b FROM t1) AS y
 ORDER BY 1, 2 DESC;
--result 1 abc 2 BCD 3 cde 4 fgh 4 DEF 5 efg 5 efg 6 fgh 6 DEF 7 cde 8 BCD 9 abc
