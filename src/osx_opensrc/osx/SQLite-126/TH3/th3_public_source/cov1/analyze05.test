/*
** Module for testing the ANALYZE statement. 
**
** The focus is on WHERE clause processing of SQLITE_STAT2
** data.
**
** SCRIPT_MODULE_NAME:        analyze05
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     MEMDB SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
*/
#ifdef SQLITE_ENABLE_STAT2
--testcase 100
SELECT th3_load_normal_extension();
--result nil


--testcase 110
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b TEXT COLLATE TH3_BINARY16LE,
  c TEXT COLLATE TH3_BINARY16BE
);
INSERT INTO t1 VALUES(1, null, null);
INSERT INTO t1 VALUES(2, null, null);
INSERT INTO t1 SELECT a+2, null, null FROM t1;
INSERT INTO t1 SELECT a+4, null, null FROM t1;
INSERT INTO t1 SELECT a+8, null, null FROM t1;
INSERT INTO t1 SELECT a+16, null, null FROM t1;
INSERT INTO t1 SELECT a+32, null, null FROM t1;
INSERT INTO t1 SELECT a+64, null, null FROM t1;
UPDATE t1 SET
  b = 'xyz' || ((a*9)%100 + 100 + ((a%2)==1)*300), /* 100..199 and 400..499 */
  c = 'xyz' || ((a*9)%200 + 200)                   /* 200..399 */
;
CREATE INDEX t1b ON t1(b);
CREATE INDEX t1c ON t1(c);
ANALYZE;
--result

--testcase 110
--oom
EXPLAIN QUERY PLAN
SELECT a FROM t1
 WHERE b BETWEEN 'xyz190' AND 'xyz390'
   AND c BETWEEN 'xyz190' AND 'xyz390';
--glob * INDEX t1b *
--testcase 111
EXPLAIN QUERY PLAN
SELECT a FROM t1
 WHERE b BETWEEN 'xyz110' AND 'xyz210'
   AND c BETWEEN 'xyz110' AND 'xyz210';
--glob * INDEX t1c *
--testcase 112
EXPLAIN QUERY PLAN
SELECT a FROM t1
 WHERE b BETWEEN 'xyz390' AND 'xyz500'
   AND c BETWEEN 'xyz390' AND 'xyz500';
--glob * INDEX t1c *
--testcase 113
EXPLAIN QUERY PLAN
SELECT a FROM t1
 WHERE b BETWEEN 'xyz210' AND 'xyz410'
   AND c BETWEEN 'xyz210' AND 'xyz410';
--glob * INDEX t1b *

/* Test the ability to analyze columns consisting of zero-length strings */
--testcase 120
CREATE TABLE t120 AS SELECT a, b, c FROM t1;
CREATE INDEX t120b ON t120(b);
UPDATE t120 SET b='' WHERE a>64;
ANALYZE;
SELECT a FROM t120 WHERE b BETWEEN 'a' AND 'x';
--result

--db 1
--open test.db
--testcase 200
EXPLAIN QUERY PLAN
SELECT a FROM t1
 WHERE b BETWEEN 'xyz190' AND 'xyz390'
   AND c BETWEEN 'xyz190' AND 'xyz390';
--result SQLITE_ERROR {no such collation sequence: TH3_BINARY16LE}

--db 0
--testcase 300
CREATE TABLE t3(a INTEGER PRIMARY KEY, b, c);
INSERT INTO t3 SELECT * FROM t1;
INSERT INTO t3 SELECT NULL, NULL, random() FROM t1 LIMIT 30;
INSERT INTO t3 SELECT NULL, NULL, th3randomBlob(10) FROM t1 LIMIT 30;
CREATE INDEX t3b ON t3(b);
CREATE INDEX t3c ON t3(c);
ANALYZE;
--result

--testcase 310
--oom
EXPLAIN QUERY PLAN
SELECT a FROM t3
 WHERE b BETWEEN 'xyz190' AND 'xyz390'
   AND c BETWEEN 'xyz190' AND 'xyz390';
--glob * INDEX t3b *
--testcase 311
EXPLAIN QUERY PLAN
SELECT a FROM t3
 WHERE b BETWEEN 'xyz110' AND 'xyz210'
   AND c BETWEEN 'xyz110' AND 'xyz210';
--glob * INDEX t3c *
--testcase 312
EXPLAIN QUERY PLAN
SELECT a FROM t3
 WHERE b BETWEEN 'xyz390' AND 'xyz500'
   AND c BETWEEN 'xyz390' AND 'xyz500';
--glob * INDEX t3c *
--testcase 313
EXPLAIN QUERY PLAN
SELECT a FROM t3
 WHERE b BETWEEN 'xyz210' AND 'xyz410'
   AND c BETWEEN 'xyz210' AND 'xyz410';
--glob * INDEX t3b *

--testcase 320
EXPLAIN QUERY PLAN
SELECT a FROM t3
 WHERE b BETWEEN 'xyz390' AND x'00'
   AND c BETWEEN 'xyz390' AND x'00';
--glob * INDEX t3c *


#endif /* SQLITE_ENABLE_STAT2 */
