/*
** This module contains tests of vdbe.c source module.
**
** SCRIPT_MODULE_NAME:        vdbe19
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** The OP_SeekLt, OP_SeekLe, OP_SeekGt, and OP_SeekGe opcodes
**
** When constructing the test database, add zeroblobs() that are half the
** page size.  This will force the minimum fan-out of 4 rows/page.
*/
--testcase 100
CREATE TABLE t1(a, b, c, UNIQUE(a,c));
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES('b',1, zeroblob($pgsz/2));
INSERT INTO t1 VALUES('c',2, zeroblob($pgsz/2));
INSERT INTO t1 VALUES('d',3, zeroblob($pgsz/2));
INSERT INTO t1 VALUES('e',4, zeroblob($pgsz/2));
INSERT INTO t1 VALUES('f',5, zeroblob($pgsz/2));
SELECT b FROM t1 ORDER BY a;
--result 1 2 3 4 5

--testcase 110
--oom
SELECT b FROM t1 WHERE a>'d' ORDER BY a;
--result 4 5
--testcase 111
SELECT b FROM t1 WHERE a>='d' ORDER BY a;
--result 3 4 5
--testcase 112
SELECT b FROM t1 WHERE a<'d' ORDER BY a;
--result 1 2
--testcase 113
SELECT b FROM t1 WHERE a<='d' ORDER BY a;
--result 1 2 3
--testcase 114
SELECT b FROM t1 WHERE a>'d' ORDER BY a DESC;
--result 5 4
--testcase 115
SELECT b FROM t1 WHERE a>='d' ORDER BY a DESC;
--result 5 4 3
--testcase 116
SELECT b FROM t1 WHERE a<'d' ORDER BY a DESC;
--result 2 1
--testcase 117
SELECT b FROM t1 WHERE a<='d' ORDER BY a DESC;
--result 3 2 1

--testcase 120
SELECT b FROM t1 WHERE a>'a' ORDER BY a;
--result 1 2 3 4 5
--testcase 121
SELECT b FROM t1 WHERE a>='a' ORDER BY a;
--result 1 2 3 4 5
--testcase 122
SELECT b FROM t1 WHERE a<'a' ORDER BY a;
--result 
--testcase 123
SELECT b FROM t1 WHERE a<='a' ORDER BY a;
--result 
--testcase 124
SELECT b FROM t1 WHERE a>'a' ORDER BY a DESC;
--result 5 4 3 2 1
--testcase 125
SELECT b FROM t1 WHERE a>='a' ORDER BY a DESC;
--result 5 4 3 2 1
--testcase 126
SELECT b FROM t1 WHERE a<'a' ORDER BY a DESC;
--result 
--testcase 127
SELECT b FROM t1 WHERE a<='a' ORDER BY a DESC;
--result 

--testcase 130
SELECT b FROM t1 WHERE a>'z' ORDER BY a;
--result 
--testcase 131
SELECT b FROM t1 WHERE a>='z' ORDER BY a;
--result 
--testcase 132
SELECT b FROM t1 WHERE a<'z' ORDER BY a;
--result 1 2 3 4 5
--testcase 133
SELECT b FROM t1 WHERE a<='z' ORDER BY a;
--result 1 2 3 4 5
--testcase 134
SELECT b FROM t1 WHERE a>'z' ORDER BY a DESC;
--result 
--testcase 135
SELECT b FROM t1 WHERE a>='z' ORDER BY a DESC;
--result 
--testcase 136
SELECT b FROM t1 WHERE a<'z' ORDER BY a DESC;
--result 5 4 3 2 1
--testcase 137
SELECT b FROM t1 WHERE a<='z' ORDER BY a DESC;
--result 5 4 3 2 1
