/*
** A module used to test FTS3 snippet and offsets functions.
**
** SCRIPT_MODULE_NAME:        fts3_04
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
#ifdef SQLITE_ENABLE_FTS3
--testcase 100
CREATE VIRTUAL TABLE t1 USING fts3(a,b);
INSERT INTO t1(docid,a,b)
VALUES(1,
'one two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo twentythree twentyfour twentyfive',
'alpha bravo charlie delta echo foxtrot golf hotel india juliet kilo lima mike november oscar papa quebec romeo sierra tango uniform victor whiskey xray yankee zulu');
--result

--testcase 200
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'one';
--result {[one] two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}
--testcase 201
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE a MATCH 'one';
--result {[one] two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}
--testcase 202
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:one';
--result {[one] two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}

--testcase 210
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'two';
--result {one [two] three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}
--testcase 211
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE a MATCH 'two';
--result {one [two] three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}
--testcase 212
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:two';
--result {one [two] three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}

--testcase 220
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'three';
--result {one two [three] four five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}
--testcase 221
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE a MATCH 'three';
--result {one two [three] four five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}
--testcase 222
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:three';
--result {one two [three] four five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}

--testcase 230
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'four';
--result {one two three [four] five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}
--testcase 231
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE a MATCH 'four';
--result {one two three [four] five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}
--testcase 232
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:four';
--result {one two three [four] five six seven eight nine ten eleven twelve thirteen fourteen fifteen...}

--testcase 240
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'twentytwo';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone [twentytwo] twentythree twentyfour twentyfive}
--testcase 241
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE a MATCH 'twentytwo';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone [twentytwo] twentythree twentyfour twentyfive}
--testcase 242
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:twentytwo';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone [twentytwo] twentythree twentyfour twentyfive}

--testcase 250
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'twentythree';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo [twentythree] twentyfour twentyfive}
--testcase 251
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE a MATCH 'twentythree';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo [twentythree] twentyfour twentyfive}
--testcase 252
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:twentythree';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo [twentythree] twentyfour twentyfive}

--testcase 260
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'twentyfour';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo twentythree [twentyfour] twentyfive}
--testcase 261
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE a MATCH 'twentyfour';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo twentythree [twentyfour] twentyfive}
--testcase 262
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:twentyfour';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo twentythree [twentyfour] twentyfive}

--testcase 270
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'twentyfive';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]}
--testcase 271
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE a MATCH 'twentyfive';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]}
--testcase 272
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:twentyfive';
--result {...eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]}

--testcase 300
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'twentyfive alpha';
--result {...eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]...[alpha] bravo charlie delta echo foxtrot golf hotel...}
--testcase 301
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:twentyfive alpha';
--result {...eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]...[alpha] bravo charlie delta echo foxtrot golf hotel...}
--testcase 302
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'twentyfive b:alpha';
--result {...eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]...[alpha] bravo charlie delta echo foxtrot golf hotel...}
--testcase 303
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'a:twentyfive b:alpha';
--result {...eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]...[alpha] bravo charlie delta echo foxtrot golf hotel...}
--testcase 304
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'alpha twentyfive';
--result {...eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]...[alpha] bravo charlie delta echo foxtrot golf hotel...}
--testcase 305
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'alpha a:twentyfive';
--result {...eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]...[alpha] bravo charlie delta echo foxtrot golf hotel...}
--testcase 306
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'b:alpha twentyfive';
--result {...eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]...[alpha] bravo charlie delta echo foxtrot golf hotel...}
--testcase 307
SELECT snippet(t1,'[',']','...') FROM t1
 WHERE t1 MATCH 'b:alpha a:twentyfive';
--result {...eighteen nineteen twenty twentyone twentytwo twentythree twentyfour [twentyfive]...[alpha] bravo charlie delta echo foxtrot golf hotel...}

--testcase 400
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'one nine';
--result {[one] two three four five six seven eight [nine] ten eleven twelve thirteen fourteen fifteen...}
--testcase 401
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'nine one';
--result {[one] two three four five six seven eight [nine] ten eleven twelve thirteen fourteen fifteen...}
--testcase 402
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'one nine one nine';
--result {[one] two three four five six seven eight [nine] ten eleven twelve thirteen fourteen fifteen...}
--testcase 403
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'nine OR one';
--result {[one] two three four five six seven eight [nine] ten eleven twelve thirteen fourteen fifteen...}
--testcase 404
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'xyzzy OR nine OR one ';
--result {[one] two three four five six seven eight [nine] ten eleven twelve thirteen fourteen fifteen...}

--testcase 410
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'one ten';
--result {[one] two three four five six seven eight nine [ten] eleven twelve thirteen fourteen fifteen...}
--testcase 411
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'ten one';
--result {[one] two three four five six seven eight nine [ten] eleven twelve thirteen fourteen fifteen...}

--testcase 420
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'one eleven';
--result {[one] two three four five six seven eight nine ten [eleven] twelve thirteen fourteen fifteen...}
--testcase 421
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'eleven one';
--result {[one] two three four five six seven eight nine ten [eleven] twelve thirteen fourteen fifteen...}

--testcase 500
UPDATE t1 SET a=
'supercalifragilisticexpialidocious one two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo twentythree twentyfour twentyfive supercalifragilisticexpialidocious'
 WHERE docid=1;
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'seven';
--result {supercalifragilisticexpialidocious one two three four five six [seven] eight nine ten eleven twelve thirteen fourteen...}
--testcase 501
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'twentythree';
--result {...twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twentyone twentytwo [twentythree] twentyfour twentyfive supercalifragilisticexpialidocious}


--testcase 600
UPDATE t1 SET a='one two three one one four one five one' WHERE docid=1;
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'one three four';
--result {[one] two [three] [one] [one] [four] [one] five [one]}
--testcase 601
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'three one four one';
--result {[one] two [three] [one] [one] [four] [one] five [one]}
--testcase 602
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'four three one OR one';
--result {[one] two [three] [one] [one] [four] [one] five [one]}

--testcase 700
UPDATE t1 SET a='one two three four five one six seven' WHERE docid=1;
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH 'one NEAR/2 five';
--result {one two three four [five] [one] six seven}

--testcase 710
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH '"five one" NEAR/2 six';
--result {one two three four [five] [one] [six] seven}
--testcase 711
SELECT snippet(t1,'[',']','...') FROM t1 WHERE t1 MATCH '"five one" NEAR/2 four';
--result {one two three [four] [five] [one] six seven}

#endif /* SQLITE_ENABLE_FTS3 */
