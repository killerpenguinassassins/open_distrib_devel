/*
** Common use routines for managing sessions.
**
** This file appears in the INCLUDE: line of test modules that want to
** make use of these facilities.
*/
#ifdef SQLITE_ENABLE_SESSION

/*
** Overview:
**
** --call th3session_test_init
** --call th3ssssion_test_shutdown
**
** --call th3session_create SNAME DB
** --call th3session_delete SNAME
** --call th3session_enable SNAME 0|1
** --call th3session_attach SNAME TABNAME
** --call th3session_list OUTPUT-TABLE
**
** --call th3changeset_focus SNAME CNAME
** --call th3changeset_delete CNAME
** --call th3changeset_dump CNAME OUTPUT-TABLE
** --call th3changeset_apply CNAME
** --call th3changeset_blob CNAME VARNAME
** --call th3changeset_list OUTPUT-TABLE
*/

/*
** Information about a single session in a test module.   An array
** of these is attached to the th3state.pTestData pointer.
*/
typedef struct th3session th3session;
struct th3session {
  char *zName;           /* Name of this session */
  sqlite3_session *pX;   /* The actual session implementation */
};

/*
** Information about a single changeset in a test module.   An array
** of these is attached to the th3state.pTestData pointer.
*/
typedef struct th3chngset th3chngset;
struct th3chngset {
  char *zName;       /* Name of this session */
  int nX;            /* Size of the change set in bytes */
  void *pX;          /* Content of the changes set */
};

/*
** The state information attached to th3state.pTestData
*/
typedef struct th3sessionInfo th3sessionInfo;
struct th3sessionInfo {
  int nSession;
  th3session *aSession;
  int nChngset;
  th3chngset *aChngset;
};

/*
** The input, zArg, is a writable zero-terminated string.  Break
** it up into N substrings, space separated.
*/
static void th3session_splitarg(char *zArg, int N, char **az){
  while( N>0 ){
    az[0] = zArg;
    az++;
    N--;
    while( zArg[0] && zArg[0]!=' ' ) zArg++;
    if( zArg[0]==' ' ){
      zArg[0] = 0;
      zArg++;
      while( zArg[0]==' ' ) zArg++;
    }
  }
}

/*
** Return a pointer to th3sessionInfo structure.  Create a new one
** if it does not exist and the createFlag is true.
*/
static th3sessionInfo *th3sessionGetInfo(th3state *p, int createFlag){
  if( p->pTestData==0 && createFlag ){
    th3sessionInfo *pInfo = th3malloc(p, sizeof(*pInfo));
    memset(pInfo, 0, sizeof(*pInfo));
    p->pTestData = pInfo;
  }
  return (th3sessionInfo*)p->pTestData;
}

/*
** --call th3session_test_shutdown
**
** Delete all sessions and changesets and close down the test.
*/
void th3session_test_shutdown(th3state *p, int iDb, char *zArg){
  th3sessionInfo *pInfo = th3sessionGetInfo(p, 0);
  if( pInfo ){
    th3session *pSes;
    th3chngset *pChng;
    int i;
    for(i=0, pSes=pInfo->aSession; i<pInfo->nSession; i++, pSes++){
      th3free(p, pSes->zName);
      sqlite3session_delete(pSes->pX);
    }
    th3free(p, pInfo->aSession);
    for(i=0, pChng=pInfo->aChngset; i<pInfo->nChngset; i++, pChng++){
      th3free(p, pChng->zName);
      sqlite3_free(pChng->pX);
    }
    th3free(p, pInfo->aChngset);
    th3free(p, pInfo);
    p->pTestData = 0;
  }
}

/*
** Find a session named zSName
*/
static th3session *th3sessionFind(th3state *p, const char *zSName, int *pI){
  int i;
  th3sessionInfo *pInfo = th3sessionGetInfo(p, 0);
  if( pInfo ){
    for(i=0; i<pInfo->nSession; i++){
      if( strcmp(pInfo->aSession[i].zName, zSName)==0 ){
        if( pI ) *pI = i;
        return &pInfo->aSession[i];
      }
    }
  }
  return 0;
}

/*
** --call th3session_create SNAME DB
**
** Create a named SNAME if it does not already exist.
*/
void th3session_create(th3state *p, int iDb, char *zArg){
  th3sessionInfo *pInfo = th3sessionGetInfo(p, 1);
  char *azArg[2];
  th3session *pSes;
  th3session_splitarg(zArg, 2, azArg);
  pSes = th3sessionFind(p, azArg[0], 0);
  if( pSes==0 ){
    int rc;
    sqlite3 *db;
    th3session *pSes;
    pInfo->nSession++;
    pInfo->aSession = th3realloc(p, pInfo->aSession, 
                        pInfo->nSession*sizeof(th3session));
    pSes = &pInfo->aSession[pInfo->nSession-1];
    pSes->zName = th3strndup(p, azArg[0], -1);
    db = p->aDb[iDb].db;
    rc = sqlite3session_create(db, azArg[1], &pSes->pX);
    if( rc!=SQLITE_OK ){
      th3testAppendResultTerm(p, th3errorCodeName(rc));
      th3testAppendResultTerm(p, "sqlite3session_create failed");
      th3free(p, pSes->zName);
      pInfo->nSession--;
    }
  }
}

/*
** --call th3session_delete SNAME
**
** Delete session SNAME if it exists
*/
void th3session_delete(th3state *p, int iDb, char *zArg){
  th3sessionInfo *pInfo = th3sessionGetInfo(p, 1);
  th3session *pSes;
  int i;
  pSes = th3sessionFind(p, zArg, &i);
  if( pSes ){
    sqlite3session_delete(pSes->pX);
    th3free(p, pSes->zName);
    pInfo->aSession[i] = pInfo->aSession[pInfo->nSession-1];
    pInfo->nSession--;
  }
}

/*
** --call th3session_enable SNAME BOOLEAN
**
** Enable or disable the session SNAME.
*/
void th3session_enable(th3state *p, int iDb, char *zArg){
  th3sessionInfo *pInfo = th3sessionGetInfo(p, 1);
  char *azArg[2];
  th3session *pSes;
  th3session_splitarg(zArg, 2, azArg);
  pSes = th3sessionFind(p, azArg[0], 0);
  if( pSes ){
    int x = (int)th3atoi64(azArg[1]);
    char zBuf[30];
    x = sqlite3session_enable(pSes->pX, x);
    sqlite3_snprintf(sizeof(zBuf), zBuf, "%d", x);
    th3testAppendResultTerm(p, zBuf);
  }
}
#endif /* SQLITE_ENABLE_SESSION */
