<title>Obtaining TH3 Source Code</title>

<h2>Selecting A Version Of TH3 To Download</h2>

As a licensee of TH3 (which presumably you are if you are able to read
this page) you have direct access to the software configuration management
system used by the TH3 developers.  
You get to see everything the developers do - both the good and the bad.
Just because a new version of TH3 has been checked in does <u>not</u> mean
that it has been vetted and validated.
If you are uncertain about which version of TH3 you should be using, please
talk to one of the developers.

All versions of TH3 can be viewed on the [/timeline | project timeline].
The easiest way to find the version you want is to browse the timeline.
To look for the latest check-ins, have a look at the
[/leaves | leaves page], which shows the tip of each branch in the
source code repository.

<h2>Getting The Code</h2>

Once you have identified the particular version of TH3 that you want to
use, click on the 10-character hexadecimal identifier for that version
to go to a page that contains detailed information about that version.

On this details page, toward the bottom of the "Overview" section and to
the right of "Other Links" you will see a hyperlink called "ZIP archive".
Click on this link to download a ZIP archive containing the complete
source code.
