<title>TH3 Test Modules</title>

Every non-trivial TH3 test program needs to have one or more test modules.
Each test module implements zero or more test cases.  By convention,
each test module implements test cases that verify a group of related
behaviors.  Test modules are implemented as either
[./c-modules.wiki | ANSI-C code] or
[./sql-modules.wiki | SQL script] or
a mixture of script and code.

Every test module should begin with a header comment that following
this template:

<blockquote><pre>
/*
** Comment describing what the module does
**
** MODULE_NAME:               example1
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     NO_OOM
** MINIMUM_HEAPSIZE:          100000
** MINIMUM_DISK:              100000
** MINIMUM_TEMPDISK:          100000
** MAXIMUM_STRING:            100000
** STATEMENT_CACHE_SIZE:      0
** IF:                        defined(unix)
** INCOMPATIBLE_WITH:
** COMPATIBLE_ONLY_WITH:
*/
</pre></blockquote>

The MODULE_NAME parameter is required and must specify a unique name
for the test module.  The name must be a valid C-language identifier since
the C function that implements the module must have the same name.
Use the MODULE_NAME: label for test modules written in ANSI-C.  For
test modules written in SQL, use SCRIPT_TEST_MODULE: instead. 
For example:

<blockquote><pre>
/*
** Comment describing what the module does
**
** SCRIPT_MODULE_NAME:        example2
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
** MINIMUM_DISK:              100000
** MINIMUM_TEMPDISK:          100000
** MAXIMUM_STRING:            100000
** STATEMENT_CACHE_SIZE:      0
** IF:                        defined(SQLITE_ENABLE_FTS3)
** INCOMPATIBLE_WITH:         c4
** COMPATIBLE_ONLY_WITH:
*/
</pre></blockquote>

For a mixed module containing both script and C code, use MIXED_TEST_MODULE
for the name.  For instance:

<blockquote><pre>
/*
** Comment describing what the module does
**
** MIXED_MODULE_NAME:         example3
** REQUIRED_PROPERTIES:       
** IF:                        defined(unix)
** COMPATIBLE_ONLY_WITH:      u*
*/
</pre></blockquote>

REQUIRED_PROPERTIES and DISALLOWED_PROPERTIES are followed by lists
of properties.  The set of properties are those defined for the
PROPERTIES parameter of a [./config.wiki | test configuration].
Any REQUIRE_PROPERTIES must
be present in the test configuration or else the module and configuration
are incompatible.  DISALLOWED_PROPERTIES must not be in the test
configuration in order for the module to be compatible.

The MINIMUM_HEAPSIZE, MINIMUM_DISK, and MINIMUM_TEMPDISK parameters 
specify minimum amounts of heap, disk and temporary disk space needed to
run this test module.  If the test configuration has less space than
specified, then the module is considered to incompatible.

The MAXIMUM_STRING parameter specifies the length of the longest string
that will ever be returned by th3format() or th3vformat() within this
module.

The STATEMENT_CACHE_SIZE parameter specifies the size of the
prepared statement cache that is used by the th3dbEval() interface
and by all SQL statement in a script test module.  This is an
override for the STATEMENT_CACHE_SIZE setting contained in the
configuration.  If unspecified by the test module, the size of the
prepared statement cache is determined by the configuration.

The IF parameter specifies C a preprocessor expression which must be
true in order for the module to operate.  In the generated C code,
implementation of this module will be surrounded by appropriate
#if...#endif preprocessor directives.

The INCOMPATIBLE_WITH parameter specifies one or more configurations
which are incompatible with this module.  Multiple configurations can
be specified, separated by whitespace.  The ".cfg" suffix is omitted from
the configuration name.  A glob pattern can be used to specify multiple
configurations with a single term.

The COMPATIBLE_ONLY_WITH parameter specifies glob patterns for those
configurations that the module is compatible with.  If any patterns
are specified, then this test module will be run only with those
configurations.  The usual situation is for no COMPATIBLE_ONLY_WITH
to be specified, in which case the module works with all
configurations.
