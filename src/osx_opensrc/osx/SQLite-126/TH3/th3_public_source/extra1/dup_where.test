/*
** A test to verify correct operation of indices when a term in the
** WHERE clause is duplicated.
**
** SCRIPT_MODULE_NAME:        e1_dup_where
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a,b,c);
INSERT INTO t1 VALUES(1,2,3);
INSERT INTO t1 VALUES(4,5,6);
INSERT INTO t1 VALUES(7,8,9);
CREATE TABLE t2(d,e,f);
INSERT INTO t2 SELECT a+10, b+20, c FROM t1;
--result

--testcase 110
SELECT * FROM t1, t2 WHERE c=f ORDER BY a
--result 1 2 3 11 22 3 4 5 6 14 25 6 7 8 9 17 28 9
--testcase 111
SELECT * FROM t1, t2 WHERE c=f AND c=f ORDER BY a
--result 1 2 3 11 22 3 4 5 6 14 25 6 7 8 9 17 28 9
--testcase 112
--oom
SELECT * FROM t1, t2 WHERE c=f AND c=f AND c=f ORDER BY a
--result 1 2 3 11 22 3 4 5 6 14 25 6 7 8 9 17 28 9
--testcase 113
SELECT * FROM t1, t2 WHERE c=f AND f=c ORDER BY a
--result 1 2 3 11 22 3 4 5 6 14 25 6 7 8 9 17 28 9

--testcase 200
CREATE INDEX t1c ON t1(c);
CREATE INDEX t2f ON t2(f);
--result

--testcase 210
SELECT * FROM t1, t2 WHERE c=f ORDER BY a
--result 1 2 3 11 22 3 4 5 6 14 25 6 7 8 9 17 28 9
--testcase 211
SELECT * FROM t1, t2 WHERE c=f AND c=f ORDER BY a
--result 1 2 3 11 22 3 4 5 6 14 25 6 7 8 9 17 28 9
--testcase 212
--oom
SELECT * FROM t1, t2 WHERE c=f AND c=f AND c=f ORDER BY a
--result 1 2 3 11 22 3 4 5 6 14 25 6 7 8 9 17 28 9
--testcase 213
SELECT * FROM t1, t2 WHERE c=f AND f=c ORDER BY a
--result 1 2 3 11 22 3 4 5 6 14 25 6 7 8 9 17 28 9
