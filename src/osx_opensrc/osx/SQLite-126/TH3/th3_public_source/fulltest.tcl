#!/usr/bin/tclsh
#
# This script runs TH3 in multiple configurations in order to fully
# test a version of the code prior to release.  To run this test,
# first generate the sqlite3.c and sqlite3.h files to be tested into
# the top-level directory of TH3 (the same directory in which this
# script is found) then run:
#
#     tclsh fulltest.tcl
#
# To run some subset of tests, specify either alignment files or test
# configuration files on the command-line and only those cases that have
# at least one match will run.  For example:
#
#     tclsh fulltest.tcl fast.rc
#
# Will run only the fast.rc configuration but with all alignments.
#
set ALIGNMENTS {
  1 alignment1.rc
  4 alignment4.rc
  5 alignment5.rc
  6 alignment6.rc
}
set CASES {
  t test.rc
  f fast.rc
  d memdebug.rc
  m min.rc
  tx test-ex.rc
  fx fast-ex.rc
}

# compute a timestamp
proc timestamp {} {
  return [clock format [clock seconds] -format {%H:%M:%S}]
}

# count the number of occurrances of FAILED in a file and return
# that number.
#
proc failed_count {filename} {
  set f [open $filename]
  fconfigure $f -translation binary
  set txt [read $f [file size $filename]]
  close $f
  set all {}
  regexp {: \d+ errors out of \d+ tests in \d+ configurations} $txt all
  if {$all==""} {
    return "** test did not complete **"
  } else {
    return [string trim [string trim $all :]]
  }
}

foreach x $argv {
  set arg($x) 1
}
foreach {alabel align} $ALIGNMENTS {
  if {[info exists arg($align)]} {
    unset arg($align)
    set hasalign($align) 1
  }
}
foreach {clabel case} $CASES {
  if {[info exists arg($case)]} {
    unset arg($case)
    set hascase($case) 1
  }
}
set arglist [array names arg]

set total_err 0
foreach {alabel align} $ALIGNMENTS {
  foreach {clabel case} $CASES {
    if {![info exists hascase($case)] && ![info exists hasalign($align)]} {
      continue
    }
    set lbl "$clabel$alabel"
    puts "******** $align $case ([timestamp]) **********"
    flush stdout
    file delete -force th3
    catch {eval exec ./th3make $case $align $arglist -norun} out
    set f [open th3-$lbl-out.txt w]
    puts $f $out
    close $f
    if {![file executable th3]} {
      puts "ERROR: failed to build..."
      continue
    }
    file delete -force th3-$lbl
    file rename th3 th3-$lbl
    file delete -force th3-$lbl.c
    file rename th3.c th3-$lbl.c
    puts "running: ./th3-$lbl >th3-$lbl-out.txt"
    flush stdout
    catch {exec ./th3-$lbl >>th3-$lbl-out.txt}
    set result [failed_count th3-$lbl-out.txt]
    puts "result: $result"
    set nerr 1
    regexp {\d+} $result nerr
    incr total_err $nerr
  }
}
puts "******** FINISHED ([timestamp]) with $total_err total errors *********"
