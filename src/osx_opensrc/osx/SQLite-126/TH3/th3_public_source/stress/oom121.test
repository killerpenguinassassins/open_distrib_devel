/*
** Out-of-memory test module for th3.
**
** SCRIPT_MODULE_NAME:        oom121
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 10000
--oom
CREATE TABLE t1(x int, y int);
--result 

--testcase 10010
INSERT INTO t1 VALUES(31,10);
--result 

--testcase 10020
INSERT INTO t1 VALUES(30,9);
--result 

--testcase 10030
INSERT INTO t1 VALUES(1,5);
--result 

--testcase 10040
SELECT DISTINCT y FROM t1 ORDER BY y;
--result 5 9 10

--testcase 10050
SELECT y, count(*) FROM t1 GROUP BY y ORDER BY y;
--result 5 1 9 1 10 1

--testcase 10060
SELECT y, count(*) FROM t1 GROUP BY y ORDER BY count(*), y;
--result 5 1 9 1 10 1

--testcase 10070
SELECT count(*), y FROM t1 GROUP BY y ORDER BY count(*), y;
--result 1 5 1 9 1 10

--testcase 10080
SELECT y, count(*) FROM t1 GROUP BY y HAVING count(*)<3 ORDER BY y;
--result 5 1 9 1 10 1

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 10090
SELECT x, count(*), avg(y) FROM t1 GROUP BY x HAVING x<4 ORDER BY x;
--result 1 1 5.0
#endif

--testcase 10100
SELECT avg(x) FROM t1 WHERE x>100;
--result nil

--testcase 10110
SELECT count(x) FROM t1 WHERE x>100;
--result 0

--testcase 10120
SELECT min(x) FROM t1 WHERE x>100;
--result nil

--testcase 10130
SELECT max(x) FROM t1 WHERE x>100;
--result nil

--testcase 10140
SELECT sum(x) FROM t1 WHERE x>100;
--result nil

--testcase 10150
CREATE TABLE t2(a, b, c);
--result 

--testcase 10160
INSERT INTO t2 VALUES(1, 2, 3);
--result 

--testcase 10170
INSERT INTO t2 VALUES(1, 4, 5);
--result 

--testcase 10180
INSERT INTO t2 VALUES(6, 4, 7);
--result 

--testcase 10190
CREATE INDEX t2_idx ON t2(a);
--result 

--testcase 10200
SELECT a FROM t2 GROUP BY a;
--result 1 6

--testcase 10210
SELECT a FROM t2 WHERE a>2 GROUP BY a;
--result 6

--testcase 10220
SELECT a, b FROM t2 GROUP BY a, b;
--result 1 2 1 4 6 4

--testcase 10230
SELECT a, b FROM t2 GROUP BY a;
--result 1 4 6 4

--testcase 10240
SELECT max(c), b*a, b, a FROM t2 GROUP BY b*a, b, a;
--result 3 2 2 1 5 4 4 1 7 24 4 6

--testcase 10250
CREATE TABLE t3(x,y);
--result 

--testcase 10260
INSERT INTO t3 VALUES(1,NULL);
--result 

--testcase 10270
INSERT INTO t3 VALUES(2,NULL);
--result 

--testcase 10280
INSERT INTO t3 VALUES(3,4);
--result 

--testcase 10290
SELECT count(x), y FROM t3 GROUP BY y ORDER BY 1;
--result 1 4 2 nil

--testcase 10300
CREATE TABLE t4(x,y,z);
--result 

--testcase 10310
INSERT INTO t4 VALUES(1,2,NULL);
--result 

--testcase 10320
INSERT INTO t4 VALUES(2,3,NULL);
--result 

--testcase 10330
INSERT INTO t4 VALUES(3,NULL,5);
--result 

--testcase 10340
INSERT INTO t4 VALUES(4,NULL,6);
--result 

--testcase 10350
INSERT INTO t4 VALUES(4,NULL,6);
--result 

--testcase 10360
INSERT INTO t4 VALUES(5,NULL,NULL);
--result 

--testcase 10370
INSERT INTO t4 VALUES(5,NULL,NULL);
--result 

--testcase 10380
INSERT INTO t4 VALUES(6,7,8);
--result 

--testcase 10390
SELECT max(x), count(x), y, z FROM t4 GROUP BY y, z ORDER BY 1;
--result 1 1 2 nil 2 1 3 nil 3 1 nil 5 4 2 nil 6 5 2 nil nil 6 1 7 8

--testcase 10400
SELECT count(*), count(x) as cnt FROM t4 GROUP BY y ORDER BY cnt;
--result 1 1 1 1 1 1 5 5

--testcase 10410
CREATE TABLE t8a(a,b);
--result 

--testcase 10420
CREATE TABLE t8b(x);
--result 

--testcase 10430
INSERT INTO t8a VALUES('one', 1);
--result 

--testcase 10440
INSERT INTO t8a VALUES('one', 2);
--result 

--testcase 10450
INSERT INTO t8a VALUES('two', 3);
--result 

--testcase 10460
INSERT INTO t8a VALUES('one', NULL);
--result 

--testcase 10470
INSERT INTO t8b(rowid,x) VALUES(1,111);
--result 

--testcase 10480
INSERT INTO t8b(rowid,x) VALUES(2,222);
--result 

--testcase 10490
INSERT INTO t8b(rowid,x) VALUES(3,333);
--result 

--testcase 10500
SELECT a, count(b) FROM t8a, t8b WHERE b=t8b.rowid GROUP BY a ORDER BY a;
--result one 2 two 1

--testcase 10510
SELECT a, count(b) FROM t8a, t8b WHERE b=+t8b.rowid GROUP BY a ORDER BY a;
--result one 2 two 1

--testcase 10520
SELECT t8a.a, count(t8a.b) FROM t8a, t8b WHERE t8a.b=t8b.rowid GROUP BY 1 ORDER BY 1;
--result one 2 two 1

--testcase 10530
SELECT a, count(*) FROM t8a, t8b WHERE b=+t8b.rowid GROUP BY a ORDER BY a;
--result one 2 two 1

--testcase 10540
SELECT a, count(b) FROM t8a, t8b WHERE b<x GROUP BY a ORDER BY a;
--result one 6 two 3

--testcase 10550
SELECT a, count(t8a.b) FROM t8a, t8b WHERE b=t8b.rowid GROUP BY a ORDER BY 2;
--result two 1 one 2

--testcase 10560
SELECT a, count(b) FROM t8a, t8b GROUP BY a ORDER BY 2;
--result two 3 one 6

--testcase 10570
SELECT a, count(*) FROM t8a, t8b GROUP BY a ORDER BY 2;
--result two 3 one 9
