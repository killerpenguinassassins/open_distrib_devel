/*
** Out-of-memory test module for th3.
**
** SCRIPT_MODULE_NAME:        oom107a
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     FORMAT4
** MINIMUM_HEAPSIZE:          100000
*/
#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 10000
--oom
CREATE TABLE t1(i INTEGER PRIMARY KEY,a,b,c,d);
--result 

--testcase 10010
--oom
CREATE INDEX t1i1 ON t1(a DESC, b ASC, c DESC);
--result 

--testcase 10020
--oom
CREATE INDEX t1i2 ON t1(b DESC, c ASC, d DESC);
--result 

--testcase 10030
--oom
INSERT INTO t1 VALUES(1, NULL, NULL, NULL, NULL);
--result 

--testcase 10040
--oom
INSERT INTO t1 VALUES(2, 2, 2, 2, 2);
--result 

--testcase 10050
INSERT INTO t1 VALUES(3, 3, 3, 3, 3);
--result 

--testcase 10060
--oom
INSERT INTO t1 VALUES(4, 2.5, 2.5, 2.5, 2.5);
--result 

--testcase 10070
INSERT INTO t1 VALUES(5, -5, -5, -5, -5);
--result 

--testcase 10080
--oom
INSERT INTO t1 VALUES(6, 'six', 'six', 'six', 'six');
--result 

--testcase 10090
--oom
INSERT INTO t1 VALUES(7, x'77', x'77', x'77', x'77');
--result 

--testcase 10100
INSERT INTO t1 VALUES(8,'eight','eight','eight','eight');
--result 

--testcase 10110
INSERT INTO t1 VALUES(9, x'7979', x'7979', x'7979', x'7979');
--result 

--testcase 10120
--oom
SELECT count(*) FROM t1;
--result 9

--testcase 10130
--oom
SELECT i FROM t1 ORDER BY a;
--result 1 5 2 4 3 8 6 7 9

--testcase 10140
--oom
SELECT i FROM t1 ORDER BY a DESC;
--result 9 7 6 8 3 4 2 5 1

--testcase 10150
--oom
SELECT i FROM t1 WHERE a<=x'7979';
--result 5 2 4 3 8 6 7 9

--testcase 10160
--oom
SELECT i FROM t1 WHERE a>-99;
--result 5 2 4 3 8 6 7 9

--testcase 10170
--oom
UPDATE t1 SET a=1;
--result 

--testcase 10180
SELECT i FROM t1 ORDER BY a;
--result 1 5 2 4 3 8 6 7 9

--testcase 10190
--oom
SELECT i FROM t1 WHERE a=1 AND b>0 AND b<'zzz';
--result 2 4 3 8 6

--testcase 10200
--oom
SELECT i FROM t1 WHERE b>0 AND b<'zzz';
--result 2 4 3 8 6

--testcase 10210
--oom
SELECT i FROM t1 WHERE a=1 AND b>-9999 AND b<x'ffffffff';
--result 5 2 4 3 8 6 7 9

--testcase 10220
--oom
SELECT i FROM t1 WHERE b>-9999 AND b<x'ffffffff';
--result 5 2 4 3 8 6 7 9

--testcase 10230
--oom
UPDATE t1 SET a=2 WHERE i<6;
--result 

--testcase 10240
--oom
SELECT i FROM t1 WHERE a IN (1,2) AND b>0 AND b<'zzz';
--result 8 6 2 4 3

--testcase 10250
UPDATE t1 SET a=1;
--result 

--testcase 10260
SELECT i FROM t1 WHERE a IN (1,2) AND b>0 AND b<'zzz';
--result 2 4 3 8 6

--testcase 10270
--oom
UPDATE t1 SET b=2;
--result 

--testcase 10280
SELECT i FROM t1 WHERE a IN (1,2) AND b>0 AND b<'zzz';
--result 1 5 2 4 3 8 6 7 9
#endif /* SQLITE_OMIT_FLOATING_POINT */
