/*
** Out-of-memory test module for th3.
**
** SCRIPT_MODULE_NAME:        oom129
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 10000
--oom
CREATE TABLE t1(w int, x int, y int);
--result 

--testcase 10010
CREATE TABLE t2(p int, q int, r int, s int);
--result 

--testcase 10020
INSERT INTO t1 VALUES(1,0,4);
--result 

--testcase 10030
INSERT INTO t1 VALUES(8,3,81);
--result 

--testcase 10040
INSERT INTO t1 VALUES(9,3,100);
--result 

--testcase 10050
INSERT INTO t1 VALUES(100,6,10201);
--result 

--testcase 10060
INSERT INTO t2 SELECT 101-w, x, (SELECT max(y) FROM t1)+1-y, y FROM t1;
--result 

--testcase 10070
CREATE INDEX i1w ON t1(w);
--result 

--testcase 10080
CREATE INDEX i1xy ON t1(x,y);
--result 

--testcase 10090
CREATE INDEX i2p ON t2(p);
--result 

--testcase 10100
CREATE INDEX i2r ON t2(r);
--result 

--testcase 10110
CREATE INDEX i2qs ON t2(q, s);
--result 

--testcase 10120
SELECT x, y, w FROM t1 WHERE w=10;
--result 

--testcase 10130
SELECT x, y, w AS abc FROM t1 WHERE abc=10;
--result 

--testcase 10140
SELECT x, y, w FROM t1 WHERE w=11;
--result 

--testcase 10150
SELECT x, y, w AS abc FROM t1 WHERE abc=11;
--result 

--testcase 10160
SELECT x, y, w AS abc FROM t1 WHERE 11=w;
--result 

--testcase 10170
SELECT x, y, w AS abc FROM t1 WHERE 11=abc;
--result 

--testcase 10180
SELECT w, x, y FROM t1 WHERE 11=w AND x>2;
--result 

--testcase 10190
SELECT w AS a, x AS b, y FROM t1 WHERE 11=a AND b>2;
--result 

--testcase 10200
SELECT x, y FROM t1 WHERE y<200 AND w=11 AND x>2;
--result 

--testcase 10210
SELECT x, y FROM t1 WHERE y<200 AND x>2 AND w=11;
--result 

--testcase 10220
SELECT x, y FROM t1 WHERE w=11 AND y<200 AND x>2;
--result 

--testcase 10230
SELECT x, y FROM t1 WHERE w>10 AND y=144 AND x=3;
--result 

--testcase 10240
SELECT x, y FROM t1 WHERE y=144 AND x=3;
--result 

--testcase 10250
SELECT x, y FROM t1 WHERE y=144 AND w>10 AND x=3;
--result 

--testcase 10260
SELECT x, y FROM t1 WHERE x=3 AND w>=10 AND y=121;
--result 

--testcase 10270
SELECT x, y FROM t1 WHERE x=3 AND y=100 AND w<10;
--result 3 100

--testcase 10280
SELECT w FROM t1 WHERE x=3 AND y<100;
--result 8

--testcase 10290
SELECT w FROM t1 WHERE x=3 AND 100>y;
--result 8

--testcase 10300
SELECT w FROM t1 WHERE 3=x AND y<100;
--result 8

--testcase 10310
SELECT w FROM t1 WHERE 3=x AND 100>y;
--result 8

--testcase 10320
SELECT w FROM t1 WHERE x=3 AND y<=100;
--result 8 9

--testcase 10330
SELECT w FROM t1 WHERE x=3 AND 100>=y;
--result 8 9

--testcase 10340
SELECT w FROM t1 WHERE x=3 AND y>225;
--result 

--testcase 10350
SELECT w FROM t1 WHERE x=3 AND 225<y;
--result 

--testcase 10360
SELECT w FROM t1 WHERE x=3 AND y>=225;
--result 

--testcase 10370
SELECT w FROM t1 WHERE x=3 AND 225<=y;
--result 

--testcase 10380
SELECT w FROM t1 WHERE x=3 AND y>121 AND y<196;
--result 

--testcase 10390
SELECT w FROM t1 WHERE x=3 AND y>=121 AND y<=196;
--result 

--testcase 10400
SELECT w FROM t1 WHERE x=3 AND 121<y AND 196>y;
--result 

--testcase 10410
SELECT w FROM t1 WHERE x=3 AND 121<=y AND 196>=y;
--result 

--testcase 10420
SELECT w FROM t1 WHERE x=3 AND y+1==122;
--result 

--testcase 10430
SELECT w FROM t1 WHERE x+1=4 AND y+1==122;
--result 

--testcase 10440
SELECT w FROM t1 WHERE y==121;
--result 

--testcase 10450
SELECT w FROM t1 WHERE w>97;
--result 100

--testcase 10460
SELECT w FROM t1 WHERE w>=97;
--result 100

--testcase 10470
SELECT w FROM t1 WHERE w==97;
--result 

--testcase 10480
SELECT w FROM t1 WHERE w<=97 AND w==97;
--result 

--testcase 10490
SELECT w FROM t1 WHERE w<98 AND w==97;
--result 

--testcase 10500
SELECT w FROM t1 WHERE w>=97 AND w==97;
--result 

--testcase 10510
SELECT w FROM t1 WHERE w>96 AND w==97;
--result 

--testcase 10520
SELECT w FROM t1 WHERE w==97 AND w==97;
--result 

--testcase 10530
SELECT w FROM t1 WHERE w+1==98;
--result 

--testcase 10540
SELECT w FROM t1 WHERE w<3;
--result 1

--testcase 10550
SELECT w FROM t1 WHERE w<=3;
--result 1

--testcase 10560
SELECT w FROM t1 WHERE w+1<=4 ORDER BY w;
--result 1

--testcase 10570
SELECT (w) FROM t1 WHERE (w)>(97);
--result 100

--testcase 10580
SELECT (w) FROM t1 WHERE (w)>=(97);
--result 100

--testcase 10590
SELECT (w) FROM t1 WHERE (w)==(97);
--result 

--testcase 10600
SELECT (w) FROM t1 WHERE ((w)+(1))==(98);
--result 

--testcase 10610
SELECT w, p FROM t2, t1 WHERE x=q AND y=s AND r=8977;
--result 

--testcase 10620
SELECT w, p FROM t2, t1 WHERE x=q AND s=y AND r=8977;
--result 

--testcase 10630
SELECT w, p FROM t2, t1 WHERE x=q AND s=y AND r=8977 AND w>10;
--result 

--testcase 10640
SELECT w, p FROM t2, t1 WHERE p<80 AND x=q AND s=y AND r=8977 AND w>10;
--result 

--testcase 10650
SELECT w, p FROM t2, t1 WHERE p<80 AND x=q AND 8977=r AND s=y AND w>10;
--result 

--testcase 10660
SELECT w, p FROM t2, t1 WHERE x=q AND p=77 AND s=y AND w>5;
--result 

--testcase 10670
SELECT w, p FROM t1, t2 WHERE x=q AND p>77 AND s=y AND w=5;
--result 

--testcase 10680
SELECT A.w, B.p, C.w FROM t1 as A, t2 as B, t1 as C WHERE C.w=101-B.p AND B.r=10202-A.y AND A.w=11;
--result 

--testcase 10690
SELECT A.w, B.p, C.w FROM t1 as A, t2 as B, t1 as C WHERE C.w=101-B.p AND B.r=10202-A.y AND A.w=12;
--result 

--testcase 10700
SELECT A.w, B.p, C.w FROM t1 as A, t2 as B, t1 as C WHERE A.w=15 AND B.p=C.w AND B.r=10202-A.y;
--result 

--testcase 10710
SELECT * FROM t1 WHERE 0;
--result 

--testcase 10720
SELECT * FROM t1 WHERE 1 LIMIT 1;
--result 1 0 4

--testcase 10730
SELECT 99 WHERE 0;
--result 

--testcase 10740
SELECT 99 WHERE 1;
--result 99

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 10750
SELECT 99 WHERE 0.1;
--result 99

--testcase 10760
SELECT 99 WHERE 0.0;
--result 
#endif

--testcase 10770
SELECT count(*) FROM t1 WHERE t1.w;
--result 4

--testcase 10780
SELECT * FROM t1 WHERE rowid IN (1,2,3,1234) order by 1;
--result 1 0 4 8 3 81 9 3 100

--testcase 10790
SELECT * FROM t1 WHERE rowid+0 IN (1,2,3,1234) order by 1;
--result 1 0 4 8 3 81 9 3 100

--testcase 10800
SELECT * FROM t1 WHERE w IN (-1,1,2,3) order by 1;
--result 1 0 4

--testcase 10810
SELECT * FROM t1 WHERE w+0 IN (-1,1,2,3) order by 1;
--result 1 0 4

--testcase 10820
SELECT * FROM t1 WHERE rowid IN (select rowid from t1 where rowid IN (-1,2,4)) ORDER BY 1;
--result 8 3 81 100 6 10201

--testcase 10830
SELECT * FROM t1 WHERE rowid+0 IN (select rowid from t1 where rowid IN (-1,2,4)) ORDER BY 1;
--result 8 3 81 100 6 10201

--testcase 10840
SELECT * FROM t1 WHERE w IN (select rowid from t1 where rowid IN (-1,2,4)) ORDER BY 1;
--result 

--testcase 10850
SELECT * FROM t1 WHERE w+0 IN (select rowid from t1 where rowid IN (-1,2,4)) ORDER BY 1;
--result 

--testcase 10860
SELECT * FROM t1 WHERE x IN (1,7) ORDER BY 1;
--result 

--testcase 10870
SELECT * FROM t1 WHERE x+0 IN (1,7) ORDER BY 1;
--result 

--testcase 10880
SELECT * FROM t1 WHERE y IN (6400,8100) ORDER BY 1;
--result 

--testcase 10890
SELECT * FROM t1 WHERE x=6 AND y IN (6400,8100) ORDER BY 1;
--result 

--testcase 10900
SELECT * FROM t1 WHERE x IN (1,7) AND y NOT IN (6400,8100) ORDER BY 1;
--result 

--testcase 10910
SELECT * FROM t1 WHERE x IN (1,7) AND y IN (9,10) ORDER BY 1;
--result 

--testcase 10920
SELECT * FROM t1 WHERE x IN (1,7) AND y IN (9,16) ORDER BY 1;
--result 

--testcase 10930
CREATE TABLE t3(a,b,c);
--result 

--testcase 10940
CREATE INDEX t3a ON t3(a);
--result 

--testcase 10950
CREATE INDEX t3bc ON t3(b,c);
--result 

--testcase 10960
CREATE INDEX t3acb ON t3(a,c,b);
--result 

--testcase 10970
INSERT INTO t3 SELECT w, 101-w, y FROM t1;
--result 

--testcase 10980
SELECT count(*), sum(a), sum(b), sum(c) FROM t3;
--result 4 118 286 10386

--testcase 10990
SELECT * FROM t3 ORDER BY a LIMIT 3;
--result 1 100 4 8 93 81 9 92 100

--testcase 11000
SELECT * FROM t3 ORDER BY a+1 LIMIT 3;
--result 1 100 4 8 93 81 9 92 100

--testcase 11010
SELECT * FROM t3 WHERE a<10 ORDER BY a LIMIT 3;
--result 1 100 4 8 93 81 9 92 100

--testcase 11020
SELECT * FROM t3 WHERE a>0 AND a<10 ORDER BY a LIMIT 3;
--result 1 100 4 8 93 81 9 92 100

--testcase 11030
SELECT * FROM t3 WHERE a>0 ORDER BY a LIMIT 3;
--result 1 100 4 8 93 81 9 92 100

--testcase 11040
SELECT * FROM t3 WHERE b>0 ORDER BY a LIMIT 3;
--result 1 100 4 8 93 81 9 92 100

--testcase 11050
SELECT * FROM t3 WHERE a IN (3,5,7,1,9,4,2) ORDER BY a LIMIT 3;
--result 1 100 4 9 92 100

--testcase 11060
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY a LIMIT 3;
--result 1 100 4

--testcase 11070
SELECT * FROM t3 WHERE a>=1 AND a=1 AND c>0 ORDER BY a LIMIT 3;
--result 1 100 4

--testcase 11080
SELECT * FROM t3 WHERE a<2 AND a=1 AND c>0 ORDER BY a LIMIT 3;
--result 1 100 4

--testcase 11090
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY a,c LIMIT 3;
--result 1 100 4

--testcase 11100
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY c LIMIT 3;
--result 1 100 4

--testcase 11110
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY a DESC LIMIT 3;
--result 1 100 4

--testcase 11120
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY a DESC, c DESC LIMIT 3;
--result 1 100 4

--testcase 11130
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY c DESC LIMIT 3;
--result 1 100 4

--testcase 11140
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY c,a LIMIT 3;
--result 1 100 4

--testcase 11150
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY a DESC, c ASC LIMIT 3;
--result 1 100 4

--testcase 11160
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY a ASC, c DESC LIMIT 3;
--result 1 100 4

--testcase 11170
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY a LIMIT 3;
--result 1 100 4

--testcase 11180
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY a,c LIMIT 3;
--result 1 100 4

--testcase 11190
SELECT * FROM t3 WHERE a=1 AND c>0 ORDER BY a,c,b LIMIT 3;
--result 1 100 4

--testcase 11200
SELECT * FROM t3 WHERE a>0 ORDER BY a DESC LIMIT 3;
--result 100 1 10201 9 92 100 8 93 81

--testcase 11210
SELECT * FROM t3 WHERE a>0 ORDER BY -a LIMIT 3;
--result 100 1 10201 9 92 100 8 93 81

--testcase 11220
SELECT * FROM t3 ORDER BY b LIMIT 3;
--result 100 1 10201 9 92 100 8 93 81

--testcase 11230
SELECT t3.a, t1.x FROM t3, t1 WHERE t3.a=t1.w ORDER BY t3.a LIMIT 3;
--result 1 0 8 3 9 3

--testcase 11240
SELECT t3.a, t1.x FROM t3, t1 WHERE t3.a=t1.w ORDER BY t1.x, t3.a LIMIT 3;
--result 1 0 8 3 9 3

--testcase 11250
SELECT y FROM t1 ORDER BY w LIMIT 3;
--result 4 81 100

--testcase 11260
SELECT y FROM t1 ORDER BY rowid LIMIT 3;
--result 4 81 100

--testcase 11270
SELECT y FROM t1 ORDER BY rowid, y LIMIT 3;
--result 4 81 100

--testcase 11280
SELECT y FROM t1 ORDER BY rowid, y DESC LIMIT 3;
--result 4 81 100

--testcase 11290
SELECT y FROM t1 WHERE y>4 ORDER BY rowid, w, x LIMIT 3;
--result 81 100 10201

--testcase 11300
SELECT y FROM t1 WHERE y>=9 ORDER BY rowid, x DESC, w LIMIT 3;
--result 81 100 10201

--testcase 11310
SELECT y FROM t1 WHERE y>4 AND y<25 ORDER BY rowid;
--result 

--testcase 11320
SELECT y FROM t1 WHERE y>=4 AND y<=25 ORDER BY oid;
--result 4

--testcase 11330
SELECT y FROM t1 WHERE y<=25 ORDER BY _rowid_, w+y;
--result 4

--testcase 11340
SELECT w FROM t1 WHERE x=3 ORDER BY y;
--result 8 9

--testcase 11350
SELECT w FROM t1 WHERE x=3 ORDER BY y DESC;
--result 9 8

--testcase 11360
SELECT w FROM t1 WHERE x=3 AND y>100 ORDER BY y LIMIT 3;
--result 

--testcase 11370
SELECT w FROM t1 WHERE x=3 AND y>100 ORDER BY y DESC LIMIT 3;
--result 

--testcase 11380
SELECT w FROM t1 WHERE x=3 AND y>121 ORDER BY y DESC;
--result 

--testcase 11390
SELECT w FROM t1 WHERE x=3 AND y>=121 ORDER BY y DESC;
--result 

--testcase 11400
SELECT w FROM t1 WHERE x=3 AND y>=121 AND y<196 ORDER BY y DESC;
--result 

--testcase 11410
SELECT w FROM t1 WHERE x=3 AND y>=121 AND y<=196 ORDER BY y DESC;
--result 

--testcase 11420
SELECT w FROM t1 WHERE x=3 AND y>121 AND y<=196 ORDER BY y DESC;
--result 

--testcase 11430
SELECT w FROM t1 WHERE x=3 AND y>100 AND y<196 ORDER BY y DESC;
--result 

--testcase 11440
SELECT w FROM t1 WHERE x=3 AND y>=121 AND y<196 ORDER BY y;
--result 

--testcase 11450
SELECT w FROM t1 WHERE x=3 AND y>=121 AND y<=196 ORDER BY y;
--result 

--testcase 11460
SELECT w FROM t1 WHERE x=3 AND y>121 AND y<=196 ORDER BY y;
--result 

--testcase 11470
SELECT w FROM t1 WHERE x=3 AND y>100 AND y<196 ORDER BY y;
--result 

--testcase 11480
SELECT w FROM t1 WHERE x=3 AND y<81 ORDER BY y;
--result 

--testcase 11490
SELECT w FROM t1 WHERE x=3 AND y<=81 ORDER BY y;
--result 8

--testcase 11500
SELECT w FROM t1 WHERE x=3 AND y>256 ORDER BY y;
--result 

--testcase 11510
SELECT w FROM t1 WHERE x=3 AND y>=256 ORDER BY y;
--result 

--testcase 11520
SELECT w FROM t1 WHERE x=3 AND y<81 ORDER BY y DESC;
--result 

--testcase 11530
SELECT w FROM t1 WHERE x=3 AND y<=81 ORDER BY y DESC;
--result 8

--testcase 11540
SELECT w FROM t1 WHERE x=3 AND y>256 ORDER BY y DESC;
--result 

--testcase 11550
SELECT w FROM t1 WHERE x=3 AND y>=256 ORDER BY y DESC;
--result 

--testcase 11560
SELECT w FROM t1 WHERE x=0 AND y<4 ORDER BY y;
--result 

--testcase 11570
SELECT w FROM t1 WHERE x=0 AND y<=4 ORDER BY y;
--result 1

--testcase 11580
SELECT w FROM t1 WHERE x=6 AND y>10201 ORDER BY y;
--result 

--testcase 11590
SELECT w FROM t1 WHERE x=6 AND y>=10201 ORDER BY y;
--result 100

--testcase 11600
SELECT w FROM t1 WHERE x=0 AND y<4 ORDER BY y DESC;
--result 

--testcase 11610
SELECT w FROM t1 WHERE x=0 AND y<=4 ORDER BY y DESC;
--result 1

--testcase 11620
SELECT w FROM t1 WHERE x=6 AND y>10201 ORDER BY y DESC;
--result 

--testcase 11630
SELECT w FROM t1 WHERE x=6 AND y>=10201 ORDER BY y DESC;
--result 100

--testcase 11640
SELECT y FROM t1 ORDER BY rowid DESC LIMIT 3;
--result 10201 100 81

--testcase 11650
SELECT y FROM t1 WHERE y<25 ORDER BY rowid DESC;
--result 4

--testcase 11660
SELECT y FROM t1 WHERE y<=25 ORDER BY rowid DESC;
--result 4

--testcase 11670
SELECT y FROM t1 WHERE y<25 AND y>4 ORDER BY rowid DESC, y DESC;
--result 

--testcase 11680
SELECT y FROM t1 WHERE y<25 AND y>=4 ORDER BY rowid DESC;
--result 4

--testcase 11690
CREATE TABLE t4 AS SELECT * FROM t1;
--result 

--testcase 11700
CREATE INDEX i4xy ON t4(x,y);
--result 

--testcase 11710
SELECT w FROM t4 WHERE x=4 and y<1000 ORDER BY y DESC limit 3;
--result 

--testcase 11720
DELETE FROM t4;
--result 

--testcase 11730
SELECT w FROM t4 WHERE x=4 and y<1000 ORDER BY y DESC limit 3;
--result 

--testcase 11740
CREATE TABLE t5(x PRIMARY KEY);
--result 

--testcase 11750
SELECT * FROM t5 WHERE x<10;
--result 

--testcase 11760
SELECT * FROM t5 WHERE x<10 ORDER BY x DESC;
--result 

--testcase 11770
SELECT * FROM t5 WHERE x=10;
--result 

--testcase 11780
SELECT 1 WHERE abs(random())<0;
--result 

--testcase 11790
CREATE TABLE t99(Dte INT, X INT);
--result 

--testcase 11800
DELETE FROM t99 WHERE (Dte = 2451337) OR (Dte = 2451339) OR (Dte BETWEEN 2451345 AND 2451347) OR (Dte = 2451351) OR (Dte BETWEEN 2451355 AND 2451356) OR (Dte = 2451358) OR (Dte = 2451362) OR (Dte = 2451365) OR (Dte = 2451367) OR (Dte BETWEEN 2451372 AND 2451376) OR (Dte BETWEEN 2451382 AND 2451384) OR (Dte = 2451387) OR (Dte BETWEEN 2451389 AND 2451391) OR (Dte BETWEEN 2451393 AND 2451395) OR (Dte = 2451400) OR (Dte = 2451402) OR (Dte = 2451404) OR (Dte BETWEEN 2451416 AND 2451418) OR (Dte = 2451422) OR (Dte = 2451426) OR (Dte BETWEEN 2451445 AND 2451446) OR (Dte = 2451456) OR (Dte = 2451458) OR (Dte BETWEEN 2451465 AND 2451467) OR (Dte BETWEEN 2451469 AND 2451471) OR (Dte = 2451474) OR (Dte BETWEEN 2451477 AND 2451501) OR (Dte BETWEEN 2451503 AND 2451509) OR (Dte BETWEEN 2451511 AND 2451514) OR (Dte BETWEEN 2451518 AND 2451521) OR (Dte BETWEEN 2451523 AND 2451531) OR (Dte BETWEEN 2451533 AND 2451537) OR (Dte BETWEEN 2451539 AND 2451544) OR (Dte BETWEEN 2451546 AND 2451551) OR (Dte BETWEEN 2451553 AND 2451555) OR (Dte = 2451557) OR (Dte BETWEEN 2451559 AND 2451561) OR (Dte = 2451563) OR (Dte BETWEEN 2451565 AND 2451566) OR (Dte BETWEEN 2451569 AND 2451571) OR (Dte = 2451573) OR (Dte = 2451575) OR (Dte = 2451577) OR (Dte = 2451581) OR (Dte BETWEEN 2451583 AND 2451586) OR (Dte BETWEEN 2451588 AND 2451592) OR (Dte BETWEEN 2451596 AND 2451598) OR (Dte = 2451600) OR (Dte BETWEEN 2451602 AND 2451603) OR (Dte = 2451606) OR (Dte = 2451611);
--result 

--testcase 11810
CREATE TABLE t6(a INTEGER PRIMARY KEY, b TEXT);
--result 

--testcase 11820
INSERT INTO t6 VALUES(1,'one');
--result 

--testcase 11830
INSERT INTO t6 VALUES(4,'four');
--result 

--testcase 11840
CREATE INDEX t6i1 ON t6(b);
--result 

--testcase 11850
SELECT * FROM t6 ORDER BY b;
--result 4 four 1 one

--testcase 11860
SELECT * FROM t6 ORDER BY b, a;
--result 4 four 1 one

--testcase 11870
SELECT * FROM t6 ORDER BY a;
--result 1 one 4 four

--testcase 11880
SELECT * FROM t6 ORDER BY a, b;
--result 1 one 4 four

--testcase 11890
SELECT * FROM t6 ORDER BY b DESC;
--result 1 one 4 four

--testcase 11900
SELECT * FROM t6 ORDER BY b DESC, a DESC;
--result 1 one 4 four

--testcase 11910
SELECT * FROM t6 ORDER BY b DESC, a ASC;
--result 1 one 4 four

--testcase 11920
SELECT * FROM t6 ORDER BY b ASC, a DESC;
--result 4 four 1 one

--testcase 11930
SELECT * FROM t6 ORDER BY a DESC;
--result 4 four 1 one

--testcase 11940
SELECT * FROM t6 ORDER BY a DESC, b DESC;
--result 4 four 1 one

--testcase 11950
SELECT * FROM t6 ORDER BY a DESC, b ASC;
--result 4 four 1 one

--testcase 11960
SELECT * FROM t6 ORDER BY a ASC, b DESC;
--result 1 one 4 four

--testcase 11970
CREATE TABLE t7(a INTEGER PRIMARY KEY, b TEXT);
--result 

--testcase 11980
INSERT INTO t7 VALUES(1,'one');
--result 

--testcase 11990
INSERT INTO t7 VALUES(4,'four');
--result 

--testcase 12000
CREATE INDEX t7i1 ON t7(b);
--result 

--testcase 12010
SELECT * FROM t7 ORDER BY b;
--result 4 four 1 one

--testcase 12020
SELECT * FROM t7 ORDER BY b, a;
--result 4 four 1 one

--testcase 12030
SELECT * FROM t7 ORDER BY a;
--result 1 one 4 four

--testcase 12040
SELECT * FROM t7 ORDER BY a, b;
--result 1 one 4 four

--testcase 12050
SELECT * FROM t7 ORDER BY b DESC;
--result 1 one 4 four

--testcase 12060
SELECT * FROM t7 ORDER BY b DESC, a DESC;
--result 1 one 4 four

--testcase 12070
SELECT * FROM t7 ORDER BY b DESC, a ASC;
--result 1 one 4 four

--testcase 12080
SELECT * FROM t7 ORDER BY b ASC, a DESC;
--result 4 four 1 one

--testcase 12090
SELECT * FROM t7 ORDER BY a DESC;
--result 4 four 1 one

--testcase 12100
SELECT * FROM t7 ORDER BY a DESC, b DESC;
--result 4 four 1 one

--testcase 12110
SELECT * FROM t7 ORDER BY a DESC, b ASC;
--result 4 four 1 one

--testcase 12120
SELECT * FROM t7 ORDER BY a ASC, b DESC;
--result 1 one 4 four

--testcase 12130
CREATE TABLE t8(a INTEGER PRIMARY KEY, b TEXT UNIQUE);
--result 

--testcase 12140
INSERT INTO t8 VALUES(1,'one');
--result 

--testcase 12150
INSERT INTO t8 VALUES(4,'four');
--result 

--testcase 12160
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.a, y.b;
--result 1/4 1/1 4/4 4/1

--testcase 12170
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.a, y.b DESC;
--result 1/1 1/4 4/1 4/4

--testcase 12180
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.a, x.b;
--result 1/1 1/4 4/1 4/4

--testcase 12190
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.a, x.b DESC;
--result 1/1 1/4 4/1 4/4

--testcase 12200
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, x.a||x.b;
--result 4/1 4/4 1/1 1/4

--testcase 12210
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, x.a||x.b DESC;
--result 4/1 4/4 1/1 1/4

--testcase 12220
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, y.a||y.b;
--result 4/1 4/4 1/1 1/4

--testcase 12230
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, x.a, y.a||y.b;
--result 4/1 4/4 1/1 1/4

--testcase 12240
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, x.a, x.a||x.b;
--result 4/1 4/4 1/1 1/4

--testcase 12250
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, y.a||y.b DESC;
--result 4/4 4/1 1/4 1/1

--testcase 12260
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, x.a||y.b;
--result 4/4 4/1 1/4 1/1

--testcase 12270
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, x.a||y.b DESC;
--result 4/1 4/4 1/1 1/4

--testcase 12280
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, y.a||x.b;
--result 4/1 4/4 1/1 1/4

--testcase 12290
SELECT x.a || '/' || y.a FROM t8 x, t8 y ORDER BY x.b, y.a||x.b DESC;
--result 4/4 4/1 1/4 1/1

--testcase 12300
CREATE TEMP TABLE t1 (a, b, c, d, e);
--result 

--testcase 12310
CREATE TEMP TABLE t2 (f);
--result 

--testcase 12320
SELECT t1.e AS alias FROM t2, t1 WHERE alias = 1;
--result 

--testcase 12330
CREATE TABLE a1(id INTEGER PRIMARY KEY, v);
--result 

--testcase 12340
CREATE TABLE a2(id INTEGER PRIMARY KEY, v);
--result 

--testcase 12350
INSERT INTO a1 VALUES(1, 'one');
--result 

--testcase 12360
INSERT INTO a1 VALUES(2, 'two');
--result 

--testcase 12370
INSERT INTO a2 VALUES(1, 'one');
--result 

--testcase 12380
INSERT INTO a2 VALUES(2, 'two');
--result 

--testcase 12390
SELECT * FROM a2 CROSS JOIN a1 WHERE a1.id=1 AND a1.v='one';
--result 1 one 1 one 2 two 1 one

--testcase 12400
CREATE TEMP TABLE foo(idx INTEGER);
--result 

--testcase 12410
INSERT INTO foo VALUES(1);
--result 

--testcase 12420
INSERT INTO foo VALUES(1);
--result 

--testcase 12430
INSERT INTO foo VALUES(1);
--result 

--testcase 12440
INSERT INTO foo VALUES(2);
--result 

--testcase 12450
INSERT INTO foo VALUES(2);
--result 

--testcase 12460
CREATE TEMP TABLE bar(stuff INTEGER);
--result 

--testcase 12470
INSERT INTO bar VALUES(100);
--result 

--testcase 12480
INSERT INTO bar VALUES(200);
--result 

--testcase 12490
INSERT INTO bar VALUES(300);
--result 

--testcase 12500
SELECT bar.RowID id FROM foo, bar WHERE foo.idx = bar.RowID AND id = 2;
--result 2 2

--testcase 12510
PRAGMA integrity_check;
--result ok

--testcase 12520
CREATE TABLE tbooking ( id INTEGER PRIMARY KEY, eventtype INTEGER NOT NULL );
--result 

--testcase 12530
INSERT INTO tbooking VALUES(42, 3);
--result 

--testcase 12540
INSERT INTO tbooking VALUES(43, 4);
--result 

--testcase 12550
SELECT a.id FROM tbooking AS a WHERE a.eventtype=3;
--result 42

--testcase 12560
SELECT a.id, (SELECT b.id FROM tbooking AS b WHERE b.id>a.id) FROM tbooking AS a WHERE a.eventtype=3;
--result 42 43

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 12570
SELECT a.id, (SELECT b.id FROM tbooking AS b WHERE b.id>a.id) FROM (SELECT 1.5 AS id) AS a;
--result 1.5 42

--testcase 12580
CREATE TABLE tother(a, b);
--result 

--testcase 12590
INSERT INTO tother VALUES(1, 3.7);
--result 

--testcase 12600
SELECT id, a FROM tbooking, tother WHERE id>a;
--result 42 1 43 1
#endif /* SQLITE_OMIT_FLOATING_POINT */
