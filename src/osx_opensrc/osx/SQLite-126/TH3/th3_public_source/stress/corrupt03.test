/*
** Corrupt database test module for th3.
**
** This module checks SQLite for robustness against corrupt database
** files.  The purpose of this module is to verify that a corrupt
** database file will not cause SQLite to crash.
**
** This is a black-box test.  Testing begins by creating a template
** database.  The database is constructed so as to have at least one
** page of every type:  btree-root, btree-leaf, overflow, freepage-trunk,
** freepage-leaf, pointer-map.    Once the database is created, a snapshot
** of the filesystem is made.
**
** Next is a loop that corrupts a small part of the database.  In this
** particular module, the corruption focuses on setting or clearing
** the high-order bits in a span of adjacent bytes.  This kind of 
** corruption is intended to stress the variable-length integer decoder.
** of the loop sets a single byte of the database to a single value.
** Afterwards, the database is read and written to verify either that the
** corruption is harmless.  It might be the an unused byte of the file
** is changed, in which case the database is unperturbed.  Or it might
** be that a databyte changes, in which case the file is still well-formed
** but contains new data.  Or, it might be that the structure of the database
** is corrupted.  Since this is a black-box text, we do not know which
** result the byte change will have.  The point is to make sure that the
** code does not crash or corrupt memory.
**
** The loop repeats for all possible values of all bytes in the database.
** There are a lot of tests.
**
** MODULE_NAME:               corrupt03
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
int corrupt03(th3state *p){
  int I;
  int i, k;
  int rc = 1;
  char zBuf[18];

  /* Create the sample database file */
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
    "CREATE TABLE t1(x,y);"
    "INSERT INTO t1 VALUES(1,2);"
    "INSERT INTO t1 VALUES(3,4.5);"
    "INSERT INTO t1 VALUES(6,'hello');"
    "INSERT INTO t1 VALUES(7,null);"
    "INSERT INTO t1 VALUES(8,0);"
    "INSERT INTO t1 VALUES(123, zeroblob(2000));"
    "CREATE TABLE t2 AS SELECT * FROM t1;"
    "DROP TABLE t1;"
  );
  th3dbClose(p, 0);

  /* Make a snapshot of the filesystem */
  th3filesystemSnapshot(p);

  /* Do the tests */
  for(I=0; rc; I++){
    for(k=1; k<=sizeof(zBuf); k++){
      th3dbCloseAll(p);
      th3filesystemRevert(p);
      rc = th3testvfsRead(p, "test.db", I, k, zBuf);
      if( rc==0 ) break;
      for(i=0; i<rc; i++) zBuf[i] |= 0x80;
      rc = th3testvfsWrite(p, "test.db", I, rc, zBuf);
      if( rc==0 ) break;
      th3testBegin(p, th3format(p, "%d.%d.%d", I, k));
      th3dbOpen(p, 0, "test.db", 0);
      th3dbEval(p, 0,
        "UPDATE t2 SET x=x+1000000000;"
        "UPDATE t2 SET y=zeroblob(600);"
        "UPDATE t2 SET y=NULL;"
        "VACUUM;"
        "PRAGMA integrity_check;"
      );
      th3testCheckInt(p, 0, 0);
    }
    if( I%100==99 ){
      th3print(p, th3format(p, "%s.corrupt03.%d\n",
                            p->config.zConfigName, I+1));
      th3flush(p);
    }
  }

  return 0;
}
