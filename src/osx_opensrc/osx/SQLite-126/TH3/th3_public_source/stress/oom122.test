/*
** Out-of-memory test module for th3.
**
** SCRIPT_MODULE_NAME:        oom122
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 10000
--oom
CREATE TABLE t1(x, y);
--result 

--testcase 10010
INSERT INTO t1 VALUES(1,1);
--result 

--testcase 10020
SELECT DISTINCT y FROM t1 ORDER BY y;
--result 1

--testcase 10030
SELECT count(*) FROM (SELECT y FROM t1);
--result 1

--testcase 10040
SELECT count(*) FROM (SELECT DISTINCT y FROM t1);
--result 1

--testcase 10050
SELECT count(*) FROM (SELECT DISTINCT * FROM (SELECT y FROM t1));
--result 1

--testcase 10060
SELECT count(*) FROM (SELECT * FROM (SELECT DISTINCT y FROM t1));
--result 1

--testcase 10070
SELECT * FROM (SELECT count(*),y FROM t1 GROUP BY y) AS a, (SELECT max(x),y FROM t1 GROUP BY y) as b WHERE a.y=b.y ORDER BY a.y;
--result 1 1 1 1

--testcase 10080
SELECT a.y, a.[count(*)], [max(x)], [count(*)] FROM (SELECT count(*),y FROM t1 GROUP BY y) AS a, (SELECT max(x),y FROM t1 GROUP BY y) as b WHERE a.y=b.y ORDER BY a.y;
--result 1 1 1 1

--testcase 10090
SELECT q, p, r FROM (SELECT count(*) as p , y as q FROM t1 GROUP BY y) AS a, (SELECT max(x) as r, y as s FROM t1 GROUP BY y) as b WHERE q=s ORDER BY s;
--result 1 1 1

--testcase 10100
SELECT q, p, r, b.[min(x)+y] FROM (SELECT count(*) as p , y as q FROM t1 GROUP BY y) AS a, (SELECT max(x) as r, y as s, min(x)+y FROM t1 GROUP BY y) as b WHERE q=s ORDER BY s;
--result 1 1 1 2

--testcase 10110
CREATE TABLE t2(a INTEGER PRIMARY KEY, b);
--result 

--testcase 10120
INSERT INTO t2 SELECT * FROM t1;
--result 

--testcase 10130
SELECT DISTINCT b FROM t2 ORDER BY b;
--result 1

--testcase 10140
SELECT count(*) FROM (SELECT b FROM t2);
--result 1

--testcase 10150
SELECT count(*) FROM (SELECT DISTINCT b FROM t2);
--result 1

--testcase 10160
SELECT count(*) FROM (SELECT DISTINCT * FROM (SELECT b FROM t2));
--result 1

--testcase 10170
SELECT count(*) FROM (SELECT * FROM (SELECT DISTINCT b FROM t2));
--result 1

--testcase 10180
SELECT * FROM (SELECT count(*),b FROM t2 GROUP BY b) AS a, (SELECT max(a),b FROM t2 GROUP BY b) as b WHERE a.b=b.b ORDER BY a.b;
--result 1 1 1 1

--testcase 10190
SELECT a.b, a.[count(*)], [max(a)], [count(*)] FROM (SELECT count(*),b FROM t2 GROUP BY b) AS a, (SELECT max(a),b FROM t2 GROUP BY b) as b WHERE a.b=b.b ORDER BY a.b;
--result 1 1 1 1

--testcase 10200
SELECT q, p, r FROM (SELECT count(*) as p , b as q FROM t2 GROUP BY b) AS a, (SELECT max(a) as r, b as s FROM t2 GROUP BY b) as b WHERE q=s ORDER BY s;
--result 1 1 1

--testcase 10210
SELECT a.q, a.p, b.r FROM (SELECT count(*) as p , b as q FROM t2 GROUP BY q) AS a, (SELECT max(a) as r, b as s FROM t2 GROUP BY s) as b WHERE a.q=b.s ORDER BY a.q;
--result 1 1 1

--testcase 10220
SELECT * FROM (SELECT a.q, a.p, b.r FROM (SELECT count(*) as p , b as q FROM t2 GROUP BY q) AS a, (SELECT max(a) as r, b as s FROM t2 GROUP BY s) as b WHERE a.q=b.s ORDER BY a.q) ORDER BY "a.q";
--result 1 1 1

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 10230
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', avg(y) as 'b' FROM t1);
--result 1.0 1.0 2.0

--testcase 10240
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', avg(y) as 'b' FROM t1 WHERE y=4);
--result nil nil nil

--testcase 10250
SELECT x,y,x+y FROM (SELECT avg(a) as 'x', avg(b) as 'y' FROM t2 WHERE a=4);
--result nil nil nil

--testcase 10260
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', avg(y) as 'b' FROM t1) WHERE a>10;
--result 

--testcase 10270
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', avg(y) as 'b' FROM t1) WHERE a<10;
--result 1.0 1.0 2.0

--testcase 10280
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', avg(y) as 'b' FROM t1 WHERE y=4) WHERE a>10;
--result 

--testcase 10290
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', avg(y) as 'b' FROM t1 WHERE y=4) WHERE a<10;
--result 

--testcase 10300
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', y as 'b' FROM t1 GROUP BY b) ORDER BY a;
--result 1.0 1 2.0

--testcase 10310
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', y as 'b' FROM t1 GROUP BY b) WHERE b<4 ORDER BY a;
--result 1.0 1 2.0

--testcase 10320
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', y as 'b' FROM t1 GROUP BY b HAVING a>1) WHERE b<4 ORDER BY a;
--result 

--testcase 10330
SELECT a,b,a+b FROM (SELECT avg(x) as 'a', y as 'b' FROM t1 GROUP BY b HAVING a>1) ORDER BY a;
--result 

--testcase 10340
SELECT [count(*)],y FROM (SELECT count(*), y FROM t1 GROUP BY y) ORDER BY [count(*)];
--result 1 1

--testcase 10350
SELECT [count(*)],y FROM (SELECT count(*), y FROM t1 GROUP BY y) ORDER BY y;
--result 1 1

--testcase 10360
SELECT a,b,c FROM (SELECT x AS 'a', y AS 'b', x+y AS 'c' FROM t1 WHERE y=4) WHERE a<10 ORDER BY a;
--result 

--testcase 10370
SELECT y FROM (SELECT DISTINCT y FROM t1) WHERE y<5 ORDER BY y;
--result 1

--testcase 10380
SELECT DISTINCT y FROM (SELECT y FROM t1) WHERE y<5 ORDER BY y;
--result 1

--testcase 10390
SELECT avg(y) FROM (SELECT DISTINCT y FROM t1) WHERE y<5 ORDER BY y;
--result 1.0

--testcase 10400
SELECT avg(y) FROM (SELECT DISTINCT y FROM t1 WHERE y<5) ORDER BY y;
--result 1.0
#endif /* SQLITE_OMIT_FLOATING_POINT */

--testcase 10410
SELECT a,x,b FROM (SELECT x+3 AS 'a', x FROM t1 WHERE y=3) AS 'p', (SELECT x AS 'b' FROM t1 WHERE y=4) AS 'q' WHERE a=b ORDER BY a;
--result 

--testcase 10420
SELECT a,x,b FROM (SELECT x+3 AS 'a', x FROM t1 WHERE y=3), (SELECT x AS 'b' FROM t1 WHERE y=4) WHERE a=b ORDER BY a;
--result 

--testcase 10430
DELETE FROM t1 WHERE x>4;
--result 

--testcase 10440
SELECT * FROM t1;
--result 1 1

--testcase 10450
SELECT * FROM ( SELECT x AS 'a' FROM t1 UNION ALL SELECT x+10 AS 'a' FROM t1 ) ORDER BY a;
--result 1 11

--testcase 10460
SELECT * FROM ( SELECT x AS 'a' FROM t1 UNION ALL SELECT x+1 AS 'a' FROM t1 ) ORDER BY a;
--result 1 2

--testcase 10470
SELECT * FROM ( SELECT x AS 'a' FROM t1 UNION SELECT x+1 AS 'a' FROM t1 ) ORDER BY a;
--result 1 2

--testcase 10480
SELECT * FROM ( SELECT x AS 'a' FROM t1 INTERSECT SELECT x+1 AS 'a' FROM t1 ) ORDER BY a;
--result 

--testcase 10490
SELECT * FROM ( SELECT x AS 'a' FROM t1 EXCEPT SELECT x*2 AS 'a' FROM t1 ) ORDER BY a;
--result 1

--testcase 10500
SELECT * FROM (SELECT 1);
--result 1

--testcase 10510
SELECT c,b,a,* FROM (SELECT 1 AS 'a', 2 AS 'b', 'abc' AS 'c');
--result abc 2 1 1 2 abc

--testcase 10520
SELECT c,b,a,* FROM (SELECT 1 AS 'a', 2 AS 'b', 'abc' AS 'c' WHERE 0);
--result 

--testcase 10530
CREATE TABLE t3(p,q);
--result 

--testcase 10540
INSERT INTO t3 VALUES(1,11);
--result 

--testcase 10550
INSERT INTO t3 VALUES(2,22);
--result 

--testcase 10560
CREATE TABLE t4(q,r);
--result 

--testcase 10570
INSERT INTO t4 VALUES(11,111);
--result 

--testcase 10580
INSERT INTO t4 VALUES(22,222);
--result 

--testcase 10590
SELECT * FROM t3 NATURAL JOIN t4;
--result 1 11 111 2 22 222

--testcase 10600
SELECT y, p, q, r FROM (SELECT t1.y AS y, t2.b AS b FROM t1, t2 WHERE t1.x=t2.a) AS m, (SELECT t3.p AS p, t3.q AS q, t4.r AS r FROM t3 NATURAL JOIN t4) as n WHERE y=p;
--result 1 1 11 111

--testcase 10610
SELECT DISTINCT y, p, q, r FROM (SELECT t1.y AS y, t2.b AS b FROM t1, t2 WHERE t1.x=t2.a) AS m, (SELECT t3.p AS p, t3.q AS q, t4.r AS r FROM t3 NATURAL JOIN t4) as n WHERE y=p;
--result 1 1 11 111

--testcase 10620
SELECT * FROM (SELECT y, p, q, r FROM (SELECT t1.y AS y, t2.b AS b FROM t1, t2 WHERE t1.x=t2.a) AS m, (SELECT t3.p AS p, t3.q AS q, t4.r AS r FROM t3 NATURAL JOIN t4) as n WHERE y=p) AS e, (SELECT r AS z FROM t4 WHERE q=11) AS f WHERE e.r=f.z;
--result 1 1 11 111 111

--testcase 10630
SELECT a.x, b.x FROM t1 AS a, (SELECT x FROM t1 LIMIT 2) AS b;
--result 1 1

--testcase 10640
SELECT x FROM (SELECT x FROM t1 LIMIT 2);
--result 1

--testcase 10650
SELECT x FROM (SELECT x FROM t1 LIMIT 2 OFFSET 1);
--result 

--testcase 10660
SELECT x FROM (SELECT x FROM t1) LIMIT 2;
--result 1

--testcase 10670
SELECT x FROM (SELECT x FROM t1) LIMIT 2 OFFSET 1;
--result 

--testcase 10680
SELECT x FROM (SELECT x FROM t1 LIMIT 2) LIMIT 3;
--result 1

--testcase 10690
SELECT x FROM (SELECT x FROM t1 LIMIT -1) LIMIT 3;
--result 1

--testcase 10700
SELECT x FROM (SELECT x FROM t1 LIMIT -1);
--result 1

--testcase 10710
SELECT x FROM (SELECT x FROM t1 LIMIT -1 OFFSET 1);
--result
