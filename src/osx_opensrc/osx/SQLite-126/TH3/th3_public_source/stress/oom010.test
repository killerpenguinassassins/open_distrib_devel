/*
** Out-of-memory test module for th3. This module focuses on 
** OOM errors that occur during statement compilation.
**
** MODULE_NAME:               oom010
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     NO_OOM
** MINIMUM_HEAPSIZE:          100000
*/


/*
** For debugging, set the GROUP variable to the specific test group that
** displays a problem and then recompile and rerun.  Only that one group
** will be run.  To disable this mechanism, set the showGroup010 to 0.
*/
static const int showGroup010 = 0;

/*
** Notes so that drh will remember how to set up his breakpoints while
** working on memory issues detected by this module:
**
**     breakpoint th3oomEnable
**     run
**     disable 1
**     up
**     breakpoint
**     condition 2 ii=X && th3oom.iTestIter==Y
**     continue
**     br th3oomFault
**     continue
*/

static void oom010TestArray(th3state *p, char **az, int expectedrc, int tn){
  int ii=0;
  int rc;
  int iGroup = 1;

  if( showGroup010>1 ){
    while( az[ii] && showGroup010>iGroup ){
      while( az[ii] && az[ii][0] ){ ii++; }
      iGroup++;
      if( az[ii] ) ii++;
    }
  }
  for(; az[ii] && (showGroup010==0 || showGroup010==iGroup); ii++){
    char *zTest = th3format(p, "%d.%d[%d]", tn, iGroup, ii);
    if( (*az[ii])==0 ){
      /* th3print(p, "***** connection reset *****\n"); */
      th3dbNew(p, 0, "test.db");
      iGroup++;
    }else{
      const char *zSql = az[ii];
      int useTransaction = 1;
      if( zSql[0]=='*' ){
        zSql++;
        useTransaction = 0;
      }
      /* th3print(p, "*** "); */
      /* th3print(p, zSql); */
      /* th3print(p, " ***\n"); */
      th3oomBegin(p, zTest);
      while( th3oomNext(p) ){
        if( useTransaction ){
          th3dbEval(p, 0, "ROLLBACK");
          th3dbEval(p, 0, "BEGIN");
        }
        th3oomEnable(p, 1);
        rc = th3dbEval(p, 0, zSql);
        th3oomEnable(p, 0);
        if( th3oomHit(p) ){
          assert( rc==SQLITE_NOMEM );
          th3oomCheck(p, 1, rc==SQLITE_NOMEM );
        }else{
          assert( rc==expectedrc );
          th3oomCheck(p, 2, rc==expectedrc );
        }
      }
      th3oomEnd(p);
      th3dbEval(p, 0, "COMMIT");
    }
  }
}

int oom010(th3state *p){
  char *aStmt[] = {
    "CREATE TABLE Element(Code INTEGER PRIMARY KEY,Name VARCHAR(60))",
    "CREATE TABLE ElemOr(CodeOr INTEGER NOT NULL, Code INTEGER NOT NULL,"
          "PRIMARY KEY(CodeOr,Code))",
    "CREATE TABLE ElemAnd ("
      "CodeAnd INTEGER,"
      "Code INTEGER,"
      "Attr1 INTEGER,"
      "Attr2 INTEGER,"
      "Attr3 INTEGER,"
      "PRIMARY KEY(CodeAnd,Code))",
    
    "INSERT INTO Element VALUES(1,'Elem1')",
    "INSERT INTO Element VALUES(2,'Elem2')",
    "INSERT INTO Element VALUES(3,'Elem3')",
    "INSERT INTO Element VALUES(4,'Elem4')",
    "INSERT INTO Element VALUES(5,'Elem5')",
    "INSERT INTO ElemOr Values(3,4)",
    "INSERT INTO ElemOr Values(3,5)",
    "INSERT INTO ElemAnd VALUES(1,3,1,1,1)",
    "INSERT INTO ElemAnd VALUES(1,2,1,1,1)",
    
    "CREATE VIEW ElemView1 AS "
       "SELECT "
       " CAST(Element.Code AS VARCHAR(50)) AS ElemId,"
       " Element.Code AS ElemCode,"
       " Element.Name AS ElemName,"
       " ElemAnd.Code AS InnerCode,"
       " ElemAnd.Attr1 AS Attr1,"
       " ElemAnd.Attr2 AS Attr2,"
       " ElemAnd.Attr3 AS Attr3,"
       " 0 AS Level,"
       " 0 AS IsOrElem "
       "FROM Element JOIN ElemAnd ON ElemAnd.CodeAnd=Element.Code "
       "WHERE ElemAnd.CodeAnd NOT IN (SELECT CodeOr FROM ElemOr) "
       "UNION ALL "
       "SELECT "
       " CAST(ElemOr.CodeOr AS VARCHAR(50)) AS ElemId,"
       " Element.Code AS ElemCode,"
       " Element.Name AS ElemName,"
       " ElemOr.Code AS InnerCode,"
       " NULL AS Attr1,"
       " NULL AS Attr2,"
       " NULL AS Attr3,"
       " 0 AS Level,"
       " 1 AS IsOrElem "
       "FROM ElemOr JOIN Element ON Element.Code=ElemOr.CodeOr "
       "ORDER BY ElemId, InnerCode",
    
    "CREATE VIEW ElemView2 AS "
      "SELECT "
      " ElemId,"
      " ElemCode,"
      " ElemName,"
      " InnerCode,"
      " Attr1,"	
      " Attr2,"
      " Attr3,"
      " Level,"
      " IsOrElem "
      "FROM ElemView1 "
      "UNION ALL "
      "SELECT "
      " Element.ElemId || '.' || InnerElem.ElemId AS ElemId,"
      " InnerElem.ElemCode,"
      " InnerElem.ElemName,"
      " InnerElem.InnerCode,"
      " InnerElem.Attr1,"
      " InnerElem.Attr2,"
      " InnerElem.Attr3,"
      " InnerElem.Level+1,"
      " InnerElem.IsOrElem "
      "FROM ElemView1 AS Element "
      "JOIN ElemView1 AS InnerElem "
      "  ON Element.Level=0 AND Element.InnerCode=InnerElem.ElemCode "
      "ORDER BY ElemId, InnerCode",
 
    "SELECT * FROM ElemView1",
    "SELECT * FROM ElemView2",

    0
  };

  /* Open a database connection. This single connection will be used 
  ** for all testing done by this module.
  */
  th3dbNew(p, 0, "test.db");
  oom010TestArray(p, aStmt, SQLITE_OK, 0);

  return 0;
}
