/*
** Use this test module for interactive debugging of script tests modules.
** All files named on the th3 command line are read into memory then
** processed using th3testScript().
**
** MODULE_NAME:     script
** MAXIMUM_STRING:  10000000
*/

int script(th3state *p){
  FILE *in;
  long size;
  char *zContent;
  int i;

  for(i=1; i<th3Argc; i++){
    in = fopen(th3Argv[i], "r");
    if( in==0 ){
      th3print(p, "cannot open \"");
      th3print(p, th3Argv[i]);
      th3print(p, "\"\n");
      continue;
    }
    fseek(in, 0, SEEK_END);
    size = ftell(in);
    fseek(in, 0, SEEK_SET);
    zContent = th3malloc(p, size+2);
    if( zContent ){
      size = fread(zContent, 1, size, in);
      zContent[size] = '\n';
      zContent[size+1] = 0;
    }
    fclose(in);
    th3testScript(p, zContent, 0);
    th3free(p, zContent);
  }
  return 0;
}
