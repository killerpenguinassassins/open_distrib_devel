# This file generates (on standard output) a test script for TH2.
# Run this script using a version of TCL that has SQLite build-in,
# such as the testfixture.
#
#package require sqlite3

# Keywords.
# Do not use these as table or column names.
#
set keywords {
   ABORT                     EXCEPT                    PRAGMA
   ADD                       EXCLUSIVE                 PRIMARY
   AFTER                     EXISTS                    QUERY
   ALL                       EXPLAIN                   RAISE
   ALTER                     FAIL                      REFERENCES
   ANALYZE                   FOR                       REGEXP
   AND                       FOREIGN                   REINDEX
   AS                        FROM                      RENAME
   ASC                       FULL                      REPLACE
   ATTACH                    GLOB                      RESTRICT
   AUTOINCREMENT             GROUP                     RIGHT
   BEFORE                    HAVING                    ROLLBACK
   BEGIN                     IF                        ROW
   BETWEEN                   IGNORE                    SELECT
   BY                        IMMEDIATE                 SET
   CASCADE                   IN                        TABLE
   CASE                      INDEX                     TEMP
   CAST                      INITIALLY                 TEMPORARY
   CHECK                     INNER                     THEN
   COLLATE                   INSERT                    TO
   COLUMN                    INSTEAD                   TRANSACTION
   COMMIT                    INTERSECT                 TRIGGER
   CONFLICT                  INTO                      UNION
   CONSTRAINT                IS                        UNIQUE
   CREATE                    ISNULL                    UPDATE
   CROSS                     JOIN                      USING
   CURRENT_DATE              KEY                       VACUUM
   CURRENT_TIME              LEFT                      VALUES
   CURRENT_TIMESTAMP         LIKE                      VIEW
   DATABASE                  LIMIT                     VIRTUAL
   DEFAULT                   MATCH                     WHEN
   DEFERRABLE                NATURAL                   WHERE
   DEFERRED                  NOT                       _ROWID_
   DELETE                    NOTNULL                   MAIN
   DESC                      NULL                      OID
   DETACH                    OF                        ROWID
   DISTINCT                  OFFSET                    SQLITE_MASTER
   DROP                      ON                        SQLITE_SEQUENCE
   EACH                      OR                        SQLITE_TEMP_MASTER
   ELSE                      ORDER                     TEMP
   END                       OUTER
   ESCAPE                    PLAN
}

# Generate a random name that is not a keyword and that has not been
# used before.
#
set names_used {}
proc random_name {} {
  global names_used keywords
  set n [expr {rand()*3+1}]
  set charset abcdefghijklmnopqrstuvwxzy_0123456789
  set nused [llength $names_used]
  if {$nused>0 && [expr {rand()<0.4}]} {
    set name [lindex $names_used [expr {int(rand()*$nused)}]]
  } else {
    set name {}
  }
  set k 27
  for {set i 0} {$i<$n} {incr i} {
    append name [string index $charset [expr {int(rand()*$k)}]]
    set k 37
  }
  if {[regexp -nocase $name $keywords]} {
    set name _$name
  } elseif {[regexp -nocase {^sqlite} $name]} {
    set name _$name
  }
  while {[lsearch $names_used $name]>=0} {
    append name [expr {int(rand()*100)}]
  }
  lappend names_used $name
  return $name
}

# Scramble the $inlist into a random order.
#
proc scramble {inlist} {
  set y {}
  foreach x $inlist {
    lappend y [list [expr {rand()}] $x]
  }
  set y [lsort $y]
  set outlist {}
  foreach x $y {
    lappend outlist [lindex $x 1]
  }
  return $outlist
}

# Generate a random integer
#
proc random_int {} {
  set ndigit [expr {int(rand()*7+1)}]
  if {$ndigit==7} {
    set ndigit [expr {int(rand()*17+1)}]
  }
  set num {}
  set k 9
  for {set i 0} {$i<$ndigit} {incr i} {
    append num [string index {1234567890} [expr {int(rand()*$k)}]]
    set k 10
  }
  if {[expr {rand()<0.5}]} {
    set num -$num
  }
  return $num
}

# Generate a random floating-point value
#
proc random_real {} {
  global allow_real
  if {$allow_real} {
    return [expr {int(rand()*1024*2048)/1024.0}]
  } else {
    return [random_int]
  }
}

# Generate a random string literal
#
proc random_text {} {
  set wordlist {
    able                      group                     public
    ask                       hand                      right
    bad                       have                      same
    be                        high                      say
    big                       hundred                   see
    call                      important                 seem
    case                      know                      seven
    child                     large                     six
    come                      last                      small
    company                   leave                     take
    day                       life                      tell
    different                 little                    ten
    do                        long                      thing
    early                     look                      think
    eight                     make                      three
    eye                       man                       time
    fact                      new                       true
    false                     next                      try
    feel                      nine                      two
    few                       no                        use
    find                      number                    want
    first                     old                       way
    five                      one                       week
    four                      other                     woman
    get                       own                       work
    give                      part                      world
    go                        person                    year
    good                      place                     yes
    government                point                     young
    great                     problem
  }
  set wlen [llength $wordlist]
  set nword [expr {int(rand()*4+1)}]
  if {$nword==3} {
    set nword [expr {int(rand()*50+3)}]
  }
  set phrase {}
  
  for {set i 0} {$i<$nword} {incr i} {
    lappend phrase [lindex $wordlist [expr {int(rand()*$wlen)}]]
  }
  return '$phrase'
}

# Generate a random table declaration
#
set table_list {}
proc random_table {} {
  global table_list table_info allow_real
  set tn [random_name]
  lappend table_list $tn
  set sql "CREATE TABLE ${tn}(\n"
  set ncol [expr {int(rand()*8)+1}]
  if {$ncol==8} {
    incr ncol [expr {int(rand()*100)}]
  }
  set table_info($tn,ncol) $ncol
  set clist {}
  set intpk 0
  set intpkcn {}
  for {set i 0} {$i<$ncol} {incr i} {
    set cn [random_name]
    lappend clist $cn
    set table_info($tn,colname,$i) $cn
    set table_info($tn,colidx,$cn) $i
    if {$allow_real} {
      set ctype [expr {int(rand()*7)}]
      set typename [lindex {INTEGER TEXT CLOB INT REAL FLOAT NUMERIC} $ctype]
      set mtype [lindex {i t t i r r r} $ctype]
    } else {
      set ctype [expr {int(rand()*5)}]
      set typename [lindex {INTEGER TEXT CLOB INT NUMERIC} $ctype]
      set mtype [lindex {i t t i i} $ctype]
    }
    set table_info($tn,coltype,$cn) $mtype
    set table_info($tn,coltypename,$cn) $typename
    append sql "  $cn $typename"
    if {!$intpk && $typename=="INTEGER" && rand()<0.1} {
      set intpk 1
      set table_info($tn,intpk) $cn
      append sql " PRIMARY KEY,\n"
      set intpkcn $cn
      set table_info($tn,notnull,$cn) 0
      set table_info($tn,dflt,$cn) %
      continue
    }
    set constlist {}
    if {rand()<0.25} {
      lappend constlist {NOT NULL}
      set table_info($tn,notnull,$cn) 1
    } else {
      set table_info($tn,notnull,$cn) 0
    }
    if {rand()<0.25} {
      switch $mtype {
        i {set v [random_int]}
        r {set v [random_real]}
        t {set v [random_text]}
      }
      set table_info($tn,dflt,$cn) $v
      lappend constlist "DEFAULT $v"
    } else {
      set table_info($tn,dflt,$cn) %
    }
    if {[llength $constlist]} {
      append sql " [join [scramble $constlist] { }]"
    }
    append sql ",\n"
  }
  set table_info($tn,collist) $clist
  set npk [expr {int(rand()*4)}]
  if {$npk>$ncol} {set npk $ncol}
  if {$intpk} {set npk 0}
  set keys {}
  if {$npk>0} {
    set x [lrange [scramble $clist] 0 [expr {$npk-1}]]
    append sql "  PRIMARY KEY([join $x ,]),\n"
    set f [lindex $x 0]
    if {$npk==1 && $table_info($tn,coltypename,$f)=="INTEGER"} {
      set table_info($tn,intpk) $f
    } else {
      set table_info($tn,pk) $x
      lappend keys $x
    }
  }
  if {$npk==0} {
    set table_info($tn,pk) {}
  }
  set nunique [expr {int(rand()*5)}]
  for {set i 0} {$i<$nunique} {incr i} {
    set n [expr {int(rand()*3+1)}]
    if {$n>$ncol} {set n $ncol}
    set x [lrange [scramble $clist] 0 [expr {$n-1}]]
    if {$intpk && [lsearch $x $intpkcn]>=0} continue
    if {[lsearch $keys $x]>=0} continue
    append sql "  UNIQUE([join $x ,]),\n"
    lappend keys $x
  }
  set table_info($tn,keys) $keys
  set table_info($tn,content) {}
  set sql "[string range $sql 0 end-2]\n)"
  return $sql
}

# Generate a random INSERT statement for table $tab.  Select the
# table at random if $tab is {}
#
proc random_insert {{tab {}}} {
  if {$tab==""} {
    global table_list
    set i [expr {int(rand()*[llength $table_list])}]
    set tab [lindex $table_list $i]
  }
  global table_info
  global ins_info
  unset -nocomplain ins_info
  set ins_info(tab) $tab
  set clist $table_info($tab,collist)
  set ncol [llength $clist]
  if {[info exists table_info($tab,intpk)]} {
    set intpkcn $table_info($tab,intpk)
  } else {
    if {rand()<0.25} {
      set intpkcn [lindex {rowid _rowid_ oid} [expr {int(rand()*3)}]]
      lappend clist $intpkcn
    } else {
      set intpkcn {}
    }
  }
  set clist [scramble $table_info($tab,collist)]
  foreach x $clist {
    set ins_info(value,$x) $table_info($tab,dflt,$x)
  }
  set ins_info(value,$intpkcn) \
      [expr {[llength $table_info($tab,content)] + 1}]
  if {rand()<0.2} {
    set n [llength $clist]
    set i [expr {int(rand()*$n)}]
    set clist [lrange $clist 0 $i]
  }
  set vlist {}
  foreach x $clist {
    if {$intpkcn==$x} {
      lappend vlist $ins_info(value,$x)
      continue
    }
    set type $table_info($tab,coltype,$x)
    if {rand()<0.05 && $table_info($tab,notnull,$x)==0} {
      lappend vlist NULL
      set ins_info(value,$x) %
      continue
    }
    if {$type=="i"} {
      set v [random_int]
    } elseif {$type=="r"} {
      set v [random_real]
    } else {
      set v [random_text]
    }
    set ins_info(value,$x) $v
    lappend vlist $v
  }
  set sql "INSERT INTO ${tab}([join $clist ,]) VALUES([join $vlist ,])"
  return $sql
}

# Indent $text by $amt spaces
#
proc indent {amt text} {
  set sp [string repeat { } $amt]
  return "$sp[string map [list \n "\n$sp"] [string trim $text]]"
}

# Generate a random WHERE clause against table $tab that will
# select a single row from that table.
#
# Return a list of two elements:  The first element is the
# text of the WHERE clause and the second element is row that
# remains after the WHERE clause has run.
#
proc random_where {tab} {
  global table_info
  set clist $table_info($tab,collist)
  set ncol [llength $clist]
  set keylist $table_info($tab,keys)
  set nkey [llength $keylist]
  if {$nkey>0 && rand()<0.25} {
    set tlist [lindex $keylist [expr {int(rand()*$nkey)}]]
  } elseif {[info exists table_info($tab,intpk)] && rand()<0.25} {
    set tlist $table_info($tab,intpk)
  } else {
    set tlist [lrange [scramble $clist] 0 [expr {int(rand()*$ncol)}]]
  }
  set nterm [llength $tlist]
  set where {}
  set join WHERE
  set cntdown [llength $tlist]
  set content $table_info($tab,content)
  foreach x $tlist {
    incr cntdown -1
    set idx $table_info($tab,colidx,$x)
    set alldata {}
    unset -nocomplain dataset
    foreach row $content {
      set v [lindex $row $idx]
      lappend alldata $v
      if {$v!="%"} {
        set dataset($v) 1
      }
    }
    set unique [array names dataset]
    if {$cntdown && [llength $unique]>1 && rand()<0.2} {
      set n [expr {[llength $unique]/2}]
      set uset [lrange [scramble $unique] 0 $n]
      set uset2 [string map {% NULL} $uset]
      append where "$join $x IN ([join $uset2 ,])"
      set join { AND}
      set ncontent {}
      foreach row $content {
        set v [lindex $row $idx]
        if {[lsearch $uset $v]<0} continue
        lappend ncontent $row
      }
      set content $ncontent
      continue
    }

    set nrow [llength $content]
    set row [lindex $content [expr {int(rand()*$nrow)}]]
    set v [lindex $row $idx]
    if {$v=="%"} {
      append where "$join $x IS NULL"
    } elseif {rand()<0.5} {
      append where "$join $x=$v"
    } else {
      append where "$join $v=$x"
    }
    set join { AND}
    set ncontent {}
    foreach row $content {
      set k [lindex $row $idx]
      if {$v==$k} {
        lappend ncontent $row
      }
    }
    set content $ncontent
  }
  return [list $where $content]
}

##############################################################################
# Begin generating the test script.
#

# Multiple random tests.
#
for {set randid 1} {$randid<=20} {incr randid} {
expr {srand($randid+100)}
set module_name [format select01%02d $randid]
set outfile $module_name.test
set out [open $outfile w]
set allow_real [expr {$randid%2==0}]

# Output initialization code.
# 
puts $out "/*
** Generated by script $argv0.
**
** SCRIPT_MODULE_NAME:        $module_name
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
** MAXIMUM_STRING:            1000000
*/"
if {$allow_real} {
  puts $out "#ifndef SQLITE_OMIT_FLOATING_POINT"
}
puts $out "--testcase $randid.0"

# Do some CREATE TABLE statements.  The verify that all tables
# are accounted for.
#
set ntab [expr {int(rand()*5+2)}]
sqlite3 db :memory:
for {set i 0} {$i<$ntab} {incr i} {
  set sql [random_table]
  puts $out "$sql;"
  db eval $sql
}
puts $out "/* Verify that all tables appear in the sqlite_master table */"
puts $out "SELECT name FROM sqlite_master WHERE type='table';"
puts $out "--result $table_list\n"

# For each table, verify that the correct number of indices are
# created.
#
foreach tab $table_list {
  set keys $table_info($tab,keys)
  set nkey [expr {[llength $keys]+1}]
  puts $out ""
  puts $out "/* Verify that there are [expr {$nkey-1}] indices on table $tab */"
  puts $out "--testcase $randid-$tab"
  puts $out "SELECT count(*) FROM sqlite_master WHERE tbl_name='$tab';"
  puts $out "--result $nkey\n"
}

# Generate a bunch of INSERT statements.
#
set total_length 0
set nins [expr {$ntab*int(rand()*50+5)}]
puts $out ""
puts $out "/* Insert some data */"
puts $out "--testcase $randid-ins"
for {set i 0} {$i<$nins} {incr i} {
  set sql [random_insert]
  if {[catch {db eval $sql}]} {
    incr i -1
    continue
  }
  puts $out "$sql;"
  set tab $ins_info(tab)
  set clist $table_info($tab,collist)
  set row {}
  foreach x $clist {
    lappend row $ins_info(value,$x)
  }
  lappend table_info($tab,content) $row
}
puts $out "--result\n"


# Do lots of single-row SELECT statements.
#
for {set i 1} {$i<=100} {incr i} {
  set j [expr {$i%[llength $table_list]}]
  set tab [lindex $table_list $j]
  foreach {where content} [random_where $tab] break
  if {[llength $content]!=1} continue
  puts $out ""
  puts $out "--testcase $randid.$tab.$i"
  set clist $table_info($tab,collist)
  set ncol [llength $clist]
  if {$ncol<=3} {
    set idxlist [scramble [lrange {0 1 2} 0 [expr {$ncol-1}]]]
  } else {
    set n [expr {int(rand()*8+1)}]
    set idxlist {}
    for {set j 0} {$j<$n} {incr j} {
      lappend idxlist [expr {int(rand()*$ncol)}]
    }
  }
  set sep "SELECT"
  set sql {}
  foreach idx $idxlist {
    append sql "$sep [lindex $clist $idx]"
    set sep ,
  }
  append sql " FROM $tab $where"
  puts $out $sql;
  set res {}
  set row [lindex $content 0]
  foreach idx $idxlist {
    set v [string trim [lindex $row $idx] ']
    if {$v=="%"} {
      append res " nil"
    } elseif {[string first " " $v]>=0} {
      append res " {$v}"
    } else {
      append res " $v"
    }
  }
  puts $out "--result [string trim $res]\n"
}

if {$allow_real} {
  puts $out "#endif /* SQLITE_OMIT_FLOATING_POINT */"
}

# End the outer loop
unset table_info
set table_list {}
set names_used {}
close $out
}
