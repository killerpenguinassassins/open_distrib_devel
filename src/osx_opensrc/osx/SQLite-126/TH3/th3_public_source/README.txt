This is README file is in the top-level of the source tree for TH3.
The directories of the source tree are laid out as follows:

   cfg/       Various configuration scripts
   cov1/      First set of structural coverage tests
   dev/       Development testing
   fts/       Tests for the Full Text Search engine
   hlr1/      First set of high-level requirements tests
   misc/      Miscellaneous files.  Scripts to process gcov output.
   stress/    Stress tests.  Crash tests, fuzz tests, OOM tests, etc.
   www/       Documentation and website content files.
