package require sqlite3
sqlite3 db {}
set sqlfd [open db1.sql r]
set sql [read $sqlfd]
close $sqlfd
db eval $sql
db eval {SELECT rowid, a, b, c FROM t1} {
  unset -nocomplain seenall
  foreach field {a b c} {
    set x [string tolower [string trim [set $field]]]
    regsub -all {[^a-z0-9]+} $x { } x
    set phrase($field:$rowid) $x
    unset -nocomplain seen
    foreach word $x {
      if {[info exists seen($word)]} continue
      set seen($word) 1
      lappend revidx_${field}($word) $rowid
      if {[info exists seenall($word)]} continue
      set seenall($word) 1
      lappend revidx_all($word) $rowid
    }
  }
  lappend list_rowid $rowid
}

set list_all [array names revidx_all]
set list_a [array names revidx_a]
set list_b [array names revidx_b]
set list_c [array names revidx_c]
set list_phrase [array names phrase]
set cnt_all [llength $list_all]
set cnt_a [llength $list_a]
set cnt_b [llength $list_b]
set cnt_c [llength $list_c]
set cnt_phrase [llength $list_phrase]
expr srand(0)

proc makephrase {} {
  global list_phrase cnt_phrase phrase
  set len [expr {int(rand()*4)+2}]
  while {1} {
    set p $phrase([lindex $list_phrase [expr {int(rand()*$cnt_phrase)}]])
    set n [llength $p]
    if {$n>=2} break
  }
  if {$len>$n} {set len $n}
  set i [expr {int(rand()*($n-$len))}]
  set j [expr {$i+$len-1}]
  set p [lrange $p $i $j]
  foreach id $list_phrase {
    set x $phrase($id)
    if {[string first " $p " " $x "]>=0} {
      foreach {field rowid} [split $id :] break
      set rx($rowid) 1
    }
  }
  set res [lsort -int [array names rx]]
  return [list \"$p\" $res]
}

proc isnear {leftterm rightterm dist rowid} {
  global phrase
  foreach field {a b c} {
  set p $phrase($field:$rowid)
    set ilist1 [lsearch -all $p $leftterm]
    set ilist2 [lsearch -all $p $rightterm]
    foreach i1 $ilist1 {
      foreach i2 $ilist2 {
        if {abs($i1-$i2)<=$dist+1 && $i1!=$i2} {return 1}
      }
    }
  }
  return 0
}

proc makenear {dist} {
  global list_phrase cnt_phrase phrase revidx_all
  while {1} {
    set p $phrase([lindex $list_phrase [expr {int(rand()*$cnt_phrase)}]])
    set n [llength $p]
    if {$n>=$dist} break
  }
  set i [expr {int(rand()*($n-$dist))}]
  set j [expr {$i+$dist-1}]
  if {rand()<0.5} {
    set t $j
    set j $i
    set i $t
  }
  set leftterm [lindex $p $i]
  set rightterm [lindex $p $j]
  if {$dist==10 && rand()<0.8} {
    set op NEAR
  } else {
    set op NEAR/$dist
  }
  if {rand()<0.5} {
    set term "($leftterm $op $rightterm)"
  } else {
    set term "$leftterm $op $rightterm"
  }

  foreach r $revidx_all($leftterm) {
    set lx($r) 1
  }
  set andres {} 
  foreach r $revidx_all($rightterm) {
    if {[info exists lx($r)]} {
      lappend andres $r
    }
  }
  set res {}
  foreach r $andres {
    if {[isnear $leftterm $rightterm $dist $r]} {
      lappend res $r
    }
  }
  return [list $term $res]
}

proc makeexpr {depth limit} {
  global revidx_all revidx_a revidx_b revidx_c
  global list_all list_a list_b list_c
  global cnt_all cnt_a cnt_b cnt_c
  if {rand()*$limit<$depth} {
    if {rand()<0.2} {
      return [makephrase]
    } elseif {rand()<0.2} {
      return [makenear [expr {int(rand()*10+5)}]]
    } elseif {rand()<0.75} {
      set term [lindex $list_all [expr {int(rand()*$cnt_all)}]]
      return [list $term $revidx_all($term)]
    } elseif {rand()<0.3333} {
      set term [lindex $list_a [expr {int(rand()*$cnt_a)}]]
      return [list a:$term $revidx_a($term)]
    } elseif {rand()<0.5} {
      set term [lindex $list_b [expr {int(rand()*$cnt_b)}]]
      return [list b:$term $revidx_b($term)]
    } else {
      set term [lindex $list_c [expr {int(rand()*$cnt_c)}]]
      return [list c:$term $revidx_c($term)]
    }
  } elseif {rand()<0.05} {
    foreach {term res} [makeexpr $depth $limit] break
    return [list ($term) $res]
  } else {
    set dp1 [expr {$depth+1}]
    foreach {leftterm leftres} [makeexpr $dp1 $limit] break
    foreach {rightterm rightres} [makeexpr $dp1 $limit] break
    set leftsz [llength $leftres]
    set rightsz [llength $rightres]
    unset -nocomplain lx
    foreach r $leftres {
      set lx($r) 1
      set orx($r) 1
      set notx($r) 1
    }
    set andres {} 
    foreach r $rightres {
      if {[info exists lx($r)]} {
        lappend andres $r
      }
      unset -nocomplain notx($r)
      set orx($r) 1
    }
    set orres [lsort -int [array names orx]]
    set notres [lsort -int [array names notx]]
    if {rand()<0.2 && [llength $notres]>0} {
      return [list "($leftterm NOT $rightterm)" $notres]
    } elseif {[llength $andres]>0} {
      return [list "($leftterm AND $rightterm)" $andres]
    } else {
      return [list "($leftterm OR $rightterm)" $orres]
    }
  }
}

# print a string such that it does not exceed the 80-character line.
#
proc pr_c_string {x terminator} {
  while {[string length $x]>65} {
    set j 64
    while {$j>40 && [string index $x $j]!=" "} {incr j -1}
    set y [string range $x 0 $j]
    set x [string range $x [expr {$j+1}] end]
    puts "      \"[string map {\" \\\"} $y]\""
  }
  puts "      \"[string map {\" \\\"} $x]\"$terminator"
}

if 0 {
  set testleft   change
  set testright  like
  set testdist   5

  set res {}
  foreach r $list_rowid {
    if {[isnear $testleft $testright $testdist $r]} {
      lappend res $r
    }
  }
  puts "$testleft NEAR/$testdist $testright  =>  $res"
  exit
}
if 0 {
  parray revidx_all
  parray revidx_a
  parray revidx_b
  parray revidx_c
  exit
}

for {set i 1000} {$i<3000} {incr i} {
  set limit [expr {4+($i-1000)/500}]
  while 1 {
    foreach {term res} [makeexpr 0 $limit] break
    if {[llength $res]>0 || rand()<0.05} break
  }
  puts "    \173\n      $i,"
  if {[string index $term 0]=="("} {
    set x [string range $term 1 end-1]
  } else {
    set x $term
  }
  pr_c_string $x ,
  pr_c_string $res {}
  puts "    \175,"
}
