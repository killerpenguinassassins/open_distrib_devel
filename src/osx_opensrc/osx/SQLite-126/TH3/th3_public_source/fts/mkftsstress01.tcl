# Output the test module header
#
puts {/*
** Testing of FTS3/4
**
** MODULE_NAME:               ftsstress01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
#if !defined(SQLITE_ENABLE_FTS3)
int ftsstress01(th3state *p){ return 0; }
#else}


# Load the FTS data
set fd [open kjv-data.tcl]
set data [read $fd]
close $fd

# Simplify the content by removing all excess space and punctuation and
# lower-casing everything.
#
foreach {docid content} $data {
  set cx [string tolower $content]
  regsub -all {[^a-z]} $cx { } cx
  regsub -all {  +} [string trim $cx] { } cx
  lappend data2 $docid $cx
  foreach w $cx {
    set totalcnt($w) 0
    set versecnt($w) 0
  }
}

# Count the occurrences of every word in the corpus
#
set ntoken 0
foreach {docid content} $data2 {
  unset -nocomplain seen
  foreach word $content {
    incr ntoken
    incr totalcnt($word)
    if {[info exists seen($word)]} continue
    set seen($word) 1
    incr versecnt($word)
    lappend usedin($word) $docid
  }
}

# Assign numbers to tokens such that the most frequently used tokens
# get the lowest numbers.
#
set tlist0 {}
foreach w [array names totalcnt] {
  set key [expr {1000000-$versecnt($w)}]
  lappend tlist0 [format "%07d %s %d" $key $w $versecnt($w)]
}
set tlist0 [lsort $tlist0]
set i 0
set tlist {}
foreach x $tlist0 {
  foreach {key w cnt} $x break
  set token($w) $i
  incr i
  lappend tlist [list $cnt $w]
}


# A methods for incrementally generating lines of integer initializer.
#
set zLine {}
set nLine 0
proc output_int {x} {
  global zLine nLine
  set txt "$x,"
  set nTxt [string length $txt]
  if {$nTxt+$nLine>70} flush_int
  append zLine $txt
  incr nLine $nTxt
}
proc flush_int {} {
  global nLine zLine
  if {$nLine==0} return
  puts "  $zLine"
  set zLine {}
  set nLine 0
}

# Output the aToken[] array.
#
puts {/* Integer tokens representing the content of every document */}
puts "static const short int ftsstress01_aToken\[\] = \173"
foreach {docid content} $data2 {
  foreach w $content {
    output_int $token($w)
  }
  set doc_length($docid) [llength $content]
}
flush_int
puts "\175;"

# Output the aContent[] array.
#
puts {/* The documents */}
puts "static const struct \173"
puts "  int docid;          /* The document ID */"
puts "  int nToken;         /* Number of tokens in the document */"
puts "  int iToken;         /* Index in aToken[] of the first token */"
puts "  const char *zText;  /* Document text */"
puts "\175 ftsstress01_aContent\[\] = \173"
set iToken 0
set iDoc 0
foreach {docid content} $data {
  set n $doc_length($docid)
  set txt [string map {\" \\\"} $content]
  puts "  \173 $docid, $n, $iToken, \"$txt\" \175,"
  set docid_to_id($docid) $iDoc
  incr iToken $n
  incr iDoc
}
puts "\175;"

# Output the aDoclist[] array.
#
puts {/* Document lists for each token */}
puts "static const int ftsstress01_aDoclist\[\] = \173"
set iDoclist 0
foreach x $tlist {
  foreach {cnt w} $x break
  set doclist_offset($w) $iDoclist
  foreach id $usedin($w) {
    output_int $docid_to_id($id)
    incr iDoclist
  }
}
flush_int
puts "\175;"

# Output the aLexeme[] array
#
puts {/* The tokens */}
puts "static const struct ftsstress01Lexeme \173"
puts "  int nDoc;"
puts "  int iDoc;"
puts "  const char *z;"
puts "\175 ftsstress01_aLexeme\[\] = \173"
foreach x $tlist {
  foreach {cnt w} $x break
  set ndoc [llength $usedin($w)]
  puts "  \173 $ndoc, $doclist_offset($w), \"$w\" \175,"
}
puts "\175;"

puts {

/* An instance of this structure shows which documents are part
** of the FTS table on a particular test instance.
*/
typedef struct ftsstress01Scope ftsstress01Scope;
struct ftsstress01Scope {
  int gapStart;
  int gapEnd;
};

/* A list of document IDs in sorted order */
typedef struct ftsstress01Doclist ftsstress01Doclist;
struct ftsstress01Doclist {
  th3state *p;  /* Needed for th3malloc() and th3free() */
  int nDoc;     /* Number of documents */
  int *aDoc;    /* The documents */
  int nAlloc;   /* Slots allocated for aDoc[] */
};


/* Routines for managing document lists
*/
void ftsstress01_free(ftsstress01Doclist *p1){
  if( p1->p && p1->nAlloc ) th3free(p1->p, p1->aDoc);
  memset(p1, 0, sizeof(*p1));
}

/* Append a new docid to a doclist
*/
void ftsstress01_append(ftsstress01Doclist *p1, int docid){
  if( p1->nDoc>=p1->nAlloc ){
    p1->nAlloc = p1->nAlloc*2 + 10;
    p1->aDoc = th3realloc(p1->p, p1->aDoc, sizeof(p1->aDoc[0])*p1->nAlloc);
  }
  p1->aDoc[p1->nDoc++] = docid;
}

/* Initialize a doclist from an aLexeme entry.
*/
static void ftsstress01_fromlexeme(
  const struct ftsstress01Lexeme *pSrc,
  ftsstress01Doclist *pOut,
  struct ftsstress01Scope *pScope
){
  int i, n;
  pOut->nDoc = 0;
  i = pSrc->iDoc;
  n = pSrc->nDoc + i;
  while( i<n ){
    int docid = ftsstress01_aContent[ftsstress01_aDoclist[i++]].docid;
    if( docid<pScope->gapStart || docid>pScope->gapEnd ){
      ftsstress01_append(pOut, docid);
    }
  }
}


/* Take the union of two doclists
*/
void ftsstress01_union(
  const ftsstress01Doclist *p1,
  const ftsstress01Doclist *p2,
  ftsstress01Doclist *pOut
){
  int i1 = 0;
  int i2 = 0;
  ftsstress01Doclist sCopy;
  assert( p2!=pOut );
  if( p1==pOut ){
    sCopy = *p1;
    pOut->nDoc = 0;
    pOut->aDoc = 0;
    pOut->nAlloc = 0;
    p1 = &sCopy;
  }else{
    sCopy.p = 0;
    pOut->nDoc = 0;
  }
  while( p1->nDoc>i1 && p2->nDoc>i2 ){
    if( p1->aDoc[i1]==p2->aDoc[i2] ){
      ftsstress01_append(pOut, p1->aDoc[i1]);
      i1++;
      i2++;
    }else if( p1->aDoc[i1]<p2->aDoc[i2] ){
      ftsstress01_append(pOut, p1->aDoc[i1]);
      i1++;
    }else{
      ftsstress01_append(pOut, p2->aDoc[i2]);
      i2++;
    }
  }
  while( p1->nDoc>i1 ){
   ftsstress01_append(pOut, p1->aDoc[i1++]);
  }
  while( p2->nDoc>i2 ){
   ftsstress01_append(pOut, p2->aDoc[i2++]);
  }
  ftsstress01_free(&sCopy);
}


/* Take the intersection of two doclists
*/
void ftsstress01_intersect(
  const ftsstress01Doclist *p1,
  const ftsstress01Doclist *p2,
  ftsstress01Doclist *pOut
){
  int i1 = 0;
  int i2 = 0;
  assert( p1!=pOut );
  assert( p2!=pOut );
  pOut->nDoc = 0;
  while( p1->nDoc>i1 && p2->nDoc>i2 ){
    if( p1->aDoc[i1]==p2->aDoc[i2] ){
      ftsstress01_append(pOut, p1->aDoc[i1]);
      i1++;
      i2++;
    }else if( p1->aDoc[i1]<p2->aDoc[i2] ){
      i1++;
    }else{
      i2++;
    }
  }
}

/* Render a doclist as a string.
*/
char *ftsstress01_render(ftsstress01Doclist *p1){
  char *z;
  int i, j, n;
  n = p1->nDoc * 12 + 1;
  z = th3malloc(p1->p, n);
  for(i=j=0; i<p1->nDoc; i++){
    if( i>0 ) z[j++] = ' ';
    sqlite3_snprintf(n-j, &z[j], "%d", p1->aDoc[i]);
    j += strlen(&z[j]);
  }
  z[j] = 0;
  return z;
}


/*
** Run a sequence of tests on the current FTS table.
*/
static void ftsstress01_base(
  th3state *p,
  int baseTestnum,
  ftsstress01Scope *pScope
){
  int i, c;
  char *zTruth;
  const char *z;
  const struct ftsstress01Lexeme *pLex;
  ftsstress01Doclist x1, x2, x3;

  memset(&x1, 0, sizeof(x1));
  memset(&x2, 0, sizeof(x2));
  memset(&x3, 0, sizeof(x3));
  x1.p = x2.p = x3.p = p;

  /* Do a query against every individual lexeme */
  for(i=0; i<COUNT(ftsstress01_aLexeme); i++){
    z = ftsstress01_aLexeme[i].z;
    th3testBegin(p, th3format(p, "%d-%s", baseTestnum+1, z));
    th3dbEval(p, 0, th3format(p,
        "SELECT docid FROM t1 WHERE t1 MATCH '%s' ORDER BY docid", z
    ));
    ftsstress01_fromlexeme(&ftsstress01_aLexeme[i], &x1, pScope);
    zTruth = ftsstress01_render(&x1);
    th3testCheck(p, zTruth);   
    th3free(p, zTruth);
  }

  /* Do a query against x* for all x in a..z */
  for(c='a'; c<='d'; c++){
    th3testBegin(p, th3format(p, "%d-%c*", baseTestnum+2, c));
    th3dbEval(p, 0, th3format(p,
        "SELECT docid FROM t1 WHERE t1 MATCH '%c*' ORDER BY docid", c
    ));
    x1.nDoc = 0;
    for(i=0; i<COUNT(ftsstress01_aLexeme); i++){
      if( ftsstress01_aLexeme[i].z[0]!=c ) continue;
      ftsstress01_fromlexeme(&ftsstress01_aLexeme[i], &x2, pScope);
      ftsstress01_union(&x1, &x2, &x1);
    }
    zTruth = ftsstress01_render(&x1);
    th3testCheck(p, zTruth);   
    th3free(p, zTruth);

    for(i=0; i<4; i++){
      int r;
      if( i==0 ){
        r = th3randomInt(p)%25;
      }else if( i==1 ){
        r = COUNT(ftsstress01_aLexeme)-1-(th3randomInt(p)%25);
      }else{
        r = th3randomInt(p)%COUNT(ftsstress01_aLexeme);
      }
      pLex = &ftsstress01_aLexeme[r];
      th3testBegin(p, th3format(p, "%d-%c*-%s", baseTestnum+2, c, pLex->z));
      th3dbEval(p, 0, th3format(p,
          "SELECT docid FROM t1 WHERE t1 MATCH '%c* %s' ORDER BY docid",
          c, pLex->z
      ));
      ftsstress01_fromlexeme(pLex, &x2, pScope);
      ftsstress01_intersect(&x1, &x2, &x3);
      zTruth = ftsstress01_render(&x3);
      th3testCheck(p, zTruth);   

      th3testBegin(p, th3format(p, "%d-%s-%c*", baseTestnum+2, pLex->z, c));
      th3dbEval(p, 0, th3format(p,
          "SELECT docid FROM t1 WHERE t1 MATCH '%s %c*' ORDER BY docid",
          pLex->z, c
      ));
      th3testCheck(p, zTruth);   
      th3free(p, zTruth);

      th3testBegin(p, th3format(p, "%d-%c*-OR-%s", baseTestnum+2, c, pLex->z));
      th3dbEval(p, 0, th3format(p,
          "SELECT docid FROM t1 WHERE t1 MATCH '%c* OR %s' ORDER BY docid",
          c, pLex->z
      ));
      ftsstress01_union(&x1, &x2, &x3);
      zTruth = ftsstress01_render(&x3);
      th3testCheck(p, zTruth);   

      th3testBegin(p, th3format(p, "%d-%s-OR-%c*", baseTestnum+2, pLex->z, c));
      th3dbEval(p, 0, th3format(p,
          "SELECT docid FROM t1 WHERE t1 MATCH '%s OR %c*' ORDER BY docid",
          pLex->z, c
      ));
      th3testCheck(p, zTruth);   
      th3free(p, zTruth);
    }
  }  

  ftsstress01_free(&x1);
  ftsstress01_free(&x2);
  ftsstress01_free(&x3);

}

/*
** The main test module
*/
int ftsstress01(th3state *p){
  sqlite3_stmt *pStmt;
  int i, rc;
  ftsstress01Scope scope;

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
    "CREATE VIRTUAL TABLE t1 USING fts4;"
  );
  pStmt = th3dbPrepare(p, 0, "INSERT INTO t1(docid, content) VALUES(?,?)");
  rc = SQLITE_OK;
  th3dbEval(p, 0, "BEGIN");
  for(i=0; rc==SQLITE_OK && i<COUNT(ftsstress01_aContent); i++){
    sqlite3_bind_int(pStmt, 1, ftsstress01_aContent[i].docid);
    sqlite3_bind_text(pStmt, 2,ftsstress01_aContent[i].zText,-1,SQLITE_STATIC);
    sqlite3_step(pStmt);
    rc = sqlite3_reset(pStmt);
  }
  th3dbEval(p, 0, "COMMIT");
  sqlite3_finalize(pStmt);
  th3testCheckInt(p, SQLITE_OK, rc);
  scope.gapStart = 0;
  scope.gapEnd = 0;
  ftsstress01_base(p, 100, &scope);

  th3testBegin(p, "200");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
    "CREATE VIRTUAL TABLE t1 USING fts4;"
  );
  pStmt = th3dbPrepare(p, 0, "INSERT INTO t1(docid, content) VALUES(?,?)");
  rc = SQLITE_OK;
  th3dbEval(p, 0, "BEGIN");
  for(i=COUNT(ftsstress01_aContent)-1; rc==SQLITE_OK && i>=0; i--){
    sqlite3_bind_int(pStmt, 1, ftsstress01_aContent[i].docid);
    sqlite3_bind_text(pStmt, 2,ftsstress01_aContent[i].zText,-1,SQLITE_STATIC);
    sqlite3_step(pStmt);
    rc = sqlite3_reset(pStmt);
  }
  th3dbEval(p, 0, "COMMIT");
  sqlite3_finalize(pStmt);
  th3testCheckInt(p, SQLITE_OK, rc);
  scope.gapStart = 0;
  scope.gapEnd = 0;
  ftsstress01_base(p, 200, &scope);

  th3testBegin(p, "300");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
    "CREATE VIRTUAL TABLE t1 USING fts4;"
  );
  pStmt = th3dbPrepare(p, 0, "INSERT INTO t1(docid, content) VALUES(?,?)");
  rc = SQLITE_OK;
  th3dbEval(p, 0, "BEGIN");
  for(i=COUNT(ftsstress01_aContent)-1; rc==SQLITE_OK && i>=0; i--){
    sqlite3_bind_int(pStmt, 1, ftsstress01_aContent[i].docid);
    sqlite3_bind_text(pStmt, 2,ftsstress01_aContent[i].zText,-1,SQLITE_STATIC);
    sqlite3_step(pStmt);
    rc = sqlite3_reset(pStmt);
  }
  th3dbEval(p, 0, "COMMIT");
  sqlite3_finalize(pStmt);
  i = COUNT(ftsstress01_aContent)/3;
  scope.gapStart = ftsstress01_aContent[i].docid;
  scope.gapEnd = ftsstress01_aContent[i+i].docid;
  th3dbEval(p, 0, th3format(p, 
     "DELETE FROM t1 WHERE docid BETWEEN %d and %d",
     scope.gapStart, scope.gapEnd
  ));
  th3testCheckInt(p, SQLITE_OK, rc);
  ftsstress01_base(p, 300, &scope);

  return 0;
}
}

# Terminate the #ifdef
#
puts {#endif /* SQLITE_ENABLE_FTS3 */}
