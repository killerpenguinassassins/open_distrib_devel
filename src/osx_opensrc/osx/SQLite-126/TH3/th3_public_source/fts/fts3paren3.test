/*
** A module used to test the parentheses syntax of FTS3
**
** MODULE_NAME:               fts3paren3
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
#if !defined(SQLITE_ENABLE_FTS3) || !defined(SQLITE_ENABLE_FTS3_PARENTHESIS)
int fts3paren3(th3state *p){ return 0; }
#else

static const char *azFts3Paren3Data[] = {
  /* Ticket #1 (record 1) title */
  "Make the LIKE operator a user-installable function",

  /* Ticket #1 (record 1) description */
  "The LIKE operator is currently a special VDBE opcode.  Change this so"
  " that LIKE is implemented by a user-installable function.  That way, "
  "users can change the functionality of LIKE if desired (for example to"
  " make it case sensitive.)",

  /* Ticket #1 (record 1) remarks */
  "Also made the GLOB operator a function.",

  /* Ticket #2 (record 2) title */
  "Add support for TABLE.* in a query",

  /* Ticket #2 (record 2) description */
  "SQLite supports \"SELECT * FROM TABLE\" but it does not support \"SELECT"
  " TABLE.* FROM TABLE\".  The latter needs to be added.",

  /* Ticket #2 (record 2) remarks */
  "",

  /* Ticket #3 (record 3) title */
  "Rejects \"view\" as column name",

  /* Ticket #3 (record 3) description */
  "Neither CREATE TABLE nor SELECT parse \"view\" as a column name properly"
  "; it is rejected as a syntax error before processing.  Test SQL: \"S"
  "ELECT col1,view,col3 FROM dummytable\"  errmsg contains 'near \"view\":"
  " syntax error' and returns 1.",

  /* Ticket #3 (record 3) remarks */
  "",

  /* Ticket #4 (record 4) title */
  "Change prompt of 'sqlite' query tool.",

  /* Ticket #4 (record 4) description */
  "I'm using SQLite as the storage for configuration data.  I would like "
  "to have the facility to change the prompts 'sqlite> ' and ' ...> ' to "
  "something that more accurately reflects the system it's part of. You c"
  "ould do this as part of the \"config\" process like you handle UTF-8 or "
  "ISO-8859.",

  /* Ticket #4 (record 4) remarks */
  "Fixed in shell.c 1.53 by persicom",

  /* Ticket #5 (record 5) title */
  "Bug submitter ignores severity/priority",

  /* Ticket #5 (record 5) description */
  "Ticket 3 was submitted with a severity of 2 and a priority of 3, neith"
  "er of which seemed to stick. Please investigate. (Note, this ticket wa"
  "s submitted with a severity of 3 and a priority of 4, for reference.)",

  /* Ticket #5 (record 5) remarks */
  "This is (or was) a bug in CVSTrac, not SQLite.  The problem has alrea"
  "dy been fixed.  Thanks for noticing it, though.",

  /* Ticket #6 (record 6) title */
  "Query optimizer does not consider function arguments",

  /* Ticket #6 (record 6) description */
  "The query optimizer in \"where.c\" is slightly broken.   The code that"
  " determines the list of tables which an expression (Expr) depends on o"
  "nly considers the pLeft and pRight expressions, where it should consid"
  "er pLeft, pRight and pList. It occurs all throughout the file.  For "
  "example, exprTableUsage() should add the following:    if (p->pList)"
  "        for (int ii = 0; ii < p->pList->nExpr; ii++)            mask"
  " |= exprTableUsage(base, p->pList->a[ii].pExpr);   To produce an il"
  "lustration of the problem:    CREATE TABLE aa (a);   CREATE TABLE b"
  "b (b);   EXPLAIN SELECT * FROM aa, bb WHERE max(a, b);   0     Colum"
  "nCount   2           0                                               "
  "  1     ColumnName    0           0           aa.a                    "
  "              2     ColumnName    1           0           bb.b       "
  "                           3     Open          0           3         "
  "  aa                                    4     Ver",

  /* Ticket #6 (record 6) remarks */
  "",

  /* Ticket #7 (record 7) title */
  "VDBE trace prints out garbage for P3 of Function opcodes",

  /* Ticket #7 (record 7) description */
  "When the vdbe_trace pragma is active, and the VDBE executes a Function"
  " opcode, garbage is printed out for P3. eg:     5 Function        0 "
  "   0 `@  The EXPLAIN command deals with this nicely.",

  /* Ticket #7 (record 7) remarks */
  "",

  /* Ticket #8 (record 8) title */
  "More problems with where.c",

  /* Ticket #8 (record 8) description */
  "There's still a small problem in where.c. Some parts of the code assu"
  "me that only binary operators may be part of WHERE clauses, and thus"
  " expressions may be evaluated outside of table-scan loops that they d"
  "epend on. Examples:    CREATE TABLE aa(a);   CREATE TABLE bb(b);  "
  " SELECT * from aa, bb WHERE b;   SELECT * from aa, bb WHERE max(a, b)"
  ";   SELECT * from aa, bb WHERE CASE WHEN a = b THEN 1 END;",

  /* Ticket #8 (record 8) remarks */
  "",

  /* Ticket #9 (record 9) title */
  "platform-independent db files",

  /* Ticket #9 (record 9) description */
  "Trying to create an application that will run in a heterogeneous envio"
  "ronment of x86, sparc, mips, and alpha machines running a variety of o"
  "perating systems.  User's home directories and data files are shared a"
  "cross all platforms.  I'd like to use SQLite in an application but I w"
  "ant users to be able to share a database file across all platforms. "
  " The \"simple\" fix is to change all database reads/writes to use Networ"
  "k Byte Order for the on-disk file format, and convert to local byte-or"
  "der for in-memory use.  This might be more of a problem due to the fac"
  "t that SQLite is untyped.  If you don't know that data is an integer v"
  "s. a string, you may not know the appropriate byte order.  OTOH, if yo"
  "u store everything as strings, then you must have some way to convert "
  "the integer \"10\" to the two-byte string \"10\", and back, so the only by"
  "te-order issue would be data lengths.  Unfortunately SQLite is usele"
  "ss to me without portable data files.  Thanks,",

  /* Ticket #9 (record 9) remarks */
  "Changing the database file format to be byteorder independent is a re"
  "asonable request.  However, it is also a major rewrite of the BTree l"
  "ayer.  We'll put this off for the time being. ----- OK.  It turned o"
  "ut to be easier than expected. ----- The first fix is incomplete.  T"
  "he journal file still is byte-order dependent.  So if a big-endian ma"
  "chine tries to rollback a transaction started by a little-endian mach"
  "ine, it will fail. ----- Numbers are now written into the rollback j"
  "ournal as big-endian, regardless of the byte-order of the host machin"
  "e.  There is a new magic number at the beginning of journal files to "
  "identify them as such.  Old journals can still be rolled back, but on"
  "ly by a process on the same byte-order machine as the journal was or"
  "iginally created on.",

  /* Ticket #10 (record 10) title */
  "Stack grows without bound on UPDATE OR IGNORE",

  /* Ticket #10 (record 10) description */
  "Everytime the IGNORE action is taken on an UPDATE OR IGNORE statement"
  ", the depth of the VDBE stack grows by one.",

  /* Ticket #10 (record 10) remarks */
  "",

};

int fts3paren3(th3state *p){
  /*
  ** This module is only operational if FTS3 is available and uses the
  ** parenthesis syntax.
  */
  sqlite3 *db;
  sqlite3_stmt *pStmt;
  int rc, i;

  /* Test cases */
#define ERRMSG "SQLITE_ERROR {SQL logic error or missing database}"
  static const struct {
    int id;               /* Test case identifier */
    const char *zWhere;   /* The WHERE clause */
    const char *zResult;  /* Expected result */
  } aTestCase[] = {
    {
      1000,
      "(((application) OR (\"out for p3 eg\" NOT was)) AND order) OR "
      "(case NEAR/13 change)",
      "1 "
      "{+++ function.  That way, users can [change] the functionality "
       "of LIKE if desired (for example to make it [case] sensitive.)} "
       "{1 8 148 6 1 7 216 4} "
      "9 "
      "{Trying to create an [application] that will run in a heterogeneous "
       "envioronment +++ database reads/writes to use Network Byte [Order] "
       "for the on-disk file format, and convert +++} "
      "{1 0 20 11 1 0 261 11 1 6 425 5 1 6 486 5 1 6 679 5 1 6 841 5 "
       "2 6 304 5 2 6 525 5 2 6 720 5}"
    },
    {
      1001,
      "systems AND (file NEAR/9 reasonable)",
      "9 "
      "{+++ machines running a variety of operating [systems].  User's "
       "home directories and data files +++ Changing the database [file] "
       "format to be byteorder independent is a [reasonable] request.  "
       "However, it is also a major +++} "
      "{1 0 149 7 2 1 22 4 2 2 67 10}"
    },
    {
      1002,
      "(((have NOT pexpr) OR sql) NOT ((exprtableusage NOT \"prints out "
      "garbage\") NOT error NEAR/11 col1)) AND ((c:into AND appropriate) "
      "AND for NEAR/14 a)",
      "9 "
      "{+++ sparc, mips, and alpha machines running [a] variety of operating "
       "systems.  User's +++ database reads/writes to use Network Byte Order "
       "[for] the on-disk file format, and convert +++ [a] string, you may "
       "not know the [appropriate] byte order.  OTOH, if you store everything "
       "as strings, then you must [have] some way to convert the integer "
       "\"10\" +++ will fail. ----- Numbers are now written [into] the "
       "rollback journal as big-endian, +++} "
      "{1 12 126 1 1 11 431 3 1 11 492 3 1 12 534 1 1 10 662 11 1 0 743 4 "
       "2 12 108 1 2 11 164 3 2 12 367 1 2 12 392 1 2 9 461 4 2 12 562 1}"
    },
    {
      1003,
      "(want OR (\"c 1 53\" OR functionality)) OR ((((\"supports select "
      "from table\" NOT more) NOT (expression AND b:file)) AND (ii OR "
      "(8859 OR (be NEAR/12 does)))))",
      "1 "
      "{+++ function.  That way, users can change the [functionality] of "
       "LIKE if desired (for example to make it +++} "
      "{1 4 159 13} "
      "2 "
      "{SQLite [supports] \"[SELECT] * [FROM] [TABLE]\" but it [does] not "
       "support \"SELECT TABLE.* FROM TABLE\".  The latter needs to "
       "[be] added.} "
      "{1 5 7 8 1 6 17 6 1 7 26 4 1 8 31 5 1 15 45 4 1 14 112 2} "
      "4 "
      "{+++ process like you handle UTF-8 or ISO-[8859]. +++ Fixed in "
       "shell.[c] [1].[53] by persicom} "
      "{1 13 284 4 2 1 15 1 2 2 17 1 2 3 19 2} "
      "9 "
      "{+++ to use SQLite in an application but I [want] users to [be] able "
       "to share a database file +++} "
      "{1 0 279 4}"
    },
    {
      1004,
      "((c:little NOT a:change) OR ((8859 AND storage) NOT a)) AND "
      "(alpha OR (\"shell c 1 53 by\" OR max))",
      "4 "
      "{I'm using SQLite as the [storage] for configuration data.  I would "
       "like to +++ process like you handle UTF-8 or ISO-[8859]. +++ Fixed "
       "in [shell].[c] [1].[53] [by] persicom} "
      "{1 3 24 7 1 2 284 4 2 6 9 5 2 7 15 1 2 8 17 1 2 9 19 2 2 10 22 2} "
      "9 "
      "{+++ envioronment of x86, sparc, mips, and [alpha] machines running "
       "a variety of operating systems. +++ rollback a transaction started "
       "by a [little]-endian machine, it will fail. ----- +++} "
      "{1 5 103 5 2 0 394 6}"
    },
    {
      1005,
      "(then NOT needs) AND (files OR like NEAR this)",
      "9 "
      "{platform-independent db [files] +++ OTOH, if you store everything as "
       "strings, [then] you must have some way to convert the +++} "
      "{0 2 24 5 1 2 192 5 1 0 729 4 1 2 937 5 2 2 609 5}"
    },
    {
      1006,
      "((rejects OR (\"garbage for p3\" OR c:a)) AND view) OR "
      "(((determines OR \"stack grows without bound on\") AND (action NOT "
      "file)) NOT (c:first OR \"problems with where\"))",
      "3 "
      "{[Rejects] \"[view]\" as column name} "
      "{0 0 0 7 0 5 9 4 1 5 39 4 1 5 148 4 1 5 199 4} "
      "10 "
      "{[Stack] [grows] [without] [bound] [on] UPDATE OR IGNORE +++ Everytime "
       "the IGNORE [action] is taken on an UPDATE OR IGNORE statement, +++} "
      "{0 7 0 5 0 8 6 5 0 9 12 7 0 10 20 5 0 11 26 2 1 12 21 6}"
    },
    {
      1007,
      "(((a:like OR \"not sqlite the problem\") OR b:more) AND (a:ignores "
      "AND to)) OR deals",
      "5 "
      "{Bug submitter [ignores] severity/priority +++ a priority of 3, "
       "neither of which seemed [to] stick. Please investigate. (Note, "
       "this +++ This is (or was) a bug in CVSTrac, [not] [SQLite].  "
       "[The] [problem] has already been fixed.  Thanks for +++} "
      "{0 6 14 7 1 7 89 2 2 1 35 3 2 2 39 6 2 3 48 3 2 4 52 7} "
      "7 "
      "{+++        0    0 `@  The EXPLAIN command [deals] with this nicely.} "
      "{1 8 167 5}"
    },
    {
      1008,
      "((((evaluated OR \"bug submitter ignores severity priority\") OR "
      "\"format and convert to local\") OR b:0) NOT (ii OR one))",
      "5 "
      "{[Bug] [submitter] [ignores] [severity]/[priority]} "
      "{0 1 0 3 0 2 4 9 0 3 14 7 0 4 22 8 0 5 31 8} "
      "7 "
      "{+++ out for P3. eg:     5 Function        [0]    [0] `@  The "
       "EXPLAIN command deals +++} "
      "{1 11 136 1 1 11 141 1} "
      "8 "
      "{+++ clauses, and thus expressions may be [evaluated] outside of "
       "table-scan loops that they +++} "
      "{1 0 157 9} "
      "9 "
      "{+++ Network Byte Order for the on-disk file [format], [and] [convert] "
       "[to] [local] byte-order for in-memory use.  This might +++} "
      "{1 6 452 6 1 7 460 3 1 8 464 7 1 9 472 2 1 10 475 5}"
    },
    {
      1009,
      "(deals OR at) AND ((can AND c:transaction) OR (\"optimizer does "
      "not consider function\" OR (this NEAR/13 has)))",
      "9 "
      "{+++ a big-endian machine tries to rollback a [transaction] started "
       "by a little-endian machine, it +++ machine.  There is a new magic "
       "number [at] the beginning of journal files to identify them as such.  "
       "Old journals [can] still be rolled back, but only by a +++} "
      "{2 3 369 11 2 1 581 2 2 2 655 3}"
    },
    {
      1010,
      "0 AND (we OR ((nicely OR reference) OR (still AND aa)))",
      "7 "
      "{+++ out for P3. eg:     5 Function        [0]    [0] `@  The "
       "EXPLAIN command deals with this [nicely].} "
      "{1 0 136 1 1 0 141 1 1 2 183 6}"
    },
    {
      1011,
      "(iso NOT ((old AND out) OR (is NEAR/13 command))) OR ((\"parts of "
      "the code assume\" OR pright) NOT submitter)",
      "4 "
      "{+++ \"config\" process like you handle UTF-8 or [ISO]-8859.} "
      "{1 0 280 3} "
      "6 "
      "{+++ depends on only considers the pLeft and [pRight] expressions, "
       "where it should consider pLeft, +++} "
      "{1 10 166 6 1 10 218 6} "
      "8 "
      "{There's still a small problem in where.c. Some [parts] [of] [the] "
       "[code] [assume] that only binary operators may be part +++} "
      "{1 5 47 5 1 6 53 2 1 7 56 3 1 8 60 4 1 9 65 6}"
    },
    {
      1012,
      "((system OR systems) OR (a:grows OR (name NEAR/5 rejects)))",
      "3 "
      "{[Rejects] \"view\" as column [name]} "
      "{0 4 0 7 0 3 25 4} "
      "4 "
      "{+++ something that more accurately reflects the [system] it's part "
       "of. You could do this as part of +++} "
      "{1 0 184 6} "
      "9 "
      "{+++ machines running a variety of operating [systems].  User's home "
       "directories and data files +++} "
      "{1 1 149 7} "
      "10 "
      "{Stack [grows] without bound on UPDATE OR IGNORE} "
      "{0 2 6 5}"
    },
    {
      1013,
      "(binary OR byte NEAR/7 10) AND (request OR (case OR like NEAR/5 "
      "facility))",
      "8 "
      "{+++ Some parts of the code assume that only [binary] operators may "
       "be part of WHERE clauses, and +++ max(a, b);   SELECT * from aa, bb "
       "WHERE [CASE] WHEN a = b THEN 1 END;} "
      "{1 0 82 6 1 4 372 4} "
      "9 "
      "{+++ have some way to convert the integer \"[10]\" to the two-[byte] "
       "string \"[10]\", and back, so the only +++ +++} "
      "{1 2 781 2 1 1 796 4 1 2 809 2 1 1 836 4 2 3 78 7}"
    },
    {
      1014,
      "((syntax OR (application AND (\"change prompt of sqlite\" OR "
      "first))) OR ((\"in shell c\" OR ((((consider OR (please NEAR/8 "
      "with)) OR x86) OR vdbe))) AND ((a:on NOT sparc) NOT (it NEAR/6 "
      "supports OR beginning)))) OR ((it NEAR/8 like OR (s NEAR/8 "
      "that)) AND ((a:the OR which) NOT as NEAR/9 as))",
      "1 "
      "{Make [the] [LIKE] operator a user-installable function +++ The "
       "[LIKE] operator is currently a special [VDBE] opcode.  Change this "
       "so [that] [LIKE] is +++ users can change the functionality of "
       "[LIKE] if desired (for example to make [it] case sensitive.)} "
      "{0 24 5 3 1 14 41 4 1 21 176 4 1 20 213 2} "
      "3 "
      "{+++ column name properly; [it] is rejected as a [syntax] error "
       "before processing.  Test SQL: +++} "
      "{1 0 92 6 1 0 206 6} "
      "9 "
      "{Trying to create an [application] [that] will run in a "
       "heterogeneous envioronment of [x86], sparc, mips, and alpha "
       "machines running +++ are shared across all platforms.  I'd "
       "[like] to use SQLite in an [application] but I +++ this off for "
       "the time being. ----- OK.  [It] turned out to be easier than "
       "expected. ----- The [first] fix is incomplete.  The journal file +++} "
      "{1 1 20 11 1 13 81 3 1 21 236 4 1 1 261 11 2 20 195 2 2 6 247 5} "
      "10 "
      "{Stack grows without bound [on] UPDATE OR IGNORE +++ OR IGNORE "
       "statement, the depth of the [VDBE] stack grows by one.} "
      "{0 15 26 2 1 14 88 4}"
    },
  };

  th3testBegin(p, "1.1");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, "CREATE VIRTUAL TABLE t1 USING fts3(a,b,c)");
  th3testCheck(p, "");
  th3testBegin(p, "1.2");
  db = th3dbPointer(p, 0);
  rc = sqlite3_prepare_v2(db, "INSERT INTO t1(a,b,c) VALUES(?,?,?)",
                          -1, &pStmt, 0);
  th3assert( pStmt!=0 );
  for(i=0; i<sizeof(azFts3Paren3Data)/sizeof(azFts3Paren3Data[0]); i+=3){
    sqlite3_bind_text(pStmt, 1, azFts3Paren3Data[i], -1, SQLITE_STATIC);
    sqlite3_bind_text(pStmt, 2, azFts3Paren3Data[i+1], -1, SQLITE_STATIC);
    sqlite3_bind_text(pStmt, 3, azFts3Paren3Data[i+2], -1, SQLITE_STATIC);
    sqlite3_step(pStmt);
    sqlite3_reset(pStmt);
  }
  sqlite3_finalize(pStmt);
  th3dbEval(p, 0, "SELECT count(*) FROM t1");
  th3testCheck(p, "10");

  for(i=0; i<(int)(sizeof(aTestCase)/sizeof(aTestCase[0])); i++){
    char zTestId[100];
    char zSql[20000];
    th3snprintf(zTestId, sizeof(zTestId), "2.%d", aTestCase[i].id);
    th3testBegin(p, zTestId);
    sqlite3_snprintf(sizeof(zSql), zSql,
       "SELECT rowid, snippet(t1,'[',']','+++'), offsets(t1) FROM t1"
       " WHERE t1 MATCH '%s' ORDER BY rowid",
       aTestCase[i].zWhere
    );
    th3dbEval(p, 0, zSql);
    th3testCheck(p, aTestCase[i].zResult);
  }
  th3dbClose(p, 0);

  return 0;
}
#endif /* SQLITE_ENABLE_FTS3 && SQLITE_ENABLE_FTS3_PARENTHESES */
