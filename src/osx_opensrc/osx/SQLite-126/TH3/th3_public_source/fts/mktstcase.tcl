package require sqlite3
sqlite3 db {}
set sqlfd [open db1.sql r]
set sql [read $sqlfd]
close $sqlfd
db eval $sql
db eval {SELECT rowid, a, b, c FROM t1} {
  unset -nocomplain seenall
  foreach field {a b c} {
    set x [string tolower [string trim [set $field]]]
    regsub -all {[^a-z0-9]+} $x { } x
    unset -nocomplain seen
    foreach word $x {
      if {[string length $word]<3} continue
      if {[info exists seen($word)]} continue
      set seen($word) 1
      lappend revidx_${field}($word) $rowid
      if {[info exists seenall($word)]} continue
      set seenall($word) 1
      lappend revidx_all($word) $rowid
    }
  }
}
if 0 {
  parray revidx_all
  parray revidx_a
  parray revidx_b
  parray revidx_c
  exit
}

set list_all [array names revidx_all]
set list_a [array names revidx_a]
set list_b [array names revidx_b]
set list_c [array names revidx_c]
set cnt_all [llength $list_all]
set cnt_a [llength $list_a]
set cnt_b [llength $list_b]
set cnt_c [llength $list_c]
expr srand(0)

proc makeexpr {depth} {
  global revidx_all revidx_a revidx_b revidx_c
  global list_all list_a list_b list_c
  global cnt_all cnt_a cnt_b cnt_c
  if {rand()*10<$depth} {
    if {rand()<0.75} {
      set term [lindex $list_all [expr {int(rand()*$cnt_all)}]]
      return [list $term $revidx_all($term)]
    } elseif {rand()<0.3333} {
      set term [lindex $list_a [expr {int(rand()*$cnt_a)}]]
      return [list a:$term $revidx_a($term)]
    } elseif {rand()<0.5} {
      set term [lindex $list_b [expr {int(rand()*$cnt_b)}]]
      return [list b:$term $revidx_b($term)]
    } else {
      set term [lindex $list_c [expr {int(rand()*$cnt_c)}]]
      return [list c:$term $revidx_c($term)]
    }
  } else {
    set dp1 [expr {$depth+1}]
    foreach {leftterm leftres} [makeexpr $dp1] break
    foreach {rightterm rightres} [makeexpr $dp1] break
    if {rand()<0.2} {
      set term "($leftterm AND $rightterm)"
      unset -nocomplain lx
      foreach r $leftres {set lx($r) 1}
      set res {}
      foreach r $rightres {
        if {[info exists lx($r)]} {lappend res $r}
      }
      set res [lsort -int $res]
      return [list $term $res]
    } elseif {rand()<0.6} {
      set term "($leftterm OR $rightterm)"
      unset -nocomplain lx
      foreach r $leftres {set lx($r) 1}
      foreach r $rightres {set lx($r) 1}
      set res [lsort -int [array names lx]]
      return [list $term $res]
    } else {
      set term "($leftterm NOT $rightterm)"
      unset -nocomplain lx
      foreach r $leftres {set lx($r) 1}
      foreach r $rightres {unset -nocomplain lx($r)}
      set res [lsort -int [array names lx]]
      return [list $term $res]
    }
  }
}

# print a string such that it does not exceed the 80-character line.
#
proc pr_c_string {x terminator} {
  while {[string length $x]>70} {
    set j 69
    while {$j>40 && [string index $x $j]!=" "} {incr j -1}
    set y [string range $x 0 $j]
    set x [string range $x [expr {$j+1}] end]
    puts "      \"$y\""
  }
  puts "      \"$x\"$terminator"
}

for {set i 101} {$i<200} {incr i} {
  while 1 {
    foreach {term res} [makeexpr 0] break
    if {[llength $res]>0 || rand()<0.05} break
  }
  puts "    \173\n      $i,"
  set x "t1 MATCH '$term'"
  pr_c_string $x ,
  pr_c_string $res {}
  puts "    \175,"
}

proc makeexpr2 {depth limit} {
  global revidx_all revidx_a revidx_b revidx_c
  global list_all list_a list_b list_c
  global cnt_all cnt_a cnt_b cnt_c
  if {rand()*$limit<$depth} {
    if {rand()<0.75} {
      set term [lindex $list_all [expr {int(rand()*$cnt_all)}]]
      return [list $term $revidx_all($term)]
    } elseif {rand()<0.3333} {
      set term [lindex $list_a [expr {int(rand()*$cnt_a)}]]
      return [list a:$term $revidx_a($term)]
    } elseif {rand()<0.5} {
      set term [lindex $list_b [expr {int(rand()*$cnt_b)}]]
      return [list b:$term $revidx_b($term)]
    } else {
      set term [lindex $list_c [expr {int(rand()*$cnt_c)}]]
      return [list c:$term $revidx_c($term)]
    }
  } else {
    set dp1 [expr {$depth+1}]
    foreach {leftterm leftres} [makeexpr2 $dp1 $limit] break
    foreach {rightterm rightres} [makeexpr2 $dp1 $limit] break
    set leftsz [llength $leftres]
    set rightsz [llength $rightres]
    unset -nocomplain lx
    foreach r $leftres {
      set lx($r) 1
      set orx($r) 1
      set notx($r) 1
    }
    set andres {} 
    foreach r $rightres {
      if {[info exists lx($r)]} {
        lappend andres $r
      }
      unset -nocomplain notx($r)
      set orx($r) 1
    }
    set orres [lsort -int [array names orx]]
    set notres [lsort -int [array names notx]]
    if {rand()<0.2 && [llength $notres]>0} {
      return [list "($leftterm NOT $rightterm)" $notres]
    } elseif {[llength $andres]>0} {
      return [list "($leftterm AND $rightterm)" $andres]
    } else {
      return [list "($leftterm OR $rightterm)" $orres]
    }
  }
}

for {set i 201} {$i<600} {incr i} {
  while 1 {
    foreach {term res} [makeexpr2 0 15] break
    if {[llength $res]>0 || rand()<0.05} break
  }
  puts "    \173\n      $i,"
  set x "t1 MATCH '$term'"
  pr_c_string $x ,
  pr_c_string $res {}
  puts "    \175,"
}
