1) Assemble your tools.

a. You're going to need a compiler.  For this README, we're using Visual Studio 2005
  Team edition.   Another option is CeGCC (http://cegcc.sourceforge.net/).  

  The "express" editions of Visual Studio do not support targetting embedded platforms.
  However, you can download an evaluation of Visual Studio 2008 Professional 
  here:
  http://www.microsoft.com/visualstudio/en-us/products/professional/default.mspx

  Or, if you already have a version of Visual Studio, but don't have support for
  embedded platforms with it, you can download 
  "Windows Embedded CE 6.0 Evaluation Edition" here:
  http://www.microsoft.com/downloads/details.aspx?familyid=7E286847-6E06-4A0C-8CAC-CA7D4C09CB56&displaylang=en

b. You're going to need a TCL implementation.  For this README, we're using
  ActiveTCL (http://www.activestate.com/activetcl/).

c. A version of the SQLite library suitable for use on your target platform. 
  We recommend using the SQLite amalgamation source code file to generate the
  TH3 test application, but you are free to use any compilation process you are 
  comfortable with.  You can get the latest source (and amalgamation) here:
  http://www.sqlite.org/download.html

d. The TH3 source code. You should get access to the TH3 source code with your
  license.  Simply unzip the ZIP archive into a directory on your development machine.
  See the TH3 README for a description of the TH3 source files and
  directories.

2) Create test program source code.

Generate your th3.c source file using the mkth3.tcl TCL script.  
This uses the indicated config and test scripts to generate a single
source file, th3.c.

Under Windows, you can use the ActiveTCL to run the mkth3.tcl script.
From the command line:

tclsh85 mkth3.tcl cfg/*.cfg cov1/*.test > th3.c

This generates the th3.c test file from the "100% branch test coverage"
sources.  Note that even though this is Windows, UNIX-like
forward slashes are used for directory seperators.

You can of course pick and choose the individual configurations and test scripts
you would like to test as well.  See http://www.sqlite.org/th3/doc/trunk/www/th3.wiki
for more information.

We will also be using custom platform interfaces for WINCE, so you should rename
the th3_alt_os.h.wince file to th3_alt_os.h.


3) Compile your project.

In Visual Studio, create a "Win32 Smart Device Project", with the 
"Application Type" set to "Console application".  Choose an approprate 
device SDK and instruction set, such as "Pocket PC 2003" and "ARMV4".

Add the sqlite3.c amalgamation you want to test to the project dependencies.  
Add the th3.c file you generated in step 2 above to the project dependencies.  
Ensure that the sqlite3.h and th3_alt_os.h header files are in the same 
directory as your two source files, or add their respective directories to 
the project properties as an "Additional Include Directory".

You may also need to disable the "Buffer Security Check" option (/GS-)
and set the "Floating Point Model" to precise (/fp:precise).

The following compiler options should be added to both the DEBUG and RELEASE
build "Preprocessor Definitions" in the project properties (be careful not 
to change the default WINCE options Visual Studio adds automatically):

SQLITE_OS_WIN=1
SQLITE_NO_SYNC=1
SQLITE_DISABLE_LFS=1
SQLITE_MAX_TRIGGER_DEPTH=100
TH3_ALT_OS=1
main=wmain
gmtime=localtime

The following should be added only to the DEBUG options set:

SQLITE_DEBUG=1

If you would like "verbose" output for each test case in the th3.c source file,
rather than simply output on failures, add the following define to the 
respective "Preprocessor Definitions":

TH3_VERBOSE=1

At this point, you are ready to compile the executable using the 
"Build Solution" menu choice.


4) Run your test program and collect the output.  The TH3 program takes no 
command-line arguments and requires no configuration or setup on the target.

For our testing, we used the "Microsoft Device Emulator", with the 
"ARMv4 Pocket PC 2003 SE Emulator" system image (PPC_2003_SE_WWE_ARMv4.bin).
You may choose other system images to match your target platform, just 
be sure to set your project properties in step 3 appropriately.

Visual Studio can be configured to copy the TH3 program to the emulator, 
run, and attach to it.  Typically, this should be configured automatically 
for you when you create your project in step 3.  You can control the options
that Visual Studio starts the emulator with under 
"Tools->Options->Device Tools->Devices" configuration window.

On a typical WINCE device, there's no "console" in which to run the 
TH3 executable to get output.  So we run the executable remotely 
(in the emulator) and attach to it with the debugger and capture 
it's output with the OutputDebugString() API.  Visual Studio captures
output from this API and displays it in the "Output" panel 
(Debug->Windows->Output) while the application is running.

The TH3_ALT_OS define set in the project properties in step 3 configures 
the th3.c source file to use the external th3_alt_os.h file for 
platform specific interfaces.  We use this to override the normal output of
the TH3 application and redirect it from "stdout" to use the 
OutputDebugString() available to most standard Windows debuggers.

You may also modifiy the th3_alt_os.h if you wish to have it write to
a log file.

The WINCE platform doesn't provide a gmtime() API which is why we provide
the gmtime=localtime mapping in the compiler options.  This may result in 
some of the date/time tests failing, such as:

c4.date2.2.1... FAILED 
Expected: [{2000-01-01 05:00:00}]
     Got: [{2000-01-01 12:00:00}]

If you have a gmtime() implementation for your WINCE target, then you can 
add it to the th3_alt_os.h file and remove the mapping.

Additionally, you may see rounding errors causing failures such as the 
following:

c4.update06.100... FAILED 
Expected: [999 empty 9.0e+99 012345]
     Got: [999 empty 9.00000000000001e+99 012345]


