/*
** This module contains tests for the sqlite3_vfs_register(), 
** sqlite3_vfs_find(), and sqlite3_vfs_unregister().
**
** MODULE_NAME:               req1_vfs01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
*/
int req1_vfs01(th3state *p){
  sqlite3_vfs tempVfs1;
  sqlite3_vfs tempVfs2;
  sqlite3_vfs *pDfltVfs;
  sqlite3_vfs *pVfs;
  const char *zDfltName;


  /* EVIDENCE-OF: R-27702-51733 New VFSes are registered with
  ** sqlite3_vfs_register().
  */
  th3testBegin(p, "setup");
  pDfltVfs = sqlite3_vfs_find(0);
  zDfltName = pDfltVfs->zName;
  memcpy(&tempVfs1, pDfltVfs, sizeof(tempVfs1));
  tempVfs1.zName = "temp-vfs-1";
  sqlite3_vfs_register(&tempVfs1, 0);
  memcpy(&tempVfs2, pDfltVfs, sizeof(tempVfs2));
  tempVfs2.zName = "temp-vfs-2";
  sqlite3_vfs_register(&tempVfs2, 0);
  pVfs = sqlite3_vfs_find("temp-vfs-2");
  th3testCheckInt(p, 1, pVfs!=0);

  /* EVIDENCE-OF: R-61177-47713 The sqlite3_vfs_find() interface returns a
  ** pointer to a VFS given its name.
  */
  th3testBegin(p, "100");
  pVfs = sqlite3_vfs_find(zDfltName);
  th3testCheckInt(p, 1, pVfs==pDfltVfs);
  th3testBegin(p, "101");
  pVfs = sqlite3_vfs_find("temp-vfs-1");
  th3testCheckInt(p, 1, pVfs==&tempVfs1);
  th3testBegin(p, "102");
  pVfs = sqlite3_vfs_find("temp-vfs-2");
  th3testCheckInt(p, 1, pVfs==&tempVfs2);

  /* EVIDENCE-OF: R-52918-29603 Names are case sensitive.
  **
  ** EVIDENCE-OF: R-22782-22472 Names are zero-terminated UTF-8 strings.
  **
  ** EVIDENCE-OF: R-19515-61262 If there is no match, a NULL pointer is
  ** returned.
  */
  th3testBegin(p, "110");
  pVfs = sqlite3_vfs_find("temp-vfs-1alskjdkajsoiaosidfskjf");
  th3testCheckInt(p, 1, pVfs==0);
  th3testBegin(p, "111");
  pVfs = sqlite3_vfs_find("TEMP-VFS-1");
  th3testCheckInt(p, 1, pVfs==0);

  /* EVIDENCE-OF: R-54918-11103 If zVfsName is NULL then the default VFS is
  ** returned.
  */
  th3testBegin(p, "120");
  pVfs = sqlite3_vfs_find(0);
  th3testCheckInt(p, 1, pVfs==pDfltVfs);

  /* EVIDENCE-OF: R-03529-13145 Each new VFS becomes the default VFS if the
  ** makeDflt flag is set.
  **
  ** EVIDENCE-OF: R-55438-03590 The same VFS can be registered multiple
  ** times without injury.
  **
  ** EVIDENCE-OF: R-28170-10886 To make an existing VFS into the default
  ** VFS, register it again with the makeDflt flag set.
  */
  th3testBegin(p, "200");
  sqlite3_vfs_register(&tempVfs1, 1);
  pVfs = sqlite3_vfs_find(0);
  th3testCheckInt(p, 1, pVfs==&tempVfs1);
  th3testBegin(p, "201");
  sqlite3_vfs_register(&tempVfs2, 1);
  pVfs = sqlite3_vfs_find(0);
  th3testCheckInt(p, 1, pVfs==&tempVfs2);

  /* EVIDENCE-OF: R-20210-23164 Unregister a VFS with the
  ** sqlite3_vfs_unregister() interface.
  **
  ** EVIDENCE-OF: R-51358-63229 If the default VFS is unregistered, another
  ** VFS is chosen as the default. The choice for the new VFS is arbitrary.
  */
  th3testBegin(p, "300");
  sqlite3_vfs_unregister(&tempVfs2);
  pVfs = sqlite3_vfs_find(0);
  th3testCheckInt(p, 1, pVfs!=0 && pVfs!=&tempVfs2);
  th3testBegin(p, "301");
  sqlite3_vfs_unregister(&tempVfs1);
  pVfs = sqlite3_vfs_find(0);
  th3testCheckInt(p, 1, pVfs!=0 && pVfs!=&tempVfs1);
  sqlite3_vfs_register(pDfltVfs, 1);

  return 0;
}
