/*
** This module contains tests for the sqlite3_bind_xxxxx() family of
** interfaces.
**
** MODULE_NAME:               req1_bind01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* Destructor for allocated objects */
static void req1bind01_destructor(void *pObj){
  char *p = (char*)pObj;
  p -= 8;
  assert( memcmp(p, "req1bind1", 8)==0 );
  memset(p, 0, 8);
  sqlite3_free(p);
}

/* An allocator that works with the destructor */
static void *req1bind01_malloc(int nByte){
  char *p = sqlite3_malloc(nByte + 8);
  if( p ){
    memcpy(p, "req1bind1", 8);
    p += 8;
  }
  return p;
}

int req1_bind01(th3state *p){
  sqlite3_stmt *pStmt;
  sqlite3_stmt *pS2;
  int iLimit;
  char *z;
  int i;
  int rc;
  unsigned short int aText[16];
  char zBuf[10];

  /* EVIDENCE-OF: R-63285-37134 In the SQL statement text input to
  ** sqlite3_prepare_v2() and its variants, literals may be replaced by a
  ** parameter that matches one of following templates: ? ?NNN :VVV @VVV
  ** $VVV In the templates above, NNN represents an integer literal, and
  ** VVV represents an alphanumeric identifier.
  **
  ** EVIDENCE-OF: R-06094-55152 Unbound parameters are interpreted as NULL.
  */
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  pStmt = th3dbPrepare(p, 0, "SELECT ?, ?5, :a, @b, $c");
  th3dbStep(p, pStmt);
  th3testCheck(p, "nil nil nil nil nil");

  /* EVIDENCE-OF: R-25857-01867 The values of these parameters (also called
  ** "host parameter names" or "SQL parameters") can be set using the
  ** sqlite3_bind_*() routines defined here.
  **
  ** EVIDENCE-OF: R-05367-01575 The first argument to the sqlite3_bind_*()
  ** routines is always a pointer to the sqlite3_stmt object returned from
  ** sqlite3_prepare_v2() or its variants.
  **
  ** EVIDENCE-OF: R-49438-33626 The second argument is the index of the SQL
  ** parameter to be set.
  **
  ** EVIDENCE-OF: R-42798-04097 The leftmost SQL parameter has an index of 1.
  **
  ** EVIDENCE-OF: R-01766-15387 The third argument is the value to bind to
  ** the parameter.
  */
#ifndef SQLITE_OMIT_FLOATING_POINT
  th3testBegin(p, "110");
  sqlite3_reset(pStmt);
  sqlite3_bind_blob(pStmt, 1, "0123", 4, SQLITE_STATIC);
  sqlite3_bind_double(pStmt, 5, 234.5);
  sqlite3_bind_int(pStmt, 6, 456);
  sqlite3_bind_int64(pStmt, 7, (((sqlite3_int64)0x7fffffff)<<32) + 0xffffffff);
  sqlite3_bind_text(pStmt, 8, "hello", 5, SQLITE_STATIC);
  th3dbStep(p, pStmt);
  th3testCheck(p, "0123 234.5 456 9223372036854775807 hello");
#endif /* SQLITE_OMIT_FLOATING_POINT */
  th3testBegin(p, "111");
  sqlite3_reset(pStmt);
  th3_ascii_to_utf16("xyzzy", aText);
  sqlite3_bind_text16(pStmt, 1, aText, -1, SQLITE_TRANSIENT);
  sqlite3_bind_zeroblob(pStmt, 5, 4);
  pS2 = th3dbPrepare(p, 0, "SELECT 'whatever'");
  sqlite3_step(pS2);
  sqlite3_bind_value(pStmt, 6, sqlite3_column_value(pS2, 0));
  sqlite3_finalize(pS2);
  sqlite3_bind_null(pStmt, 7);
  sqlite3_bind_null(pStmt, 8);
  th3dbStep(p, pStmt);
  th3testCheck(p, "xyzzy {} whatever nil nil");
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-52009-56800 When the same named SQL parameter is used
  ** more than once, second and subsequent occurrences have the same index
  ** as the first occurrence.  
  */
  th3testBegin(p, "120");
  pStmt = th3dbPrepare(p, 0, "SELECT $a, :b, $a, :c");
  sqlite3_bind_int(pStmt, 1, 123);
  sqlite3_bind_int(pStmt, 2, 456);
  sqlite3_bind_int(pStmt, 3, 789);
  th3dbStep(p, pStmt);
  sqlite3_finalize(pStmt);
  th3testCheck(p, "123 456 123 789");

  /* EVIDENCE-OF: R-05272-22592 The index for named parameters can be
  ** looked up using the sqlite3_bind_parameter_index() API if desired.
  */
  th3testBegin(p, "130");
  pStmt = th3dbPrepare(p, 0, "SELECT ?, ?5, $a, :b, @c");
  /* EVIDENCE-OF: R-10933-37470 The index for "?NNN" parameters is the
  ** value of NNN. */
  th3testCheckInt(p, 5, sqlite3_bind_parameter_index(pStmt, "?5"));
  th3testBegin(p, "131");
  th3testCheckInt(p, 6, sqlite3_bind_parameter_index(pStmt, "$a"));
  th3testBegin(p, "132");
  th3testCheckInt(p, 7, sqlite3_bind_parameter_index(pStmt, ":b"));
  th3testBegin(p, "133");
  th3testCheckInt(p, 8, sqlite3_bind_parameter_index(pStmt, "@c"));
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-43536-61251 The NNN value must be between 1 and the
  ** sqlite3_limit() parameter SQLITE_LIMIT_VARIABLE_NUMBER (default value:
  ** 999).
  */
  th3testBegin(p, "140");
#ifdef SQLITE_MAX_VARIABLE_NUMBER
  sqlite3_limit(th3dbPointer(p,0), SQLITE_LIMIT_VARIABLE_NUMBER, 999);
#endif
  iLimit = sqlite3_limit(th3dbPointer(p, 0), SQLITE_LIMIT_VARIABLE_NUMBER, -1);
  th3testCheckInt(p, 999, iLimit);
  th3testBegin(p, "141");
  th3dbEval(p, 0, "SELECT ?0");
  th3testCheck(p, "SQLITE_ERROR {variable number must be between ?1 and ?999}");
  th3testBegin(p, "142");
  th3dbEval(p, 0, "SELECT ?1000");
  th3testCheck(p, "SQLITE_ERROR {variable number must be between ?1 and ?999}");
  th3testBegin(p, "143");
  th3dbEval(p, 0, "SELECT ?999, ?1");
  th3testCheck(p, "nil nil");

  /* EVIDENCE-OF: R-40874-07817 In those routines that have a fourth
  ** argument, its value is the number of bytes in the parameter. To be
  ** clear: the value is the number of bytes in the value, not the number
  ** of characters.
  */
  th3testBegin(p, "150");
  th3_ascii_to_utf16("xyzzy", aText);
  pStmt = th3dbPrepare(p, 0, "SELECT ?1");
  sqlite3_bind_text16(pStmt, 1, aText, 4, SQLITE_STATIC);
  th3dbStep(p, pStmt);
  sqlite3_finalize(pStmt);
  th3testCheck(p, "xy");
 
  /* EVIDENCE-OF: R-30625-54804 If the fourth parameter is negative, the
  ** length of the string is the number of bytes up to the first zero
  ** terminator.
  */
  th3testBegin(p, "160");
  th3_ascii_to_utf16("xyzzy", aText);
  pStmt = th3dbPrepare(p, 0, "SELECT ?1, ?2");
  sqlite3_bind_text16(pStmt, 1, aText, -1, SQLITE_STATIC);
  sqlite3_bind_text(pStmt, 2, "abc123", -1, SQLITE_STATIC);
  th3dbStep(p, pStmt);
  sqlite3_finalize(pStmt);
  th3testCheck(p, "xyzzy abc123");

  /* EVIDENCE-OF: R-57891-22954 The fifth argument to sqlite3_bind_blob(),
  ** sqlite3_bind_text(), and sqlite3_bind_text16() is a destructor used to
  ** dispose of the BLOB or string after SQLite has finished with it.
  **
  ** Verify that the destructor is used by using the req1bind01_malloc()
  ** and req1bind01_destructor() wrappers around sqlite3_malloc().  If the
  ** destructor is not called, a memory leak will be detected.  And the
  ** header check in req1bind01_destructor() ensures that the destructor
  ** really is called exactly once.
  */
  th3testBegin(p, "170");
  z = req1bind01_malloc(6);
  memcpy(z, "vwxyz", 6);
  pStmt = th3dbPrepare(p, 0, "SELECT $a");
  sqlite3_bind_text(pStmt, 1, z, -1, req1bind01_destructor);
  th3dbStep(p, pStmt);
  sqlite3_finalize(pStmt);
  th3testCheck(p, "vwxyz");
  th3testBegin(p, "171");
  z = req1bind01_malloc(6);
  memcpy(z, "fghij", 6);
  pStmt = th3dbPrepare(p, 0, "SELECT $a");
  sqlite3_bind_blob(pStmt, 1, z, 5, req1bind01_destructor);
  th3dbStep(p, pStmt);
  sqlite3_finalize(pStmt);
  th3testCheck(p, "fghij");
  th3testBegin(p, "172");
  z = req1bind01_malloc(14);
  th3_ascii_to_utf16("klmnop", (unsigned short int*)z);
  pStmt = th3dbPrepare(p, 0, "SELECT $a");
  sqlite3_bind_text16(pStmt, 1, z, -1, req1bind01_destructor);
  th3dbStep(p, pStmt);
  sqlite3_finalize(pStmt);
  th3testCheck(p, "klmnop");

  /* EVIDENCE-OF: R-55630-01276 The destructor is called to dispose of the
  ** BLOB or string even if the call to sqlite3_bind_blob(),
  ** sqlite3_bind_text(), or sqlite3_bind_text16() fails.
  **
  ** Same drill as for R-57891 above...
  */
  th3testBegin(p, "173");
  z = req1bind01_malloc(6);
  memcpy(z, "abcde", 6);
  rc = sqlite3_bind_text(0, 1, z, -1, req1bind01_destructor);
  th3testCheckInt(p, SQLITE_MISUSE, rc);

  th3testBegin(p, "174");
  z = req1bind01_malloc(6);
  memcpy(z, "hijkl", 6);
  pStmt = th3dbPrepare(p, 0, "SELECT $a");
  rc = sqlite3_bind_blob(pStmt, 0, z, 5, req1bind01_destructor);
  sqlite3_finalize(pStmt);
  th3testCheckInt(p, SQLITE_RANGE, rc);
   
  th3testBegin(p, "175");
  z = req1bind01_malloc(14);
  th3_ascii_to_utf16("klmnop", (unsigned short int*)z);
  pStmt = th3dbPrepare(p, 0, "SELECT $a UNION ALL SELECT $b");
  th3dbStep(p, pStmt);
  rc = sqlite3_bind_text16(pStmt, 1, z, -1, req1bind01_destructor);
  sqlite3_finalize(pStmt);
  th3testCheckInt(p, SQLITE_MISUSE, rc);


  /* EVIDENCE-OF: R-54601-24304 If the fifth argument is the special value
  ** SQLITE_STATIC, then SQLite assumes that the information is in static,
  ** unmanaged space and does not need to be freed.
  **
  ** Use static space.  No memory free routine will be called.
  */
  th3testBegin(p, "180");
  th3strcpy(zBuf, "xyzzy");
  th3_ascii_to_utf16("abc123", aText);
  pStmt = th3dbPrepare(p, 0, "SELECT $a, $b, $c");
  sqlite3_bind_text(pStmt, 1, zBuf, -1, SQLITE_STATIC);
  sqlite3_bind_blob(pStmt, 2, zBuf, 4, SQLITE_STATIC);
  sqlite3_bind_text16(pStmt, 3, aText, 6, SQLITE_STATIC);
  th3dbStep(p, pStmt);
  sqlite3_finalize(pStmt);
  th3testCheck(p, "xyzzy xyzz abc");

  /* EVIDENCE-OF: R-36996-21017 If the fifth argument has the value
  ** SQLITE_TRANSIENT, then SQLite makes its own private copy of the data
  ** immediately, before the sqlite3_bind_*() routine returns.
  **
  ** Bind then immediately overwrite the buffers.  Show that the original
  ** binds come through.
  */
  th3testBegin(p, "190");
  th3strcpy(zBuf, "xyzzy");
  th3_ascii_to_utf16("abc123", aText);
  pStmt = th3dbPrepare(p, 0, "SELECT $a, $b, $c");
  sqlite3_bind_text(pStmt, 1, zBuf, -1, SQLITE_TRANSIENT);
  th3strcpy(zBuf, "uvwxy");
  sqlite3_bind_blob(pStmt, 2, zBuf, 4, SQLITE_TRANSIENT);
  th3strcpy(zBuf, "01234");
  sqlite3_bind_text16(pStmt, 3, aText, 6, SQLITE_TRANSIENT);
  th3_ascii_to_utf16("fault", aText);
  th3dbStep(p, pStmt);
  sqlite3_finalize(pStmt);
  th3testCheck(p, "xyzzy uvwx abc");

  /* EVIDENCE-OF: R-19162-55388 The sqlite3_bind_zeroblob() routine binds a
  ** BLOB of length N that is filled with zeroes.
  */
  th3testBegin(p, "200");
  pStmt = th3dbPrepare(p, 0, "SELECT ?");
  sqlite3_bind_zeroblob(pStmt, 1, 200);
  sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_BLOB, sqlite3_column_type(pStmt, 0));
  th3testBegin(p, "201");
  th3testCheckInt(p, 200, sqlite3_column_bytes(pStmt, 0));
  th3testBegin(p, "202");
  z = (char*)sqlite3_column_blob(pStmt, 0);
  for(i=0; i<200 && z[i]==0; i++){}
  th3testCheckInt(p, 200, i);
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-09849-00220 A negative value for the zeroblob results
  ** in a zero-length BLOB.
  */
  th3testBegin(p, "210");
  pStmt = th3dbPrepare(p, 0, "SELECT length(?)");
  sqlite3_bind_zeroblob(pStmt, 1, -1);
  th3dbStep(p, pStmt);
  th3testCheck(p, "0");
  th3testBegin(p, "211");
  sqlite3_reset(pStmt);
  sqlite3_bind_zeroblob(pStmt, 1, 0);
  th3dbStep(p, pStmt);
  th3testCheck(p, "0");
  th3testBegin(p, "212");
  sqlite3_reset(pStmt);
  sqlite3_bind_zeroblob(pStmt, 1, 10000);
  th3dbStep(p, pStmt);
  th3testCheck(p, "10000");
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-12385-29357 If any of the sqlite3_bind_*() routines are
  ** called with a NULL pointer for the prepared statement or with a
  ** prepared statement for which sqlite3_step() has been called more
  ** recently than sqlite3_reset(), then the call will return
  ** SQLITE_MISUSE.
  */
  th3testBegin(p, "300");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_blob(0, 1, "", 1, SQLITE_STATIC));
#ifndef SQLITE_OMIT_FLOATING_POINT
  th3testBegin(p, "301");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_double(0, 1, 1.23));
#endif
  th3testBegin(p, "302");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_int(0, 1, 2));
  th3testBegin(p, "303");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_int64(0, 1, 3));
  th3testBegin(p, "304");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_null(0, 1));
  th3testBegin(p, "305");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_text(0, 1, "hi", 2, SQLITE_STATIC));
  th3testBegin(p, "306");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_text16(0, 1, aText, 2, SQLITE_STATIC));
  th3testBegin(p, "307");
  pS2 = th3dbPrepare(p, 0, "SELECT 12345");
  sqlite3_step(pS2);
  rc = sqlite3_bind_value(0, 1, sqlite3_column_value(pS2, 0));
  sqlite3_finalize(pS2);
  th3testCheckInt(p, SQLITE_MISUSE, rc);
  th3testBegin(p, "308");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_zeroblob(0, 1, 100));
  th3testBegin(p, "310");
  pStmt = th3dbPrepare(p, 0, "SELECT ?");
  sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_blob(pStmt, 1, "", 1, SQLITE_STATIC));
#ifndef SQLITE_OMIT_FLOATING_POINT
  th3testBegin(p, "311");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_double(pStmt, 1, 1.23));
#endif
  th3testBegin(p, "312");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_int(pStmt, 1, 2));
  th3testBegin(p, "313");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_int64(pStmt, 1, 3));
  th3testBegin(p, "314");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_null(pStmt, 1));
  th3testBegin(p, "315");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_text(pStmt, 1, "hi", 2, SQLITE_STATIC));
  th3testBegin(p, "316");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_text16(pStmt, 1, aText, 2, SQLITE_STATIC));
  th3testBegin(p, "317");
  pS2 = th3dbPrepare(p, 0, "SELECT 12345");
  sqlite3_step(pS2);
  rc = sqlite3_bind_value(pStmt, 1, sqlite3_column_value(pS2, 0));
  sqlite3_finalize(pS2);
  th3testCheckInt(p, SQLITE_MISUSE, rc);
  th3testBegin(p, "318");
  th3testCheckInt(p, SQLITE_MISUSE, 
             sqlite3_bind_zeroblob(pStmt, 1, 100));
  sqlite3_finalize(pStmt);


  /* EVIDENCE-OF: R-14744-61161 Bindings are not cleared by the
  ** sqlite3_reset() routine.
  */
  th3testBegin(p, "320");
  pStmt = th3dbPrepare(p, 0, "SELECT ?, ?, ?");
  sqlite3_bind_int(pStmt, 1, 123);
  sqlite3_bind_text(pStmt, 2, "hi", -1, SQLITE_STATIC);
  sqlite3_bind_int64(pStmt, 3, 4567890);
  rc = th3dbStep(p, pStmt);
  th3testCheck(p, "123 hi 4567890");
  th3testBegin(p, "321");
  th3testCheckInt(p, SQLITE_ROW, rc);
  th3testBegin(p, "322");
  rc = sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_DONE, rc);
  th3testBegin(p, "323");
  sqlite3_reset(pStmt);
  th3dbStep(p, pStmt);
  th3testCheck(p, "123 hi 4567890");
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-26972-25540 The sqlite3_bind_* routines return
  ** SQLITE_OK on success or an error code if anything goes wrong.
  */
  th3testBegin(p, "330a");
  pStmt = th3dbPrepare(p, 0, "SELECT typeof($a), length($a), $a");
  rc = sqlite3_bind_blob(pStmt, 1, "1", 1, SQLITE_STATIC);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "330b");
  th3dbStep(p, pStmt);
  sqlite3_reset(pStmt);
  th3testCheck(p, "blob 1 1");
#ifndef SQLITE_OMIT_FLOATING_POINT
  th3testBegin(p, "331a");
  rc = sqlite3_bind_double(pStmt, 1, 234.5);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "331b");
  th3dbStep(p, pStmt);
  sqlite3_reset(pStmt);
  th3testCheck(p, "real 5 234.5");
#endif
  th3testBegin(p, "332a");
  rc = sqlite3_bind_int(pStmt, 1, 56);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "332b");
  th3dbStep(p, pStmt);
  sqlite3_reset(pStmt);
  th3testCheck(p, "integer 2 56");
  th3testBegin(p, "333a");
  rc = sqlite3_bind_int64(pStmt, 1, 78);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "333b");
  th3dbStep(p, pStmt);
  sqlite3_reset(pStmt);
  th3testCheck(p, "integer 2 78");
  th3testBegin(p, "334a");
  rc = sqlite3_bind_null(pStmt, 1);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "334b");
  th3dbStep(p, pStmt);
  sqlite3_reset(pStmt);
  th3testCheck(p, "null nil nil");
  th3testBegin(p, "335a");
  rc = sqlite3_bind_text(pStmt, 1, "xyzzy", 5, SQLITE_STATIC);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "335b");
  th3dbStep(p, pStmt);
  sqlite3_reset(pStmt);
  th3testCheck(p, "text 5 xyzzy");
  th3testBegin(p, "335a");
  th3_ascii_to_utf16("xyzzy", aText);
  rc = sqlite3_bind_text16(pStmt, 1, aText, 10, SQLITE_STATIC);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "335b");
  th3dbStep(p, pStmt);
  sqlite3_reset(pStmt);
  th3testCheck(p, "text 5 xyzzy");
  th3testBegin(p, "336a");
  pS2 = th3dbPrepare(p, 0, "SELECT 'whatever'");
  sqlite3_step(pS2);
  rc = sqlite3_bind_value(pStmt, 1, sqlite3_column_value(pS2, 0));
  sqlite3_finalize(pS2);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "336b");
  th3dbStep(p, pStmt);
  sqlite3_reset(pStmt);
  th3testCheck(p, "text 8 whatever");
  th3testBegin(p, "337a");
  rc = sqlite3_bind_zeroblob(pStmt, 1, 20);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "337b");
  th3dbStep(p, pStmt);
  sqlite3_reset(pStmt);
  th3testCheck(p, "blob 20 {}");
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-18773-06579 SQLITE_RANGE is returned if the parameter
  ** index is out of range.
  */
  th3testBegin(p, "340");
  pStmt = th3dbPrepare(p, 0, "SELECT ?2, ?10");
  rc = sqlite3_bind_int(pStmt, 1, 123);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "341");
  rc = sqlite3_bind_int(pStmt, 0, -123);
  th3testCheckInt(p, SQLITE_RANGE, rc);
  th3testBegin(p, "342");
  rc = sqlite3_bind_int(pStmt, 9, 999);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "343");
  rc = sqlite3_bind_int(pStmt, 10, 101010);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "344");
  rc = sqlite3_bind_int(pStmt, 11, 111111);
  th3testCheckInt(p, SQLITE_RANGE, rc);
  th3testBegin(p, "349");
  th3dbStep(p, pStmt);
  sqlite3_finalize(pStmt);
  th3testCheck(p, "nil 101010");

  /* EVIDENCE-OF: R-50561-21716 SQLITE_NOMEM is returned if malloc() fails.
  */
  th3oomBegin(p, "350");
  pStmt = th3dbPrepare(p, 0, "SELECT typeof($v), length($v), $v");
  while( th3oomNext(p) ){
    int rc;
    sqlite3_reset(pStmt);
    th3oomEnable(p, 1);
    rc = sqlite3_bind_text(pStmt, 1, "fuzzy", 5, SQLITE_TRANSIENT);
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK );
      th3dbStep(p, pStmt);
      th3oomResult(p,3, "text 5 fuzzy");
    }
  }
  th3oomEnd(p);
  th3oomBegin(p, "351");
  th3_ascii_to_utf16("fuzzy", aText);
  while( th3oomNext(p) ){
    int rc;
    sqlite3_reset(pStmt);
    th3oomEnable(p, 1);
    rc = sqlite3_bind_text16(pStmt, 1, aText, 10, SQLITE_TRANSIENT);
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK );
      th3dbStep(p, pStmt);
      th3oomResult(p,3, "text 5 fuzzy");
    }
  }
  th3oomEnd(p);
  th3oomBegin(p, "352");
  while( th3oomNext(p) ){
    int rc;
    sqlite3_reset(pStmt);
    th3oomEnable(p, 1);
    rc = sqlite3_bind_blob(pStmt, 1, "fuzzy", 5, SQLITE_TRANSIENT);
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK );
      th3dbStep(p, pStmt);
      th3oomResult(p,3, "blob 5 fuzzy");
    }
  }
  th3oomEnd(p);
  th3oomBegin(p, "353");
  pS2 = th3dbPrepare(p, 0, "SELECT 'whatever'");
  sqlite3_step(pS2);
  while( th3oomNext(p) ){
    int rc;
    sqlite3_reset(pStmt);
    th3oomEnable(p, 1);
    rc = sqlite3_bind_value(pStmt, 1, sqlite3_column_value(pS2, 0));
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK );
      th3dbStep(p, pStmt);
      th3oomResult(p,3, "text 8 whatever");
    }
  }
  sqlite3_finalize(pS2);
  sqlite3_finalize(pStmt);
  th3oomEnd(p);

  
  return 0;
}
