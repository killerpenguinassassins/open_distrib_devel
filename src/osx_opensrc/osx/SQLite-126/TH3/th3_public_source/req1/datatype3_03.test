/*
** This module contains tests for datatypes as described in the
** datatype3.html document.
**
** SCRIPT_MODULE_NAME:        req1_datatype3_03
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* EVIDENCE-OF: R-16625-30785-18660
**
** The big column affinity example in section 2.3 of the datatype3.html
** document.
*/
--testcase 1
CREATE TABLE t1(
    t  TEXT,     -- text affinity by rule 2
    nu NUMERIC,  -- numeric affinity by rule 5
    i  INTEGER,  -- integer affinity by rule 1
    r  REAL,     -- real affinity by rule 4
    no BLOB      -- no affinity by rule 3
);
--result

#ifndef SQLITE_OMIT_FLOATING_POINT
---- Values stored as TEXT, INTEGER, INTEGER, REAL, TEXT.
--testcase 100
INSERT INTO t1 VALUES('500.0', '500.0', '500.0', '500.0', '500.0');
SELECT typeof(t), typeof(nu), typeof(i), typeof(r), typeof(no) FROM t1;
--result text integer integer real text

---- Values stored as TEXT, INTEGER, INTEGER, REAL, REAL.
--testcase 110
DELETE FROM t1;
INSERT INTO t1 VALUES(500.0, 500.0, 500.0, 500.0, 500.0);
SELECT typeof(t), typeof(nu), typeof(i), typeof(r), typeof(no) FROM t1;
--result text integer integer real real

---- Values stored as TEXT, INTEGER, INTEGER, REAL, INTEGER.
--testcase 120
DELETE FROM t1;
INSERT INTO t1 VALUES(500, 500, 500, 500, 500);
SELECT typeof(t), typeof(nu), typeof(i), typeof(r), typeof(no) FROM t1;
--result text integer integer real integer
#endif

---- BLOBs are always stored as BLOBs regardless of column affinity.
--testcase 130
DELETE FROM t1;
INSERT INTO t1 VALUES(x'0500', x'0500', x'0500', x'0500', x'0500');
SELECT typeof(t), typeof(nu), typeof(i), typeof(r), typeof(no) FROM t1;
--result blob blob blob blob blob

---- NULLs are also unaffected by affinity
--testcase 140
DELETE FROM t1;
INSERT INTO t1 VALUES(NULL,NULL,NULL,NULL,NULL);
SELECT typeof(t), typeof(nu), typeof(i), typeof(r), typeof(no) FROM t1;
--result null null null null null
