/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_foreignkeys21
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* 
** EV: R-08908-23439 A CREATE TABLE command operates the same whether or
** not foreign key constraints are enabled.
**
** EV: R-36018-21755 The parent key definitions of foreign key constraints
** are not checked when a table is created.
**
** EV: R-25384-39337 There is nothing stopping the user from creating a
** foreign key definition that refers to a parent table that does not
** exist, or to parent key columns that do not exist or are not
** collectively bound by a PRIMARY KEY or UNIQUE constraint.
**
** This test script runs both with and without foreign key constraints
** in order to demonstrate the same answer is obtained in either case.
*/
--testcase 100
CREATE TABLE t1(a REFERENCES t2(x));  -- reference non-existant table
--result
--testcase 101
SELECT sql FROM sqlite_master
--result {CREATE TABLE t1(a REFERENCES t2(x))}

--testcase 110
CREATE TABLE t2(b);  -- table now exists but has no column x
--result
--testcase 111
SELECT sql FROM sqlite_master
--result {CREATE TABLE t1(a REFERENCES t2(x))} {CREATE TABLE t2(b)}

--testcase 120
CREATE TABLE t3(c REFERENCES t4(d));
CREATE TABLE t4(d); -- Referenced column is not PRIMARY KEY or UNIQUE
--result

/*
** EV: R-47952-62498 It is not possible to use the
** "ALTER TABLE ... ADD COLUMN" syntax to add a column that includes
** a REFERENCES clause, unless the default value of the new column is NULL.
** Attempting to do so returns an error.
*/
--testcase 200
PRAGMA foreign_keys(TRUE);
--result
--testcase 201
ALTER TABLE t4 ADD COLUMN e DEFAULT 123 REFERENCES t3;
--result SQLITE_ERROR {Cannot add a REFERENCES column with non-NULL default value}
--testcase 202
ALTER TABLE t4 ADD COLUMN e DEFAULT null REFERENCES t3;
--result

/*
** EV: R-47080-02069 If an "ALTER TABLE ... RENAME TO" command is used to
** rename a table that is the parent table of one or more foreign key
** constraints, the definitions of the foreign key constraints are modified
** to refer to the parent table by its new name
**
** EV: R-63827-54774 The text of the child CREATE TABLE statement or
** statements stored in the sqlite_master table are modified to reflect
** the new parent table name.
*/
--testcase 300
CREATE TABLE [name with spaces](x INTEGER PRIMARY KEY);
CREATE TABLE t3a(x, y, FOREIGN KEY(y)REFERENCES "Name With Spaces"(X));
SELECT sql FROM sqlite_master WHERE name='t3a';
--result {CREATE TABLE t3a(x, y, FOREIGN KEY(y)REFERENCES "Name With Spaces"(X))}

--testcase 310
ALTER TABLE [name with spaces] RENAME TO "special^symbols";
SELECT sql FROM sqlite_master WHERE name='t3a';
--result {CREATE TABLE t3a(x, y, FOREIGN KEY(y)REFERENCES "special^symbols"(X))}
