/*
** This module contains tests for the legacy_file_format pragma
**
** SCRIPT_MODULE_NAME:        req1_pragma22
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
--raw-new test.db

/* EVIDENCE-OF: R-20944-50040 The default file format is set by the
** SQLITE_DEFAULT_FILE_FORMAT compile-time option.
*/
#if defined(SQLITE_DEFAULT_FILE_FORMAT) && SQLITE_DEFAULT_FILE_FORMAT>1
--testcase 100a
SELECT * FROM sqlite_master WHERE 0;
PRAGMA legacy_file_format;
--result 0
#else
--testcase 100b
SELECT * FROM sqlite_master WHERE 0;
PRAGMA legacy_file_format;
--result 1
#endif

/* EVIDENCE-OF: R-33638-42108 PRAGMA legacy_file_format; PRAGMA
** legacy_file_format = boolean This pragma sets or queries the value of
** the legacy_file_format flag.
**
** EVIDENCE-OF: R-26226-51609 When the legacy_file_format pragma is
** issued with no argument, it returns the setting of the flag.
*/
--testcase 110
PRAGMA legacy_file_format=ON;
PRAGMA legacy_file_format;
--result 1
--testcase 111
PRAGMA legacy_file_format=OFF;
PRAGMA legacy_file_format;
--result 0

/* EVIDENCE-OF: R-40228-05483 When this flag is on, new SQLite databases
** are created in a file format that is readable and writable by all
** versions of SQLite going back to 3.0.0.
*/
--raw-new test.db
--testcase 200
PRAGMA legacy_file_format=ON;
CREATE TABLE t1(x);
--result
--testcase 201
seek 44 read32
--edit test.db
--result 1

/* EVIDENCE-OF: R-45936-30472 This pragma does not tell which file format
** the current database is using; it tells what format will be used by
** any newly created databases.
*/
--testcase 202
PRAGMA legacy_file_format=OFF;
PRAGMA legacy_file_format;
--result 0
--testcase 203
seek 44 read32
--edit test.db
--result 1


/* EVIDENCE-OF: R-09458-10155 When the flag is off, new databases are
** created using the latest file format which might not be readable or
** writable by versions of SQLite prior to 3.3.0.
*/
--raw-new test.db
--testcase 300
PRAGMA legacy_file_format=OFF;
CREATE TABLE t1(x);
PRAGMA legacy_file_format=ON;
PRAGMA legacy_file_format;
--result 1
--testcase 301
seek 44 read32
--edit test.db
--result 4

/* EVIDENCE-OF: R-64727-38667 The legacy_file_format pragma is
** initialized to OFF when an existing database in the newer file format
** is first opened.
*/
--open test.db
--testcase 400
SELECT * FROM sqlite_master WHERE 0;
PRAGMA legacy_file_format;
--result 0

--raw-new test.db
--testcase 410
PRAGMA legacy_file_format=ON;
CREATE TABLE t1(x);
--result
--testcase 411
seek 44 read32
--edit test.db
--result 1
--close all
--raw-open test.db
#if defined(SQLITE_DEFAULT_FILE_FORMAT) && SQLITE_DEFAULT_FILE_FORMAT>1
--testcase 412a
SELECT * FROM sqlite_master WHERE 0;
PRAGMA legacy_file_format;
--result 0
#else
--testcase 412b
SELECT * FROM sqlite_master WHERE 0;
PRAGMA legacy_file_format;
--result 1
#endif
