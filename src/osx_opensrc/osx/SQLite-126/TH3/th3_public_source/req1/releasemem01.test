/*
** This module contains tests for sqlite3_release_memory() interface.
**
** MODULE_NAME:               req1_releasemem01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     THREADS ALT_PCACHE MEMDB NO_MEMSTATUS
** MINIMUM_HEAPSIZE:          100000
*/
int req1_releasemem01(th3state *p){
  int nBefore;
  int nAfter;
  int n1, n2;

  if( p->config.nPagecache ) return 0;
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, 
    "PRAGMA cache_size=100;"
    "CREATE TABLE t1(x INTEGER PRIMARY KEY, y BLOB);"
    "INSERT INTO t1 VALUES(1, randomblob(3000));"
    "INSERT INTO t1 VALUES(2, randomblob(6000));"
    "INSERT INTO t1 SELECT x+2, randomblob(4000) FROM t1;"
    "SELECT sum(x), sum(length(y)) FROM t1;"
  );
  th3testCheck(p, "10 17000");

  /* EVIDENCE-OF: R-45051-55006 The sqlite3_release_memory() interface
  ** attempts to free N bytes of heap memory by deallocating non-essential
  ** memory allocations held by the database library.
  */
  th3testBegin(p, "110");
  nBefore = sqlite3_memory_used();
  n1 = sqlite3_release_memory(1000);
  nAfter = sqlite3_memory_used();
  th3testCheckInt(p, nBefore-nAfter, n1);
#ifdef SQLITE_ENABLE_MEMORY_MANAGEMENT
  th3testBegin(p, "111");
  th3testCheckInt(p, 1, n1>0);
#else
  /* EVIDENCE-OF: R-34391-24921 The sqlite3_release_memory() routine is a
  ** no-op returning zero if SQLite is not compiled with
  ** SQLITE_ENABLE_MEMORY_MANAGEMENT.
  */
  th3testBegin(p, "112");
  th3testCheckInt(p, 0, n1);
#endif

  
  th3testBegin(p, "120");
  nBefore = sqlite3_memory_used();
  n2 = sqlite3_release_memory(200000);
  nAfter = sqlite3_memory_used();
  th3testCheckInt(p, nBefore-nAfter, n2);
  /* EVIDENCE-OF: R-08782-34587 sqlite3_release_memory() returns the number
  ** of bytes actually freed, which might be more or less than the amount
  ** requested.
  */
#ifdef SQLITE_ENABLE_MEMORY_MANAGEMENT
  th3testBegin(p, "121");
  th3testCheckInt(p, 1, n1>1000);
#endif
  th3testBegin(p, "122");
  th3testCheckInt(p, 1, n2<200000);

  
  return 0;
}
