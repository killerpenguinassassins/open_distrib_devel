/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_foreignkeys05
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/

/* EV: R-41784-13339 Tip: If the command "PRAGMA foreign_keys" returns no data
** instead of a single row containing "0" or "1", then the version of
** SQLite you are using does not support foreign keys (either because
** it is older than 3.6.19 or because it was compiled with
** SQLITE_OMIT_FOREIGN_KEY or SQLITE_OMIT_TRIGGER defined). 
*/
#if SQLITE_VERSION_NUMBER<3006019 || defined(SQLITE_OMIT_FOREIGN_KEY) || defined(SQLITE_OMIT_TRIGGER)
--testcase 100
PRAGMA foreign_keys=ON;
PRAGMA foreign_keys;
--result
#endif
#if SQLITE_VERSION_NUMBER>=3006019 && !defined(SQLITE_OMIT_FOREIGN_KEY) && !defined(SQLITE_OMIT_TRIGGER)
--testcase 200
PRAGMA foreign_keys=ON;
PRAGMA foreign_keys;
--result 1
#endif

/* EV: R-33710-56344 In order to use foreign key constraints in SQLite,
** the library must be compiled with neither SQLITE_OMIT_FOREIGN_KEY
** or SQLITE_OMIT_TRIGGER defined.
**
** EV: R-44697-61543 If SQLITE_OMIT_TRIGGER is defined but
** SQLITE_OMIT_FOREIGN_KEY is not, then SQLite behaves as it did prior
** to version 3.6.19 - foreign key definitions are parsed and may be
** queried using PRAGMA foreign_key_list, but foreign key constraints
** are not enforced.
**
** EV: R-22567-44039 The PRAGMA foreign_keys command is a no-op in
** this configuration.
*/
#if defined(SQLITE_OMIT_TRIGGER) && !defined(SQLITE_OMIT_FOREIGN_KEY)
--testcase 300
CREATE TABLE t1(a INTEGER PRIMARY KEY);
INSERT INTO t1 VALUES(123);
CREATE TABLE t2(b INTEGER, FOREIGN KEY(b) REFERENCES t1(a) ON DELETE CASCADE);
INSERT INTO t2 VALUES(456);
SELECT * FROM t1, t2;
--result 123 456
#endif

/* EV: R-58428-36660 If OMIT_FOREIGN_KEY is defined, then foreign key
** definitions cannot even be parsed (attempting to specify a foreign key
** definition is a syntax error).
*/
#if defined(SQLITE_OMIT_FOREIGN_KEY)
--testcase 400
CREATE TABLE t2(b INTEGER, FOREIGN KEY(b) REFERENCES t1(a) ON DELETE CASCADE);
--result SQLITE_ERROR {near "b": syntax error}
#endif
