/*
** This module contains tests for the sqlite3_blob_reopen() interface.
**
** MODULE_NAME:               req1_blob01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     UTF16LE UTF16BE
** MINIMUM_HEAPSIZE:          100000
*/
int req1_blob01(th3state *p){
  sqlite3 *db;
  sqlite3_blob *pBlob;
  int rc;
  sqlite3_int64 i64big, i64small;
  char zBuf[100];

  th3testBegin(p, "100");

  /* Construct a sample database */
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  th3dbEval(p, 0,
    "CREATE TABLE t1(a INTEGER PRIMARY KEY,b,c,d);"
    "INSERT INTO t1 VALUES(5, 'this', 'is', 'a-test');"
    "INSERT INTO t1 VALUES(3, 'another', 'test', 'case');"
    "INSERT INTO t1 VALUES(-9, 'uses', 'negative', 'rowid');"
    "CREATE TABLE t2(w INTEGER PRIMARY KEY,x,y,z);"
    "INSERT INTO t2 VALUES(9223372036854775807, 'maximum', 'rowid', 'value');"
    "INSERT INTO t2 VALUES(-9223372036854775808,"
                           "'rowid-value', 'minimized', null);"
    "INSERT INTO t2 VALUES(0, 'z-column', 'numeric', 12345);"
    "INSERT INTO t2 VALUES(1, 'blob', 'value', x'b0b1b2b3');"
#ifndef SQLITE_OMIT_FLOATING_POINT
    "INSERT INTO t2 VALUES(2, 'floating', 'point', 2.5);"
#endif    
    "ATTACH ':memory:' AS db2;"
    "CREATE TABLE db2.t3(a INTEGER PRIMARY KEY, b, c, d);"
    "INSERT INTO t3 SELECT a,c,d,b FROM t1;"
    "INSERT INTO t3 SELECT w,y,z,x FROM t2;"
  );

  /* Get the blob handle */
  rc = sqlite3_blob_open(db, "main", "t2", "y", 0, 1, &pBlob);
  assert( rc==SQLITE_OK );

  /* Verify the handle */
  sqlite3_blob_read(pBlob, zBuf, 7, 0);
  zBuf[7] = 0;
  th3testAppendResultTerm(p, zBuf);
  th3testCheck(p, "numeric");

  /* EVIDENCE-OF: R-53212-31827 This function is used to move an existing
  ** blob handle so that it points to a different row of the same database
  ** table.
  **
  ** EVIDENCE-OF: R-59275-48655 The new row is identified by the rowid
  ** value passed as the second argument.
  **
  ** EVIDENCE-OF: R-45889-48790 The database, table and column on which the
  ** blob handle is open remain the same.
  */
  th3testBegin(p, "110");
  i64big = (922337203*(sqlite3_int64)100000 + 68547)*100000 + 75807;
  i64small = -i64big-1;
  sqlite3_blob_reopen(pBlob, i64big);
  sqlite3_blob_read(pBlob, zBuf, 5, 0);
  zBuf[5] = 0;
  th3testAppendResultTerm(p, zBuf);
  th3testCheck(p, "rowid");

  th3testBegin(p, "111");
  sqlite3_blob_reopen(pBlob, i64small);
  sqlite3_blob_read(pBlob, zBuf, 5, 0);
  zBuf[5] = 0;
  th3testAppendResultTerm(p, zBuf);
  th3testCheck(p, "minim");
  sqlite3_blob_close(pBlob);
 
  /* EVIDENCE-OF: R-53739-48047 The new row must meet the same criteria as
  ** for sqlite3_blob_open() - it must exist and there must be either a
  ** blob or text value stored in the nominated column.
  **
  ** EVIDENCE-OF: R-55970-49067 If the new row is not present in the table,
  ** or if it does not contain a blob or text value, or if another error
  ** occurs, an SQLite error code is returned and the blob handle is
  ** considered aborted.
  **
  ** EVIDENCE-OF: R-63408-55464 All subsequent calls to
  ** sqlite3_blob_read(), sqlite3_blob_write() or sqlite3_blob_reopen() on
  ** an aborted blob handle immediately return SQLITE_ABORT.
  **
  ** EVIDENCE-OF: R-01277-06213 This function sets the database handle
  ** error code and message.
  */
  th3testBegin(p, "200");
  rc = sqlite3_blob_open(db, "main", "t2", "y", 0, 4, &pBlob);
  assert( rc==SQLITE_OK );
  rc = sqlite3_blob_reopen(pBlob, 11);
  th3testAppendResultTerm(p, th3errorCodeName(rc));
  th3testAppendResultTerm(p, th3errorCodeName(sqlite3_errcode(db)));
  th3testAppendResultTerm(p, sqlite3_errmsg(db));
  th3testCheck(p, "SQLITE_ERROR SQLITE_ERROR {no such rowid: 11}");
  th3testBegin(p, "201");
  rc = sqlite3_blob_reopen(pBlob, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  th3testBegin(p, "202");
  rc = sqlite3_blob_read(pBlob, zBuf, 1, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  th3testBegin(p, "203");
  rc = sqlite3_blob_write(pBlob, "x", 1, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  sqlite3_blob_close(pBlob);


  th3testBegin(p, "210");
  rc = sqlite3_blob_open(db, "main", "t2", "z", i64big, 1, &pBlob);
  assert( rc==SQLITE_OK );
  rc = sqlite3_blob_reopen(pBlob, i64small);
  th3testAppendResultTerm(p, th3errorCodeName(rc));
  th3testAppendResultTerm(p, th3errorCodeName(sqlite3_errcode(db)));
  th3testAppendResultTerm(p, sqlite3_errmsg(db));
  th3testCheck(p, "SQLITE_ERROR SQLITE_ERROR {cannot open value of type null}");
  th3testBegin(p, "211");
  rc = sqlite3_blob_reopen(pBlob, i64big);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  th3testBegin(p, "212");
  rc = sqlite3_blob_read(pBlob, zBuf, 1, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  th3testBegin(p, "213");
  rc = sqlite3_blob_write(pBlob, "x", 1, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  sqlite3_blob_close(pBlob);

  th3testBegin(p, "220");
  rc = sqlite3_blob_open(db, "main", "t2", "z", i64big, 1, &pBlob);
  assert( rc==SQLITE_OK );
  rc = sqlite3_blob_reopen(pBlob, 0);
  th3testAppendResultTerm(p, th3errorCodeName(rc));
  th3testAppendResultTerm(p, th3errorCodeName(sqlite3_errcode(db)));
  th3testAppendResultTerm(p, sqlite3_errmsg(db));
  th3testCheck(p,
      "SQLITE_ERROR SQLITE_ERROR {cannot open value of type integer}");
  th3testBegin(p, "221");
  rc = sqlite3_blob_reopen(pBlob, i64big);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  th3testBegin(p, "222");
  rc = sqlite3_blob_read(pBlob, zBuf, 1, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  th3testBegin(p, "223");
  rc = sqlite3_blob_write(pBlob, "x", 1, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  sqlite3_blob_close(pBlob);

#ifndef SQLITE_OMIT_FLOATING_POINT
  th3testBegin(p, "230");
  rc = sqlite3_blob_open(db, "main", "t2", "z", i64big, 1, &pBlob);
  assert( rc==SQLITE_OK );
  rc = sqlite3_blob_reopen(pBlob, 2);
  th3testAppendResultTerm(p, th3errorCodeName(rc));
  th3testAppendResultTerm(p, th3errorCodeName(sqlite3_errcode(db)));
  th3testAppendResultTerm(p, sqlite3_errmsg(db));
  th3testCheck(p, "SQLITE_ERROR SQLITE_ERROR {cannot open value of type real}");
  th3testBegin(p, "231");
  rc = sqlite3_blob_reopen(pBlob, i64big);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  th3testBegin(p, "232");
  rc = sqlite3_blob_read(pBlob, zBuf, 1, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  th3testBegin(p, "233");
  rc = sqlite3_blob_write(pBlob, "x", 1, 0);
  th3testCheckInt(p, SQLITE_ABORT, rc);
  sqlite3_blob_close(pBlob);
#endif

  th3testBegin(p, "240");
  rc = sqlite3_blob_open(db, "main", "t2", "z", i64big, 1, &pBlob);
  assert( rc==SQLITE_OK );
  rc = sqlite3_blob_reopen(pBlob, 1);
  th3testCheckTrue(p, rc==SQLITE_OK);
  sqlite3_blob_close(pBlob);



  return 0;
}
