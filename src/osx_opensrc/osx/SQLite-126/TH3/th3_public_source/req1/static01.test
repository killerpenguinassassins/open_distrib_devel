/*
** This module contains tests for constants SQLITE_STATIC and SQLITE_TRANSIENT
** as used in sqlite3_result_text().
**
** MODULE_NAME:               req1_static01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
*/

/*
** This function binds a string STATIC, then changes the string
** value before returning.
**
** EVIDENCE-OF: R-31518-37056 If the destructor argument is
** SQLITE_STATIC, it means that the content pointer is constant and will
** never change.
**
** By changing the value after binding and showing that the return is
** the new value we demonstrate SQLite continues
** to use the old pointer.
*/
static void req1static01_static(
  sqlite3_context *pContext,
  int nArg,
  sqlite3_value **apArg
){
  static char zBuf[10];
  th3strcpy(zBuf, "hello-1");
  sqlite3_result_text(pContext, zBuf, -1, SQLITE_STATIC);
  th3strcpy(zBuf, "hello-2");
}

/*
** This function binds a string TRANSIENT , then changes the string
** value before returning.
**
** EVIDENCE-OF: R-15807-22461 The SQLITE_TRANSIENT value means that the
** content will likely change in the near future and that SQLite should
** make its own private copy of the content before returning.
**
** By changing the value after binding and showing that the original
** value is returned, we demonstrate that SQLite made its own copy.
*/
static void req1static01_transient(
  sqlite3_context *pContext,
  int nArg,
  sqlite3_value **apArg
){
  static char zBuf[10];
  th3strcpy(zBuf, "hello-3");
  sqlite3_result_text(pContext, zBuf, -1, SQLITE_TRANSIENT);
  th3strcpy(zBuf, "hello-4");
}

int req1_static01(th3state *p){
  sqlite3 *db;

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  sqlite3_create_function(db, "static1", -1, SQLITE_ANY, 0, 
                          req1static01_static, 0, 0);
  sqlite3_create_function(db, "transient1", -1, SQLITE_ANY, 0, 
                          req1static01_transient, 0, 0);
  th3dbEval(p, 0, "SELECT static1(1,2), transient1('a','b','c');");
  th3testCheck(p, "hello-2 hello-3");

  return 0;
}
