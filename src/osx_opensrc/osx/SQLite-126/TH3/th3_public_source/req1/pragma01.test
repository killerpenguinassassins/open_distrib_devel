/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_pragma01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/*
** EV: R-05855-44863
**
** EV: R-08964-37480
**
** (Syntax diagrams)
**
** EVIDENCE-OF: R-64973-09381 A pragma can take either zero or one
** argument.
**
** EVIDENCE-OF: R-60831-05341 The argument is may be either in
** parentheses or it may be separated from the pragma name by an equal
** sign.
**
** EVIDENCE-OF: R-04709-28430 The two syntaxes yield identical results.
*/
--testcase 100
PRAGMA cache_size=2000;
PRAGMA cache_size;
--result 2000
--testcase 110
PRAGMA main.cache_size;
--result 2000
--testcase 120
PRAGMA cache_size = +1000;
PRAGMA cache_size;
--result 1000
--testcase 121
PRAGMA cache_size(+800);
PRAGMA cache_size;
--result 800
--testcase 130
PRAGMA user_version = -1234;
PRAGMA user_version;
--result -1234
--testcase 131
PRAGMA main.user_version(-4321);
PRAGMA main.user_version;
--result -4321

/* EVIDENCE-OF: R-36822-49898 In many pragmas, the argument is a boolean.
** The boolean can be one of: 1 yes true on0 no false off
**
** EVIDENCE-OF: R-15737-42560 Keyword arguments can optionally appear in
** quotes.
*/
--testcase 140
PRAGMA foreign_keys = 'yes';
PRAGMA foreign_keys;
--result 1
--testcase 141
PRAGMA foreign_keys("NO");
PRAGMA foreign_keys;
--result 0
--testcase 142
PRAGMA foreign_keys('ON');
PRAGMA foreign_keys;
--result 1
--testcase 143
PRAGMA foreign_keys = off;
PRAGMA foreign_keys;
--result 0
--testcase 144
PRAGMA foreign_keys(true);
PRAGMA foreign_keys;
--result 1
--testcase 145
PRAGMA foreign_keys = [FALSE];
PRAGMA foreign_keys;
--result 0
--testcase 146
PRAGMA foreign_keys = 1;
PRAGMA foreign_keys;
--result 1
--testcase 147
PRAGMA foreign_keys(0);
PRAGMA foreign_keys;
--result 0

/* EVIDENCE-OF: R-60193-46484 A pragma may have an optional database name
** before the pragma name.
**
** EVIDENCE-OF: R-57233-37249 The database name is the name of an
** ATTACH-ed database or it can be "main" or "temp" for the main and the
** TEMP databases.
**
** EVIDENCE-OF: R-23452-42564 If the optional database name is omitted,
** "main" is assumed.
*/
--store $pgsz PRAGMA page_size;
--testcase 200
ATTACH ':memory:' AS aux1;
CREATE TABLE main.t1(x);
INSERT INTO t1 VALUES(zeroblob(11*$pgsz));
CREATE TEMP TABLE t2(y);
INSERT INTO t2 VALUES(zeroblob(21*$pgsz));
CREATE TABLE aux1.t3(z);
INSERT INTO t3 VALUES(zeroblob(31000));
PRAGMA main.page_count;
PRAGMA temp.page_count;
PRAGMA aux1.page_count;
PRAGMA page_count;
--glob 1# 2# 3# 1#

/* EVIDENCE-OF: R-51351-46040 In some pragmas, the database name is
** meaningless and is simply ignored.
*/
--testcase 210
PRAGMA main.full_column_names;
PRAGMA temp.full_column_names;
PRAGMA aux1.full_column_names;
PRAGMA full_column_names;
--result 0 0 0 0
--testcase 211
PRAGMA main.full_column_names=ON;
PRAGMA main.full_column_names;
PRAGMA temp.full_column_names;
PRAGMA aux1.full_column_names;
PRAGMA full_column_names;
--result 1 1 1 1
