/*
** This module contains tests for datatypes as described in the
** datatype3.html document.
**
** SCRIPT_MODULE_NAME:        req1_datatype3_05
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** The focus of this file is the following two requirements.  The following
** two requirements are verified by all test cases in this file in
** aggregation.
**
** EVIDENCE-OF: R-64839-61120 SQLite may attempt to convert values
** between the storage classes INTEGER, REAL, and/or TEXT before
** performing a comparison.
**
** EVIDENCE-OF: R-53396-21131 Whether or not any conversions are
** attempted before the comparison takes place depends on the affinity of
** the operands.
**
** This file also, as a whole, verifies the following:
**
** EVIDENCE-OF: R-65146-56212 Each column in an SQLite 3 database is
** assigned one of the following type affinities: TEXT NUMERIC INTEGER
** REAL NONE
*/

/* EVIDENCE-OF: R-07643-43989 An expression that is a simple reference to
** a column value has the same affinity as the column.
*/
--testcase 100
CREATE TABLE t100(
  x INTEGER PRIMARY KEY,
  a TEXT,      -- TEXT affinity
  b INTEGER,   -- INTEGER affinity
  c REAL,      -- REAL affinity
  d NUMERIC,   -- NUMERIC affinity
  e BLOB       -- NONE affinity
);
INSERT INTO t100 VALUES(1, 20,20,20,20,20);
INSERT INTO t100 VALUES(2, '20','20','20','20','20');
--result

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 101fp
SELECT a, typeof(a), b, typeof(b), c, typeof(c), d, typeof(d), e, typeof(e)
  FROM t100 ORDER BY x;
--result 20 text 20 integer 20.0 real 20 integer 20 integer 20 text 20 integer 20.0 real 20 integer 20 text
#else
--testcase 101omitfp
SELECT a, typeof(a), b, typeof(b), d, typeof(d), e, typeof(e)
  FROM t100 ORDER BY x;
--result 20 text 20 integer 20 integer 20 integer 20 text 20 integer 20 integer 20 text
#endif

--testcase 110
SELECT x FROM t100 WHERE a<'100' ORDER BY x;
--result
--testcase 111
SELECT x FROM t100 WHERE a<3 ORDER BY x;
--result 1 2
--testcase 112
SELECT x FROM t100 WHERE a>30 ORDER BY x;
--result

--testcase 120
SELECT x FROM t100 WHERE b<'100' ORDER BY x;
--result 1 2
--testcase 121
SELECT x FROM t100 WHERE b<3 ORDER BY x;
--result
--testcase 122
SELECT x FROM t100 WHERE b>30 ORDER BY x;
--result

--testcase 130
SELECT x FROM t100 WHERE c<'100' ORDER BY x;
--result 1 2
--testcase 131
SELECT x FROM t100 WHERE c<3 ORDER BY x;
--result
--testcase 132
SELECT x FROM t100 WHERE c>30 ORDER BY x;
--result

--testcase 140
SELECT x FROM t100 WHERE d<'100' ORDER BY x;
--result 1 2
--testcase 141
SELECT x FROM t100 WHERE d<3 ORDER BY x;
--result
--testcase 142
SELECT x FROM t100 WHERE d>30 ORDER BY x;
--result

--testcase 150
SELECT x FROM t100 WHERE e<'100' ORDER BY x;
--result 1
--testcase 151
SELECT x FROM t100 WHERE e<3 ORDER BY x;
--result
--testcase 152
SELECT x FROM t100 WHERE e>30 ORDER BY x;
--result 2


/* EVIDENCE-OF: R-29555-61994 Note that if X and Y.Z are column names,
** then +X and +Y.Z are considered expressions for the purpose of
** determining affinity.
*/
--testcase 210
SELECT x FROM t100 WHERE +a<'100' ORDER BY x;
--result 
--testcase 211
SELECT x FROM t100 WHERE +t100.a<'100' ORDER BY x;
--result
--testcase 212
SELECT x FROM t100 WHERE +a>300 ORDER BY x;
--result 1 2
--testcase 213
SELECT x FROM t100 WHERE +t100.a>300 ORDER BY x;
--result 1 2

--testcase 220
SELECT x FROM t100 WHERE +b<'10' ORDER BY x;
--result 1 2
--testcase 221
SELECT x FROM t100 WHERE +t100.b<'10' ORDER BY x;
--result 1 2
--testcase 222
SELECT x FROM t100 WHERE +b>30 ORDER BY x;
--result 

/* EVIDENCE-OF: R-53367-44371 An expression of the form "CAST(expr AS
** type)" has an affinity that is the same as a column with a declared
** type of "type".
**
** EVIDENCE-OF: R-47189-13533 Otherwise, an expression has NONE affinity.
*/
--testcase 300
SELECT '5' < 6;
--result 0
--testcase 301
SELECT '5' < CAST(6 AS int);
--result 1

/* EVIDENCE-OF: R-50978-04100 If one operand has INTEGER, REAL or NUMERIC
** affinity and the other operand as TEXT or NONE affinity then NUMERIC
** affinity is applied to other operand.
*/
--testcase 400
SELECT '5' < '40';
--result 0
--testcase 401
SELECT CAST('5' AS int) < '40';
--result 1
--testcase 402
SELECT CAST('5' AS int) < '4';
--result 0
--testcase 403
SELECT '5' < CAST('40' AS int);
--result 1
--testcase 404
SELECT '5' < CAST('4' AS int);
--result 0

--testcase 411
SELECT CAST('5' AS real) < '40';
--result 1
--testcase 412
SELECT CAST('5' AS real) < '4';
--result 0
--testcase 413
SELECT '5' < CAST('40' AS real);
--result 1
--testcase 414
SELECT '5' < CAST('4' AS real);
--result 0

--testcase 421
SELECT CAST('5' AS numeric) < '40';
--result 1
--testcase 422
SELECT CAST('5' AS numeric) < '4';
--result 0
--testcase 423
SELECT '5' < CAST('40' AS numeric);
--result 1
--testcase 424
SELECT '5' < CAST('4' AS numeric);
--result 0

/* EVIDENCE-OF: R-02762-06118 If one operand has TEXT affinity and the
** other has NONE affinity, then TEXT affinity is applied to the other
** operand.
*/
--testcase 500
SELECT 5 < 40;
--result 1
--testcase 500
SELECT '5' < '40';
--result 0

--testcase 510
SELECT CAST('5' AS text) < 40;
--result 0
--testcase 511
SELECT CAST('5' AS text) < 60;
--result 1
--testcase 512
SELECT 5 < CAST('40' AS text);
--result 0
--testcase 513
SELECT 5 < CAST('60' AS text);
--result 1

/* EVIDENCE-OF: R-19178-27645 Otherwise, no affinity is applied and both
** operands are compared as is.
*/
--testcase 600
SELECT 5 < '4';
--result 1
--testcase 601
SELECT '5' < 6;
--result 0

/* EVIDENCE-OF: R-10393-27560 Affinity is applied to operands of a
** comparison operator prior to the comparison according to the following
** rules in the order shown:
**
** Clarification:  INTEGER/REAL/NUMERIC affinity takes priority over
** TEXT affinity.
*/
--testcase 700
SELECT CAST(5 AS int) < CAST('4' AS text);
--result 0
--testcase 701
SELECT CAST('5' AS text) < CAST(4 AS int);
--result 0
--testcase 702
SELECT CAST(5 AS real) < CAST('4' AS text);
--result 0
--testcase 703
SELECT CAST('5' AS text) < CAST(4 AS real);
--result 0
--testcase 704
SELECT CAST(5 AS numeric) < CAST('4' AS text);
--result 0
--testcase 705
SELECT CAST('5' AS text) < CAST(4 AS numeric);
--result 0

--testcase 710
CREATE TABLE t710(x INTEGER PRIMARY KEY, a TEXT, b INTEGER, c REAL, d NUMERIC);
INSERT INTO t710 VALUES(1, '5', 40, 40, 40);
INSERT INTO t710 VALUES(2, '50', 6, 6, 6);
SELECT x FROM t710 WHERE a<b;
--result 1
--testcase 711
SELECT x FROM t710 WHERE a<c;
--result 1
--testcase 712
SELECT x FROM t710 WHERE a<d;
--result 1
--testcase 713
SELECT x FROM t710 WHERE a<+b;
--result 2
--testcase 714
SELECT x FROM t710 WHERE a<+c;
--result 2
--testcase 715
SELECT x FROM t710 WHERE a<+d;
--result 2
--testcase 716
SELECT x FROM t710 WHERE +a<b;
--result 1
--testcase 717
SELECT x FROM t710 WHERE +a<c;
--result 1
--testcase 718
SELECT x FROM t710 WHERE +a<d;
--result 1
--testcase 719
SELECT x FROM t710 WHERE +a<+b;
--result 
--testcase 720
SELECT x FROM t710 WHERE +a<+c;
--result 
--testcase 721
SELECT x FROM t710 WHERE +a<+d;
--result

/* EVIDENCE-OF: R-46475-65007 The expression "a BETWEEN b AND c" is
** treated as two separate binary comparisons "a >= b AND a <= c", even
** if that means different affinities are applied to 'a' in each of the
** comparisons.
*/
--testcase 800
SELECT 20 BETWEEN 3 AND '2';
--result 1
--testcase 801
SELECT 20 BETWEEN '3' AND 40;
--result 0
--testcase 802
SELECT '20' BETWEEN 3 AND 40;
--result 0
--testcase 803
SELECT '20' BETWEEN '3' AND '40';
--result 0
--testcase 804
SELECT '20' BETWEEN 3 AND '40';
--result 1
--testcase 805
SELECT 20 BETWEEN 3 AND '4';
--result 1
--testcase 806
SELECT '20' BETWEEN 3 AND '4';
--result 1

--testcase 810
SELECT 20 BETWEEN CAST(3 AS int) AND CAST('4' AS text);
--result 1
--testcase 811
SELECT '20' BETWEEN CAST(3 AS int) AND CAST('4' AS text);
--result 1
--testcase 812
SELECT 20 BETWEEN CAST('100' AS text) AND CAST(100 AS int);
--result 1
--testcase 813
SELECT CAST(20 AS int) BETWEEN CAST(3 AS int) AND CAST('4' AS text);
--result 0

/* EVIDENCE-OF: R-11162-32742 Datatype conversions in comparisons of the
** form "x IN (SELECT y ...)" are handled is if the comparison were
** really "x=y".
*/
--testcase 900
SELECT 123 IN (SELECT '123');
--result 0
--testcase 901
SELECT 123 IN (SELECT 123);
--result 1
--testcase 902
SELECT '123' IN (SELECT '123');
--result 1
--testcase 903
SELECT '123' IN (SELECT 123);
--result 0

--testcase 910
SELECT CAST(123 AS int) IN (SELECT '123');
--result 1
--testcase 911
SELECT '123' IN (SELECT CAST(123 as int));
--result 1
--testcase 912
SELECT CAST(123 AS real) IN (SELECT '123');
--result 1
--testcase 913
SELECT '123' IN (SELECT CAST(123 as real));
--result 1
--testcase 914
SELECT CAST(123 AS numeric) IN (SELECT '123');
--result 1
--testcase 915
SELECT '123' IN (SELECT CAST(123 as numeric));
--result 1

--testcase 920
SELECT CAST(123 AS int) IN (SELECT '0123');
--result 1
--testcase 921
SELECT '0123' IN (SELECT CAST(123 as int));
--result 1
--testcase 922
SELECT CAST(123 AS real) IN (SELECT '0123');
--result 1
--testcase 923
SELECT '0123' IN (SELECT CAST(123 as real));
--result 1
--testcase 924
SELECT CAST(123 AS numeric) IN (SELECT '0123');
--result 1
--testcase 925
SELECT '0123' IN (SELECT CAST(123 as numeric));
--result 1

--testcase 930
SELECT CAST(123 AS int) IN (SELECT CAST('0123' AS text));
--result 1
--testcase 931
SELECT CAST('0123' AS text) IN (SELECT CAST(123 as int));
--result 1
--testcase 932
SELECT CAST(123 AS real) IN (SELECT CAST('0123' AS text));
--result 1
--testcase 933
SELECT CAST('0123' AS text) IN (SELECT CAST(123 as real));
--result 1
--testcase 934
SELECT CAST(123 AS numeric) IN (SELECT CAST('0123' AS text));
--result 1
--testcase 935
SELECT CAST('0123' AS text) IN (SELECT CAST(123 as numeric));
--result 1

--testcase 940
SELECT 123 IN (SELECT CAST('0123' AS text));
--result 0
--testcase 941
SELECT CAST('0123' AS text) IN (SELECT 123);
--result 0
--testcase 942
SELECT 123 IN (SELECT CAST('123' AS text));
--result 1
--testcase 943
SELECT CAST('123' AS text) IN (SELECT 123);
--result 1

/* EVIDENCE-OF: R-64049-08691 The expression "a IN (x, y, z, ...)" is
** equivalent to "a = +x OR a = +y OR a = +z OR ...".
**
** EVIDENCE-OF: R-18219-48316 In other words, the values to the right of
** the IN operator (the "x", "y", and "z" values in this example) are
** considered to have no affinity, even if they happen to be column
** values or CAST expressions.
*/
--testcase 1000
SELECT 123 IN ('123','0123','234');
--result 0
--testcase 1001
SELECT '123' IN ('123','0123','234');
--result 1
--testcase 1002
SELECT CAST(123 AS int) IN ('123','0123','234');
--result 1
--testcase 1003
SELECT '123' IN (123,234,345);
--result 0
--testcase 1004
SELECT '123' IN (CAST(123 AS int),234,345);
--result 0
--testcase 1005
SELECT CAST('123' AS text) IN (CAST(123 AS int),234,345);
--result 1

/* EVIDENCE-OF: R-65304-57881
**
** The big example in datatype3.html section 3.4
*/
--testcase 1100
CREATE TABLE t1(
    a TEXT,      -- text affinity
    b NUMERIC,   -- numeric affinity
    c BLOB,      -- no affinity
    d            -- no affinity
);
--result

---- Values will be stored as TEXT, INTEGER, TEXT, and INTEGER respectively
--testcase 1101
INSERT INTO t1 VALUES('500', '500', '500', 500);
SELECT typeof(a), typeof(b), typeof(c), typeof(d) FROM t1;
--result text integer text integer

---- Because column "a" has text affinity, numeric values on the
---- right-hand side of the comparisons are converted to text before
---- the comparison occurs.
--testcase 1102
SELECT a < 40,   a < 60,   a < 600 FROM t1;
--result 0 1 1

---- Text affinity is applied to the right-hand operands but since
---- they are already TEXT this is a no-op; no conversions occur.
--testcase 1103
SELECT a < '40', a < '60', a < '600' FROM t1;
--result 0 1 1

---- Column "b" has numeric affinity and so numeric affinity is applied
---- to the operands on the right.  Since the operands are already numeric,
---- the application of affinity is a no-op; no conversions occur.  All
---- values are compared numerically.
--testcase 1104
SELECT b < 40,   b < 60,   b < 600 FROM t1;
--result 0 0 1

---- Numeric affinity is applied to operands on the right, converting them
---- from text to integers.  Then a numeric comparison occurs.
--testcase 1105
SELECT b < '40', b < '60', b < '600' FROM t1;
--result 0 0 1

---- No affinity conversions occur.  Right-hand side values all have
---- storage class INTEGER which are always less than the TEXT values
---- on the left.
--testcase 1106
SELECT c < 40,   c < 60,   c < 600 FROM t1;
--result 0 0 0

---- No affinity conversions occur.  Values are compared as TEXT.
--testcase 1106
SELECT c < '40', c < '60', c < '600' FROM t1;
--result 0 1 1

---- No affinity conversions occur.  Right-hand side values all have
---- storage class INTEGER which compare numerically with the INTEGER
---- values on the left.
--testcase 1107
SELECT d < 40,   d < 60,   d < 600 FROM t1;
--result 0 0 1

---- No affinity conversions occur.  INTEGER values on the left are
---- always less than TEXT values on the right.
--testcase 1108
SELECT d < '40', d < '60', d < '600' FROM t1;
--result 1 1 1

/* EVIDENCE-OF: R-57806-53405 All of the result in the example are the
** same if the comparisons are commuted - if expressions of the form
** "a<40" are rewritten as "40>a".
*/
--testcase 1200
DROP TABLE t1;
CREATE TABLE t1(
    a TEXT,      -- text affinity
    b NUMERIC,   -- numeric affinity
    c BLOB,      -- no affinity
    d            -- no affinity
);
--result

---- Values will be stored as TEXT, INTEGER, TEXT, and INTEGER respectively
--testcase 1201
INSERT INTO t1 VALUES('500', '500', '500', 500);
SELECT typeof(a), typeof(b), typeof(c), typeof(d) FROM t1;
--result text integer text integer

---- Because column "a" has text affinity, numeric values on the
---- right-hand side of the comparisons are converted to text before
---- the comparison occurs.
--testcase 1202
SELECT 40 > a,   60 > a,   600 > a FROM t1;
--result 0 1 1

---- Text affinity is applied to the right-hand operands but since
---- they are already TEXT this is a no-op; no conversions occur.
--testcase 1203
SELECT '40' > a, '60' > a, '600' > a FROM t1;
--result 0 1 1

---- Column "b" has numeric affinity and so numeric affinity is applied
---- to the operands on the right.  Since the operands are already numeric,
---- the application of affinity is a no-op; no conversions occur.  All
---- values are compared numerically.
--testcase 1204
SELECT 40 > b,   60 > b,   600 > b FROM t1;
--result 0 0 1

---- Numeric affinity is applied to operands on the right, converting them
---- from text to integers.  Then a numeric comparison occurs.
--testcase 1205
SELECT '40' > b, '60' > b, '600' > b FROM t1;
--result 0 0 1

---- No affinity conversions occur.  Right-hand side values all have
---- storage class INTEGER which are always less than the TEXT values
---- on the left.
--testcase 1206
SELECT 40 > c,   60 > c,   600 > c FROM t1;
--result 0 0 0

---- No affinity conversions occur.  Values are compared as TEXT.
--testcase 1206
SELECT '40' > c, '60' > c, '600' > c FROM t1;
--result 0 1 1

---- No affinity conversions occur.  Right-hand side values all have
---- storage class INTEGER which compare numerically with the INTEGER
---- values on the left.
--testcase 1207
SELECT 40 > d,   60 > d,   600 > d FROM t1;
--result 0 0 1

---- No affinity conversions occur.  INTEGER values on the left are
---- always less than TEXT values on the right.
--testcase 1208
SELECT '40' > d, '60' > d, '600' > d FROM t1;
--result 1 1 1
