/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_foreignkeys13
** REQUIRED_PROPERTIES:       FOREIGN_KEYS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* EV: R-24499-57071
**
** The following example illustrates the effect of using a deferred
** foreign key constraint....
*/
--testcase 100
--- Database schema. Both tables are initially empty. 
CREATE TABLE artist(
  artistid    INTEGER PRIMARY KEY, 
  artistname  TEXT
);
CREATE TABLE track(
  trackid     INTEGER,
  trackname   TEXT, 
  trackartist INTEGER REFERENCES artist(artistid) DEFERRABLE INITIALLY DEFERRED
);
--result
--testcase 101
--- If the foreign key constraint were immediate, this INSERT would
--- cause an error (since as there is no row in table artist with
--- artistid=5). But as the constraint is deferred and there is an
--- open transaction, no error occurs.
BEGIN;
  INSERT INTO track VALUES(1, 'White Christmas', 5);

  -- The following COMMIT fails, as the database is in a state that
  -- does not satisfy the deferred foreign key constraint. The
  -- transaction remains open.
COMMIT;
--result SQLITE_CONSTRAINT {foreign key constraint failed}

--testcase 102
  -- After inserting a row into the artist table with artistid=5, the
  -- deferred foreign key constraint is satisfied. It is then possible
  -- to commit the transaction without error.
  INSERT INTO artist VALUES(5, 'Bing Crosby');
COMMIT;
--result
