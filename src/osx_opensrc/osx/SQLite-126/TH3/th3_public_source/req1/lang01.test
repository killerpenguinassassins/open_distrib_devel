/*
** This module contains tests for lang.html
**
** MODULE_NAME:               req1_lang01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/*
** The following structure holds a sequence of SQL statements that
** collectively run all statement types shown in the sql-stmt syntax
** diagram.  By prepending each of these with EXPLAIN and with
** EXPLAIN QUERY PLAN, we run all paths through the sql-stmt syntax
** diagram.
**
** EVIDENCE-OF: R-50292-60524 -- syntax diagram sql-stmt
*/
static const struct {
  const char *zPrep;
  const char *zSql;
} req1_lang01_sql[] = {
  { "CREATE TABLE t1(x)", "ALTER TABLE t1 RENAME TO t2" },
  { 0, "ANALYZE" },
  { 0, "ATTACH ':memory:' AS mem" },
  { 0, "BEGIN" },
  { "BEGIN", "COMMIT" },
  { "CREATE TABLE t1(x)", "CREATE INDEX i1 ON t1(x)" },
  { 0, "CREATE TABLE t1(x)" },
  { "CREATE TABLE t1(x)", "CREATE TRIGGER r1 AFTER INSERT ON t1 BEGIN"
                          "  DELETE FROM t1 WHERE x=new.x; END" },
  { "CREATE TABLE t1(x)", "CREATE VIEW v1 AS SELECT x+1 FROM t1" },
  { 0, "CREATE VIRTUAL TABLE vt1 USING bvs" },
  { "CREATE TABLE t1(x)", "DELETE FROM t1 WHERE x=5" },
#ifdef SQLITE_ENABLE_UPDATE_DELELE_LIMIT
  { "CREATE TABLE t1(x)", "DELETE FROM t1 ORDER BY x LIMIT 3" },
#endif
  { "ATTACH ':memory:' AS mem", "DETACH mem" },
  { "CREATE TABLE t1(x); CREATE INDEX t1x ON t1(x)", "DROP INDEX t1x" },
  { "CREATE TABLE t1(x)", "DROP TABLE t1" },
  { "CREATE TABLE t1(x); CREATE TRIGGER r1 AFTER INSERT ON t1 BEGIN"
                          "  DELETE FROM t1 WHERE x=new.x; END",
    "DROP TRIGGER r1" },
  { "CREATE TABLE t1(x);CREATE VIEW v1 AS SELECT x+1 FROM t1", "DROP VIEW v1" },
  { "CREATE TABLE t1(x)", "INSERT INTO t1 VALUES(1)" },
  { 0, "PRAGMA journal_mode" },
  { 0, "REINDEX" },
  { "SAVEPOINT xyz", "RELEASE xyz" },
  { "BEGIN", "ROLLBACK" },
  { 0, "SAVEPOINT xyz" },
  { "CREATE TABLE t1(x)", "SELECT x+1 FROM t1" },
  { "CREATE TABLE t1(x)", "UPDATE t1 SET x=x+1" },
#ifdef SQLITE_ENABLE_UPDATE_DELELE_LIMIT
  { "CREATE TABLE t1(x)", "UPDATE t1 SET x=x+1 ORDER BY x LIMIT 2" },
#endif
  { 0, "VACUUM" },
  { 0, 0 },  /* Special case to generate an empty string */
};

int req1_lang01(th3state *p){
  char *zSql, *zTail;
  unsigned short int *zSql16, *zTail16;
  int iSql;
  int iPrefix;
  int rc;
  int nRow, nCol;
  char **azResult;
  sqlite3 *db;
  int i;
  sqlite3_stmt *pStmt;

  /* Prefixes to the req1_lang01_sql[].zSql[] strings.  By
  ** combining prefixes all paths through the sql-stmt diagram
  ** are traversed.  Because of the ";" entry, and the semicolons
  ** added to join statements together, all paths through the
  ** sql-stmt-list syntax diagram are also traversed.
  **
  ** EVIDENCE-OF: R-48104-38769 -- syntax diagram sql-stmt-list
  */
  static const char *azPrefix[] = {
     "",
     ";",
     "EXPLAIN ",
     "EXPLAIN QUERY PLAN "
  };

  zSql = th3malloc(p, 500);
  zSql16 = th3malloc(p, 1000);
  for(iSql=0; iSql<COUNT(req1_lang01_sql); iSql++){
    for(iPrefix=0; iPrefix<COUNT(azPrefix); iPrefix++){
      if( req1_lang01_sql[iSql].zSql==0 ){
        sqlite3_snprintf(500, zSql, "");
      }else if( req1_lang01_sql[iSql].zPrep==0 ){
        sqlite3_snprintf(500, zSql, "%s%s;", 
          azPrefix[iPrefix],
          req1_lang01_sql[iSql].zSql
        );
      }else{
        sqlite3_snprintf(500, zSql, "%s; %s%s;", 
          req1_lang01_sql[iSql].zPrep,
          azPrefix[iPrefix],
          req1_lang01_sql[iSql].zSql
        );
      }
      th3_ascii_to_utf16(zSql, zSql16);

      /* The SQL statements are now in zSql and zSql16.  The following
      ** tests run the SQL through all functions that accept SQL.
      **
      ** EVIDENCE-OF: R-59738-58751 The routines sqlite3_prepare_v2(),
      ** sqlite3_prepare(), sqlite3_prepare16(), sqlite3_prepare16_v2(),
      ** sqlite3_exec(), and sqlite3_get_table() accept an SQL statement list
      ** (sql-stmt-list) which is a semicolon-separated list of statements.
      */
      th3testBegin(p, th3format(p, "%d.%d.1", iSql, iPrefix));
      th3dbNew(p, 0, "test.db");
      db = th3dbPointer(p, 0);
      rc = sqlite3_exec(db, zSql, 0, 0, 0);
      th3testCheckInt(p, SQLITE_OK, rc);

      th3testBegin(p, th3format(p, "%d.%d.2", iSql, iPrefix));
      th3dbNew(p, 0, "test.db");
      db = th3dbPointer(p, 0);
      rc = sqlite3_get_table(db, zSql, &azResult, &nRow, &nCol, 0);
      sqlite3_free_table(azResult);
      th3testCheckInt(p, SQLITE_OK, rc);

      zTail = zSql;
      i = 1;
      th3dbNew(p, 0, "test.db");
      db = th3dbPointer(p, 0);
      while( zTail && zTail[0] && i<100 ){
        th3testBegin(p, th3format(p, "%d.%d.3.%d", iSql, iPrefix, i++));
        rc = sqlite3_prepare(db, zTail, -1, &pStmt, (const char**)&zTail);
        th3testCheckInt(p, SQLITE_OK, rc);
        if( pStmt ){
          th3testBegin(p, th3format(p, "%d.%d.3.%d", iSql, iPrefix, i++));
          sqlite3_step(pStmt);
          rc = sqlite3_finalize(pStmt);
          th3testCheckInt(p, SQLITE_OK, rc);
        }
      }

      zTail = zSql;
      i = 1;
      th3dbNew(p, 0, "test.db");
      db = th3dbPointer(p, 0);
      while( zTail && zTail[0] && i<100 ){
        th3testBegin(p, th3format(p, "%d.%d.4.%d", iSql, iPrefix, i++));
        rc = sqlite3_prepare_v2(db, zTail, -1, &pStmt, (const char**)&zTail);
        th3testCheckInt(p, SQLITE_OK, rc);
        if( pStmt ){
          th3testBegin(p, th3format(p, "%d.%d.4.%d", iSql, iPrefix, i++));
          sqlite3_step(pStmt);
          rc = sqlite3_finalize(pStmt);
          th3testCheckInt(p, SQLITE_OK, rc);
        }
      }

      zTail16 = zSql16;
      i = 1;
      th3dbNew(p, 0, "test.db");
      db = th3dbPointer(p, 0);
      while( zTail16 && zTail16[0] && i<100 ){
        th3testBegin(p, th3format(p, "%d.%d.5.%d", iSql, iPrefix, i++));
        rc = sqlite3_prepare16(db, (const void*)zTail16, -1, &pStmt,
                               (const void**)&zTail16);
        th3testCheckInt(p, SQLITE_OK, rc);
        if( pStmt ){
          th3testBegin(p, th3format(p, "%d.%d.5.%d", iSql, iPrefix, i++));
          sqlite3_step(pStmt);
          rc = sqlite3_finalize(pStmt);
          th3testCheckInt(p, SQLITE_OK, rc);
        }
      }

      zTail16 = zSql16;
      i = 1;
      th3dbNew(p, 0, "test.db");
      db = th3dbPointer(p, 0);
      while( zTail16 && zTail16[0] && i<100 ){
        th3testBegin(p, th3format(p, "%d.%d.6.%d", iSql, iPrefix, i++));
        rc = sqlite3_prepare16_v2(db, (const void*)zTail16, -1, &pStmt,
                                 (const void**)&zTail16);
        th3testCheckInt(p, SQLITE_OK, rc);
        if( pStmt ){
          th3testBegin(p, th3format(p, "%d.%d.6.%d", iSql, iPrefix, i++));
          sqlite3_step(pStmt);
          rc = sqlite3_finalize(pStmt);
          th3testCheckInt(p, SQLITE_OK, rc);
        }
      }

      if( zSql[0]==0 ) break;
    }
  }

  return 0; 
}
