/*
** This module contains tests for the sqilte3_auto_extension() interface.
**
** MODULE_NAME:               req1_autoext01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
*/

/* Global variables passed from the main test routine into the
** extension functions.  This is the only means we have of passing
** parameters into the extension functions.  But we can't use threads
** on this test anyway, so no harm done.
*/
static struct {
  int nReg;
} req1_autoext01_g = { 0 };

/*
** Some functions to be registered by the automatic extension loader.
*/
static void req1_autoext01_x1(
  sqlite3_context *pContext,
  int nArg,
  sqlite3_value **apArg
){
  sqlite3_result_int(pContext, nArg);
}
static void req1_autoext01_x2(
  sqlite3_context *pContext,
  int nArg,
  sqlite3_value **apArg
){
  int x = sqlite3_value_int(apArg[0]);
  sqlite3_result_int(pContext, x*x);
}


/*
** Automatic extension loaders
**
** EVIDENCE-OF: R-08468-48792 Even though the function prototype shows
** that xEntryPoint() takes no arguments and returns void, SQLite invokes
** xEntryPoint() with three arguments and expects and integer result as
** if the signature of the entry point where as follows: &nbsp; int
** xEntryPoint( &nbsp; sqlite3 *db, &nbsp; const char **pzErrMsg,
** &nbsp; const struct sqlite3_api_routines *pThunk &nbsp; );
*/
static int req1_autoext01_0(
  sqlite3 *db,
  const char **pzErrMsg,
  const sqlite3_api_routines *pNotUsed
){
  req1_autoext01_g.nReg++;

  /* EVIDENCE-OF: R-40045-04159 SQLite ensures that *pzErrMsg is NULL
  ** before calling the xEntryPoint(). */
  assert( (*pzErrMsg)==0 );

  sqlite3_create_function(db, "narg", -1, SQLITE_ANY, 0,
                          req1_autoext01_x1, 0, 0);

  /* EVIDENCE-OF: R-61815-26679 SQLite will invoke sqlite3_free() on
  ** *pzErrMsg after xEntryPoint() returns.
  **
  ** Where it not so, the following line of code would case a memory leak.
  */
  *pzErrMsg = sqlite3_mprintf("not an error");

  return SQLITE_OK;
}
static int req1_autoext01_1(
  sqlite3 *db,
  const char **pzErrMsg,
  const sqlite3_api_routines *pNotUsed
){
  /* EVIDENCE-OF: R-40045-04159 SQLite ensures that *pzErrMsg is NULL
  ** before calling the xEntryPoint(). */
  assert( (*pzErrMsg)==0 );

  sqlite3_create_function(db, "squared", 1, SQLITE_ANY, 0,
                          req1_autoext01_x2, 0, 0);
  return SQLITE_OK;
}
static int req1_autoext01_2(
  sqlite3 *db,
  const char **pzErrMsg,
  const sqlite3_api_routines *pNotUsed
){
  /* EVIDENCE-OF: R-40045-04159 SQLite ensures that *pzErrMsg is NULL
  ** before calling the xEntryPoint(). */
  assert( (*pzErrMsg)==0 );

  sqlite3_create_function(db, "square2", 1, SQLITE_ANY, 0,
                          req1_autoext01_x2, 0, 0);
  *pzErrMsg = sqlite3_mprintf("deliberate failures");
  return SQLITE_INTERNAL;
}


int req1_autoext01(th3state *p){
  int rc;
  sqlite3 *db;
  unsigned short utf16[20];

  /* EVIDENCE-OF: R-23980-43280 This interface causes the xEntryPoint()
  ** function to be invoked for each new database connection that is
  ** created.
  */
  th3testBegin(p, "100");
  req1_autoext01_g.nReg = 0;
  sqlite3_auto_extension((void(*)(void))req1_autoext01_0);
  sqlite3_auto_extension((void(*)(void))req1_autoext01_1);
  th3testCheckInt(p, 0, req1_autoext01_g.nReg);

  th3testBegin(p, "110");
  th3dbNew(p, 0, "test.db");
  th3testCheckInt(p, 1, req1_autoext01_g.nReg);
  th3testBegin(p, "111");
  th3dbEval(p, 0, "SELECT narg(1,2,3,4), squared(4)");
  th3testCheck(p, "4 16");

  th3testBegin(p, "120");
  th3dbNew(p, 1, "test.db");
  th3testCheckInt(p, 2, req1_autoext01_g.nReg);
  th3testBegin(p, "121");
  th3dbEval(p, 1, "SELECT narg(1,2,3,4), squared(4)");
  th3testCheck(p, "4 16");
  th3dbCloseAll(p);

  /* EVIDENCE-OF: R-08916-58393 This interface disables all automatic
  ** extensions previously registered using sqlite3_auto_extension().
  */
  th3testBegin(p, "200");
  req1_autoext01_g.nReg = 0;
  sqlite3_reset_auto_extension();
  th3testCheckInt(p, 0, req1_autoext01_g.nReg);

  th3testBegin(p, "210");
  th3dbNew(p, 0, "test.db");
  th3testCheckInt(p, 0, req1_autoext01_g.nReg);
  th3testBegin(p, "211");
  th3dbEval(p, 0, "SELECT narg(1,2,3,4)");
  th3testCheck(p, "SQLITE_ERROR {no such function: narg}");

  /* EVIDENCE-OF: R-45318-21719 If any xEntryPoint() returns an error, the
  ** sqlite3_open(), sqlite3_open16(), or sqlite3_open_v2() call that
  ** provoked the xEntryPoint() will fail.
  */
  th3testBegin(p, "300");
  sqlite3_auto_extension((void(*)(void))req1_autoext01_2);
  rc = sqlite3_open("test.db", &db);
  th3testAppendResultTerm(p, th3errorCodeName(rc));
  th3testAppendResultTerm(p, sqlite3_errmsg(db));
  sqlite3_close(db);
  th3testCheck(p, 
    "SQLITE_ERROR {automatic extension loading failed: deliberate failures}");

  th3testBegin(p, "310");
  th3_ascii_to_utf16("test.db", utf16);
  rc = sqlite3_open16(utf16, &db);
  th3testAppendResultTerm(p, th3errorCodeName(rc));
  th3testAppendResultTerm(p, sqlite3_errmsg(db));
  sqlite3_close(db);
  th3testCheck(p, 
    "SQLITE_ERROR {automatic extension loading failed: deliberate failures}");

  th3testBegin(p, "320");
  rc = sqlite3_open_v2((void*)utf16, &db,
                       SQLITE_OPEN_CREATE|SQLITE_OPEN_READWRITE, 0);
  th3testAppendResultTerm(p, th3errorCodeName(rc));
  th3testAppendResultTerm(p, sqlite3_errmsg(db));
  sqlite3_close(db);
  th3testCheck(p, 
    "SQLITE_ERROR {automatic extension loading failed: deliberate failures}");
  sqlite3_reset_auto_extension();

  /* EVIDENCE-OF: R-29907-62378 Calling sqlite3_auto_extension(X) with an
  ** entry point X that is already on the list of automatic extensions is a
  ** harmless no-op.
  **
  ** EVIDENCE-OF: R-47326-42054 No entry point will be called more than
  ** once for each database connection that is opened.
  */
  th3testBegin(p, "400");
  sqlite3_auto_extension((void(*)(void))req1_autoext01_0);
  sqlite3_auto_extension((void(*)(void))req1_autoext01_1);
  sqlite3_auto_extension((void(*)(void))req1_autoext01_0);
  sqlite3_auto_extension((void(*)(void))req1_autoext01_1);
  sqlite3_auto_extension((void(*)(void))req1_autoext01_0);
  sqlite3_auto_extension((void(*)(void))req1_autoext01_1);
  th3testCheckInt(p, 0, req1_autoext01_g.nReg);

  th3testBegin(p, "410");
  th3dbNew(p, 0, "test.db");
  th3testCheckInt(p, 1, req1_autoext01_g.nReg);
  th3testBegin(p, "411");
  th3dbEval(p, 0, "SELECT narg(1,2,3,4), squared(4)");
  th3testCheck(p, "4 16");
  sqlite3_reset_auto_extension();

  return 0;
}
