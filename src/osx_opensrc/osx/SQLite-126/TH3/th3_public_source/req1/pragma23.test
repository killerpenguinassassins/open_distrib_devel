/*
** This module contains tests for the journal_mode pragma
**
** SCRIPT_MODULE_NAME:        req1_pragma23
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     MEMDB TEMPSTORE_MEM ZIPVFS
** MINIMUM_HEAPSIZE:          100000
*/

/* EVIDENCE-OF: R-06467-48849 PRAGMA journal_mode; PRAGMA
** database.journal_mode; PRAGMA journal_mode = DELETE | TRUNCATE |
** PERSIST | MEMORY | WAL | OFF PRAGMA database.journal_mode = DELETE |
** TRUNCATE | PERSIST | MEMORY | WAL | OFF This pragma queries or sets
** the journal mode for databases associated with the current database
** connection.
**
** EVIDENCE-OF: R-63738-52347 The first two forms of this pragma query
** the current journaling mode for database.
**
** EVIDENCE-OF: R-45251-44035 When database is omitted, the "main"
** database is queried.
**
** EVIDENCE-OF: R-15181-32692 The last two forms change the journaling
** mode.
**
** EVIDENCE-OF: R-41190-61687 The 4th form changes the journaling mode
** for a specific database connection named.
**
** EVIDENCE-OF: R-41966-43200 Use "main" for the main database (the
** database that was opened by the original sqlite3_open(),
** sqlite3_open16(), or sqlite3_open_v2() interface call) and use "temp"
** for database that holds TEMP tables.
**
** EVIDENCE-OF: R-44053-43152 The 3rd form changes the journaling mode on
** all databases attached to the connection.
**
** EVIDENCE-OF: R-62787-27093 The new journal mode is returned.
*/
--raw-new test.db
--new-filename test2.db 
--testcase 100
ATTACH :filename AS f2;
CREATE TABLE t1(x);
CREATE TEMP TABLE t2(y);
CREATE TABLE f2.t3(z);
PRAGMA main.journal_mode=TRUNCATE;
PRAGMA temp.journal_mode=PERSIST;
PRAGMA f2.journal_mode=MEMORY;
--result truncate persist memory
--testcase 101
PRAGMA journal_mode;
PRAGMA main.journal_mode;
PRAGMA temp.journal_mode;
PRAGMA f2.journal_mode;
--result truncate truncate persist memory
--testcase 110
PRAGMA main.journal_mode=WAL;
PRAGMA temp.journal_mode=DELETE;
PRAGMA f2.journal_mode=OFF;
--result wal delete off
--testcase 111
PRAGMA journal_mode;
PRAGMA main.journal_mode;
PRAGMA temp.journal_mode;
PRAGMA f2.journal_mode;
--result wal wal delete off
--testcase 120
PRAGMA journal_mode=TRUNCATE;
--result truncate
--testcase 121
PRAGMA journal_mode;
PRAGMA main.journal_mode;
PRAGMA temp.journal_mode;
PRAGMA f2.journal_mode;
--result truncate truncate truncate truncate

/* EVIDENCE-OF: R-10827-60271 If the journal mode could not be changed,
** the original journal mode is returned.
**
** EVIDENCE-OF: R-26383-00685 Note that the journal_mode for an in-memory
** database is either MEMORY or OFF and can not be changed to a different
** value.
**
** EVIDENCE-OF: R-04857-57605 An attempt to change the journal_mode of an
** in-memory database to any setting other than MEMORY or OFF is ignored.
*/
--testcase 200
ATTACH ':memory:' AS m1;
PRAGMA m1.journal_mode;
--result memory
--testcase 210
PRAGMA m1.journal_mode=OFF;
PRAGMA m1.journal_mode;
--result off off
--testcase 220
PRAGMA m1.journal_mode=DELETE;
PRAGMA m1.journal_mode=TRUNCATE;
PRAGMA m1.journal_mode=PERSIST;
PRAGMA m1.journal_mode=WAL;
--result off off off off
--testcase 230
PRAGMA journal_mode=DELETE;
PRAGMA main.journal_mode;
PRAGMA temp.journal_mode;
PRAGMA f2.journal_mode;
PRAGMA m1.journal_mode;
--result delete delete delete delete off
--testcase 240
PRAGMA m1.journal_mode=MEMORY;
PRAGMA m1.journal_mode;
--result memory memory
--testcase 250
PRAGMA m1.journal_mode=DELETE;
PRAGMA m1.journal_mode=TRUNCATE;
PRAGMA m1.journal_mode=PERSIST;
PRAGMA m1.journal_mode=WAL;
--result memory memory memory memory
--testcase 260
PRAGMA journal_mode=DELETE;
PRAGMA main.journal_mode;
PRAGMA temp.journal_mode;
PRAGMA f2.journal_mode;
PRAGMA m1.journal_mode;
--result delete delete delete delete memory

/* EVIDENCE-OF: R-44336-03155 Note also that the journal_mode cannot be
** changed while a transaction is active.
*/
--testcase 300
BEGIN;
INSERT INTO t1 VALUES(123);
PRAGMA main.journal_mode=TRUNCATE;
PRAGMA main.journal_mode;
--result delete delete
--testcase 301
COMMIT;
PRAGMA main.journal_mode=TRUNCATE;
PRAGMA main.journal_mode;
--result truncate truncate

/* EVIDENCE-OF: R-19245-12964 The DELETE journaling mode is the normal
** behavior.
*/
--close all
--raw-new test.db
--testcase 400
PRAGMA main.journal_mode;
PRAGMA temp.journal_mode;
--result delete delete
