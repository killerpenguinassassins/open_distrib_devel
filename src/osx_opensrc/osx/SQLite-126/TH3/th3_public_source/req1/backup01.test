/*
** This module contains tests for backup API
**
** MODULE_NAME:               req1_backup01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
** INCOMPATIBLE_WITH:         nolock
**
** EVIDENCE-OF: R-47666-36489 To perform a backup operation:
** sqlite3_backup_init() is called once to initialize the backup,
** sqlite3_backup_step() is called one or more times to transfer the data
** between the two databases, and finally sqlite3_backup_finish() is
** called to release all resources associated with the backup operation.
**
**  ^---- Tested by this whole file.
*/
int req1_backup01(th3state *p){
  sqlite3_backup *pBackup;
  int rc;
  int szPg;

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, 
    "CREATE TABLE t1(a,b,c);"
    "PRAGMA page_size;"
  );
  szPg = (int)th3atoi64(p->zResult);
  th3testResetResult(p);
  th3bindInt(p, "$pgsz", szPg);
  th3dbEval(p, 0, 
    "INSERT INTO t1 VALUES(1,2,zeroblob($pgsz*10));"
    "CREATE TABLE t2(x,y,z);"
    "INSERT INTO t2 VALUES(7,8,zeroblob($pgsz*10));"
    "SELECT a, b, x, y FROM t1, t2;"
  );
  th3testCheck(p, "1 2 7 8");
  th3testBegin(p, "110");
  th3dbNew(p, 1, "test2.db");
  th3dbEval(p, 1,
    "CREATE TABLE t3(p,q,r); INSERT INTO t3 VALUES('a','b','c');"
    "SELECT * FROM t3;"
    "PRAGMA cache_size=1;"
  );
  th3testCheck(p, "a b c");
  th3testBegin(p, "120");
  pBackup = sqlite3_backup_init(th3dbPointer(p, 1), "main",
                                th3dbPointer(p, 0), "main");
  th3testCheckTrue(p, pBackup!=0);


  /* EVIDENCE-OF: R-19628-13587 SQLite holds a write transaction open on
  ** the destination database file for the duration of the backup
  ** operation.
  */
  th3testBegin(p, "130");
  rc = sqlite3_backup_step(pBackup, 15);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3dbOpen(p, 2, "test2.db", 0);
  th3dbEval(p, 2, "SELECT * FROM sqlite_master");
  if( (p->config.maskProp & TH3_JOURNAL_WAL)==0 ){
    /* Reading with a write-lock is not allowed in rollback mode */
    th3testCheck(p, "SQLITE_BUSY {database is locked}");
  }else{
    /* Reading with a write-lock is allowed in WAL mode */
    th3testCheckGlob(p, "table t3 t3 # {CREATE TABLE t3(p,q,r)}");
  }
  th3testBegin(p, "131");
  th3dbEval(p, 2, "CREATE TABLE t4(x);");
  th3testCheck(p, "SQLITE_BUSY {database is locked}");

  /* EVIDENCE-OF: R-59712-18547 The source database is read-locked only
  ** while it is being read; it is not locked continuously for the entire
  ** backup operation.  
  **
  ** EVIDENCE-OF: R-33315-53384 Thus, the backup may be performed on a live
  ** source database without preventing other database connections from
  ** reading or writing to the source database while the backup is
  ** underway.
  */
  th3testBegin(p, "140");
  th3dbEval(p, 0, "UPDATE t1 SET a=a+1; SELECT a FROM t1;");
  th3testCheck(p, "2");

  /* Verify that the change made by the update in 140 makes it into
  ** the backup
  */
  th3testBegin(p, "150");
  sqlite3_backup_step(pBackup, -1);
  sqlite3_backup_finish(pBackup);
  th3dbEval(p, 2, "SELECT a, b, x, y FROM t1, t2");
  th3testCheck(p, "2 2 7 8");

  return 0;
}
