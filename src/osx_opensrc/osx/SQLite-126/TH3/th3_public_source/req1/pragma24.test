/*
** This module contains tests for the journal_mode pragma for modes
** DELETE, TRUNCATE, PERSIST, and WAL.
**
** SCRIPT_MODULE_NAME:        req1_pragma24
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     MEMDB ZIPVFS
** MINIMUM_HEAPSIZE:          100000
*/

/* EVIDENCE-OF: R-05617-50091 In the DELETE mode, the rollback journal is
** deleted at the conclusion of each transaction.
*/
--testcase 200
PRAGMA journal_mode=DELETE;
CREATE VIRTUAL TABLE fs USING vfs;
SELECT sz FROM fs WHERE zFilename='test.db-journal';
--result delete
--store $pgsz PRAGMA page_size
--testcase 210
PRAGMA cache_size=10;
BEGIN;
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(zeroblob($pgsz*11));
SELECT sz>0 FROM fs WHERE zFilename='test.db-journal';
--result 1
--testcase 220
COMMIT;
SELECT sz>0 FROM fs WHERE zFilename='test.db-journal';
--result

/* EVIDENCE-OF: R-08198-48522 The TRUNCATE journaling mode commits
** transactions by truncating the rollback journal to zero-length instead
** of deleting it.
*/
--testcase 300
PRAGMA journal_mode=TRUNCATE;
BEGIN;
DELETE FROM t1;
SELECT sz>0 FROM fs WHERE zFilename='test.db-journal';
--result truncate 1
--testcase 310
COMMIT;
SELECT sz FROM fs WHERE zFilename='test.db-journal';
--result 0

/* EVIDENCE-OF: R-44624-50921 The PERSIST journaling mode prevents the
** rollback journal from being deleted at the end of each transaction.
** Instead, the header of the journal is overwritten with zeros.
*/
--testcase 400
PRAGMA journal_mode=PERSIST;
--result persist
--testcase 410
BEGIN;
INSERT INTO t1 VALUES(zeroblob($pgsz*12));
SELECT sz>0 FROM fs WHERE zFilename='test.db-journal';
--result 1
--testcase 420
COMMIT;
SELECT sz>0 FROM fs WHERE zFilename='test.db-journal';
--result 1
--testcase 430
read 10
--edit test.db-journal
--result 00000000000000000000

/* EVIDENCE-OF: R-41417-64456 The WAL journaling mode uses a write-ahead
** log instead of a rollback journal to implement transactions.
*/
--store $mode PRAGMA journal_mode=WAL;
--if $mode=='wal'
--testcase 510
BEGIN;
INSERT INTO t1 VALUES(zeroblob($pgsz*11));
SELECT zFilename FROM fs
 WHERE zFilename GLOB '*-journal'
    OR zFilename GLOB '*-wal';
--result test.db-wal
--testcase 520
COMMIT;
SELECT sz>0 FROM fs WHERE zFilename='test.db-wal';
--result 1
--endif $mode=='wal'

/* EVIDENCE-OF: R-64967-62742 The WAL journaling mode is persistent;
** after being set it stays in effect across multiple database
** connections and after closing and reopening the database.
*/
--close all
--raw-open test.db
--testcase 550
PRAGMA journal_mode;
--result wal
