/*
** This module contains tests for the fullfsync pragma
**
** MIXED_MODULE_NAME:         req1_pragma11b
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     MEMDB ZIPVFS
** MINIMUM_HEAPSIZE:          100000
*/
/*
** Store in variable $nsync the value of the number of sync operations
** since the last call to this routine.  Store the number of full-fsync
** operations in $nfullsync.
*/
static void req1_pragma11b_nsync(th3state *p, int iDb, char *zArg){
  th3bindInt(p, "$nsync", p->fsysTest.nSync);
  p->fsysTest.nSync = 0;
  th3bindInt(p, "$nfullsync", p->fsysTest.nFullSync);
  p->fsysTest.nFullSync = 0;
}
/************************ Script *************************/
/* EVIDENCE-OF: R-29353-26294 PRAGMA fullfsync PRAGMA fullfsync =
** boolean; Query or change the fullfsync flag.
**
** EVIDENCE-OF: R-30269-25284 The default value of the fullfsync flag is
** off.
*/
--testcase 100
PRAGMA fullfsync;
--result 0
--testcase 101
PRAGMA fullfsync=ON;
PRAGMA fullfsync;
--result 1
--testcase 102
PRAGMA fullfsync=OFF;
PRAGMA fullfsync;
--result 0

/* EVIDENCE-OF: R-09748-03241 This flag determines whether or not the
** F_FULLFSYNC syncing method is used on systems that support it.
*/
--new test.db
--call req1_pragma11b_nsync
--testcase 200
PRAGMA fullfsync=OFF;
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(1);
INSERT INTO t1 VALUES(2);
INSERT INTO t1 VALUES(3);
INSERT INTO t1 VALUES(4);
INSERT INTO t1 VALUES(5);
SELECT x FROM t1 ORDER BY x;
--result 1 2 3 4 5
--call req1_pragma11b_nsync
--testcase 211
PRAGMA fullfsync;
SELECT $nsync>0 AND $nfullsync==0;
--result 0 1

--new test.db
--testcase 300
PRAGMA fullfsync=ON;
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(1);
INSERT INTO t1 VALUES(2);
INSERT INTO t1 VALUES(3);
INSERT INTO t1 VALUES(4);
INSERT INTO t1 VALUES(5);
SELECT x FROM t1 ORDER BY x;
--result 1 2 3 4 5
--call req1_pragma11b_nsync
--testcase 311
PRAGMA fullfsync;
SELECT $nsync>0 AND $nfullsync>0;
--result 1 1
