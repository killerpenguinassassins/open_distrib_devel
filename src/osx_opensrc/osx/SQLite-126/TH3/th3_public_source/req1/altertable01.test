/*
** This module contains tests for lang_altertable.html
**
** SCRIPT_MODULE_NAME:        req1_altertable01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

# EVIDENCE-OF: R-19083-27730 -- syntax diagram alter-table-stmt
#
--column-names 1
--testcase 100
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(1);
ALTER TABLE main.t1 ADD y;
SELECT * FROM t1;
--result x 1 y nil
--testcase 101
ALTER TABLE t1 ADD COLUMN z;
SELECT * FROM t1;
--result x 1 y nil z nil
--testcase 102
ALTER TABLE main.t1 RENAME TO t2;
SELECT * FROM t2;
--result x 1 y nil z nil
--testcase 103
ALTER TABLE t2 RENAME TO t1;
SELECT * FROM t1;
--result x 1 y nil z nil
--column-names 0

# EVIDENCE-OF: R-48353-20008 The RENAME TO syntax is used to rename the
# table identified by [database-name.]table-name to new-table-name.

--testcase 110
SELECT type, name, sql FROM sqlite_master WHERE name IN ('t1','xyzzy');
--result table t1 {CREATE TABLE "t1"(x, y, z)}
--testcase 111
ALTER TABLE t1 RENAME to xyzzy;
SELECT type, name, sql FROM sqlite_master WHERE name IN ('t1','xyzzy');
--result table xyzzy {CREATE TABLE "xyzzy"(x, y, z)}

# EVIDENCE-OF: R-18692-17694 If the table being renamed has triggers or
# indices, then these remain attached to the table after it has been
# renamed.

--testcase 120
CREATE INDEX i1 ON xyzzy(x);
CREATE TRIGGER r1 AFTER INSERT ON xyzzy BEGIN SELECT null; END;
SELECT name, tbl_name, sql FROM sqlite_master WHERE type='index';
--result i1 xyzzy {CREATE INDEX i1 ON xyzzy(x)}
--testcase 121
SELECT name, tbl_name, sql FROM sqlite_master WHERE type='trigger';
--result r1 xyzzy {CREATE TRIGGER r1 AFTER INSERT ON xyzzy BEGIN SELECT null; END}
--testcase 122
ALTER TABLE xyzzy RENAME TO abcde;
SELECT name, tbl_name, sql FROM sqlite_master WHERE type='index';
--result i1 abcde {CREATE INDEX i1 ON "abcde"(x)}
--testcase 123
SELECT name, tbl_name, sql FROM sqlite_master WHERE type='trigger';
--result r1 abcde {CREATE TRIGGER r1 AFTER INSERT ON "abcde" BEGIN SELECT null; END}

# EVIDENCE-OF: R-17878-35294 However, if there are any view definitions,
# or statements executed by triggers that refer to the table being
# renamed, these are not automatically modified to use the new table
# name.

--testcase 130
CREATE VIEW v1 AS SELECT x+coalesce(y+z,99) FROM abcde;
SELECT * FROM v1;
--result 100
--testcase 131
ALTER TABLE abcde RENAME TO fghij;
SELECT * FROM v1;
--result SQLITE_ERROR {no such table: main.abcde}
--testcase 132
CREATE TABLE t2(a);
CREATE TRIGGER r2 AFTER INSERT ON t2 BEGIN
  UPDATE fghij SET x=new.a;
END;
INSERT INTO t2 VALUES(123);
SELECT * FROM fghij;
--result 123 nil nil
--testcase 133
ALTER TABLE fghij RENAME TO xyzzy;
INSERT INTO t2 VALUES(456);
--result SQLITE_ERROR {no such table: main.fghij}

# EVIDENCE-OF: R-40827-27357 If foreign key constraints are enabled when
# a table is renamed, then any REFERENCES clauses in any table (either
# the table being renamed or some other table) that refer to the table
# being renamed are modified to refer to the renamed table by its new
# name.

--if $property_FOREIGN_KEYS
--testcase 200
CREATE TABLE t201(
  id INTEGER PRIMARY KEY,
  parent INTEGER REFERENCES t201,
  payload TEXT
);
CREATE TABLE t202(
  attrid INTEGER PRIMARY KEY,
  node INTEGER NOT NULL REFERENCES t201,
  value TEXT
);
ALTER TABLE t201 RENAME TO t201xyz;
SELECT sql FROM sqlite_master WHERE name GLOB 't20*' ORDER BY name;
--result "CREATE TABLE \"t201xyz\"(\012  id INTEGER PRIMARY KEY,\012  parent INTEGER REFERENCES \"t201xyz\",\012  payload TEXT\012)" "CREATE TABLE t202(\012  attrid INTEGER PRIMARY KEY,\012  node INTEGER NOT NULL REFERENCES \"t201xyz\",\012  value TEXT\012)"

--testcase 210
DROP TABLE t201xyz;
DROP TABLE t202;
CREATE TABLE t201(
  id INTEGER PRIMARY KEY,
  parent INTEGER,
  payload TEXT,
  FOREIGN KEY(parent) REFERENCES t201(a)
);
CREATE TABLE t202(
  attrid INTEGER PRIMARY KEY,
  node INTEGER NOT NULL,
  value TEXT,
  FOREIGN KEY(node) REFERENCES t201(a)
);
ALTER TABLE t201 RENAME TO t201xyz;
SELECT sql FROM sqlite_master WHERE name GLOB 't20*' ORDER BY name;
--result "CREATE TABLE \"t201xyz\"(\012  id INTEGER PRIMARY KEY,\012  parent INTEGER,\012  payload TEXT,\012  FOREIGN KEY(parent) REFERENCES \"t201xyz\"(a)\012)" "CREATE TABLE t202(\012  attrid INTEGER PRIMARY KEY,\012  node INTEGER NOT NULL,\012  value TEXT,\012  FOREIGN KEY(node) REFERENCES \"t201xyz\"(a)\012)"
--endif # FOREIGN_KEYS

# EVIDENCE-OF: R-10948-48115 The ADD COLUMN syntax is used to add a new
# column to an existing table.
#
# EVIDENCE-OF: R-23336-07085 The new column is always appended to the
# end of the list of existing columns.
#
# EVIDENCE-OF: R-14302-38934 The new column may take any of the forms
# permissible in a CREATE TABLE statement, with the following
# restrictions:

--column-names 1
--testcase 300
CREATE TABLE t300(a INTEGER PRIMARY KEY, b);
CREATE TABLE t301(c0);
ALTER TABLE t301 ADD COLUMN c1;
ALTER TABLE t301 ADD COLUMN c2 ONE TWO THREE FOUR;
ALTER TABLE t301 ADD COLUMN c3 TYPE(+5);
ALTER TABLE t301 ADD COLUMN c4 TYPE(-42332,17);
ALTER TABLE t301 ADD COLUMN c5 ANY CONSTRAINT c5const NOT NULL DEFAULT 0;
ALTER TABLE t301 ADD COLUMN c6 ANY CHECK( c6>11 OR c6<=0 ) DEFAULT -1;
ALTER TABLE t301 ADD COLUMN c7 ANY DEFAULT 'hello' COLLATE nocase;
ALTER TABLE t301 ADD COLUMN c8 INTEGER REFERENCES t300;
INSERT INTO t301 DEFAULT VALUES;
SELECT * FROM t301;
--result c0 nil c1 nil c2 nil c3 nil c4 nil c5 0 c6 -1 c7 hello c8 nil

# EVIDENCE-OF: R-45735-05060 The column may not have a PRIMARY KEY or
# UNIQUE constraint.

--testcase 310
ALTER TABLE t301 ADD COLUMN c9 UNIQUE;
--result SQLITE_ERROR {Cannot add a UNIQUE column}
--testcase 311
SELECT * FROM t301;
--result c0 nil c1 nil c2 nil c3 nil c4 nil c5 0 c6 -1 c7 hello c8 nil
--column-names 0

# EVIDENCE-OF: R-37287-38238 The column may not have a default value of
# CURRENT_TIME, CURRENT_DATE, CURRENT_TIMESTAMP, or an expression in
# parentheses.

--testcase 400
CREATE TABLE t400(a INTEGER PRIMARY KEY);
ALTER TABLE t400 ADD COLUMN b TEXT DEFAULT CURRENT_TIME;
--result SQLITE_ERROR {Cannot add a column with non-constant default}
--testcase 401
ALTER TABLE t400 ADD COLUMN b TEXT DEFAULT CURRENT_DATE;
--result SQLITE_ERROR {Cannot add a column with non-constant default}
--testcase 402
ALTER TABLE t400 ADD COLUMN b TEXT DEFAULT CURRENT_TIMESTAMP;
--result SQLITE_ERROR {Cannot add a column with non-constant default}
--testcase 403
ALTER TABLE t400 ADD COLUMN b TEXT DEFAULT (4+5);
--result SQLITE_ERROR {Cannot add a column with non-constant default}

# EVIDENCE-OF: R-29868-13536 If a NOT NULL constraint is specified, then
# the column must have a default value other than NULL.

--testcase 410
ALTER TABLE t400 ADD COLUMN b NOT NULL;
--result SQLITE_ERROR {Cannot add a NOT NULL column with default value NULL}
--testcase 411
ALTER TABLE t400 ADD COLUMN b NOT NULL DEFAULT null;
--result SQLITE_ERROR {Cannot add a NOT NULL column with default value NULL}

# EVIDENCE-OF: R-13876-13274 If foreign key constraints are enabled and
# a column with a REFERENCES clause is added, the column must have a
# default value of NULL.

--testcase 500
PRAGMA foreign_keys=ON;
CREATE TABLE t501(a INTEGER PRIMARY KEY);
INSERT INTO t501 VALUES(123);
CREATE TABLE t502(x INTEGER PRIMARY KEY);
ALTER TABLE t502 ADD COLUMN y REFERENCES t501 DEFAULT 456;
--result SQLITE_ERROR {Cannot add a REFERENCES column with non-NULL default value}
--testcase 501
ALTER TABLE t502 ADD COLUMN y REFERENCES t501 DEFAULT null;
--result

# EVIDENCE-OF: R-64634-29359 Note also that when adding a CHECK
# constraint, the CHECK constraint is not tested against preexisting
# rows of the table.
#
# EVIDENCE-OF: R-41438-41245 This can result in a table that contains
# data that is in violation of the CHECK constraint.

--testcase 600
CREATE TABLE t600(a INTEGER PRIMARY KEY, b);
INSERT INTO t600 VALUES(123,456);
ALTER TABLE t600 ADD COLUMN c CHECK( b<300 );
--result
--testcase 601
VACUUM;
--result
--testcase 602
PRAGMA integrity_check;
--result ok
--testcase 603
CREATE TEMP TABLE t600t AS SELECT * FROM t600;
DELETE FROM t600;
INSERT INTO t600 SELECT * FROM t600t;
--result SQLITE_CONSTRAINT {constraint failed}
