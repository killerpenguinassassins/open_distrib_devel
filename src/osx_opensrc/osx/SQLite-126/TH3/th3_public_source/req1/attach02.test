/*
** Crash test module for th3.
**
** This test module does a transaction over two attahed databases.
** It verifies that a crash in the middle of a two-database transaction
** is atomic over both databases if the main database is not :memory:
** and the journal mode is not WAL.  It also verifies that individual
** databases are atomic regardless of whether the main database is
** :memory: or the journal mode is WAL.
**
** EVIDENCE-OF: R-10872-35808 Transactions involving multiple attached
** databases are atomic, assuming that the main database is not
** ":memory:" and the journal_mode is not WAL.
**
** EVIDENCE-OF: R-34979-54336 If the main database is ":memory:" or if
** the journal_mode is WAL, then transactions continue to be atomic
** within each individual database file. But if the host computer crashes
** in the middle of a COMMIT where two or more database files are
** updated, some of those files might get the changes where others might
** not.
**
** MODULE_NAME:               req1_attach02
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     JOURNAL_OFF JOURNAL_MEMORY MEMDB ZIPVFS
** MINIMUM_HEAPSIZE:          100000
*/

int req1_attach02(th3state *p){
  int nStep;
  int iCrash, nCrash;
  char *zMainDb;
  char zSig1pre[40];
  char zSig1post[40];
  char zSig2pre[40];
  char zSig2post[40];


  /* Sometimes we want the main database to be "test.db" and other times
  ** we want it to be ":memory:".  Use TH3_SINGLETHREAD to signal the
  ** use of :memory:.
  */
  if( p->config.maskProp & TH3_SINGLETHREAD ){
    zMainDb = ":memory:";
  }else{
    zMainDb = "test.db";
  }

  p->fsysTest.ssCntdown = 0;
  for(nStep=1; p->fsysTest.ssCntdown==0; nStep++){
    /* Initialize the two databases */
    th3filesystemReset(&p->fsysTest);
    th3dbNew(p, 0, "test.db");
    th3fileDelete(p, "test1.db");
    th3fileDelete(p, "test2.db");
    th3dbEval(p, 0, th3format(p,
       "ATTACH DATABASE 'test1.db' AS test1;\n"
       "ATTACH DATABASE 'test2.db' AS test2;\n"
       "CREATE TABLE test1.t1(x,y);\n"
       "CREATE TABLE test2.t2(a,b);\n"
    ));

    /* Record in zSig1 the checksum of over both databases before the
    ** start of the transaction during which the crash will occur.  If
    ** the transaction rolls back, it should rollback to this checksum.
    */
    th3md5Init(p);
    th3dbMd5(p, 0, 
      "SELECT rowid, * FROM test1.sqlite_master;\n"
      "SELECT rowid, * FROM t1;\n"
    );
    th3strcpy(zSig1pre, th3md5Finish(p));
    th3md5Init(p);
    th3dbMd5(p, 0, 
      "SELECT rowid, * FROm test2.sqlite_master;\n"
      "SELECT rowid, * FROM t2;\n"
    );
    th3strcpy(zSig2pre, th3md5Finish(p));
    p->fsysTest.ssCntdown = nStep;
    p->fsysTest.ssOmitMask = TH3VFS_IOERR_PREDELETE;

    /* Begin populating the two database files as part of a transaction.
    ** A snapshot of the filesystem will be made at some point in the
    ** middle of this operation.
    */
    th3randomReseed(p, nStep);
    th3dbEval(p, 0,
      "BEGIN;\n"
      "INSERT INTO t1 VALUES(1, th3randomblob(50));\n"
      "INSERT INTO t1 SELECT x+1, th3randomblob(12) FROM t1;\n"
      "INSERT INTO t1 SELECT x+2, th3randomblob(72) FROM t1;\n"
      "INSERT INTO t1 SELECT x+4, th3randomblob(130) FROM t1;\n"
      "INSERT INTO t2 SELECT x, y FROM t1;\n"
      "COMMIT;\n"
    );

    /* If no snapshot occurred we can abort */
    if( p->fsysTest.ssCntdown>0 ){
      p->fsysTest.ssCntdown = 0;
      break;
    }

    /* Record a signature for after the transaction commits */
    th3md5Init(p);
    th3dbMd5(p, 0, 
      "SELECT rowid, * FROM test1.sqlite_master;\n"
      "SELECT rowid, * FROM t1;\n"
    );
    th3strcpy(zSig1post, th3md5Finish(p));
    th3md5Init(p);
    th3dbMd5(p, 0, 
      "SELECT rowid, * FROm test2.sqlite_master;\n"
      "SELECT rowid, * FROM t2;\n"
    );
    th3strcpy(zSig2post, th3md5Finish(p));

    /* Verify that we have the correct number of entires in tables,
    ** just to make sure the setup above does not contain hidden errors. */
    th3testBegin(p, th3format(p, "%d", nStep));
    th3dbEval(p, 0, 
       "SELECT count(*) FROM t1;"
       "SELECT count(*) FROM t2;"
       "SELECT count(*), sum(x) FROM t1 WHERE typeof(x)=='integer';"
    );
    th3testCheck(p, "8 8 8 36");

    /* Determine the number of crash scenarios to run based on how many
    ** unsynced pages and files there are in the snapshot
    */
    if( p->fsysSS.nNotSynced==0 ){
      nCrash = 1;
    }else{
      nCrash = p->fsysSS.nNotSynced*10;
    }

    /* Using the snapshot, simulate nCrash different crash scenarios */
    for(iCrash=1; iCrash<=nCrash; iCrash++){
      char *zSig;        /* Database checksum after recovery */
      int isComplete1;   /* True if first database completed */
      int isComplete2;   /* True if second database completed */

      /* Go back to the snapshot and simulate a crash at that point */
      th3filesystemRevert(p);
      th3filesystemCrash(p, iCrash);

      /* Verify that we can open the first database file and that it
      ** passes an integrity check. */
      th3testBegin(p, th3format(p, "%d.%d.1", nStep, iCrash));
      th3dbOpen(p, 0, "test1.db", 0);
      th3dbEval(p, 0, "PRAGMA integrity_check;");
      th3testCheck(p, "ok");

      /* Get the checksum over the first database.  The checksum must match
      ** the checksum before the start of the transaction or after the
      ** end of the transaction.
      */
      th3testBegin(p, th3format(p, "%d.%d.2", nStep, iCrash));
      th3md5Init(p);
      th3dbMd5(p, 0, 
        "SELECT rowid, * FROM sqlite_master;\n"
        "SELECT rowid, * FROM t1;\n"
      );
      zSig = th3md5Finish(p);
      isComplete1 = th3strcmp(zSig, zSig1post)==0;
      if( th3strcmp(zSig, zSig1pre)==0 || isComplete1 ){
        th3testOk(p);
      }else{
        th3testFailed(p, 0);
        th3print(p, th3format(p, " Expected: [%s] or [%s]\n",
                                 zSig1pre, zSig1post));
        th3print(p, th3format(p, "      Got: [%s]\n", zSig));
      }
      th3dbClose(p, 0);

      /* Verify that we can open the second database file and that it
      ** passes an integrity check. */
      th3testBegin(p, th3format(p, "%d.%d.3", nStep, iCrash));
      th3dbOpen(p, 0, "test2.db", 0);
      th3dbEval(p, 0, "PRAGMA integrity_check;");
      th3testCheck(p, "ok");

      /* Get the checksum over the second database.  The checksum must match
      ** the checksum before the start of the transaction or after the
      ** end of the transaction.
      */
      th3testBegin(p, th3format(p, "%d.%d.4", nStep, iCrash));
      th3md5Init(p);
      th3dbMd5(p, 0, 
        "SELECT rowid, * FROM sqlite_master;\n"
        "SELECT rowid, * FROM t2;\n"
      );
      zSig = th3md5Finish(p);
      isComplete2 = th3strcmp(zSig, zSig2post)==0;
      if( th3strcmp(zSig, zSig2pre)==0 || isComplete2 ){
        th3testOk(p);
      }else{
        th3testFailed(p, 0);
        th3print(p, th3format(p, " Expected: [%s] or [%s]\n",
                                 zSig2pre, zSig2post));
        th3print(p, th3format(p, "      Got: [%s]\n", zSig));
      }
      th3dbClose(p, 0);

      /* If the main DB is not :memory: and not in WAL mode, then isComplete1
      ** and isComplete2 must be tht same.
      */
      if( (p->config.maskProp & (TH3_JOURNAL_WAL|TH3_SINGLETHREAD))==0 ){
        th3testBegin(p, th3format(p, "%d.%d.5", nStep, iCrash));
        th3testCheckInt(p, isComplete1, isComplete2);
      }
    }
  }
  return 0;
}
