/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_foreignkeys16
** REQUIRED_PROPERTIES:       FOREIGN_KEYS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* This entire file contributes to the proof of the following:
** EV: R-33326-45252 The ON DELETE and ON UPDATE action associated with
** each foreign key in an SQLite database is one of "NO ACTION", "RESTRICT",
** "SET NULL", "SET DEFAULT" or "CASCADE".
*/

--testcase 100
/* EV: R-19971-54976 Configuring "NO ACTION" means just that: when a parent
** key is modified or deleted from the database, no special action is taken.
*/
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON DELETE NO ACTION
);
INSERT INTO t1 VALUES(1,null);
INSERT INTO t1 VALUES(2,1);
INSERT INTO t1 VALUES(3,1);
INSERT INTO t1 VALUES(4,2);
INSERT INTO t1 VALUES(5,2);
INSERT INTO t1 VALUES(6,3);
INSERT INTO t1 VALUES(7,3);
CREATE TABLE saved AS SELECT * FROM t1;
--result
--testcase 101
DELETE FROM t1 WHERE a=1;
--result SQLITE_CONSTRAINT {foreign key constraint failed}

--testcase 200
/* EV: R-19803-45884 If an action is not explicitly specified, it defaults
** to "NO ACTION".
*/
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 201
DELETE FROM t1 WHERE a=1;
--result SQLITE_CONSTRAINT {foreign key constraint failed}

--testcase 300
/* EV: R-04272-38653 The "RESTRICT" action means that the application
** is prohibited from deleting (for ON DELETE RESTRICT) or modifying
** (for ON UPDATE RESTRICT) a parent key when there exists one or more
** child keys mapped to it.
*/
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON DELETE RESTRICT
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 301
DELETE FROM t1 WHERE a=1;
--result SQLITE_CONSTRAINT {foreign key constraint failed}

--testcase 310
/* EV: R-37997-42187 The difference between the effect of a RESTRICT action
** and normal foreign key constraint enforcement is that the RESTRICT
** action processing happens as soon as the field is updated - not at the
** end of the current statement as it would with an immediate constraint,
** or at the end of the current transaction as it would with a deferred
** constraint.
**
** The ON DELETE RESTRICT prevents us from deleting the entire table
** because each row is checked for constraints as it is deleted, rather
** that waiting until the entire delete operation finishes.
**
** EV: R-14208-23986 If foreign key constraints are enabled when it
** is prepared, the DROP TABLE command performs an implicit DELETE to
** remove all rows from the table before dropping it.
**
** EV: R-11078-03945 The implicit DELETE does not cause any SQL triggers
** to fire, but may invoke foreign key actions or constraint violations.
**
** EV: R-32768-47925 If an immediate foreign key constraint is violated,
** the DROP TABLE statement fails and the table is not dropped.
*/
DROP TABLE t1;
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 311
UPDATE t1 SET b=NULL;
DROP TABLE t1;
--result

--testcase 320
/* EV: R-24179-60523 Even if the foreign key constraint it is attached to
** is deferred, configuring a RESTRICT action causes SQLite to return an
** error immediately if a parent key with dependent child keys is deleted
** or modified.
*/
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON DELETE RESTRICT DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 311
BEGIN;
DELETE FROM t1 WHERE a=1;
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 312
DROP TABLE t1;
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 313
UPDATE t1 SET b=NULL;
DROP TABLE t1;
COMMIT;
--result

--testcase 400
/* EV: R-03353-05327 If the configured action is "SET NULL", then when a
** parent key is deleted (for ON DELETE SET NULL) or modified (for ON
** UPDATE SET NULL), the child key columns of all rows in the child table
** that mapped to the parent key are set to contain SQL NULL values.
*/
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON DELETE SET NULL DEFAULT 7
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 401
DELETE FROM t1 WHERE a=2;
SELECT a, b FROM t1 ORDER BY a;
--result 1 nil 3 1 4 nil 5 nil 6 3 7 3

--testcase 500
/* EV: R-43054-54832 The "SET DEFAULT" actions are similar to "SET NULL",
** except that each of the child key columns is set to contain the columns
** default value instead of NULL.
*/
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON DELETE SET DEFAULT DEFAULT 7
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 501
DELETE FROM t1 WHERE a=2;
SELECT a, b FROM t1 ORDER BY a;
--result 1 nil 3 1 4 7 5 7 6 3 7 3

--testcase 600
/* EV: R-61376-57267 A "CASCADE" action propagates the delete or update
** operation on the parent key to each dependent child key.
**
** EV: R-61809-62207 For an "ON DELETE CASCADE" action, this means that
** each row in the child table that was associated with the deleted parent
** row is also deleted.
*/
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON DELETE CASCADE
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 601
DELETE FROM t1 WHERE a=2;
SELECT a, b FROM t1 ORDER BY a;
--result 1 nil 3 1 6 3 7 3

/***************************************************************************
** To finish the proof, repeat all of the above tests of ON UPDATE
**/


--testcase 1100
/* EV: R-19971-54976 Configuring "NO ACTION" means just that: when a parent
** key is modified or deleted from the database, no special action is taken.
*/
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON UPDATE NO ACTION
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 1101
UPDATE t1 SET a=100 WHERE a=1;
--result SQLITE_CONSTRAINT {foreign key constraint failed}

--testcase 1200
/* EV: R-19803-45884 If an action is not explicitly specified, it defaults
** to "NO ACTION".
*/
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 1201
UPDATE t1 SET a=100 WHERE a=1;
--result SQLITE_CONSTRAINT {foreign key constraint failed}

--testcase 1202
/* EV: R-37997-42187 */
UPDATE t1 SET a=a+100, b=b+100;
SELECT * FROM t1 ORDER BY a;
--result 101 nil 102 101 103 101 104 102 105 102 106 103 107 103


--testcase 1300
/* EV: R-04272-38653 The "RESTRICT" action means that the application
** is prohibited from deleting (for ON DELETE RESTRICT) or modifying
** (for ON UPDATE RESTRICT) a parent key when there exists one or more
** child keys mapped to it.
*/
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON UPDATE RESTRICT
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 1301
UPDATE t1 SET a=100 WHERE a=1;
--result SQLITE_CONSTRAINT {foreign key constraint failed}

--testcase 1310
/* EV: R-37997-42187 The difference between the effect of a RESTRICT action
** and normal foreign key constraint enforcement is that the RESTRICT
** action processing happens as soon as the field is updated - not at the
** end of the current statement as it would with an immediate constraint,
** or at the end of the current transaction as it would with a deferred
** constraint.
*/
UPDATE t1 SET a=a+100, b=b+100;
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 1311
UPDATE t1 SET b=NULL;
DROP TABLE t1;
--result

--testcase 1320
/* EV: R-24179-60523 Even if the foreign key constraint it is attached to
** is deferred, configuring a RESTRICT action causes SQLite to return an
** error immediately if a parent key with dependent child keys is deleted
** or modified.
*/
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 1311
BEGIN;
UPDATE t1 SET a=a+100, b=b+100;
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 1313
UPDATE t1 SET b=NULL;
DROP TABLE t1;
COMMIT;
--result

--testcase 1400
/* EV: R-03353-05327 If the configured action is "SET NULL", then when a
** parent key is deleted (for ON DELETE SET NULL) or modified (for ON
** UPDATE SET NULL), the child key columns of all rows in the child table
** that mapped to the parent key are set to contain SQL NULL values.
*/
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON UPDATE SET NULL DEFAULT 7
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 1401
UPDATE t1 SET a=a+100 WHERE a=2;
SELECT a, b FROM t1 ORDER BY a;
--result 1 nil 3 1 4 nil 5 nil 6 3 7 3 102 1

--testcase 1500
/* EV: R-43054-54832 The "SET DEFAULT" actions are similar to "SET NULL",
** except that each of the child key columns is set to contain the columns
** default value instead of NULL.
*/
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON UPDATE SET DEFAULT DEFAULT 7
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 1501
UPDATE t1 SET a=a+100 WHERE a=2;
SELECT a, b FROM t1 ORDER BY a;
--result 1 nil 3 1 4 7 5 7 6 3 7 3 102 1

--testcase 1600
/* EV: R-61376-57267 A "CASCADE" action propagates the delete or update
** operation on the parent key to each dependent child key.
**
** EV: R-13877-64542 For an "ON UPDATE CASCADE" action, it means that the
** values stored in each dependent child key are modified to match the new
** parent key values.
*/
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON UPDATE CASCADE
);
INSERT INTO t1 SELECT * FROM saved;
--result
--testcase 1601
UPDATE t1 SET a=a+100 WHERE a=2;
SELECT a, b FROM t1 ORDER BY a;
--result 1 nil 3 1 4 102 5 102 6 3 7 3 102 1
