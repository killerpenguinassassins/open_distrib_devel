/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_foreignkeys03
** REQUIRED_PROPERTIES:       FOREIGN_KEYS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* EVIDENCE-OF: R-32809-14620 The parent key must be a named column or
** columns in the parent table, not the rowid.
**
** EVIDENCE-OF: R-60116-07512 The parent key of a foreign key constraint
** is not allowed to use the rowid. The parent key must used named
** columns only.
*/
--testcase 50
CREATE TABLE t0(x INTEGER PRIMARY KEY, y, z, UNIQUE(y,z));
INSERT INTO t0 VALUES(99,100,101);
CREATE TABLE t50err(a, b REFERENCES t0(rowid));
--result
--testcase 51
INSERT INTO t50err VALUES(98,99);
--result SQLITE_ERROR {foreign key mismatch}
--testcase 52
DROP TABLE t50err;
CREATE TABLE t50err(a, b REFERENCES t0(x));
INSERT INTO t50err VALUES(98,99);
--result
--testcase 53
DROP TABLE t50err;
CREATE TABLE t50err(a, b, c, FOREIGN KEY(b,c) REFERENCES t0(y,rowid));
--result
--testcase 54
INSERT INTO t50err VALUES(98,100,99);
--result SQLITE_ERROR {foreign key mismatch}
--testcase 55
DROP TABLE t50err;
CREATE TABLE t50err(a, b, c, FOREIGN KEY(b,c) REFERENCES t0(y,z));
INSERT INTO t50err VALUES(98,100,101);
--result


/* EV: R-56032-24923 The foreign key constraint is satisfied if for each
** row in the child table either one or more of the child key columns
** are NULL, or there exists a row in the parent table for which each
** parent key column contains a value equal to the value in its associated
** child key column.
*/
--testcase 100
CREATE TABLE t1(a,b,c,d,x, PRIMARY KEY(a,b,c,d));
INSERT INTO t1 VALUES(1,2,3,4,'abc');
CREATE TABLE t2(a,b,c,d,x TEXT, FOREIGN KEY(d,c,b,a) REFERENCES t1);
INSERT INTO t2 VALUES(4,3,2,1,'0000');
INSERT INTO t2 VALUES(6,7,8,null,'0001');
INSERT INTO t2 VALUES(6,7,null,9,'0010');
INSERT INTO t2 VALUES(6,7,null,null,'0011');
INSERT INTO t2 VALUES(6,null,8,9,'0100');
INSERT INTO t2 VALUES(6,null,8,null,'0101');
INSERT INTO t2 VALUES(6,null,null,9,'0110');
INSERT INTO t2 VALUES(6,null,null,null,'0111');
INSERT INTO t2 VALUES(null,7,8,9,'1000');
INSERT INTO t2 VALUES(null,7,8,null,'1001');
INSERT INTO t2 VALUES(null,7,null,9,'1010');
INSERT INTO t2 VALUES(null,7,null,null,'1011');
INSERT INTO t2 VALUES(null,null,8,9,'1100');
INSERT INTO t2 VALUES(null,null,8,null,'1101');
INSERT INTO t2 VALUES(null,null,null,9,'1110');
INSERT INTO t2 VALUES(null,null,null,null,'1111');
SELECT count(*) FROM t2;
--result 16

--testcase 110
INSERT INTO t2 VALUES(4,3,2,9,'x0001');
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 111
INSERT INTO t2 VALUES(4,3,9,1,'x0010');
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 112
INSERT INTO t2 VALUES(4,9,2,1,'x0100');
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 113
INSERT INTO t2 VALUES(9,3,2,1,'x1000');
--result SQLITE_CONSTRAINT {foreign key constraint failed}

#ifndef SQLITE_OMIT_FLOATING_POINT
/* EV: R-57765-12380 In the above paragraph, the term "equal" means equal
** when values are compared using the rules specified here.
*/
--new test.db
--testcase 200
CREATE TABLE t1(a BLOB PRIMARY KEY);
INSERT INTO t1 VALUES(1);
INSERT INTO t1 VALUES('23');
INSERT INTO t1 VALUES(4.0);
CREATE TABLE t2(x INTEGER PRIMARY KEY, y BLOB REFERENCES t1);
--result

--testcase 210
INSERT INTO t2 VALUES(1, '1');
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 211
INSERT INTO t2 VALUES(2, 23);
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 212
INSERT INTO t2 VALUES(3, 1);
--result
--testcase 213
INSERT INTO t2 VALUES(4, '23');
--result
--testcase 214
INSERT INTO t2 VALUES(5, 1.0);
INSERT INTO t2 VALUES(6, 4);
INSERT INTO t2 VALUES(7, 4.0);
SELECT y FROM t2 ORDER BY x;
--result 1 23 1.0 4 4.0
#endif /* SQLITE_OMIT_FLOATING_POINT */

/* EV: R-15796-47513 When comparing text values, the collating sequence
** associated with the parent key column is always used. 
*/
--new test.db
--testcase 300
CREATE TABLE t1(a TEXT PRIMARY KEY COLLATE nocase);
INSERT INTO t1 VALUES('abc');
INSERT INTO t1 VALUES('DEF');
--result
--testcase 301
INSERT INTO t1 VALUES('ABC');
--result SQLITE_CONSTRAINT {column a is not unique}

--testcase 310
CREATE TABLE t2(b TEXT COLLATE rtrim REFERENCES t1);
INSERT INTO t2 VALUES('ABC');
--result
--testcase 311
INSERT INTO t2 VALUES('abc   ');
--result SQLITE_CONSTRAINT {foreign key constraint failed}

/* EV: R-04240-13860 When comparing values, if the parent key column has
** an affinity, then that affinity is applied to the child key value before
** the comparison is performed. 
*/
--new test.db
--testcase 400
CREATE TABLE t1(a TEXT PRIMARY KEY);
INSERT INTO t1 VALUES('1');
INSERT INTO t1 VALUES('abc');
CREATE TABLE t2(b INTEGER REFERENCES t1);
INSERT INTO t2 VALUES(1);
--result
--testcase 401
SELECT typeof(a), typeof(b) FROM t1, t2;
--result text integer text integer

--testcase 410
CREATE TABLE t3(a INT PRIMARY KEY);
INSERT INTO t3 VALUES('1');
INSERT INTO t3 VALUES('abc');
SELECT typeof(a) FROM t3 ORDER BY a;
--result integer text
--testcase 411
CREATE TABLE t4(b TEXT REFERENCES t3);
INSERT INTO t4 VALUES(1);
SELECT typeof(b) FROM t4;
--result text
