/*
** This module contains tests for the the sqilte3_create_function family of
** routines.
**
** MODULE_NAME:               req1_createfunc03
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/

/* Scalar functions that return a text string that identifies
** themselves and the number of arguments with which they were called.
*/
static void req1_createfunc03_x1(
  sqlite3_context *pContext,
  int nArg,
  sqlite3_value **apArg
){
  int sum = 0;
  int i;
  char zResult[100];
  for(i=0; i<nArg; i++){
    sum += sqlite3_value_int(apArg[i]);
  }
  sqlite3_snprintf(sizeof(zResult), zResult, "x1-%d-%d", nArg, sum);
  sqlite3_result_text(pContext, zResult, -1, SQLITE_TRANSIENT);
}

/* Aggregate functions that record their name and the number of
** arguments.
*/
static void req1_createfunc03_a1(
  sqlite3_context *pContext,
  int nArg,
  sqlite3_value **apArg
){
  char *zMsg = sqlite3_aggregate_context(pContext, 300);
  int sum = 0;
  int i;
  for(i=0; i<nArg; i++){
    sum += sqlite3_value_int(apArg[i]);
  }
  if( zMsg ){
    if( zMsg[0]==0 ){
      sqlite3_snprintf(300, zMsg, "a1-%d-%d-", nArg, sum);
    }else{
      i = strlen(zMsg);
      sqlite3_snprintf(300-i, &zMsg[i], "%d-", sum);
    }
  }
}
static void req1_createfunc03_f1(sqlite3_context *pContext){
  char *zMsg = sqlite3_aggregate_context(pContext, 300);
  if( zMsg ){
    int n = strlen(zMsg);
    sqlite3_snprintf(30-n, &zMsg[n], "f1");
    sqlite3_result_text(pContext, zMsg, -1, SQLITE_TRANSIENT);
  }
}


int req1_createfunc03(th3state *p){
  sqlite3 *db;
  int rc;

  /* EVIDENCE-OF: R-56801-63935 The sixth, seventh and eighth parameters,
  ** xFunc, xStep and xFinal, are pointers to C-language functions that
  ** implement the SQL function or aggregate.
  **
  ** EVIDENCE-OF: R-27228-35217 A scalar SQL function requires an
  ** implementation of the xFunc callback only; NULL pointers must be
  ** passed as the xStep and xFinal parameters.
  */
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  rc = sqlite3_create_function(db, "x100", 1, SQLITE_ANY, 0,
                               req1_createfunc03_x1, 0, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "101");
  th3dbEval(p, 0, "SELECT x100(123);");
  th3testCheck(p, "x1-1-123");

  th3testBegin(p, "110");
  rc = sqlite3_create_function(db, "x110", 1, SQLITE_ANY, 0,
                               req1_createfunc03_x1, req1_createfunc03_a1, 0);
  th3testCheckTrue(p, rc!=SQLITE_OK);
  th3testBegin(p, "111");
  th3dbEval(p, 0, "SELECT x110(123);");
  th3testCheck(p, "SQLITE_ERROR {no such function: x110}");

  th3testBegin(p, "120");
  rc = sqlite3_create_function(db, "x120", 1, SQLITE_ANY, 0,
             req1_createfunc03_x1, req1_createfunc03_a1, req1_createfunc03_f1);
  th3testCheckTrue(p, rc!=SQLITE_OK);
  th3testBegin(p, "121");
  th3dbEval(p, 0, "SELECT x120(123);");
  th3testCheck(p, "SQLITE_ERROR {no such function: x120}");

  th3testBegin(p, "130");
  rc = sqlite3_create_function(db, "x130", 1, SQLITE_ANY, 0,
             req1_createfunc03_x1, 0, req1_createfunc03_f1);
  th3testCheckTrue(p, rc!=SQLITE_OK);
  th3testBegin(p, "131");
  th3dbEval(p, 0, "SELECT x130(123);");
  th3testCheck(p, "SQLITE_ERROR {no such function: x130}");

  /* EVIDENCE-OF: R-17719-44813 An aggregate SQL function requires an
  ** implementation of xStep and xFinal and NULL pointer must be passed for
  ** xFunc.
  */
  th3testBegin(p, "200");
  th3dbEval(p, 0,
    "CREATE TABLE t1(x);"
    "INSERT INTO t1 VALUES(1);"
    "INSERT INTO t1 VALUES(2);"
    "INSERT INTO t1 VALUES(3);"
  );
  rc = sqlite3_create_function(db, "x200", 1, SQLITE_ANY, 0,
              0, req1_createfunc03_a1, req1_createfunc03_f1);
  th3testCheckTrue(p, rc==SQLITE_OK);
  th3testBegin(p, "131");
  th3dbEval(p, 0, "SELECT x200(x) FROM t1;");
  th3testCheck(p, "a1-1-1-2-3-f1");

  th3testBegin(p, "210");
  rc = sqlite3_create_function(db, "x210", 1, SQLITE_ANY, 0,
              0, req1_createfunc03_a1, 0);
  th3testCheckTrue(p, rc!=SQLITE_OK);
  th3testBegin(p, "211");
  th3dbEval(p, 0, "SELECT x210(x) FROM t1;");
  th3testCheck(p, "SQLITE_ERROR {no such function: x210}");

  th3testBegin(p, "220");
  rc = sqlite3_create_function(db, "x220", 1, SQLITE_ANY, 0,
              0, 0, req1_createfunc03_f1);
  th3testCheckTrue(p, rc!=SQLITE_OK);
  th3testBegin(p, "221");
  th3dbEval(p, 0, "SELECT x220(x) FROM t1;");
  th3testCheck(p, "SQLITE_ERROR {no such function: x220}");

  /* EVIDENCE-OF: R-46513-12418 To delete an existing SQL function or
  ** aggregate, pass NULL pointers for all three function callbacks.
  */
  th3testBegin(p, "300");
  th3dbEval(p, 0, "SELECT x100(99)");
  rc = sqlite3_create_function(db, "x100", 1, SQLITE_ANY, 0,
              0, 0, 0);
  th3testAppendResultTerm(p, th3errorCodeName(rc));
  th3dbEval(p, 0, "SELECT x100(99)");
  th3testCheck(p, "x1-1-99 SQLITE_OK SQLITE_ERROR {no such function: x100}");

  th3testBegin(p, "310");
  th3dbEval(p, 0, "SELECT x200(x+1) FROM t1");
  rc = sqlite3_create_function(db, "x200", 1, SQLITE_ANY, 0,
              0, 0, 0);
  th3testAppendResultTerm(p, th3errorCodeName(rc));
  th3dbEval(p, 0, "SELECT x200(x+1) FROM t1");
  th3testCheck(p,
      "a1-1-2-3-4-f1 SQLITE_OK SQLITE_ERROR {no such function: x200}");

  return 0;
}
