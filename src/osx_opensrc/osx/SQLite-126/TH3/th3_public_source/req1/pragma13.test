/*
** This module contains tests for various pragmas:
**
**    PRAGMA collation_list
**    PRAGMA compile_options;
**
** SCRIPT_MODULE_NAME:        req1_pragma13
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
/* EVIDENCE-OF: R-08469-46782 PRAGMA collation_list; Return a list of the
** collating sequences defined for the current database connection.
*/
--testcase 100
PRAGMA collation_list;
--glob *NOCASE*
--glob *BINARY*
--glob *RTRIM*
--notglob *th3_alleq*
--notglob *th3_binary*

--testcase 110
SELECT th3_load_normal_extension();
PRAGMA collation_list;
--glob *NOCASE*
--glob *BINARY*
--glob *RTRIM*
--glob *th3_alleq*
--glob *th3_binary16le*
--glob *th3_binary16be*

/* EVIDENCE-OF: R-40541-16535 This pragma returns the names of
** compile-time options used when building SQLite, one option per row.
**
** EVIDENCE-OF: R-58101-01610 The "SQLITE_" prefix is omitted from the
** returned option names.
*/
--testcase 200
PRAGMA compile_options;
--notglob *SQLITE_*
#ifdef SQLITE_DEBUG
--glob *DEBUG*
#endif
#ifdef SQLITE_ENABLE_EXPENSIVE_ASSERT
--glob *ENABLE_EXPENSIVE_ASSERT*
#endif
#ifdef SQLITE_NO_SYNC
--glob *NO_SYNC*
#endif
#ifdef SQLITE_SECURE_DELETE
--glob *SECURE_DELETE*
#endif
#ifdef SQLITE_ENABLE_STAT2
--glob *ENABLE_STAT2*
#endif
#ifdef SQLITE_ENABLE_COLUMN_METADATA
--glob *ENABLE_COLUMN_METADATA*
#endif
#ifdef SQLITE_ENABLE_UPDATE_DELETE_LIMIT
--glob *ENABLE_UPDATE_DELETE_LIMIT*
#endif
#ifdef SQLITE_ENABLE_MEMORY_MANAGEMENT
--glob *ENABLE_MEMORY_MANAGEMENT*
#endif
#ifdef SQLITE_OMIT_DEPRECATED
--glob *OMIT_DEPRECATED*
#endif
#ifdef SQLITE_ENABLE_MEMSYS5
--glob *ENABLE_MEMSYS5*
#endif
--glob *TEMP_STORE=[0123]*
--glob *THREADSAFE=[012]*
