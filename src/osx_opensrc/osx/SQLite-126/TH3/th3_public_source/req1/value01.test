/*
** This module contains tests for the the sqilte3_value object.
**
** MODULE_NAME:               req1_value01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

static void req1_value01_func(
  sqlite3_context *pContext,
  int nArg,
  sqlite3_value **apArg
){
  sqlite3 *db = sqlite3_context_db_handle(pContext);
  th3state *p = (th3state*)sqlite3_user_data(pContext);
  sqlite3 *db2 = th3dbPointer(p, 0);
  int nTest = *(int*)p->pTestData;
  sqlite3_mutex *pMutex;

  /* EVIDENCE-OF: R-46798-50301 The sqlite3_context_db_handle() interface
  ** returns a copy of the pointer to the database connection (the 1st
  ** parameter) of the sqlite3_create_function() and
  ** sqlite3_create_function16() routines that originally registered the
  ** application defined function.
  */
  th3testBegin(p, th3format(p, "%d.1", nTest));
  th3testCheckTrue(p, db==db2);



  /* EVIDENCE-OF: R-51717-16874 The sqlite3_value objects that are passed
  ** as parameters into the implementation of application-defined SQL
  ** functions are protected.  
  */
#if defined(SQLITE_DEBUG) && !defined(NDEBUG)
  th3require(p, "value01.serialized.1");
  th3testBegin(p, th3format(p, "%d.2", nTest));
  pMutex = sqlite3_db_mutex(db);
  if( pMutex ) th3coverage(p, "value01.serialized.1");
  th3testCheckTrue(p, sqlite3_mutex_held(pMutex));
#endif
  
  sqlite3_result_int(pContext, sqlite3_value_type(apArg[0]));
}

int req1_value01(th3state *p){
  int nTest;
  sqlite3 *db;
  sqlite3_stmt *pStmt;
  sqlite3_value *pValue;
  sqlite3_mutex *pMutex;
  unsigned short int utf16[10];

  p->pTestData = (void*)&nTest;
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  sqlite3_create_function(db, "f1", -1, SQLITE_ANY, p, req1_value01_func, 0,0);
  th3_ascii_to_utf16("f2",utf16);
  sqlite3_create_function16(db, utf16,-1,SQLITE_ANY, p, req1_value01_func,0,0);

  /* EVIDENCE-OF: R-34814-22035 Values stored in sqlite3_value objects can
  ** be integers, floating point values, strings, BLOBs, or NULL.
  */
  pStmt = th3dbPrepare(p, 0, "SELECT f1(null)");
  nTest = 100;
  sqlite3_step(pStmt);
  th3testBegin(p, "101");
  th3testCheckInt(p, SQLITE_NULL, sqlite3_column_int(pStmt, 0));
  sqlite3_finalize(pStmt);

  pStmt = th3dbPrepare(p, 0, "SELECT f2(null)");
  nTest = 110;
  sqlite3_step(pStmt);
  th3testBegin(p, "111");
  th3testCheckInt(p, SQLITE_NULL, sqlite3_column_int(pStmt, 0));
  sqlite3_finalize(pStmt);

  pStmt = th3dbPrepare(p, 0, "SELECT f1(1234)");
  nTest = 120;
  sqlite3_step(pStmt);
  th3testBegin(p, "121");
  th3testCheckInt(p, SQLITE_INTEGER, sqlite3_column_int(pStmt, 0));
  sqlite3_finalize(pStmt);

  pStmt = th3dbPrepare(p, 0, "SELECT f2(1234)");
  nTest = 130;
  sqlite3_step(pStmt);
  th3testBegin(p, "131");
  th3testCheckInt(p, SQLITE_INTEGER, sqlite3_column_int(pStmt, 0));
  sqlite3_finalize(pStmt);

#ifndef SQLITE_OMIT_FLOATING_POINT
  pStmt = th3dbPrepare(p, 0, "SELECT f1(12.34)");
  nTest = 140;
  sqlite3_step(pStmt);
  th3testBegin(p, "141");
  th3testCheckInt(p, SQLITE_FLOAT, sqlite3_column_int(pStmt, 0));
  sqlite3_finalize(pStmt);

  pStmt = th3dbPrepare(p, 0, "SELECT f2(12.34)");
  nTest = 150;
  sqlite3_step(pStmt);
  th3testBegin(p, "151");
  th3testCheckInt(p, SQLITE_FLOAT, sqlite3_column_int(pStmt, 0));
  sqlite3_finalize(pStmt);
#endif

  pStmt = th3dbPrepare(p, 0, "SELECT f1('hello')");
  nTest = 160;
  sqlite3_step(pStmt);
  th3testBegin(p, "161");
  th3testCheckInt(p, SQLITE_TEXT, sqlite3_column_int(pStmt, 0));
  sqlite3_finalize(pStmt);

  pStmt = th3dbPrepare(p, 0, "SELECT f2('hello')");
  nTest = 170;
  sqlite3_step(pStmt);
  th3testBegin(p, "171");
  th3testCheckInt(p, SQLITE_TEXT, sqlite3_column_int(pStmt, 0));
  sqlite3_finalize(pStmt);

  pStmt = th3dbPrepare(p, 0, "SELECT f1(x'01020304')");
  nTest = 180;
  sqlite3_step(pStmt);
  th3testBegin(p, "181");
  th3testCheckInt(p, SQLITE_BLOB, sqlite3_column_int(pStmt, 0));
  sqlite3_finalize(pStmt);

  pStmt = th3dbPrepare(p, 0, "SELECT f2(X'050607')");
  nTest = 190;
  sqlite3_step(pStmt);
  th3testBegin(p, "191");
  th3testCheckInt(p, SQLITE_BLOB, sqlite3_column_int(pStmt, 0));

  /* EVIDENCE-OF: R-39088-19376 The sqlite3_value object returned by
  ** sqlite3_column_value() is unprotected.
  */
  th3testBegin(p, "200");
  pValue = sqlite3_column_value(pStmt, 0);
  th3testCheckTrue(p, pValue!=0);
#if defined(SQLITE_DEBUG) && !defined(NDEBUG)
  th3require(p, "value01.serialized.2");
  th3testBegin(p, "201");
  pMutex = sqlite3_db_mutex(db);
  if( pMutex ) th3coverage(p, "value01.serialized.2");
  th3testCheckTrue(p, sqlite3_mutex_notheld(pMutex));
#endif
  sqlite3_finalize(pStmt);

  return 0;
}
