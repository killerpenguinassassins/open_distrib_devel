/*
** This module contains tests for autoincrement.
**
** SCRIPT_MODULE_NAME:        req1_autoinc01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* EVIDENCE-OF: R-33323-30666 In SQLite, every row of every table has an
** 64-bit signed integer ROWID.
**
** How does one show that *every* table has a rowid?  But we can at least
** show that the rowid is a 64-bit integer.
*/
--testcase 10
CREATE TABLE t0(x);
INSERT INTO t0(rowid,x) VALUES(-9223372036854775808, 1);
INSERT INTO t0(rowid,x) VALUES(0,2);
INSERT INTO t0(rowid,x) VALUES(9223372036854775807,3);
--result
--testcase 11
SELECT oid FROM t0;
--result -9223372036854775808 0 9223372036854775807

/* EVIDENCE-OF: R-53159-40138 The ROWID for each row is unique among all
** rows in the same table.
**
** Demonstrate this by showing a uniqueness violation on a duplicate rowid.
*/
--testcase 50
INSERT INTO t0(rowid,x) VALUES(0,4);
--result SQLITE_CONSTRAINT {PRIMARY KEY must be unique}
--testcase 51
SELECT count(*) FROM t0;
--result 3

/* EVIDENCE-OF: R-44911-55124 You can access the ROWID of an SQLite table
** using one the special column names ROWID, _ROWID_, or OID. Except if
** you declare an ordinary table column to use one of those special
** names, then the use of that name will refer to the declared column not
** to the internal ROWID.
*/
--testcase 100
CREATE TABLE t1(rowid);
INSERT INTO t1(oid,rowid) VALUES(10,20);
SELECT rowid,_rowid_,oid FROM t1
--result 20 10 10
--testcase 101
SELECT ROWID,_ROWID_,OID FROM t1
--result 20 10 10
--testcase 110
CREATE TABLE t2(_rowid_);
INSERT INTO t2(oid,_rowid_) VALUES(10,20);
SELECT rowid,_rowid_,oid FROM t2
--result 10 20 10
--testcase 111
SELECT ROWID,_ROWID_,OID FROM t2
--result 10 20 10
--testcase 120
CREATE TABLE t3(oid);
INSERT INTO t3(oid,_rowid_) VALUES(10,20);
SELECT rowid,_rowid_,oid FROM t3
--result 20 20 10
--testcase 121
SELECT ROWID,_ROWID_,OID FROM t3
--result 20 20 10

/* EVIDENCE-OF: R-38485-20010 If a table contains a column of type
** INTEGER PRIMARY KEY, then that column becomes an alias for the ROWID.
*/
--testcase 200
CREATE TABLE t4(a,b,c INTEGER PRIMARY KEY, _rowid_, oid);
INSERT INTO t4 VALUES(1,2,3,4,5);
SELECT c,rowid,_rowid_,oid FROM t4;
--result 3 3 4 5

/* EVIDENCE-OF: R-05038-25064 You can then access the ROWID using any of
** four different names, the original three names described above or the
** name given to the INTEGER PRIMARY KEY column.
*/
--testcase 300
CREATE TABLE t5(a,b,c INTEGER PRIMARY KEY, d, e);
INSERT INTO t5 VALUES(1,2,3,4,5);
INSERT INTO t5 VALUES(6,7,8,9,10);
SELECT c,rowid,_rowid_,oid FROM t5 ORDER BY c;
--result 3 3 3 3 8 8 8 8

/* EVIDENCE-OF: R-37283-61388 All these names are aliases for one another
** and work equally well in any context.
*/
--testcase 310
SELECT c,rowid,_rowid_,oid FROM t5 ORDER BY rowid;
--result 3 3 3 3 8 8 8 8
--testcase 311
SELECT c,rowid,_rowid_,oid FROM t5 ORDER BY _rowid_;
--result 3 3 3 3 8 8 8 8
--testcase 312
SELECT c,rowid,_rowid_,oid FROM t5 ORDER BY oid;
--result 3 3 3 3 8 8 8 8
--testcase 320
SELECT c FROM t5 WHERE b=_rowid_-1 AND d=oid+1 AND e=rowid+2 ORDER BY oid DESC;
--result 8 3

/* EVIDENCE-OF: R-54260-17937 When a new row is inserted into an SQLite
** table, the ROWID can either be specified as part of the INSERT
** statement or it can be assigned automatically by the database engine.
*/
--testcase 400
INSERT INTO t5(a,b,d,e) VALUES(11,22,33,44);
SELECT c, rowid, _rowid_, oid FROM t5 WHERE a==11;
--result 9 9 9 9
--testcase 401
INSERT INTO t5(a,b,rowid,d,e) VALUES(22,33,44,55,66);
SELECT c, rowid, _rowid_, oid FROM t5 WHERE a==22;
--result 44 44 44 44
--testcase 402
INSERT INTO t5(a,b,d,e,oid) VALUES(33,44,55,66,77);
SELECT c, rowid, _rowid_, oid FROM t5 WHERE a==33;
--result 77 77 77 77

/* EVIDENCE-OF: R-51090-01319 To specify a ROWID manually, just include
** it in the list of values to be inserted. For example: CREATE TABLE
** test1(a INT, b TEXT); INSERT INTO test1(rowid, a, b) VALUES(123, 5,
** 'hello');
*/
--testcase 500
 CREATE TABLE
 test1(a INT, b TEXT); INSERT INTO test1(rowid, a, b) VALUES(123, 5,
 'hello');

SELECT rowid, oid, _rowid_, * FROM test1;
--result 123 123 123 5 hello

/* EVIDENCE-OF: R-12104-35971 If no ROWID is specified on the insert, or
** if the specified ROWID has a value of NULL, then an appropriate ROWID
** is created automatically.
**
** EVIDENCE-OF: R-29538-34987 The usual algorithm is to give the newly
** created row a ROWID that is one larger than the largest ROWID in the
** table prior to the insert.
**
** ANALYSIS-OF: R-60470-29837 The normal ROWID selection algorithm
** described above will generate monotonically increasing unique ROWIDs
** as long as you never use the maximum ROWID value and you never delete
** the entry in the table with the largest ROWID.
**
** R-60470 follows logically from R-29538.
*/
--testcase 510
INSERT INTO test1(a,b) VALUES(6,'hi');
SELECT rowid, * FROM test1 ORDER BY rowid;
--result 123 5 hello 124 6 hi
--testcase 520
INSERT INTO test1(rowid,a,b) VALUES(NULL,7,'howdy');
SELECT rowid, * FROM test1 ORDER BY rowid;
--result 123 5 hello 124 6 hi 125 7 howdy

/* EVIDENCE-OF: R-61914-48074 If the table is initially empty, then a
** ROWID of 1 is used.
*/
--testcase 530
DELETE FROM test1;
INSERT INTO test1(a,b) VALUES(8,'xyzzy');
SELECT rowid, * FROM test1;
--result 1 8 xyzzy

/* EVIDENCE-OF: R-07677-41881 If the largest ROWID is equal to the
** largest possible integer (9223372036854775807) then the database
** engine starts picking positive candidate ROWIDs at random until it
** finds one that is not previously used.
*/
--testcase 540
INSERT INTO test1(rowid,a,b) VALUES(9223372036854775807,9,'abc');
INSERT INTO test1(a,b) VALUES(10,'def');
INSERT INTO test1(rowid,a,b) VALUES(NULL,11,'ghi');
SELECT a, rowid<9223372036854775807 and rowid>0 FROM test1 ORDER BY a;
--result 8 1 9 0 10 1 11 1

/* EVIDENCE-OF: R-03793-19254 If you ever delete rows or if you ever
** create a row with the maximum possible ROWID, then ROWIDs from
** previously deleted rows might be reused when creating new rows and
** newly created ROWIDs might not be in strictly ascending order.
*/
--testcase 600
CREATE TABLE t6(a INTEGER PRIMARY KEY, b);
INSERT INTO t6 VALUES(NULL,1);
INSERT INTO t6 VALUES(NULL,2);
INSERT INTO t6 VALUES(NULL,3);
SELECT * FROM t6 ORDER BY a;
--result 1 1 2 2 3 3
--testcase 601
DELETE FROM t6 WHERE a=3;
INSERT INTO t6 VALUES(NULL,4);
SELECT * FROM t6 ORDER BY a;
--result 1 1 2 2 3 4

/* EVIDENCE-OF: R-43328-56724 If a column has the type INTEGER PRIMARY
** KEY AUTOINCREMENT then a slightly different ROWID selection algorithm
** is used.
**
** EVIDENCE-OF: R-18968-56672 The ROWID chosen for the new row is at
** least one larger than the largest ROWID that has ever before existed
** in that same table.
**
** EVIDENCE-OF: R-15283-49521 If the table has never before contained any
** data, then a ROWID of 1 is used.
*/
--testcase 700
CREATE TABLE t7(a INTEGER PRIMARY KEY AUTOINCREMENT, b);
INSERT INTO t7 VALUES(NULL,1);
INSERT INTO t7 VALUES(NULL,2);
INSERT INTO t7 VALUES(NULL,3);
SELECT * FROM t7 ORDER BY a;
--result 1 1 2 2 3 3
--testcase 701
DELETE FROM t7 WHERE a=3;
INSERT INTO t7 VALUES(NULL,4);
SELECT * FROM t7 ORDER BY a;
--result 1 1 2 2 4 4
--testcase 702
DELETE FROM t7;
INSERT INTO t7 VALUES(NULL,5);
SELECT * FROM t7;
--result 5 5
--testcase 703
DROP TABLE t7;
CREATE TABLE t7(a INTEGER PRIMARY KEY AUTOINCREMENT, b);
INSERT INTO t7 VALUES(NULL,6);
SELECT * FROM t7;
--result 1 6

/* EVIDENCE-OF: R-00175-32013 SQLite keeps track of the largest ROWID
** that a table has ever held using the special SQLITE_SEQUENCE table.
*/
--testcase 800
CREATE TABLE t8(a INTEGER PRIMARY KEY AUTOINCREMENT, b);
SELECT * FROM sqlite_sequence WHERE name='t8';
--result
--testcase 801
INSERT INTO t8 VALUES(123,456);
SELECT name, seq, typeof(seq) FROM sqlite_sequence WHERE name='t8';
--result t8 123 integer
--testcase 802
INSERT INTO t8 VALUES(234,567);
DELETE FROM t8 WHERE b>500;
SELECT name, seq FROM sqlite_sequence WHERE name='t8';
--result t8 234

/* EVIDENCE-OF: R-13557-65369 The SQLITE_SEQUENCE table is created and
** initialized automatically whenever a normal table that contains an
** AUTOINCREMENT column is created.
*/
--new test.db
--testcase 900
SELECT name FROM sqlite_master WHERE name='sqlite_sequence';
--result
--testcase 901
CREATE TABLE t9(a, b INTEGER PRIMARY KEY AUTOINCREMENT);
SELECT name FROM sqlite_master WHERE name='sqlite_sequence';
--result sqlite_sequence
--testcase 902
SELECT * FROM sqlite_sequence;
--result
--testcase 903
INSERT INTO t9 VALUES(1,2);
SELECT * FROM sqlite_sequence;
--result t9 2
--testcase 904
ALTER TABLE t9 RENAME TO t9b;
SELECT * FROM sqlite_sequence;
--result t9b 2
--testcase 905
VACUUM;
SELECT * FROM sqlite_sequence;
--result t9b 2

/* EVIDENCE-OF: R-31429-26731 The content of the SQLITE_SEQUENCE table
** can be modified using ordinary UPDATE, INSERT, and DELETE statements.
**
** EVIDENCE-OF: R-26490-64257 But making modifications to this table will
** likely perturb the AUTOINCREMENT key generation algorithm.
*/
--testcase 1000
UPDATE sqlite_sequence SET seq=100 WHERE name='t9b';
SELECT * FROM sqlite_sequence;
--result t9b 100
--testcase 1001
INSERT INTO t9b VALUES(2,NULL);
SELECT b FROM t9b WHERE a=2;
--result 101
--testcase 1002
DELETE FROM t9b;
DELETE FROM sqlite_sequence;
SELECT * FROM sqlite_sequence;
--result
--testcase 1003
INSERT INTO t9b VALUES(3,NULL);
SELECT b FROM t9b WHERE a=3;
--result 1
--testcase 1004
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('t9b',200);
INSERT INTO sqlite_sequence VALUES('txyz',300);
INSERT INTO t9b VALUES(4,NULL);
SELECT b FROM t9b WHERE a=4;
--result 201

/* EVIDENCE-OF: R-12275-61338 If the table has previously held a row with
** the largest possible ROWID, then new INSERTs are not allowed and any
** attempt to insert a new row will fail with an SQLITE_FULL error.
*/
--testcase 1050
INSERT INTO t9b VALUES(99,9223372036854775807);
--result
--testcase 1051
INSERT INTO t9b VALUES(100,NULL);
--result SQLITE_FULL {database or disk is full}
--testcase 1052
DELETE FROM t9b;
INSERT INTO t9b VALUES(100,NULL);
--result SQLITE_FULL {database or disk is full}


/* ANALYSIS-OF: R-57773-31134 With AUTOINCREMENT, rows with automatically
** selected ROWIDs are guaranteed to have ROWIDs that have never been
** used before by the same table in the same database.
**
** ANALYSIS-OF: R-47208-15314 And the automatically generated ROWIDs are
** guaranteed to be monotonically increasing.
** ANALYSIS-OF: R-50658-17415 One is the usual increment.
**
** R-57773, R-47208, and R-50658 are a logical consequence of R-18968 and
** are demonstrated by test cases above.
*/

/* EVIDENCE-OF: R-56805-28429 Note that "monotonically increasing" does
** not imply that the ROWID always increases by exactly one.
**
** EVIDENCE-OF: R-15078-25200 However, if an insert fails due to (for
** example) a uniqueness constraint, the ROWID of the failed insertion
** attempt might not be reused on subsequent inserts, resulting in gaps
** in the ROWID sequence.
**
** EVIDENCE-OF: R-24433-42760 AUTOINCREMENT guarantees that automatically
** chosen ROWIDs will be increasing but not that they will be sequential.
*/
--testcase 1100
CREATE TABLE t11(a UNIQUE, b INTEGER PRIMARY KEY autoincrement, c);
INSERT INTO t11 VALUES(1,2,3);
INSERT INTO t11 VALUES(4,5,6);
--result
--testcase 1101
INSERT OR IGNORE INTO t11 SELECT 2,3,4 UNION ALL SELECT 1,NULL,9;
--result
--testcase 1102
INSERT INTO t11 VALUES(99,NULL,99);
SELECT b FROM t11 WHERE a=99;
--result 7
