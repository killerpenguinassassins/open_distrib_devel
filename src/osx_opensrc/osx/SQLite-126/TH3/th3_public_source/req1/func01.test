/*
** This module contains tests for lang_func.html
**
** SCRIPT_MODULE_NAME:        req1_func01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* EVIDENCE-OF: R-58361-29089 The changes() function returns the number
** of database rows that were changed or inserted or deleted by the most
** recently completed INSERT, DELETE, or UPDATE statement, exclusive of
** statements in lower-level triggers.
*/
--testcase 100
CREATE TABLE t1(x);
SELECT changes();
--result 0
--testcase 101
INSERT INTO t1 VALUES(123);
SELECT changes();
--result 1
--testcase 102
INSERT INTO t1 VALUES(234);
INSERT INTO t1 VALUES(345);
INSERT INTO t1 SELECT x+1000 FROM t1;
SELECT changes();
--result 3
--testcase 104
UPDATE t1 SET x=x+10 WHERE (x%10)==4;
SELECT changes();
--result 2
--testcase 105
DELETE FROM t1 WHERE x>1000;
SELECT changes();
--result 3


--testcase 110
CREATE TABLE t2(y, z);
SELECT changes();
--result 3
--testcase 111
DELETE FROM t2;
SELECT changes();
--result 0
--testcase 112
INSERT INTO t2 SELECT changes(), x FROM t1;
SELECT * FROM t2 ORDER BY z;
--result 0 123 0 244 0 345
--testcase 113
SELECT changes();
--result 3

--testcase 120
CREATE TABLE t3(a);
CREATE TABLE t4(c);
CREATE TRIGGER r1 AFTER INSERT ON t1 BEGIN
  INSERT INTO t3 SELECT new.x UNION ALL SELECT new.rowid;
  INSERT INTO t4 VALUES(changes());
END;
INSERT INTO t1 VALUES(567);
SELECT changes();
--result 1
--testcase 121
SELECT * FROM t4;
--result 2

/* EVIDENCE-OF: R-61376-43478 The glob(X,Y) function is equivalent to the
** expression "Y GLOB X".
*/
--testcase 200
CREATE TABLE t200(a INTEGER PRIMARY KEY, x);
INSERT INTO t200 VALUES(1, 'abc');
INSERT INTO t200 VALUES(2, 'defghij');
INSERT INTO t200 VALUES(3, 'klm');
INSERT INTO t200 VALUES(4, 'nopqrs');
SELECT a FROM t200 WHERE x GLOB '*[fl]*' ORDER BY a;
--result 2 3
--testcase 201
SELECT a FROM t200 WHERE glob('*[fl]*',x) ORDER BY a;
--result 2 3
--testcase 202
SELECT a FROM t200 WHERE x GLOB '[dknz]*' ORDER BY a;
--result 2 3 4
--testcase 203
SELECT a FROM t200 WHERE glob('[dknz]*',x) ORDER BY a;
--result 2 3 4

/* EVIDENCE-OF: R-14875-52970 The last_insert_rowid() function returns
** the ROWID of the last row insert from the database connection which
** invoked the function.
*/
--testcase 300
CREATE TABLE t300(x);
INSERT INTO t300(rowid, x) VALUES(123,456);
SELECT last_insert_rowid();
--result 123
--testcase 310
CREATE TABLE t310(x);
CREATE TRIGGER r300 AFTER INSERT ON t300 BEGIN
  INSERT INTO t310(rowid,x) VALUES(new.rowid+10,new.x);
END;
INSERT INTO t300(rowid, x) VALUES(234,567);
SELECT rowid, x, last_insert_rowid() FROM t310;
--result 244 567 234
