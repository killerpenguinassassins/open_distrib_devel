/*
** This module contains tests for the sqlite3_bind_parameter_index()
**
** MODULE_NAME:               req1_bind03
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
int req1_bind03(th3state *p){
  sqlite3_stmt *pStmt;
  int j;
  unsigned short aSql[40];

  /* EVIDENCE-OF: R-50811-36721 Return the index of an SQL parameter given
  ** its name.
  */
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  pStmt = th3dbPrepare(p, 0, "SELECT $abc, :def, @ghi");
  j = sqlite3_bind_parameter_index(pStmt, "$abc");
  th3testCheckInt(p, 1, j);
  th3testBegin(p, "101");
  j = sqlite3_bind_parameter_index(pStmt, ":def");
  th3testCheckInt(p, 2, j);
  th3testBegin(p, "103");
  j = sqlite3_bind_parameter_index(pStmt, "@ghi");
  th3testCheckInt(p, 3, j);

  /* EVIDENCE-OF: R-37342-04007 The index value returned is suitable for
  ** use as the second parameter to sqlite3_bind().
  */
  th3testBegin(p, "110");
  sqlite3_bind_int(pStmt, sqlite3_bind_parameter_index(pStmt, ":def"), 456);
  th3dbStep(p, pStmt);
  th3testCheck(p, "nil 456 nil");

  /* EVIDENCE-OF: R-15063-51342 A zero is returned if no matching parameter
  ** is found.
  */
  th3testBegin(p, "120");
  j = sqlite3_bind_parameter_index(pStmt, "$xyz");
  th3testCheckInt(p, 0, j);

  /* EVIDENCE-OF: R-53980-12800 The parameter name must be given in UTF-8
  ** even if the original statement was prepared from UTF-16 text using
  ** sqlite3_prepare16_v2().
  */
  th3testBegin(p, "130");
  sqlite3_finalize(pStmt);
  th3_ascii_to_utf16("SELECT $abc", aSql);
  sqlite3_prepare16_v2(th3dbPointer(p, 0), aSql, -1, &pStmt, 0);
  j = sqlite3_bind_parameter_index(pStmt, "$abc");
  sqlite3_finalize(pStmt);
  th3testCheckInt(p, 1, j);
  
  return 0;
}
