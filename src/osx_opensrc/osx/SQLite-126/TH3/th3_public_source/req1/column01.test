/*
** This module contains tests for the sqlite3_column_xxxx() family of
** interfaces.
**
** MODULE_NAME:               req1_column01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** EVIDENCE-OF: R-53946-02894 These routines return information about a
** single column of the current result row of a query.
**
** EVIDENCE-OF: R-12525-60165 In every case the first argument is a
** pointer to the prepared statement that is being evaluated (the
** sqlite3_stmt* that was returned from sqlite3_prepare_v2() or one of
** its variants) and the second argument is the index of the column for
** which information should be returned.
**
** EVIDENCE-OF: R-03108-55458 The memory space used to hold strings and
** BLOBs is freed automatically.
**
** If memory space were not freed automatically, then this test script
** would show memory leaks.
*/
int req1_column01(th3state *p){
  sqlite3_stmt *pStmt;
  sqlite3_stmt *pS2;
  const char *z;
  int n;

  /* EVIDENCE-OF: R-29987-53326 The leftmost column of the result set has
  ** the index 0.
  */
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  pStmt = th3dbPrepare(p, 0, "SELECT 1, 2, 3, 4");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 1, sqlite3_column_int(pStmt, 0));

  /* EVIDENCE-OF: R-55352-50321 The number of columns in the result can be
  ** determined using sqlite3_column_count().
  */
  th3testBegin(p, "110");
  th3testCheckInt(p, 4, sqlite3_column_count(pStmt));

  /* EVIDENCE-OF: R-28995-46979 The sqlite3_column_type() routine returns
  ** the datatype code for the initial data type of the result column.
  **
  ** EVIDENCE-OF: R-15172-56782 The returned value is one of
  ** SQLITE_INTEGER, SQLITE_FLOAT, SQLITE_TEXT, SQLITE_BLOB, or
  ** SQLITE_NULL.
  */
  th3testBegin(p, "120");
  sqlite3_finalize(pStmt);
  pStmt = th3dbPrepare(p, 0, "SELECT 1, 'hello', x'303132', null");
  sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_INTEGER, sqlite3_column_type(pStmt, 0));
  th3testBegin(p, "121");
  th3testCheckInt(p, SQLITE_TEXT, sqlite3_column_type(pStmt, 1));
  th3testBegin(p, "122");
  th3testCheckInt(p, SQLITE_BLOB, sqlite3_column_type(pStmt, 2));
  th3testBegin(p, "123");
  th3testCheckInt(p, SQLITE_NULL, sqlite3_column_type(pStmt, 3));
#ifndef SQLITE_OMIT_FLOATING_POINT
  th3testBegin(p, "124");
  sqlite3_finalize(pStmt);
  pStmt = th3dbPrepare(p, 0, "SELECT 34.5");
  sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_FLOAT, sqlite3_column_type(pStmt, 0));
#endif
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-27363-63308 If the result is a BLOB or UTF-8 string
  ** then the sqlite3_column_bytes() routine returns the number of bytes in
  ** that BLOB or string.
  */
  th3testBegin(p, "130");
  pStmt = th3dbPrepare(p, 0, "SELECT 12345, 'hello-world', x'30313233', null");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 11, sqlite3_column_bytes(pStmt, 1));
  th3testBegin(p, "131");
  th3testCheckInt(p, 4, sqlite3_column_bytes(pStmt, 2));

  /* EVIDENCE-OF: R-06526-21822 If the result is a numeric value then
  ** sqlite3_column_bytes() uses sqlite3_snprintf() to convert that value
  ** to a UTF-8 string and returns the number of bytes in that string.
  */
  th3testBegin(p, "132");
  th3testCheckInt(p, 5, sqlite3_column_bytes(pStmt, 0));

  /* EVIDENCE-OF: R-07391-62365 If the result is NULL, then
  ** sqlite3_column_bytes() returns zero.
  */
  th3testBegin(p, "133");
  th3testCheckInt(p, 0, sqlite3_column_bytes(pStmt, 3));

  /* EVIDENCE-OF: R-42670-46183 If the result is a numeric value then
  ** sqlite3_column_bytes16() uses sqlite3_snprintf() to convert that value
  ** to a UTF-16 string and returns the number of bytes in that string.
  */
  th3testBegin(p, "134");
  sqlite3_reset(pStmt);
  sqlite3_step(pStmt);
  th3testCheckInt(p, 10, sqlite3_column_bytes16(pStmt, 0));

  /* EVIDENCE-OF: R-11920-30904 If the result is NULL, then
  ** sqlite3_column_bytes16() returns zero.
  */
  th3testBegin(p, "135");
  th3testCheckInt(p, 0, sqlite3_column_bytes16(pStmt, 3));
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-07987-54086 If the result is a UTF-16 string, then
  ** sqlite3_column_bytes() converts the string to UTF-8 and then returns
  ** the number of bytes.
  **
  ** EVIDENCE-OF: R-12598-40983 The values returned by
  ** sqlite3_column_bytes() and sqlite3_column_bytes16() do not include the
  ** zero terminators at the end of the string.
  */
  th3testBegin(p, "140");
  th3dbEval(p, 0,
    "PRAGMA encoding=UTF16LE;"
    "CREATE TABLE t1(x, y);"
    "INSERT INTO t1 VALUES('test-string', x'3031323334');"
    "PRAGMA encoding;"
  );
  th3testCheck(p, "UTF-16le");
  th3testBegin(p, "141");
  pStmt = th3dbPrepare(p, 0, "SELECT x FROM t1;");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 11, sqlite3_column_bytes(pStmt, 0));
  sqlite3_finalize(pStmt);

  th3testBegin(p, "142");
  th3dbNew(p, 1, "test2.db");
  th3dbEval(p, 1,
    "PRAGMA encoding=UTF16BE;"
    "CREATE TABLE t1(x, y);"
    "INSERT INTO t1 VALUES('test-string', x'3031323334');"
    "PRAGMA encoding;"
  );
  th3testCheck(p, "UTF-16be");
  th3testBegin(p, "143");
  pStmt = th3dbPrepare(p, 1, "SELECT x FROM t1;");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 11, sqlite3_column_bytes(pStmt, 0));
  sqlite3_finalize(pStmt);

  th3testBegin(p, "144");
  th3dbNew(p, 2, "test3.db");
  th3dbEval(p, 2,
    "PRAGMA encoding=UTF8;"
    "CREATE TABLE t1(x, y);"
    "INSERT INTO t1 VALUES('test-string', x'3031323334');"
    "PRAGMA encoding;"
  );
  th3testCheck(p, "UTF-8");
  th3testBegin(p, "145");
  pStmt = th3dbPrepare(p, 1, "SELECT x FROM t1;");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 11, sqlite3_column_bytes(pStmt, 0));
  sqlite3_finalize(pStmt);


  /* EVIDENCE-OF: R-30008-03647 If the result is a BLOB or UTF-16 string
  ** then the sqlite3_column_bytes16() routine returns the number of bytes
  ** in that BLOB or string.
  **
  ** EVIDENCE-OF: R-12598-40983 The values returned by
  ** sqlite3_column_bytes() and sqlite3_column_bytes16() do not include the
  ** zero terminators at the end of the string.
  */
  th3testBegin(p, "150");
  pStmt = th3dbPrepare(p, 0, "SELECT x, y FROM t1;");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 22, sqlite3_column_bytes16(pStmt,0));
  th3testBegin(p, "151");
  th3testCheckInt(p, 5, sqlite3_column_bytes16(pStmt,1));
  sqlite3_finalize(pStmt);

  th3testBegin(p, "152");
  pStmt = th3dbPrepare(p, 1, "SELECT x, y FROM t1;");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 22, sqlite3_column_bytes16(pStmt,0));
  th3testBegin(p, "153");
  th3testCheckInt(p, 5, sqlite3_column_bytes16(pStmt,1));
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-34998-49915 If the result is a UTF-8 string, then
  ** sqlite3_column_bytes16() converts the string to UTF-16 and then
  ** returns the number of bytes.
  */
  th3testBegin(p, "154");
  pStmt = th3dbPrepare(p, 2, "SELECT x, y FROM t1;");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 22, sqlite3_column_bytes16(pStmt,0));
  th3testBegin(p, "155");
  th3testCheckInt(p, 5, sqlite3_column_bytes16(pStmt,1));
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-13793-11620 For clarity: the values returned by
  ** sqlite3_column_bytes() and sqlite3_column_bytes16() are the number of
  ** bytes in the string, not the number of characters.
  **
  ** 2 charaters:  0x020ac 0x1d11e
  ** UTF-16le:     ac20    34d81edd
  ** UTF-16be:     20ac    d834dd1e
  ** UTF-8:        e282ac  f09d849e
  */
  th3testBegin(p, "160");
  th3dbEval(p, 0,
    "DELETE FROM t1;"
    "INSERT INTO t1(x) VALUES(CAST(x'ac2034d81edd' AS text));"
    "SELECT length(x) FROM t1;"
  );
  th3testCheck(p, "2");
  th3testBegin(p, "161");
  pStmt = th3dbPrepare(p, 0, "SELECT x FROM t1;");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 7, sqlite3_column_bytes(pStmt, 0));
  th3testBegin(p, "162");
  th3testCheckInt(p, 6, sqlite3_column_bytes16(pStmt, 0));
  th3testBegin(p, "163");
  sqlite3_reset(pStmt);
  sqlite3_step(pStmt);
  th3testCheckInt(p, 6, sqlite3_column_bytes16(pStmt, 0));
  th3testBegin(p, "164");
  th3testCheckInt(p, 7, sqlite3_column_bytes(pStmt, 0));
  sqlite3_finalize(pStmt);

  th3testBegin(p, "165");
  th3dbEval(p, 1,
    "DELETE FROM t1;"
    "INSERT INTO t1(x) VALUES(CAST(x'20acd834dd1e' AS text));"
    "SELECT length(x) FROM t1;"
  );
  th3testCheck(p, "2");
  th3testBegin(p, "166");
  pStmt = th3dbPrepare(p, 1, "SELECT x FROM t1;");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 7, sqlite3_column_bytes(pStmt, 0));
  th3testBegin(p, "167");
  th3testCheckInt(p, 6, sqlite3_column_bytes16(pStmt, 0));
  th3testBegin(p, "168");
  sqlite3_reset(pStmt);
  sqlite3_step(pStmt);
  th3testCheckInt(p, 6, sqlite3_column_bytes16(pStmt, 0));
  th3testBegin(p, "169");
  th3testCheckInt(p, 7, sqlite3_column_bytes(pStmt, 0));
  
  /* EVIDENCE-OF: R-59893-45467 Strings returned by sqlite3_column_text()
  ** and sqlite3_column_text16(), even empty strings, are always zero
  ** terminated.
  */
  th3testBegin(p, "170");
  sqlite3_reset(pStmt);
  sqlite3_step(pStmt);
  z = (const char*)sqlite3_column_text(pStmt, 0);
  n = sqlite3_column_bytes(pStmt, 0);
  th3testCheckInt(p, n, 7);
  th3testBegin(p, "171");
  th3testCheckTrue(p, z[n]==0);
  th3testBegin(p, "172");
  z = (const char*)sqlite3_column_text16(pStmt, 0);
  n = sqlite3_column_bytes16(pStmt, 0);
  th3testCheckInt(p, n, 6);
  th3testBegin(p, "173");
  th3testCheckTrue(p, z[n]==0 && z[n+1]==0);
  sqlite3_finalize(pStmt);

  th3testBegin(p, "175");
  pStmt = th3dbPrepare(p, 0, "SELECT ''");
  sqlite3_step(pStmt);
  th3testCheckInt(p, 0, sqlite3_column_bytes(pStmt, 0));
  th3testBegin(p, "176");
  z = (const char*)sqlite3_column_text(pStmt, 0);
  th3testCheckTrue(p, z[0]==0);
  th3testBegin(p, "177");
  sqlite3_reset(pStmt);
  sqlite3_step(pStmt);
  th3testCheckInt(p, 0, sqlite3_column_bytes16(pStmt, 0));
  th3testBegin(p, "178");
  z = (const char*)sqlite3_column_text16(pStmt, 0);
  th3testCheckTrue(p, z[0]==0 && z[1]==0);
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-39464-12723 The return value from sqlite3_column_blob()
  ** for a zero-length BLOB is a NULL pointer.
  */
  th3testBegin(p, "180");
  pStmt = th3dbPrepare(p, 0, "SELECT ?");
  sqlite3_bind_blob(pStmt, 1, "", 0, SQLITE_STATIC);
  sqlite3_step(pStmt);
  th3testCheckTrue(p, sqlite3_column_blob(pStmt,0)==0);
  sqlite3_finalize(pStmt);

  /* EVIDENCE-OF: R-05452-35198 The object returned by
  ** sqlite3_column_value() is an unprotected sqlite3_value object.
  */
  th3testBegin(p, "190");
  pStmt = th3dbPrepare(p, 0, "SELECT 12345678");
  pS2 = th3dbPrepare(p, 1, "SELECT quote(?1), typeof(?1), length(?1)");
  sqlite3_step(pStmt);
  sqlite3_bind_value(pS2, 1, sqlite3_column_value(pStmt, 0));
  sqlite3_finalize(pStmt);
  th3dbStep(p, pS2);
  sqlite3_reset(pS2);
  th3testCheck(p, "12345678 integer 8");
#ifndef SQLITE_OMIT_FLOATING_POINT
  th3testBegin(p, "191");
  pStmt = th3dbPrepare(p, 0, "SELECT 1234.5");
  sqlite3_step(pStmt);
  sqlite3_bind_value(pS2, 1, sqlite3_column_value(pStmt, 0));
  sqlite3_finalize(pStmt);
  th3dbStep(p, pS2);
  sqlite3_reset(pS2);
  th3testCheck(p, "1234.5 real 6");
#endif
  th3testBegin(p, "192");
  pStmt = th3dbPrepare(p, 0, "SELECT 'hello-world'");
  sqlite3_step(pStmt);
  sqlite3_bind_value(pS2, 1, sqlite3_column_value(pStmt, 0));
  sqlite3_finalize(pStmt);
  th3dbStep(p, pS2);
  sqlite3_reset(pS2);
  th3testCheck(p, "'hello-world' text 11");
  th3testBegin(p, "193");
  pStmt = th3dbPrepare(p, 0, "SELECT x'6162636465'");
  sqlite3_step(pStmt);
  sqlite3_bind_value(pS2, 1, sqlite3_column_value(pStmt, 0));
  sqlite3_finalize(pStmt);
  th3dbStep(p, pS2);
  sqlite3_reset(pS2);
  th3testCheck(p, "X'6162636465' blob 5");
  th3testBegin(p, "194");
  pStmt = th3dbPrepare(p, 0, "SELECT NULL");
  sqlite3_step(pStmt);
  sqlite3_bind_value(pS2, 1, sqlite3_column_value(pStmt, 0));
  sqlite3_finalize(pStmt);
  th3dbStep(p, pS2);
  sqlite3_reset(pS2);
  th3testCheck(p, "NULL null nil");
  sqlite3_finalize(pS2);

  return 0;
}
