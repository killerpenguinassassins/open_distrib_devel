/*
** This module contains tests for datatypes as described in the
** datatype3.html document.
**
** SCRIPT_MODULE_NAME:        req1_datatype3_07
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* EVIDENCE-OF: R-51848-06142 When grouping values with the GROUP BY
** clause values with different storage classes are considered distinct,
** except for INTEGER and REAL values which are considered equal if they
** are numerically equal.
**
** EVIDENCE-OF: R-40564-64233 No affinities are applied to any values as
** the result of a GROUP by clause.
*/
#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 100
CREATE TABLE t100(a);
INSERT INTO t100 VALUES(123);
INSERT INTO t100 VALUES(123.0);
INSERT INTO t100 VALUES('123');
INSERT INTO t100 VALUES('0123');
INSERT INTO t100 VALUES(null);
INSERT INTO t100 VALUES(x'313233');
INSERT INTO t100 SELECT a FROM t100;

SELECT quote(a), count(*) FROM t100 GROUP BY a ORDER BY a;
--result NULL 2 123.0 4 '0123' 2 '123' 2 X'313233' 2
#endif

/* ANALYSIS-OF: R-11730-53816 The compound SELECT operators UNION,
** INTERSECT and EXCEPT perform implicit comparisons between values.
**
** Of course it does - how else can it remove duplicates?  As evidence
** that comparisons are performed, we will demonstrate that duplicates
** are removed.
*/
--testcase 200
CREATE TABLE t201(x);
INSERT INTO t201 VALUES(123);
INSERT INTO t201 VALUES('abc');
INSERT INTO t201 SELECT * FROM t201;
CREATE TABLE t202(y);
INSERT INTO t202 VALUES(234);
INSERT INTO t202 VALUES('abc');
SELECT x FROM t201 UNION SELECT y FROM t202 ORDER BY x;
--result 123 234 abc
--testcase 201
SELECT x FROM t201 INTERSECT SELECT y FROM t202;
--result abc
--testcase 202
SELECT x FROM t201 EXCEPT SELECT y FROM t202;
--result 123
--testcase 203
SELECT y FROM t202 EXCEPT SELECT x FROM t201;
--result 234

/* EVIDENCE-OF: R-14014-59687 No affinity is applied to comparison
** operands for the implicit comparisons associated with UNION,
** INTERSECT, or EXCEPT - the values are compared as is.
*/
--testcase 300
CREATE TABLE t301(x TEXT);
INSERT INTO t301 VALUES('123');
INSERT INTO t301 VALUES('0123');
CREATE TABLE t302(y INT);
INSERT INTO t302 VALUES(123);

SELECT x FROM t301 UNION SELECT y FROM t302 ORDER BY 1;
--result 123 0123 123
--testcase 301
SELECT +x FROM t301 UNION SELECT y FROM t302 ORDER BY 1;
--result 123 0123 123
--testcase 302
SELECT y FROM t302 UNION SELECT x FROM t301 ORDER BY 1;
--result 123 0123 123
--testcase 303
SELECT +y FROM t302 UNION SELECT x FROM t301 ORDER BY 1;
--result 123 0123 123
