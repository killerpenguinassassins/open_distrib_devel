/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_foreignkeys24
** REQUIRED_PROPERTIES:       FOREIGN_KEYS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/*
** EV: R-42264-30503 The SQLITE_MAX_TRIGGER_DEPTH and
** SQLITE_LIMIT_TRIGGER_DEPTH settings determine the maximum allowable
** depth of trigger program recursion. For the purposes of these limits,
** foreign key actions are considered trigger programs.
*/
--testcase 100
PRAGMA recursive_triggers(ON);
CREATE TABLE t1(a INTEGER PRIMARY KEY, b REFERENCES t1 ON DELETE CASCADE);
INSERT INTO t1 VALUES(1, null);
INSERT INTO t1 VALUES(2,1);
INSERT INTO t1 SELECT a+2, a+1 FROM t1;
INSERT INTO t1 SELECT a+4, a+3 FROM t1;
INSERT INTO t1 SELECT a+8, a+7 FROM t1;
INSERT INTO t1 SELECT a+16, a+15 FROM t1;
INSERT INTO t1 SELECT a+32, a+31 FROM t1;
INSERT INTO t1 SELECT a+64, a+63 FROM t1;
INSERT INTO t1 SELECT a+128, a+127 FROM t1;
UPDATE t1 SET b=256 WHERE a=1;
CREATE TABLE saved AS SELECT * FROM t1;
SELECT count(*) FROM t1;
--result 256

--testcase 110
SELECT th3_load_normal_extension();
--run 0
SELECT th3_sqlite3_limit('SQLITE_LIMIT_TRIGGER_DEPTH', 255);
--result nil

--testcase 120
DELETE FROM t1 WHERE a=1;
--result SQLITE_ERROR {too many levels of trigger recursion}
--testcase 121
SELECT * FROM saved EXCEPT SELECT * FROM t1;
--result

--testcase 130
SELECT th3_sqlite3_limit('SQLITE_LIMIT_TRIGGER_DEPTH', 256);
--run 0
DELETE FROM t1 WHERE a=1;
--result
--testcase 131
SELECT * FROM t1
--result

/*
** EV: R-51769-32730 The PRAGMA recursive_triggers setting does not not
** affect the operation of foreign key actions.
*/
--testcase 200
PRAGMA recursive_triggers(OFF);
INSERT INTO t1 SELECT * FROM saved;
--result

--testcase 210
SELECT th3_sqlite3_limit('SQLITE_LIMIT_TRIGGER_DEPTH', 255);
--run 0
DELETE FROM t1 WHERE a=1;
--result SQLITE_ERROR {too many levels of trigger recursion}
--testcase 211
SELECT * FROM saved EXCEPT SELECT * FROM t1;
--result

--testcase 220
SELECT th3_sqlite3_limit('SQLITE_LIMIT_TRIGGER_DEPTH', 256);
--run 0
DELETE FROM t1 WHERE a=1;
--result
--testcase 221
SELECT * FROM t1
--result
