/*
** This module contains tests for case_sensitive_like pragma.
**
** MIXED_MODULE_NAME:         req1_pragma10
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/*
** A new implementation for LIKE that works like GLOB
*/
static void req1_pragma10_like(
  sqlite3_context *pContext,
  int nArg,
  sqlite3_value **apArg
){
 const char *zPattern = (const char*)sqlite3_value_text(apArg[1]);
 const char *zString = (const char*)sqlite3_value_text(apArg[0]);
 sqlite3_result_int(pContext, th3strglob(zPattern, zString));
}

/* Register the substitute LIKE() function */
void req1_pragma10_override_like(th3state *p, int iDb, char *zArg){
  sqlite3 *db = th3dbPointer(p, iDb);
  sqlite3_create_function(db, "like", 2, SQLITE_UTF8, p,
                          req1_pragma10_like, 0, 0);
  sqlite3_create_function(db, "glob", 2, SQLITE_UTF8, p,
                          req1_pragma10_like, 0, 0);
}

/************************ Script *************************/
/* 
** EVIDENCE-OF: R-57216-60803 The default behavior of the LIKE operator
** is to ignore case for ASCII characters. Hence, by default 'a' LIKE 'A'
** is true.
*/
--testcase 100
SELECT 'a' LIKE 'A';
--result 1
--testcase 101
SELECT 'abcdefghijklmnopqrstuvwxyz' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 1
--testcase 102
SELECT 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 1
--testcase 103
SELECT 'aBcDeFgHiJkLmNoPqRsTuVwXyZ' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 1
--testcase 104
SELECT 'AbCdEfGhIjKlMnOpQrStUvWxYz' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 1
--testcase 105
SELECT 'aBcDeFgHiJkLmNoPqRsTuVwXyZ' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 1
--testcase 106
SELECT 'AbCdEfGhIjKlMnOpQrStUvWxYz' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 1
--testcase 107
SELECT 'abcdefghijklmnopqrstuvwxyz' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 1
--testcase 108
SELECT 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 1

/* EVIDENCE-OF: R-34730-53445 The case_sensitive_like pragma installs a
** new application-defined LIKE function that is either case sensitive or
** insensitive depending on the value of the case_sensitive_like pragma.
**
** EVIDENCE-OF: R-06710-22511 When case_sensitive_like is disabled, the
** default LIKE behavior is expressed.
**
** EVIDENCE-OF: R-23019-08375 When case_sensitive_like is enabled, case
** becomes significant. So, for example, 'a' LIKE 'A' is false but 'a'
** LIKE 'a' is still true.
*/
--testcase 200
PRAGMA case_sensitive_like=OFF;
SELECT 'a' LIKE 'A';
--result 1
--testcase 201
SELECT 'abcdefghijklmnopqrstuvwxyz' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 1
--testcase 202
SELECT 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 1
--testcase 203
SELECT 'aBcDeFgHiJkLmNoPqRsTuVwXyZ' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 1
--testcase 204
SELECT 'AbCdEfGhIjKlMnOpQrStUvWxYz' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 1
--testcase 205
SELECT 'aBcDeFgHiJkLmNoPqRsTuVwXyZ' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 1
--testcase 206
SELECT 'AbCdEfGhIjKlMnOpQrStUvWxYz' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 1
--testcase 207
SELECT 'abcdefghijklmnopqrstuvwxyz' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 1
--testcase 208
SELECT 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 1

--testcase 210
PRAGMA case_sensitive_like=ON;
SELECT 'a' LIKE 'A', 'a' LIKE 'a';
--result 0 1
--testcase 211
SELECT 'abcdefghijklmnopqrstuvwxyz' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 0
--testcase 212
SELECT 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 0
--testcase 213
SELECT 'aBcDeFgHiJkLmNoPqRsTuVwXyZ' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 0
--testcase 214
SELECT 'AbCdEfGhIjKlMnOpQrStUvWxYz' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 0
--testcase 215
SELECT 'aBcDeFgHiJkLmNoPqRsTuVwXyZ' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 0
--testcase 216
SELECT 'AbCdEfGhIjKlMnOpQrStUvWxYz' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 0
--testcase 217
SELECT 'abcdefghijklmnopqrstuvwxyz' LIKE 'abcdefghijklmnopqrstuvwxyz';
--result 1
--testcase 218
SELECT 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' LIKE 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
--result 1

/* EVIDENCE-OF: R-54349-27488 This pragma uses sqlite3_create_function()
** to overload the LIKE and GLOB functions, which may override previous
** implementations of LIKE and GLOB registered by the application.
*/
--call req1_pragma10_override_like
--testcase 300
SELECT 'abc123xyz' LIKE 'abc#xyz', 'abc123xyz' GLOB 'abc#xyz';
--result 1 1
--testcase 301
PRAGMA case_sensitive_like=ON;
SELECT 'abc123xyz' LIKE 'abc#xyz', 'abc123xyz' GLOB 'abc#xyz';
--result 0 0
