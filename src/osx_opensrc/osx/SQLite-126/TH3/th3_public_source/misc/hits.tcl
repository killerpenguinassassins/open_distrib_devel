#!/usr/bin/tclsh
#
# Scan the output of [gcov -b -c] and print out the number of times every
# branch is followed.
#
# The gcov output looks something like this:
#
#        20550:47234:    for(i=0; rc==SQLITE_OK && i<db->nDb; i++){
#    branch  0 taken 20550 (fallthrough)
#    branch  1 taken 0
#    branch  2 taken 13700
#    branch  3 taken 6850 (fallthrough)
#        13700:47235:      Btree *pBt = db->aDb[i].pBt;
# 
# For the example above, this script prints:
#
#    47234.0 20550
#    47234.1 0
#    47234.2 13700
#    47234.3 6850
#
if {[llength $argv]==0} {
  set argv {sqlite3.c.gcov}
}
set infile [lindex $argv 0]
set in [open $infile]
set lineno 0
while {![eof $in]} {
  set line [gets $in]
  if {[regexp {^([- #0-9]+): *(\d+):} $line all cnt lineno]} {
    continue
  }
  if {[regexp {^branch +(\d+) taken (\d+)} $line all brno cnt]} {
    puts "$lineno.$brno $cnt"
  }
}
