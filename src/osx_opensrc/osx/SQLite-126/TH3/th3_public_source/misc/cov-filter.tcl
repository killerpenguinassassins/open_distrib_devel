#!/usr/bin/tclsh
#
# Reformat the output of gcov -b -c.
#
# The gcov output looks something like this:
#
#        20550:47234:    for(i=0; rc==SQLITE_OK && i<db->nDb; i++){
#    branch  0 taken 20550 (fallthrough)
#    branch  1 taken 0
#    branch  2 taken 13700
#    branch  3 taken 6850 (fallthrough)
#        13700:47235:      Btree *pBt = db->aDb[i].pBt;
# 
# This script removes the prefixes from the beginning of each line and
# the extraneous "branch" lines.  Instead, lines that are never executed
# have a "###" appended.  Lines that have branches that are never tested
# or taken have "### N" appended where N is a number starting with 0 of
# the particular branch that was missed.  N is the same number that appears
# after the "branch" keyword in the gcov output.
#
# The result is a file that is very close to the original source file
# and is much easier to read.
#
# The "###" marks are omitted from lines that contain a comment of the
# for /*NO_TEST*/.
#
# On linux+gcc+x86_64, the va_arg() macro has multiple code paths which
# can result in some extraneous ### marks.  To suppress these, the presence
# of "va_arg" on a line causes ### marks to be suppressed, just like
# the presence of /*NO_TEST*/.  But to guard against suppressing legitimate
# branchs that happen to be on the same line as a va_arg(), if the branch
# number ever exceeds 1 on a line with a va_arg(), a warning is issued.
# And in the source code to SQLite, we take care to never include a 
# va_arg() on the same line as an "if" statement or "?" operator.
#
if {[llength $argv]==0} {
  set argv {sqlite3.c.gcov sqlite3.c.cov}
}
set infile [lindex $argv 0]
set outfile [lindex $argv 1]
if {$outfile==""} {
  set out stdout
} else {
  set out [open $outfile w]
}
set in [open $infile]
set seenmiss 0
set linehit 0
set linemiss 0
set brhit 0
set brmiss 0
set notest 0
set lineno 0
while {![eof $in]} {
  set line [gets $in]
  if {[regexp {^([- #0-9]+): *([0-9]+):(.*)} $line all cnt lineno tail]} {
    incr lineno
    if {[info exists prevline]} {
      puts $out $prevline
      unset prevline
    }
    if {$lineno==0} continue
    set seenmiss 0
    set prevline $tail
    if {[regexp {/\*NO_TEST\*/} $tail]} {
      set notest 1
    } else {
      set notest 0
    }
    if {[regexp {va_arg\(.+,.+\)} $tail]} {
      set isvaarg 1
    } else {
      set isvaarg 0
    }
    if {$notest==0 && [string index $cnt end-1]=="#"} {
      append prevline " ###"
      set seenmiss 1
      incr linemiss
    } elseif {![regexp {^ *-:} $line]} {
      incr linehit
    }
  } elseif {$notest==0 && [regexp {^branch} $line]} {
    if {$isvaarg && [lindex $line 1]>1} {
      if {$seenmiss} {
        append prevline " ###"
        set seenmiss 1
      }
      append prevline " va_arg()-misuse"
      incr brmiss
      puts stderr "*** va_arg() paired with a conditional on line $lineno"
      set isvaarg 0
    } elseif {([regexp { taken 0} $line] && !$isvaarg)
         || [regexp { never exec} $line]} {
      if {!$seenmiss} {
        append prevline " ###"
        set seenmiss 1
      }
      append prevline " [lindex $line 1]"
      incr brmiss
    } else {
      incr brhit
    }
  }
}
puts $out $prevline
set t [expr {$linehit+$linemiss}]
set pc [format %.3f%% [expr {$linehit*100.0/$t}]]
puts $out "\n### executed $linehit of $t lines ($pc)"
puts stderr "Statement coverage: $pc"
set t [expr {$brhit+$brmiss}]
set pc [format %.3f%% [expr {$brhit*100.0/$t}]]
puts $out "### executed $brhit of $t branch choices ($pc)"
puts stderr "Branch coverage: $pc"
